# curropup(b,m,t,s) = -1im*hopping(m,b[1],t)*(op("Cdagup", [s[b[1]],s[b[2]]], 1)*op("Cup", [s[b[1]],s[b[2]]], 2) - op("Cdagup", [s[b[1]],s[b[2]]], 2)*op("Cup", [s[b[1]],s[b[2]]], 1))
# curropdn(b,m,t,s) = -1im*hopping(m,b[1],t)*(op("Cdagdn", [s[b[1]],s[b[2]]], 1)*op("Cdn", [s[b[1]],s[b[2]]], 2) - op("Cdagdn", [s[b[1]],s[b[2]]], 2)*op("Cdn", [s[b[1]],s[b[2]]], 1))
sdiop(s) = op("Cdagdn", s[1:2], 1)*op("Cdn", s[1:2], 2) + op("Cdagdn", s[1:2], 2)*op("Cdn", s[1:2], 1) + op("Cdagup", s[1:2], 1)*op("Cup", s[1:2], 2) + op("Cdagup", s[1:2], 2)*op("Cup", s[1:2], 1)

function run_vumps(d; alg = "parallel", model = Model"ionichubbard"(), tf = 1)

  d = copy(d)
  m = dict2model(d)
  dt = d[:dt]
  maxdim = d[:maxdim]

  ts = 0:dt:(m.T*tf-dt)

  plot(t->δ(m,t),t->Δ(m,t),ts)
  
  d[:ts] = ts
  d[:en01] = zeros(Union{ComplexF64,Missing}, length(ts))
  d[:en02] = zeros(Union{ComplexF64,Missing}, length(ts))
  d[:curri1] = zeros(Union{Float64,Missing}, length(ts))
  d[:curri2] = zeros(Union{Float64,Missing}, length(ts))
  d[:dup] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:ddn] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:Sz] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:currup1] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn1] = zeros(Union{Float64,Missing}, length(ts))
  d[:err] = zeros(Union{Float64,Missing}, length(ts))
  d[:cutoff] = zeros(Union{Float64,Missing}, length(ts))
  d[:qsup1] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:qsdn1] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:currup2] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn2] = zeros(Union{Float64,Missing}, length(ts))
  d[:qsup2] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:qsdn2] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:sdi] = zeros(Union{Float64,Missing}, length(ts))

  cutoff = 1e-13 # Singular value cutoff when increasing the bond dimension
  max_vumps_iters = 100 # Maximum number of iterations of the VUMPS algorithm at each bond dimension
  outer_iters = 4 # Number of times to increase the bond dimension

  model_params = (m = m, t = -m.T/16)

  N = 2 # Unit cell size
  electron_space = fill(electron_space_shift(1, 0), N)
  s = infsiteinds("Electron", N; space = electron_space)
  initstate(n) = isodd(n) ? "Up" : "Dn"

  ψ = InfMPS(s, initstate)

  @show model, model_params
  # Form the Hamiltonian
  H = ITensorInfiniteMPS.InfiniteSum{MPO}(model, s; model_params...)

  # Check translational invariance
  println("\nCheck translational invariance of initial infinite MPS")
  @show norm(contract(ψ.AL[1:N]..., ψ.C[N]) - contract(ψ.C[0], ψ.AR[1:N]...))

  outputlevel = 1
  vumps_kwargs = (tol = 1e-10, maxiter = max_vumps_iters, outputlevel = outputlevel, multisite_update_alg="sequential")
  subspace_expansion_kwargs = (cutoff = cutoff, maxdim = maxdim)

  # For now, to increase the bond dimension you must alternate
  # between steps of VUMPS and subspace expansion (which outputs
  # a new state that is equal to the original state but with
  # a larger bond dimension)  
  ψ = vumps(H, ψ; vumps_kwargs...)

  for _ = 1:outer_iters
    println("\nIncrease bond dimension")
    ψ = subspace_expansion(ψ, H; subspace_expansion_kwargs...)
    println("Run VUMPS with new bond dimension")
    ψ = vumps(H, ψ; vumps_kwargs...)
  end


  # hidecursor()
  # frame = nothing
  p = Progress(length(d[:ts]); desc="Messung: ", showspeed=true)
  for (i,t) in enumerate(d[:ts])
    model_params = (m = m, t =  t)
    H = ITensorInfiniteMPS.InfiniteSum{MPO}(model, s; model_params...)  

    ψ = vumps(H, ψ; vumps_kwargs...)

    # for _ = 1:outer_iters
    #   println("\nIncrease bond dimension")
    #   ψ = subspace_expansion(ψ, H; subspace_expansion_kwargs...)
    #   println("Run VUMPS with new bond dimension")
    #   ψ = vumps(H, ψ; vumps_kwargs...)
    # end

    # ψ = subspace_expansion(ψ, H; subspace_expansion_kwargs...)

    Nup = real.([expect(ψ,s, "Nup", n) for n = 1:N])
    Ndn = real.([expect(ψ,s, "Ndn", n) for n = 1:N])
    Sz = real.([expect(ψ,s, "Sz", n) for n = 1:N])

    bs = [(1, 2), (2, 3)]
    energy_infinite = map(b -> expect_two_site(ψ, H[b], b), bs)

    d[:en01][i] = energy_infinite[1]
    d[:en02][i] = energy_infinite[2]
    d[:dup][i, :] = Nup
    d[:ddn][i, :] = Ndn
    d[:Sz][i, :] = Sz
    d[:cutoff][i] =  minimum(diag(array(svd(ψ.C[1],inds(ψ.C[1])[1]).S)))
    ProgressMeter.next!(p; showvalues = [(:i,i), (:λmin,d[:cutoff][i]), (:Sz,Sz), (:en,energy_infinite[1])])


    # frame = UnicodePlots.lineplot(ts[1:i],replace((d[:qsup1][1:i]),missing => NaN),xlim = (0,ts[end]), ylim = (0,1),xlabel = "t", ylabel = "Q(t)")
    # addplot(frame)

  end
  # showcursor()
  # print(frame)


  # Check translational invariance
  println("\nCheck translational invariance of optimized infinite MPS")
  # d[:err] = norm(contract(ψ.AL[1:N]..., ψ.C[N]) - contract(ψ.C[0], ψ.AR[1:N]...))
  @show d[:err][end]

  d[:qs] = (d[:qsup1] + d[:qsup2] + d[:qsdn1] + d[:qsdn2])/2
  d[:qsS] = (d[:qsup1] + d[:qsup2] - d[:qsdn1] - d[:qsdn2])/2


  return strdict(d)
end

function run_vumps_evo(d; alg = "parallel", model = Model"ionichubbard"(), tf = 1, cutoff = 1e-9, tol = 1e-9)

  d = copy(d)
  m = dict2model(d)
  dt = d[:dt]
  maxdim = d[:maxdim]

  ts = 0:dt:(m.T*tf-dt)

  plot(t->δ(m,t),t->Δ(m,t),ts)
  
  d[:ts] = ts
  d[:en01] = zeros(Union{ComplexF64,Missing}, length(ts))
  d[:en02] = zeros(Union{ComplexF64,Missing}, length(ts))
  d[:curri1] = zeros(Union{Float64,Missing}, length(ts))
  d[:curri2] = zeros(Union{Float64,Missing}, length(ts))
  d[:dup] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:ddn] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:Sz] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:currup1] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn1] = zeros(Union{Float64,Missing}, length(ts))
  d[:err] = zeros(Union{Float64,Missing}, length(ts))
  d[:cutoff] = zeros(Union{Float64,Missing}, length(ts))
  d[:qsup1] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:qsdn1] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:currup2] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn2] = zeros(Union{Float64,Missing}, length(ts))
  d[:qsup2] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:qsdn2] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:sdi] = zeros(Union{Float64,Missing}, length(ts))

  max_vumps_iters = 100 # Maximum number of iterations of the VUMPS algorithm at each bond dimension
  outer_iters = 4 # Number of times to increase the bond dimension

  model_params = (m = m, t = -m.T/16)

  N = 2 # Unit cell size
  electron_space = fill(electron_space_shift(1, 0), N)
  s = infsiteinds("Electron", N; space = electron_space)
  initstate(n) = isodd(n) ? "Up" : "Dn"

  ψ = InfMPS(s, initstate)

  @show model, model_params
  # Form the Hamiltonian
  H = ITensorInfiniteMPS.InfiniteSum{MPO}(model, s; model_params...)

  current_operator_up = Observable"current_up"()
  current_operator_dn = Observable"current_dn"()

  # Check translational invariance
  println("\nCheck translational invariance of initial infinite MPS")
  @show norm(contract(ψ.AL[1:N]..., ψ.C[N]) - contract(ψ.C[0], ψ.AR[1:N]...))

  outputlevel = 1
  vumps_kwargs = (tol = tol, maxiter = max_vumps_iters, outputlevel = outputlevel, multisite_update_alg="sequential")
  subspace_expansion_kwargs = (cutoff = cutoff, maxdim = maxdim)

  # For now, to increase the bond dimension you must alternate
  # between steps of VUMPS and subspace expansion (which outputs
  # a new state that is equal to the original state but with
  # a larger bond dimension)

  #initial state:
  println("\nRun VUMPS on initial product state, unit cell size $N")
  ψ = vumps(H, ψ; vumps_kwargs...)

  for _ = 1:outer_iters
    println("\nIncrease bond dimension")
    ψ = subspace_expansion(ψ, H; subspace_expansion_kwargs...)
    println("Run VUMPS with new bond dimension")
    ψ = vumps(H, ψ; vumps_kwargs...)
  end

  subspace_expansion_kwargs = (cutoff = cutoff, maxdim = 200)


  ## time evolution:
  vumps_kwargs = (time_step = -1im*d[:dt], tol = tol, maxiter = 1, outputlevel = 0, multisite_update_alg="parallel")
  p = Progress(length(collect(-m.T/16:dt:-dt)); desc="Einschwingung: ",showspeed=true)
  for (i,t) in enumerate(collect(-m.T/16:dt:-dt))
    model_params = (m = m, t = t)
    H = InfiniteSum{MPO}(model, s; model_params...)  
    ψ = tdvp(H, ψ; vumps_kwargs...)
    # ψ = subspace_expansion(ψ, H; subspace_expansion_kwargs...)
    λmin = minimum(diag(array(svd(ψ.C[1],inds(ψ.C[1])[1]).S)))
    Sz = real.([expect(ψ,s, "Sz", n) for n = 1:N])
    ProgressMeter.next!(p; showvalues = [(:i,i), (:λmin,λmin), (:Sz,Sz)])
  end
  
  # return ψ

  # hidecursor()
  # frame = nothing
  p = Progress(length(d[:ts]); desc="Messung: ", showspeed=true)
  for (i,t) in enumerate(d[:ts])
    model_params = (m = m, t =  t)
    H = ITensorInfiniteMPS.InfiniteSum{MPO}(model, s; model_params...)  
    Jup = ITensorInfiniteMPS.InfiniteSum{MPO}(current_operator_up, s; model_params...)
    Jdn = ITensorInfiniteMPS.InfiniteSum{MPO}(current_operator_dn, s; model_params...)
    ψ = tdvp(H, ψ; vumps_kwargs...)
    # ψ = subspace_expansion(ψ, H; subspace_expansion_kwargs...)

    Nup = real.([expect(ψ,s, "Nup", n) for n = 1:N])
    Ndn = real.([expect(ψ,s, "Ndn", n) for n = 1:N])
    Sz = real.([expect(ψ,s, "Sz", n) for n = 1:N])

    bs = [(1, 2), (2, 3)]
    energy_infinite = map(b -> expect_two_site(ψ, H[b], b), bs)
    curr_infinite_up = map(b -> expect_two_site(ψ, Jup[b], b), bs)
    curr_infinite_dn = map(b -> expect_two_site(ψ, Jdn[b], b), bs)

    d[:en01][i] = energy_infinite[1]
    d[:en02][i] = energy_infinite[2]
    d[:currup1][i] = real.(curr_infinite_up[1])
    d[:currup2][i] = real.(curr_infinite_up[2])
    d[:currdn1][i] = real.(curr_infinite_dn[1])
    d[:currdn2][i] = real.(curr_infinite_dn[2])
    d[:qsup1][i+1] = d[:qsup1][i] + dt * d[:currup1][i]
    d[:qsdn1][i+1] = d[:qsdn1][i] + dt * d[:currdn1][i]
    d[:qsup2][i+1] = d[:qsup2][i] + dt * d[:currup2][i]
    d[:qsdn2][i+1] = d[:qsdn2][i] + dt * d[:currdn2][i]
    d[:dup][i, :] = Nup
    d[:ddn][i, :] = Ndn
    d[:Sz][i, :] = Sz
    d[:cutoff][i] =  minimum(diag(array(svd(ψ.C[1],inds(ψ.C[1])[1]).S)))
    ProgressMeter.next!(p; showvalues = [(:i,i), (:λmin,d[:cutoff][i]), (:Q, d[:qsup1][i]), (:Sz,Sz), (:en,energy_infinite[1])])


    # frame = UnicodePlots.lineplot(ts[1:i],replace((d[:qsup1][1:i]),missing => NaN),xlim = (0,ts[end]), ylim = (0,1),xlabel = "t", ylabel = "Q(t)")
    # addplot(frame)

  end
  # showcursor()
  # print(frame)


  # Check translational invariance
  println("\nCheck translational invariance of optimized infinite MPS")
  # d[:err] = norm(contract(ψ.AL[1:N]..., ψ.C[N]) - contract(ψ.C[0], ψ.AR[1:N]...))
  @show d[:err][end]

  d[:qs] = (d[:qsup1] + d[:qsup2] + d[:qsdn1] + d[:qsdn2])/2
  d[:qsS] = (d[:qsup1] + d[:qsup2] - d[:qsdn1] - d[:qsdn2])/2


  return strdict(d)
end

###########################################################################################################
# #
# ## Compare to DMRG
# #

# Nfinite = 100
# sfinite = siteinds("Electron", Nfinite; conserve_qns=true)
# Hfinite = MPO(model, sfinite; model_params...)
# ψfinite = randomMPS(sfinite, initstate; linkdims=10)
# println("\nQN sector of starting finite MPS")
# @show flux(ψfinite)
# sweeps = Sweeps(30)
# maxdims =
#   min.(maxdim, [2, 2, 2, 2, 4, 4, 4, 4, 8, 8, 8, 8, 16, 16, 16, 16, 32, 32, 32, 32, 50])
# @show maxdims
# setmaxdim!(sweeps, maxdims...)
# setcutoff!(sweeps, cutoff)
# println("\nRun DMRG on $Nfinite sites")
# energy_finite_total, ψfinite = dmrg(Hfinite, ψfinite, sweeps)
# println("\nEnergy density")
# @show energy_finite_total / Nfinite

# nfinite = Nfinite ÷ 2 - 1
# bsfinite = [(nfinite, nfinite + 1), (nfinite + 1, nfinite + 2)] #take two center bonds
# hfinite(b) = ITensor(model, sfinite[b[1]], sfinite[b[2]],b[1] ; model_params...) # fixed this for staggered model.
# energy_finite = map(b -> expect_two_site(ψfinite, hfinite(b), b), bsfinite)

# Nup_finite = ITensors.expect(ψfinite, "Nup")[nfinite:(nfinite + 1)]
# Ndn_finite = ITensors.expect(ψfinite, "Ndn")[nfinite:(nfinite + 1)]
# Sz_finite = ITensors.expect(ψfinite, "Sz")[nfinite:(nfinite + 1)]


# println("\nResults from VUMPS")
# @show energy_infinite
# @show Nup
# @show Ndn
# @show Nup .+ Ndn
# @show Sz

# println("\nResults from DMRG")
# @show energy_finite
# @show Nup_finite
# @show Ndn_finite
# @show Nup_finite .+ Ndn_finite
# @show Sz_finite

# nothing
###########################################################################################################


function run_itebd(d; loading = false, model = Model"ionichubbard"(), periods = 1)

  if loading == false
    ψ, s = vumps_groundstate(d; model = model)
    wsave(datadir("wavefunctions","initial.jld2"),"psi",ψ,"sites",s,"dict",d)
  else
    ψ, s, _ = load(datadir("wavefunctions", "initial.jld2"), "psi","sites","dict")
  end

  d = copy(d)
  @show m = dict2model(d)
  dt = d[:dt]
  maxdim = d[:maxdim]
  cutoff = d[:cutoff]
  ts = 0:dt:(d[:T]*periods)-dt
  N=2

  # return(ans)
  
  d[:ts] = ts
  d[:en01] = zeros(Union{ComplexF64,Missing}, length(ts))
  d[:en02] = zeros(Union{ComplexF64,Missing}, length(ts))
  d[:curri1] = zeros(Union{Float64,Missing}, length(ts))
  d[:curri2] = zeros(Union{Float64,Missing}, length(ts))
  d[:dup] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:ddn] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:Sz] = zeros(Union{Float64,Missing}, length(ts), 2)
  d[:currup1] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn1] = zeros(Union{Float64,Missing}, length(ts))
  d[:err] = zeros(Union{Float64,Missing}, length(ts))
  d[:lambdamin] = zeros(Union{Float64,Missing}, length(ts))
  d[:currup1] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn1] = zeros(Union{Float64,Missing}, length(ts))
  d[:currup2] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn2] = zeros(Union{Float64,Missing}, length(ts))
  d[:qsup1] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:qsdn1] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:qsup2] = zeros(Union{Float64,Missing}, length(ts)+1)
  d[:qsdn2] = zeros(Union{Float64,Missing}, length(ts)+1)
  
  d[:currup1_test] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn1_test] = zeros(Union{Float64,Missing}, length(ts))
  d[:currup2_test] = zeros(Union{Float64,Missing}, length(ts))
  d[:currdn2_test] = zeros(Union{Float64,Missing}, length(ts))
  d[:sdi] = zeros(Union{Float64,Missing}, length(ts))
  d[:dim] = zeros(Union{Float64,Missing}, length(ts))


  model_params = (m = m, t = -m.T/16)
  current_operator_up = Observable"current_up"()
  current_operator_dn = Observable"current_dn"()



  # ψ = load(datadir("wavefunctions","initial.jld2"),"psi") #initial state test
  en_einschwing = []
  ## time evolution:
  p = Progress(length(collect(-d[:T]/16:dt:-dt)); desc="Einschwingung: ",showspeed=true, dt =10)
  for (i,t) in enumerate(collect(-d[:T]/16:dt:-dt))
    model_params = (m = m, t = t)
    H = InfiniteSum{MPO}(model, s; model_params...)  
    ψ, en = itebd_step(H,ψ,dt; cutoff = cutoff, mindim = maxdim, steps = 1)
    push!(en_einschwing,en[end])
    λmin = minimum(abs.(diag(array(ψ.C[1]))))
    Sz = real.([expect_local(ψ,s, "Sz", n) for n = 1:N])
    ProgressMeter.next!(p; showvalues = [(:i,i),(:en,en),
    # (:maxeentry,maximum([maximum(abs.(array(ψ.C[1]))),maximum(abs.(array(ψ.C[2]))),maximum(abs.(array(ψ.AL[1]))),maximum(abs.(array(ψ.AL[2]))),maximum(abs.(array(ψ.AR[1]))),maximum(abs.(array(ψ.AR[2])))])),
    (:λmin,λmin), (:Sz,Sz), (:dim, maximum([dim(ψ.C[1],1),dim(ψ.C[2],2)]))])
  end
  
  # return ψ

  # hidecursor()
  # frame = nothing
  p = Progress(length(d[:ts]); desc="Messung: ", showspeed=true, dt = 10)
  for (i,t) in enumerate(d[:ts])
    model_params = (m = m, t =  t)
    H = InfiniteSum{MPO}(model, s; model_params...)  
    Jup = InfiniteSum{MPO}(current_operator_up, s; model_params...)
    Jdn = InfiniteSum{MPO}(current_operator_dn, s; model_params...)
    ψ, en = itebd_step(H,ψ,dt; cutoff = cutoff, mindim = maxdim, steps = 1)

    Nup = real.([expect_local(ψ,s, "Nup", n) for n = 1:N])
    Ndn = real.([expect_local(ψ,s, "Ndn", n) for n = 1:N])
    Sz = real.([expect_local(ψ,s, "Sz", n) for n = 1:N])

    # d[:sdi][i] = real.(expect_two_site_local(ψ,sdiop(s),(1,2)))

    bs = [(1, 2), (2, 3)]
    energy_infinite = map(b -> expect_two_site(ψ, H[b], b), bs)
    curr_infinite_up = map(b -> expect_two_site(ψ, Jup[b], b), bs)
    curr_infinite_dn = map(b -> expect_two_site(ψ, Jdn[b], b), bs)


    d[:en01][i] = energy_infinite[1]
    d[:en02][i] = energy_infinite[2]
    d[:currup1][i] = real.(curr_infinite_up[1])
    d[:currup2][i] = real.(curr_infinite_up[2])
    d[:currdn1][i] = real.(curr_infinite_dn[1])
    d[:currdn2][i] = real.(curr_infinite_dn[2])
    d[:qsup1][i+1] = d[:qsup1][i] + dt * d[:currup1][i]
    d[:qsdn1][i+1] = d[:qsdn1][i] + dt * d[:currdn1][i]
    d[:qsup2][i+1] = d[:qsup2][i] + dt * d[:currup2][i]
    d[:qsdn2][i+1] = d[:qsdn2][i] + dt * d[:currdn2][i]
    d[:dup][i, :] = Nup
    d[:ddn][i, :] = Ndn
    d[:Sz][i, :] = Sz
    d[:lambdamin][i] =  minimum(diag(array(ψ.C[1])))
    d[:dim][i] = maximum([dim(ψ.C[1],1),dim(ψ.C[2],2)])
    ProgressMeter.next!(p; showvalues = [(:i,i),
    # (:maxeentry,maximum([maximum(abs.(array(ψ.C[1]))),maximum(abs.(array(ψ.C[2]))),maximum(abs.(array(ψ.AL[1]))),maximum(abs.(array(ψ.AL[2]))),maximum(abs.(array(ψ.AR[1]))),maximum(abs.(array(ψ.AR[2])))])),
    (:λmin,d[:lambdamin][i]), (:Sz,Sz), (:en,energy_infinite[1]),(:dim, maximum([dim(ψ.C[1],1),dim(ψ.C[2],2)]))])


    # frame = UnicodePlots.lineplot(ts[1:i],replace((d[:qsup1][1:i]),missing => NaN),xlim = (0,ts[end]), ylim = (0,1),xlabel = "t", ylabel = "Q(t)")
    # addplot(frame)

  end
  # showcursor()
  # print(frame)

  d[:qs] = (d[:qsup1] + d[:qsup2] + d[:qsdn1] + d[:qsdn2])/2
  d[:qsS] = (d[:qsup1] + d[:qsup2] - d[:qsdn1] - d[:qsdn2])/2

  # Check translational invariance
  println("\nCheck translational invariance of optimized infinite MPS")
  # d[:err] = norm(contract(ψ.AL[1:N]..., ψ.C[N]) - contract(ψ.C[0], ψ.AR[1:N]...))
  @show d[:err][end]

  return strdict(d)
end


function vumps_groundstate(d; model = Model"ionichubbard"())
  d = copy(d)
  @show m = dict2model(d)

  cutoff = 1e-8 # Singular value cutoff when increasing the bond dimension
  max_vumps_iters = 100 # Maximum number of iterations of the VUMPS algorithm at each bond dimension
  outer_iters = 10 # Number of times to increase the bond dimension
  model_params = (m = m, t = -m.T/16)

  N = 2 # Unit cell size
  
  if model == ITensorInfiniteMPS.Model{:rmspin}()
    electron_space = fill(electron_space_shift_noSz(1), N)
  else
    electron_space = fill(electron_space_shift(1, 0), N)
  end
  s = infsiteinds("Electron", N; space = electron_space)
  initstate(n) = isodd(n) ? "UpDn" : "Emp"

  ψ = InfMPS(s, initstate)
  @show model, model_params
  H = InfiniteSum{MPO}(model, s; model_params...)

  println("\nCheck translational invariance of initial infinite MPS")
  @show norm(contract(ψ.AL[1:N]..., ψ.C[N]) - contract(ψ.C[0], ψ.AR[1:N]...))

  outputlevel = 1
  vumps_kwargs = (tol = 1e-9, maxiter = max_vumps_iters, outputlevel = outputlevel, multisite_update_alg="parallel")
  subspace_expansion_kwargs = (cutoff = cutoff, maxdim = d[:maxdim])

  ψ = vumps(H, ψ; vumps_kwargs...)

  for _ = 1:outer_iters
    println("\nIncrease bond dimension")
    ψ = subspace_expansion(ψ, H; subspace_expansion_kwargs...)
    println("Run VUMPS with new bond dimension")
    ψ = vumps(H, ψ; vumps_kwargs...)
  end

  return ψ, s
end