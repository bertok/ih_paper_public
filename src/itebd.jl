using Statistics: mean
function itebd_step(H::InfiniteSum, ψ::InfiniteCanonicalMPS, dt; cutoff = 1e-9, steps = 1, mindim = 100, progress = false, alg = "qr_iteration")
    Uₒ = exp(-1.0im * dt/2 * H[(1,2)][1]*H[(1,2)][2])
    Uₑ = exp(-1.0im * dt * H[(2,3)][1]*H[(2,3)][2])
    p = Progress(length(1:steps); desc = "itebd: ", showspeed = true, enabled = progress)
    ens = []
    Nups = []
    Ndns = []
    Szs = []

    #todo: adapt starting energy
    en = 1000
    for i in 1:steps #temporary

        link_o = tags(linkind(ψ, 1 => 2))
        link_e = tags(linkind(ψ, 2 => 3))

        #>> step 1
        AoAe = MPS([ψ.AL[1] * ψ.AL[2]])
        Φo = apply(Uₒ, AoAe)[1]
        Ão, Λ̃o, B̃e = svd(Φo * ψ.C[2], uniqueinds(ψ.AL[1], ψ.AL[2]); cutoff = cutoff, mindim = mindim, alg = alg) #schollwoeck p. 163
        Ã̃e = dag(Ão) * Φo #not left normalized.

        link_u = tags(commoninds(Ão,Λ̃o)[1])
        link_v = tags(commoninds(Λ̃o,B̃e)[1])

        replacetags!(Ão, link_u => link_o)
        ψ.AL[1] = Ão
        replacetags!(Λ̃o, link_u => link_o)
        replacetags!(Λ̃o, link_v => link_o)
        ψ.C[1] = Λ̃o/norm(Λ̃o)
        replacetags!(B̃e, link_v => link_o)
        ψ.AR[1] = B̃e
        replacetags!(Ã̃e, link_u => link_o)
        ψ.AL[2] = Ã̃e

        #>> step 2
        Ã̃eÃo = MPS([ψ.AL[2] * ψ.AL[3]])
        Φe = apply(Uₑ, Ã̃eÃo)[1]
        Ãe, Λ̃e, B̃o = svd(Φe * ψ.C[3], uniqueinds(ψ.AL[2], ψ.AL[3]); cutoff = cutoff, mindim = mindim, alg = alg)
        Ã̃o = dag(Ãe) * Φe #not left normalized.

        link_u = tags(commoninds(Ãe,Λ̃e)[1])
        link_v = tags(commoninds(Λ̃e,B̃o)[1])

        replacetags!(Ãe, link_u => link_e)
        ψ.AL[2] = Ãe
        replacetags!(Λ̃e, link_u => link_e)
        replacetags!(Λ̃e, link_v => link_e)
        ψ.C[2] = Λ̃e/norm(Λ̃e)
        replacetags!(B̃o, link_v => link_e)
        ψ.AR[2] = B̃o
        replacetags!(Ã̃o, link_u => link_e)
        ψ.AL[3] = Ã̃o


        #>> step 3
        AoAe = MPS([ψ.AL[1] * ψ.AL[2]])
        Φo = apply(Uₒ, AoAe)[1]
        Ão, Λ̃o, B̃e = svd(Φo * ψ.C[2], uniqueinds(ψ.AL[1], ψ.AL[2]); cutoff = cutoff, mindim = mindim, alg = alg) #schollwoeck p. 163
        Ã̃e = dag(Ão) * Φo #not left normalized.

        link_u = tags(commoninds(Ão,Λ̃o)[1])
        link_v = tags(commoninds(Λ̃o,B̃e)[1])

        replacetags!(Ão, link_u => link_o)
        ψ.AL[1] = Ão
        replacetags!(Λ̃o, link_u => link_o)
        replacetags!(Λ̃o, link_v => link_o)
        ψ.C[1] = Λ̃o/norm(Λ̃o)
        replacetags!(B̃e, link_v => link_o)
        ψ.AR[1] = B̃e
        replacetags!(Ã̃e, link_u => link_o)
        ψ.AL[2] = Ã̃e

        # ψtmp = InfiniteMPS([ψ.AL[1],ψ.AL[2]])

        # println(norm(contract(ψ.AL[1:2]..., ψ.C[2]) - contract(ψ.C[0], ψ.AR[1:2]...)))
        # ψ = ITensors.orthogonalize(ψtmp, : ; tol = 1e-9) # This returns wrong arrow directions.

        # ψ.AL[1] = ψt.AL[1]*ψt.C[1]
        # ψ.AL[2] = ψt.AL[2]*ψt.C[2]

        # ψ.AL[1] = noprime(AL[1])
        # ψ.AL[2] = noprime(AL[2])
        # ψ.C[1] = noprime(C[1])
        # ψ.C[2] = noprime(C[2])



        # measure energy
        bs = [(1, 2), (2, 3)]
        energy_infinite = map(b -> expect_two_site(ψ, H[b], b), bs)
        energy_bond = mean(energy_infinite)
        Δϵ = abs(en - energy_bond)
        ProgressMeter.next!(p; showvalues = [(:i, i), (:Δϵ, Δϵ),
        (:ϵbond,energy_bond),
        (:minentry,minimum([minimum(abs.(array(ψ.C[1]))),minimum(abs.(array(ψ.C[2]))),minimum(abs.(array(ψ.AL[1]))),minimum(abs.(array(ψ.AL[2]))),minimum(abs.(array(ψ.AR[1]))),minimum(abs.(array(ψ.AR[2])))])),
        (:dim, maximum([dim(ψ.C[1],1),
        dim(ψ.C[2],2)]))])
        en = energy_bond
        push!(ens,en)
    end
    return ψ, ens
end


# function itebd_canolicalization(ψ::InfiniteCanonicalMPS; cutoff = 1e-6, maxdim = 200, mindim = 40)
#   Λo = Ψ.C[1]
#   Λe = Ψ.C[2]
#   ΓeΛe =  


#   return ψ, ens
# end

#todo: half the onsite terms of gate.


function ITensorInfiniteMPS.mixed_canonical(
    ψ::InfiniteMPS; left_tags=ts"Left", right_tags=ts"Right", tol::Real=1e-12
  )
    _, ψᴿ, _ = ITensorInfiniteMPS.right_orthogonalize(ψ; left_tags=ts"", right_tags=ts"Right", tol = tol)
    ψᴸ, C, λ = ITensorInfiniteMPS.left_orthogonalize(ψᴿ; left_tags=ts"Left", right_tags=ts"Right", tol = tol)
    if λ ≉ one(λ)
      error("λ should be approximately 1 after orthogonalization, instead it is $λ")
    end
    return InfiniteCanonicalMPS(ψᴸ, C, ψᴿ)
end

ITensors.orthogonalize(ψ::InfiniteMPS, ::Colon; kwargs...) = ITensorInfiniteMPS.mixed_canonical(ψ; kwargs...)


function ITensorInfiniteMPS.right_orthogonalize_polar(
  ψ::InfiniteMPS, Cᴿᴺ::ITensor; left_tags=ts"Left", right_tags=ts"Right", imtol = 1e-12
)
  N = length(ψ)
  ψᴿ = InfiniteMPS(N; reverse=ψ.reverse)
  Cᴿ = InfiniteMPS(N; reverse=ψ.reverse)
  Cᴿ[N] = Cᴿᴺ
  λ = 1.0
  for n in reverse(1:N)
    sⁿ = uniqueinds(ψ[n], ψ[n - 1], Cᴿ[n])
    lᴿⁿ = uniqueinds(Cᴿ[n], ψ[n])
    ψᴿⁿ, Cᴿⁿ⁻¹ = polar(ψ[n] * Cᴿ[n], (sⁿ..., lᴿⁿ...))
    # TODO: set the tags in polar
    ψᴿⁿ = replacetags(ψᴿⁿ, left_tags => right_tags; plev=1)
    ψᴿⁿ = noprime(ψᴿⁿ, right_tags)
    Cᴿⁿ⁻¹ = replacetags(Cᴿⁿ⁻¹, left_tags => right_tags; plev=1)
    Cᴿⁿ⁻¹ = noprime(Cᴿⁿ⁻¹, right_tags)
    ψᴿ[n] = ψᴿⁿ
    Cᴿ[n - 1] = Cᴿⁿ⁻¹
    λⁿ = norm(Cᴿ[n - 1])
    Cᴿ[n - 1] /= λⁿ
    λ *= λⁿ
    if !isapprox(ψ[n] * Cᴿ[n], λⁿ * Cᴿ[n - 1] * ψᴿ[n]; atol=imtol)
      @show norm(ψ[n] * Cᴿ[n] - λⁿ * Cᴿ[n - 1] * ψᴿ[n])
      error("ψ[n] * Cᴿ[n] ≠ λⁿ * Cᴿ[n-1] * ψᴿ[n]")
    end
  end
  return Cᴿ, ψᴿ, λ
end

function ITensorInfiniteMPS.right_orthogonalize(
  ψ::InfiniteMPS; left_tags=ts"Left", right_tags=ts"Right", tol::Real=1e-9, imtol = 1e-12
)
  # TODO: replace dag(ψ) with ψ'ᴴ?
  ψᴴ = prime(linkinds, dag(ψ))

  N = nsites(ψ)

  # The unit cell range
  # Turn into function `eachcellindex(ψ::InfiniteMPS, cell::Integer = 1)`
  cell₁ = 1:N
  # A transfer matrix made from the 1st unit cell of
  # the infinite MPS
  # TODO: make a `Cell` Integer type and call as `ψ[Cell(1)]`
  # TODO: make a TransferMatrix wrapper that automatically
  # primes and daggers, so it can be called with:
  # T = TransferMatrix(ψ[Cell(1)])
  ψ₁ = ψ[cell₁]
  ψ₁ᴴ = ψᴴ[cell₁]
  T₀₁ = ITensorMap(
    ψ₁,
    ψ₁ᴴ;
    input_inds=unioninds(commoninds(ψ[N], ψ[N + 1]), commoninds(ψᴴ[N], ψᴴ[N + 1])),
    output_inds=unioninds(commoninds(ψ[1], ψ[0]), commoninds(ψᴴ[1], ψᴴ[0])),
  )

  # TODO: make an optional initial state
  v₁ᴿᴺ = randomITensor(dag(input_inds(T₀₁)))

  # Start by getting the right eivenvector/eigenvalue of T₀₁
  # TODO: make a function `right_environments(::InfiniteMPS)` that computes
  # all of the right environments using `eigsolve` and shifting unit cells
  λ⃗₁ᴿᴺ, v⃗₁ᴿᴺ, eigsolve_info = eigsolve(T₀₁, v₁ᴿᴺ, 1, :LM; tol=tol)
  λ₁ᴿᴺ, v₁ᴿᴺ = λ⃗₁ᴿᴺ[1], v⃗₁ᴿᴺ[1]

  if imag(λ₁ᴿᴺ) / norm(λ₁ᴿᴺ) > imtol
    @show λ₁ᴿᴺ
    # error(
      # "Imaginary part of eigenvalue is large: imag(λ₁ᴿᴺ) / norm(λ₁ᴿᴺ) = $(imag(λ₁ᴿᴺ) / norm(λ₁ᴿᴺ))",
    # )
  end

  # Fix the phase of the diagonal to make Hermitian
  v₁ᴿᴺ .*= conj(sign(v₁ᴿᴺ[1, 1]))
  if !ishermitian(v₁ᴿᴺ; rtol=tol)
    # @show λ₁ᴿᴺ
    # @show v₁ᴿᴺ
    @show norm(v₁ᴿᴺ - swapinds(dag(v₁ᴿᴺ), reverse(Pair(inds(v₁ᴿᴺ)...))))
    # error("v₁ᴿᴺ not hermitian")
  end
  if norm(imag(v₁ᴿᴺ)) > imtol
    println(
      "Norm of the imaginary part $(norm(imag(v₁ᴿᴺ))) is larger than the tolerance value 1e-15. Keeping as complex.",
    )
    @show norm(v₁ᴿᴺ - swapinds(dag(v₁ᴿᴺ), reverse(Pair(inds(v₁ᴿᴺ)...))))
  else
    v₁ᴿᴺ = real(v₁ᴿᴺ)
  end

  # Initial guess for bond matrix such that:
  # ψ₁ * C₁ᴿᴺ = C₁ᴿᴺ * ψ₁ᴿ
  C₁ᴿᴺ = sqrt(v₁ᴿᴺ)
  C₁ᴿᴺ = replacetags(C₁ᴿᴺ, left_tags => right_tags; plev=1)
  C₁ᴿᴺ = noprime(C₁ᴿᴺ, right_tags)

  ## Flip the sign:
  ## @show inds(ψ[N])
  ## @show inds(C₁ᴿᴺ)
  ## @show C₁ᴿᴺ
  ## @show rᴺ = dag(uniqueind(C₁ᴿᴺ, ψ[N]))
  ## rᴺ⁻ = Index(-space(rᴺ); tags=tags(rᴺ), plev=plev(rᴺ), dir=dir(rᴺ))
  ## C₁ᴿᴺ = C₁ᴿᴺ * δ(rᴺ, rᴺ⁻)
  ## @show C₁ᴿᴺ
  ## @show inds(C₁ᴿᴺ)

  # Normalize the center matrix
  normalize!(C₁ᴿᴺ)

  Cᴿ, ψᴿ, λᴿ = ITensorInfiniteMPS.right_orthogonalize_polar(
    ψ, C₁ᴿᴺ; left_tags=left_tags, right_tags=right_tags
  )
  # @assert λᴿ ≈ sqrt(real(λ₁ᴿᴺ))
  return Cᴿ, ψᴿ, λᴿ
end