# export Propagation, Inst

using LinearAlgebra
using ProgressMeter

"Abstract type that contains all types of propagators"
abstract type Propagator end 

abstract type NonintPropagator <: Propagator end 
abstract type IntPropagator <: Propagator end 


"2nd order Magnus propagator"
struct Magnus2 <: NonintPropagator end

"Instantanious / snapshot propagation"
struct Inst <: NonintPropagator end

"Crank-Nicholson propagator"
struct Crank <: NonintPropagator end

"Effective adiabatic propagator"
struct Kato <: NonintPropagator end

"Propagation (including Ψ) and PropKey (excluding Ψ) "
abstract type AbstractPropagation end

"Lanczos propagator"
struct TDLanczos <: IntPropagator
    ϵ #max. error
    Λ #size of Krylov space
end

TDLanczos(;ϵ = 0.001, Λ = 20) = TDLanczos(ϵ,Λ)

"Propagation type that encapsulates the model, the propagation parameters, the propagator and the physical time."
mutable struct Propagation <: AbstractPropagation
    m::Model
    p::Propagator
    dt::Float64
    t::Float64 # current time
    tf::Float64 # final time
    Ψ::Array{ComplexF64,2} # wavefunction
    timestep::Int #timestep
    occ::Int #occupation of slater det 
end

"initialize with `t=0` and `Ψ = Ψ₀`. "
Propagation(;m = RmPbc(), p = Magnus2(), dt = 0.1, ϕf = 1, occratio = 1//2, inittime = 0, kwargs...) = Propagation(m, p, dt, inittime, ϕf*m.T, eigen(hamiltonian(m,inittime)).vectors, 1, ceil(Int,m.L*occratio))


"Propagation type that encapsulates the model, the propagation parameters, the propagator and the physical time."
mutable struct LPropagation <: AbstractPropagation
    m::Model
    p::Propagator
    t::Float64 # current time
    dt0::Float64 # initial time step
    dt::Float64 # time step
    tf::Float64 # final time
    Ψ::Array{ComplexF64,1} # wavefunction
    ϵ::Float64 #error threshold
    Λ::Int #maximum Lanczos number/iteration
    Ψtmp::Array{ComplexF64,1}
    converged::Bool
end

LPropagation(;m = IntRmSpinPbc(L=10), p = TDLanczos(), dt = 0.1, ϕf = 1, ϵ = 0.001, Λ = 50, kwargs...) = LPropagation(m, p, 0., dt, dt, ϕf*m.T,normalize(rand(ComplexF64,basissize(m))), ϵ, Λ, normalize(rand(ComplexF64,basissize(m))), false)


"Exactly determine a propagation by simulation parameters (both model and prop parameters)."
struct PropKey <: AbstractPropagation
    m::Model
    p::Propagator
    dt::Float64
    tf::Float64 #final time
    occ::Int
end 


PropKey(P::Propagation) = PropKey(P.m,P.p,P.dt,P.tf,P.occ)




"Propagate system with appropriate propagator."
prop!(P::AbstractPropagation) = prop!(P, P.p)
prop!(P::LPropagation,Htemp;kwargs...) = prop!(P, P.p, Htemp;kwargs...)

propfinish!(P::AbstractPropagation) = propfinish!(P, P.p)

#make recursive copy (independent) of original P.
Base.deepcopy(P::Propagation) = Propagation([ deepcopy(getfield(P, k)) for k = 1:length(fieldnames(Propagation)) ]...)


# timerange(P::Propagation) = 0:P.dt:P.tf
"Timerange is concatenated with tf if tf is not reached."
function timerange(P::Propagation; every = 1, transient = 0)
    range = (-P.m.T*transient:P.dt:P.tf)[1:every:end]
    if range[end] != P.tf
        println("fixing timerange")
        range = [range;P.tf]
    end
    return range
end


init!(P::AbstractPropagation) = init!(P, P.p)
function init!(P::AbstractPropagation, p::NonintPropagator)
    P.Ψ = eigen(hamiltonian(m,0)).vectors
end

function init!(P::AbstractPropagation, p::TDLanczos)
    P.Ψ = rand(ComplexF64,basissize(P.m)) ###replace this with Lanczos ground state.
end

function prop!(P::AbstractPropagation, p::Kato)
    V = eigen(hamiltonian(P.m,P.t-P.dt)).vectors[:,1:P.occ] #might want to rewrite all with occupations other than groundstate possible.
    P̂1 = V * V'
    V = eigen(hamiltonian(P.m,P.t+P.dt)).vectors[:,1:P.occ]
    P̂2 = V * V'
    V = eigen(hamiltonian(P.m,P.t)).vectors[:,1:P.occ]
    P̂ = V * V'

    P̂̇ = (P̂2.-P̂1)/(2*P.dt)
    ĥ = Hermitian(1im * (P̂̇*P̂ - P̂*P̂̇))

    λ, V = eigen(ĥ)
    P.Ψ .= V' * P.Ψ # pos2eig
    P.Ψ .=  (P.Ψ .* (exp.(-λ .* P.dt .* 1im)))
    P.Ψ .= V * P.Ψ # eig2pos
    P.t += P.dt
end


function prop!(P::AbstractPropagation, p::Inst) # type unstable? Mutating.
    P.t += P.dt
    P.timestep += 1
    P.Ψ .= (eigen(hamiltonian(P.m, P.t)).vectors)[:,:]  # occupation?
end

function propfinish!(P::AbstractPropagation, p::Inst)
    P.t = P.tf
    P.timestep += 1
    P.Ψ .= (eigen(hamiltonian(P.m, P.t)).vectors)[:,:]  # occupation?
end

function prop!(P::AbstractPropagation, p::Crank)
    H = hamiltonian(P.m, P.t)
    A = (I + (1.0im) * H * P.dt / 2.)
    B = (I - (1.0im) * H * P.dt / 2.)
    P.Ψ .=   inv(A) * (B *  P.Ψ)
    P.t += P.dt
    P.timestep += 1
end

function propfinish!(P::AbstractPropagation, p::Crank)
    H = hamiltonian(P.m, P.t)
    dt = P.tf-P.t
    A = (I + (1.0im) * H * dt / 2.)
    B = (I - (1.0im) * H * dt / 2.)
    P.Ψ .=   inv(A) * (B *  P.Ψ)
    P.t += dt
    P.timestep += 1
end


function prop!(P::AbstractPropagation, p::Magnus2)
    λ, U = eigen(hamiltonian(P.m, P.t + P.dt / 2.)) # occupation?
    P.Ψ .= U' * P.Ψ # pos2eig
    P.Ψ .=  (P.Ψ .* (exp.(-λ .* P.dt .* 1im)))
    P.Ψ .= U * P.Ψ # eig2pos
    P.t += P.dt
    P.timestep += 1
end

function propfinish!(P::AbstractPropagation, p::Magnus2)
    dt = P.tf - P.t
    λ, U = eigen(hamiltonian(P.m, P.t + dt / 2.)) # occupation?
    P.Ψ .= U' * P.Ψ # pos2eig
    P.Ψ .=  (P.Ψ .* (exp.(-λ .* dt .* 1im)))
    P.Ψ .= U * P.Ψ # eig2pos
    P.t += dt
    P.timestep += 1
end

function periodicshiftop(P::AbstractPropagation)
    UT = I
    for t in timerange(P)
        λ, U = eigen(hamiltonian(P.m, t + P.dt / 2.)) # occupation?
        UT = UT * U' .* (exp.(-λ .* P.dt .* 1im)) * U
    end
    return UT
end

function prop!(P::LPropagation, p::TDLanczos, Htemp; target = x->identity(x))
    P.converged = false

    if P.dt < 1e-10
        error("Propagation could not converge.")
    end
    i = 1
    qs = Array{Complex{Float64},2}(undef, P.Λ, basissize(P.m))
    
    Ψ = normalize(P.Ψ)
    Ψtmp = similar(Ψ)
    qs[1,:] = Ψ

    as = zeros(Float64, P.Λ)
    ns = zeros(Float64, P.Λ)

    H = Hfromtemp(P.m,P.t+P.dt/2,Htemp...)

    qs[2,:] = H * qs[1,:]
    as[1] = real(qs[1,:]' * qs[2,:])
    λs = as[1]
    qs[2,:] = qs[2,:] - as[1] * qs[1,:]
    ns[2] = norm(qs[2,:])
    qs[2,:] = qs[2,:] ./ ns[2]
    # for i in 2:P.Λ - 1
    @showprogress "$(P.dt) :" for i in 2:P.Λ - 1
        qs[i + 1,:] = H * qs[i,:]
        as[i] = real(qs[i,:]' * qs[i + 1,:])
        λs, U = eigen(SymTridiagonal(as[1:i], ns[2:i]))
        tartmp = target(Ψ)

        Ψ = normalize!(qs'[:,1:i] * (U * (exp.(-1im*λs*P.dt) .* (U' * (qs[1:i,:] * P.Ψ))))) #wrong. need Ψk and Ψk-1
        if (norm(target(Ψ) - tartmp) < P.ϵ)
            P.converged = true
            P.Ψ = Ψ
            P.t += P.dt
            println("step converged. Λ = $i dt = $(P.dt) t = $(P.t)")
            break
        end
        qs[i + 1,:] = qs[i + 1,:] - as[i] * qs[i,:] - ns[i] * qs[i - 1,:]
        ns[i + 1] = norm(qs[i + 1,:])
        qs[i + 1,:] = qs[i + 1,:] ./ ns[i + 1]

        for m in 1:i # reorthogonalization
            q = (qs[m,:]' * qs[i + 1,:])
            qs[i + 1, :] = (qs[i + 1, :] - qs[m, :] * q) ./ (1 - abs2(q))
            if !(norm(qs[i+1,:]) ≈ 1.) && i > 3
                println(m,"  norm of step:",norm(qs[i+1,:]),"  overlap of last step:", (qs[m,:]' * qs[i + 1,:]))
                return HΛ, qs[1:i,:], ens
            end
        end

    end
    
    while !P.converged
        P.dt = P.dt/2
        prop!(P, Htemp)
    end
    P.dt = P.dt0;
end



















