
using LinearAlgebra
using Random


abstract type Model end
abstract type Model1d <: Model end
abstract type Model1dObc <: Model1d end
abstract type Model1dPbc <: Model1d end
abstract type Model1dSpinPbc <: Model1dPbc end
abstract type Model1dSpinObc <: Model1dObc end

abstract type IntModel <: Model end
abstract type IntModel1dSpin <: IntModel end
abstract type IntModel1dPbc <: IntModel end
abstract type IntModel1dObc <: IntModel end
abstract type IntModel1dSpinPbc <: IntModel1dSpin end
abstract type IntModel1dSpinObc <: IntModel1dSpin end
#CAREFUL THIS HAS CHANGED.






"OBC Rice-Mele model"
struct RmObc <: Model1dObc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64} # units of π
    V::Float64
end

RmObc(; L = 60, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 1, V = 0, kwargs...) = RmObc(L, RΔ, Rδ, T, ϕ₀, V)

"PBC Rice-Mele model"
struct RmPbc <: Model1dPbc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
end

RmPbc(; L = 60, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 1, kwargs...) = RmPbc(L, RΔ, Rδ, T, ϕ₀)

"PBC Rice-Mele model with interface"
struct RmPbcInterface <: Model1dPbc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
end

RmPbcInterface(; L = 60, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 0.5, kwargs...) = RmPbcInterface(L, RΔ, Rδ, T, ϕ₀)


"PBC Rice-Mele model with uniform diagonal disorder and gapless inclusion"
struct RmPbcDiagDisInclusion <: Model1dPbc
    L::Int
    Linc::Int # size of embedded island.
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    w::Float64 # disorder strength: Vⱼ ↦ Vⱼ + ϵⱼ ,  ϵⱼ ∈ w/2[-1,1].
    w2::Float64 # disorder strength  of bath
    seed::Int
    rands::Array{Float64}
    inclusionpot::Array{Float64} # potential of initial system.
end

RmPbcDiagDisInclusion(rands = rands; L = 60, Linc = 100, inclusionpot = zeros(100), Lext = 10, RΔ = 2.3, Rδ = 0.5, T = 1, ϕ₀ = 0.5, w = 0.1, w2 = 0.1, seed = 1, kwargs...) = RmPbcDiagDisInclusion(L, Linc, RΔ, Rδ, T, ϕ₀, w, w2, seed, rands, inclusionpot)


"PBC Rice-Mele model with uniform diagonal disorder"
struct RmPbcDiagDis <: Model1dPbc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    w::Float64 # disorder strength: Vⱼ ↦ Vⱼ + ϵⱼ ,  ϵⱼ ∈ w/2[-1,1].
    seed::Int
end

RmPbcDiagDis(; L = 60, RΔ = 2.3, Rδ = 0.5, T = 1, ϕ₀ = -1.0, w = 0.1, seed = 1, kwargs...) = RmPbcDiagDis(L, RΔ, Rδ, T, ϕ₀, w, seed)


"PBC Rice-Mele model with uniform disorder and twisted boundary conditions (θ)"
struct RmPbcDiagDisTwist <: Model1dPbc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    w::Float64 # disorder strength: Vⱼ ↦ Vⱼ + ϵⱼ ,  ϵⱼ ∈ w/2[-1,1].
    seed::Int
    θ::Float64
end

RmPbcDiagDisTwist(; L = 60, RΔ = 2.3, Rδ = 0.5, T = 1, ϕ₀ = -1.0, w = 0.1, seed = 1, θ = 0, kwargs...) = RmPbcDiagDisTwist(L, RΔ, Rδ, T, ϕ₀, w, seed, θ)


"AA model" # not finished!
struct AA <: Model1dPbc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64} # phase offset in time
    φ₀::Union{Int,Float64} # phase offset of pseudorandom potential
    λ::Float64
    α::Float64
end

AA(; L = 110, RΔ = 2.3, Rδ = 0.5, T = 1, ϕ₀ = -1.0, φ₀ = 0, α = (sqrt(5) - 1) / 2, λ = 0.0, kwargs...) = AA(L, RΔ, Rδ, T, ϕ₀, φ₀, λ, α)


"PBC Rice-Mele model with spins"
struct RmSpinPbc <: Model1dSpinPbc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    Vso::Union{Int,Float64}
end

RmSpinPbc(; L = 60, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 0.5, Vso = 0, kwargs...) = RmSpinPbc(L, RΔ, Rδ, T, ϕ₀, Vso)


"PBC Rice-Mele model with spins and hubbard interaction"
struct IntRmSpinPbc <: IntModel1dSpinPbc
    L::Int # number of sites
    N::Int # particle number
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    Vso::Union{Int,Float64}
    U::Float64
end

IntRmSpinPbc(; L = 8, N = L, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 0.5, Vso = 0, U = 0, kwargs...) = IntRmSpinPbc(L, N, RΔ, Rδ, T, ϕ₀, Vso, U)

"PBC Rice-Mele model with spins and hubbard interaction"
struct IntRmSpinObc <: IntModel1dSpinObc
    L::Int # number of sites
    N::Int # particle number
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    Vso::Union{Int,Float64}
    U::Float64
end

IntRmSpinObc(; L = 8, N = L, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 0.5, Vso = 0, U = 0, kwargs...) = IntRmSpinObc(L, N, RΔ, Rδ, T, ϕ₀, Vso, U)



"""
Rice Mele model with nearest-neighbor interactions.
"""
struct IntRmPbc <: IntModel1dPbc
    L::Int # number of sites
    N::Int # particle number
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    U::Float64
end

IntRmPbc(; L = 12, N = fld(L, 2), RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 0.5, U = 0, kwargs...) = IntRmPbc(L, N, RΔ, Rδ, T, ϕ₀, U)



"PBC Rice-Mele model with spins"
struct RmSpinObc <: Model1dSpinObc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    Vso::Union{Int,Float64}
    Lres::Int # length of spin polarized reservoir on both sites
end

RmSpinObc(; L = 60, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 0.5, Vso = 0, Lres = 0, kwargs...) = RmSpinObc(L, RΔ, Rδ, T, ϕ₀, Vso, Lres)

"PBC Rice-Mele model with spins and stark localization"
struct RmSpinObcStark <: Model1dSpinObc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    Vso::Union{Int,Float64}
    Lres::Int # length of spin polarized reservoir on both sites
    V::Float64 #stark tilt
end

RmSpinObcStark(; L = 20, RΔ = 3.0, Rδ = 0.9, T = 80, ϕ₀ = 0.5, V = 0, Vso = 0, Lres = 0, kwargs...) = RmSpinObcStark(L, RΔ, Rδ, T, ϕ₀, Vso, Lres, V)


struct RmSpinObcRot <: Model1dSpinObc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    Vso::Union{Int,Float64}
    Lres::Int # length of spin polarized reservoir on both sites
end

RmSpinObcRot(; L = 60, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 0.5, Vso = 0, Lres = 0, kwargs...) = RmSpinObcRot(L, RΔ, Rδ, T, ϕ₀, Vso, Lres)


"PBC Rice-Mele model with spins"
struct RmSpinObcRes <: Model1dSpinObc
    L::Int
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    Lres::Int
    Vso::Float64
end

RmSpinObcRes(; L = 60, RΔ = 3.0, Rδ = 0.5, T = 1, ϕ₀ = 0.5, Lres = 0, Vso = 1, kwargs...) = RmSpinObcRes(L, RΔ, Rδ, T, ϕ₀, Lres, Vso)


"Ionic Hubbard model, charge pump around critical Δ = U/2"
struct IonicHubbard <: IntModel1dSpinObc
    L::Int # number of sites
    N::Int # particle number
    RΔ::Float64
    Rδ::Float64
    T::Union{Int,Float64}
    ϕ₀::Union{Int,Float64}
    U::Float64
    Δc::Float64 #critical staggering
end

IonicHubbard(; L = 20, N = L, RΔ = 2, Rδ = 0.25, T = 1, ϕ₀ = 0.5, U = 0, Δc = (U - 1.6) / 2, kwargs...) = IonicHubbard(L, N, RΔ, Rδ, T, ϕ₀, U, Δc)

"Ionic Hubbard model, free Δ, δ plane."
struct IonicHubbard2 <: IntModel1dSpinObc
    L::Int # number of sites
    N::Int # particle number
    Δ::Float64
    δ::Float64
    U::Float64
end

IonicHubbard2(; L = 20, N = L, Δ = 2, δ = 0.25, U = 0, kwargs...) = IonicHubbard2(L, N, Δ, δ, U)

"Ionic Hubbard model, charge pump around critical Δ = U/2"
struct IonicHubbardPbc <: IntModel1dSpinPbc
    L::Int # number of sites
    N::Int # particle number
    RΔ::Float64
    Rδ::Float64
    U::Float64
    Δc::Float64
    T::Float64
    ϕ₀::Union{Int,Float64}
end

IonicHubbardPbc(; L = 6, N = L, RΔ = 1, Rδ = 0.5, U = 4, Δc = 1.4, T = 100, ϕ₀ = 0.5, kwargs...) = IonicHubbardPbc(L, N, RΔ, Rδ, U, Δc, T, ϕ₀)

struct IonicHubbardPbcToy <: IntModel1dSpinPbc
    L::Int # number of sites
    N::Int # particle number
    RΔ::Float64
    Rδ::Float64
    U::Float64
    Δc::Float64
    T::Float64
    J::Float64 ## Sz Sz term
    f::Float64 ## staggered magnetic field
    ϕ₀::Union{Int,Float64}
    γ::Float64 ##rand
    randpots::Array{Float64}
end

IonicHubbardPbcToy(; L = 6, N = L, RΔ = 1, Rδ = 0.5, U = 4, Δc = 1.4, T = 100, ϕ₀ = 0.5, J = 1, f = 0, γ = 0,  kwargs...) = IonicHubbardPbcToy(L, N, RΔ, Rδ, U, Δc, T, J, f, ϕ₀, γ, rand(2*L))


"Ionic Hubbard model, free Δ, δ plane."
struct IonicHubbard2Pbc <: IntModel1dSpinPbc
    L::Int # number of sites
    N::Int # particle number
    Δ::Float64
    δ::Float64
    U::Float64
end

IonicHubbard2Pbc(; L = 20, N = L, Δ = 2, δ = 0.25, U = 0, kwargs...) = IonicHubbard2Pbc(L, N, Δ, δ, U)


"Ionic Hubbard model, free Δ, δ plane with toy."
struct IonicHubbard2PbcToy <: IntModel1dSpinPbc
    L::Int # number of sites
    N::Int # particle number
    Δ::Float64
    δ::Float64
    J::Float64 ## Sz Sz term
    f::Float64 ## staggered magnetic field
    U::Float64
    γ::Float64 ##rand
    randpots::Array{Float64}
end

IonicHubbard2PbcToy(; L = 20, N = L, Δ = 2, δ = 0.25, U = 0, J = 1, f = 0, γ = 0, kwargs...) = IonicHubbard2PbcToy(L, N, Δ, δ, J, f, U, γ, rand(2*L))


"Bosonic Schwinger Model"
struct BosonicSchwinger <: IntModel
    L::Int
    K::Float64
    B::Float64
end

BosonicSchwinger(; L = 4, K = 1, B = 1) = BosonicSchwinger(L, K, B)

"""
    hamiltonian(m::Model1dObc, t)
build tight-binding Hamiltonian matrix of a 1d, obc system.
"""
function hamiltonian(m::Model1dObc, t)
    diagonal = [potential(m, i, t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = SymTridiagonal(diagonal, offdiagonal)
end


"""
    hamiltonian(m::Model1dPbc, t)
build tight-binding Hamiltonian matrix of a 1d, pbc system.
"""
function hamiltonian(m::Model1dPbc, t)
    diagonal = [potential(m, i, t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = SymTridiagonal(diagonal, offdiagonal)
    B = zeros(Float64, m.L, m.L)
    B[1, m.L] = hopping(m, m.L, t)
    HH = Hermitian(H + B)
end

function hamiltonian(m::RmPbcDiagDis, t)
    Random.seed!(m.seed) # reset RNG
    diagonal = [potential(m, i, t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = SymTridiagonal(diagonal, offdiagonal)
    B = zeros(Float64, m.L, m.L)
    B[1, m.L] = hopping(m, m.L, t)
    HH = Hermitian(H + B)
end


function hamiltonian(m::RmPbcDiagDisInclusion, t)
    # Random.seed!(m.seed) # reset RNG
    diagonal = [i > m.Linc ? potential(m, i, t) : m.inclusionpot[i] for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = SymTridiagonal(diagonal, offdiagonal)
    B = zeros(Float64, m.L, m.L)
    B[1, m.L] = hopping(m, m.L, t)
    HH = Hermitian(H + B)
end

function hamiltonian(m::RmPbcDiagDisTwist, t)
    ph = exp(2im * π / m.L * m.θ)
    Random.seed!(m.seed) # reset RNG
    diagonal = [potential(m, i, t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = Tridiagonal(offdiagonal * ph', complex(diagonal), offdiagonal * ph)
    B = zeros(Float64, m.L, m.L)
    B[1, m.L] = hopping(m, m.L, t)
    HH = Hermitian(H + B)
end

function hamiltonian(m::RmSpinPbc, t)
    diagonal = [potential(m, i, t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = SymTridiagonal([diagonal; -diagonal], [offdiagonal; 0; offdiagonal]) # 2L×2L, ↑↑↑...↑↑↑↓↓↓...↓↓↓
    B = zeros(ComplexF64, 2 * m.L, 2 * m.L)
    B[1, m.L] = hopping(m, m.L, t)
    B[m.L+1, 2*m.L] = hopping(m, 2 * m.L, t)
    for i = 1:m.L-1
        B[i, i+1+m.L] = 1im * σvec([m.Vso, 0, 0])[1, 2]
        B[i+1, i+m.L] = -1im * σvec([m.Vso, 0, 0])[1, 2]
    end
    B[1, 2*m.L] = -1im * σvec([m.Vso, 0, 0])[1, 2]
    B[m.L, m.L+1] = 1im * σvec([m.Vso, 0, 0])[1, 2]

    HH = Hermitian(H + B)
end


function hamiltonian(m::RmSpinObcStark, t; V = 1)
    diagonal = [potential(m, i, t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    externalpot = float.([V * j for j = 1:m.L])
    H = SymTridiagonal([diagonal + externalpot; -diagonal + reverse(externalpot)], [offdiagonal; 0; offdiagonal]) # 2L×2L, ↑↑↑...↑↑↑↓↓↓...↓↓↓
    B = zeros(ComplexF64, 2 * m.L, 2 * m.L)
    # B[1,m.L] = hopping(m, m.L, t)#
    # B[m.L + 1,2 * m.L] = hopping(m, 2 * m.L, t)#
    for i = 1:m.L-1
        B[i, i+1+m.L] = spinorbitcoupling(m)
        B[i+1, i+m.L] = -spinorbitcoupling(m)
    end
    # B[1,2 * m.L] = -1im * σvec([m.Vso,0,0])[1,2]#
    # B[m.L,m.L + 1] = 1im * σvec([m.Vso,0,0])[1,2]#
    HH = Hermitian(H + B)
end

function hamiltonian(m::RmSpinObc, t)
    diagonal = [potential(m, i, t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = SymTridiagonal([diagonal; -diagonal], [offdiagonal; 0; offdiagonal]) # 2L×2L, ↑↑↑...↑↑↑↓↓↓...↓↓↓
    B = zeros(ComplexF64, 2 * m.L, 2 * m.L)
    for i = 1:m.L-1
        B[i, i+1+m.L] = spinorbitcoupling(m)
        B[i+1, i+m.L] = -spinorbitcoupling(m)
    end
    HH = Hermitian(H + B)
end

function hamiltonian(m::RmSpinObcRot, t)
    diagonal = [potential(m, i, t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = Tridiagonal([conj.(offdiagonal); 0; (offdiagonal)], complex.([diagonal; -diagonal]), [offdiagonal; 0; conj.(offdiagonal)]) # 2L×2L, ↑↑↑...↑↑↑↓↓↓...↓↓↓
    B = zeros(ComplexF64, 2 * m.L, 2 * m.L)
    HH = Hermitian(H + B)
end

function hamiltonian(m::RmSpinObc, t, spinfactors, a)
    diagonal = [potential(m, i, spinfactors[1] * t) for i = 1:m.L]
    diagonal2 = [potential(m, i, spinfactors[2] * t) for i = 1:m.L]
    offdiagonal = [hopping(m, i, spinfactors[1] * t) for i = 1:m.L-1]
    offdiagonal2 = [hopping(m, i, spinfactors[2] * t) for i = 1:m.L-1]

    H = SymTridiagonal([diagonal; -a * diagonal2], [offdiagonal; 0; a * offdiagonal2]) # 2L×2L, ↑↑↑...↑↑↑↓↓↓...↓↓↓
    B = zeros(ComplexF64, 2 * m.L, 2 * m.L)
    for i = 1:m.L-1
        B[i, i+1+m.L] = spinorbitcoupling(m)
        B[i+1, i+m.L] = -spinorbitcoupling(m)
    end
    HH = Hermitian(H + B)
end



function hamiltonian(m::RmSpinPbc, t, extrapot)
    diagonal = [potential(m, i, t) + extrapot for i = 1:m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = SymTridiagonal([diagonal; -diagonal], [offdiagonal; 0; offdiagonal]) # 2L×2L, ↑↑↑...↑↑↑↓↓↓...↓↓↓
    B = zeros(ComplexF64, 2 * m.L, 2 * m.L)
    B[1, m.L] = hopping(m, m.L, t)
    B[m.L+1, 2*m.L] = hopping(m, 2 * m.L, t)
    for i = 1:m.L-1
        B[i, i+1+m.L] = spinorbitcoupling(m)
        B[i+1, i+m.L] = -spinorbitcoupling(m)
    end
    B[1, 2*m.L] = -spinorbitcoupling(m)
    B[m.L, m.L+1] = spinorbitcoupling(m)

    HH = Hermitian(H + B)
end


function hamiltonian(m::RmSpinObcRes, t)
    diagonal = [potential(m, i, t) for i = 1:2*m.L]
    offdiagonal = [hopping(m, i, t) for i = 1:m.L-1]
    H = SymTridiagonal([zeros(m.Lres); diagonal[1:m.L]; zeros(m.Lres); diagonal[m.L+1:2*m.L]], [-ones(m.Lres); offdiagonal; -ones(m.Lres); 0; offdiagonal]) # 2L×2L + free spin up sites on both sides, -- ↑↑↑...↑↑↑↓↓↓...↓↓↓ --
    B = zeros(ComplexF64, 2 * m.Lres + 2 * m.L, 2 * m.Lres + 2 * m.L)
    for i = 1:m.L-1
        B[m.Lres+i, 2*m.Lres+i+1+m.L] = spinorbitcoupling(m)
        B[m.Lres+i+1, 2*m.Lres+i+m.L] = -spinorbitcoupling(m)
    end
    HH = Hermitian(H + B)
end



### MPOs:

"Implements snake geometry for PBC MPS calculations."
function snake(i, m::Model)
    if i <= cld(m.L, 2)
        return 2i - 1
    else
        return m.L - 2(i - cld(m.L, 2) - 1)
    end
end

"Implements snake geometry for PBC MPS calculations."
function snake(i, L)
    if i <= cld(L, 2)
        return 2i - 1
    else
        return L - 2(i - cld(L, 2) - 1)
    end
end

"inverse of snake"
function isnake(i, m::Model)
    if i%2 == true #odd
        return i - cld((i-1),2)
    else
        return cld((m.L - i + 1),2) + cld(m.L,2)
    end
end

"inverse of snake"
function isnake(i, L)
    if i%2 == true #odd
        return i - cld((i-1),2)
    else
        return cld((L - i + 1),2) + cld(L,2)
    end
end


function mpo_ham(m::IntRmSpinObc, sites, t)
    ampo = AutoMPO()

    for j = 1:m.L-1
        ampo += hopping(m, j, t), "Cdagup", j, "Cup", j + 1
        ampo += hopping(m, j, t), "Cdagup", j + 1, "Cup", j
        ampo += hopping(m, j, t), "Cdagdn", j, "Cdn", j + 1
        ampo += hopping(m, j, t), "Cdagdn", j + 1, "Cdn", j
    end

    if m.Vso != 0
        for j = 1:m.L-1
            ampo += spinorbitcoupling(m), "Cdagup", j, "Cdn", j + 1
            ampo += spinorbitcoupling(m), "Cdagdn", j, "Cup", j + 1
            ampo -= spinorbitcoupling(m), "Cdagdn", j + 1, "Cup", j
            ampo -= spinorbitcoupling(m), "Cdagup", j + 1, "Cdn", j
        end
    end

    for j = 1:m.L
        ampo += potential(m, j, t), "Cdagup", j, "Cup", j
        ampo += -potential(m, j, t), "Cdagdn", j, "Cdn", j
        ampo += interaction(m), "Cdagdn", j, "Cdn", j, "Cdagup", j, "Cup", j
        # ampo += 0.001*j, "Cdagdn",j , "Cdn", j
        # ampo += 0.001*j, "Cdagup",j , "Cup", j

    end
    return MPO(ampo, sites)
end

function mpo_ham(m::IonicHubbard, sites, t)
    ampo = AutoMPO()

    for j = 1:m.L-1
        ampo += hopping(m, j, t), "Cdagup", j, "Cup", j + 1
        ampo += hopping(m, j, t), "Cdagup", j + 1, "Cup", j
        ampo += hopping(m, j, t), "Cdagdn", j, "Cdn", j + 1
        ampo += hopping(m, j, t), "Cdagdn", j + 1, "Cdn", j
    end

    for j = 1:m.L
        ampo += potential(m, j, t), "Cdagup", j, "Cup", j
        ampo += potential(m, j, t), "Cdagdn", j, "Cdn", j
        ampo += interaction(m), "Cdagdn", j, "Cdn", j, "Cdagup", j, "Cup", j
    end
    return MPO(ampo, sites)
end

function mpo_ham(m::IonicHubbardPbc, sites, t)
    ampo = AutoMPO()

    for j = 1:m.L-1
        sj1 = snake(j, m)
        sj2 = snake(j + 1, m)
        ampo += hopping(m, j, t), "Cdagup", sj1, "Cup", sj2
        ampo += hopping(m, j, t), "Cdagup", sj2, "Cup", sj1
        ampo += hopping(m, j, t), "Cdagdn", sj1, "Cdn", sj2
        ampo += hopping(m, j, t), "Cdagdn", sj2, "Cdn", sj1
    end

    sj1 = snake(m.L, m)
    sj2 = snake(1, m)
    ampo += -hopping(m, m.L, t), "Cdagup", sj1, "Cup", sj2
    ampo += -hopping(m, m.L, t), "Cdagup", sj2, "Cup", sj1
    ampo += -hopping(m, m.L, t), "Cdagdn", sj1, "Cdn", sj2
    ampo += -hopping(m, m.L, t), "Cdagdn", sj2, "Cdn", sj1

    for j = 1:m.L
        sj1 = snake(j, m)
        ampo += potential(m, j, t), "Cdagup", sj1, "Cup", sj1
        ampo += potential(m, j, t), "Cdagdn", sj1, "Cdn", sj1
        ampo += interaction(m), "Cdagdn", sj1, "Cdn", sj1, "Cdagup", sj1, "Cup", sj1
    end
    return MPO(ampo, sites)
end

function mpo_ham(m::IonicHubbard2, sites)
    ampo = AutoMPO()

    for j = 1:m.L-1
        ampo += hopping(m, j), "Cdagup", j, "Cup", j + 1
        ampo += hopping(m, j), "Cdagup", j + 1, "Cup", j
        ampo += hopping(m, j), "Cdagdn", j, "Cdn", j + 1
        ampo += hopping(m, j), "Cdagdn", j + 1, "Cdn", j
    end

    for j = 1:m.L
        ampo += potential(m, j), "Cdagup", j, "Cup", j
        ampo += potential(m, j), "Cdagdn", j, "Cdn", j
        ampo += interaction(m), "Cdagdn", j, "Cdn", j, "Cdagup", j, "Cup", j
    end
    return MPO(ampo, sites)
end

function mpo_ham(m::IonicHubbard2Pbc, sites)
    ampo = AutoMPO()

    for j = 1:m.L-1
        sj1 = snake(j, m)
        sj2 = snake(j + 1, m)
        ampo += hopping(m, j), "Cdagup", sj1, "Cup", sj2
        ampo += hopping(m, j), "Cdagup", sj2, "Cup", sj1
        ampo += hopping(m, j), "Cdagdn", sj1, "Cdn", sj2
        ampo += hopping(m, j), "Cdagdn", sj2, "Cdn", sj1
    end

    sj1 = snake(m.L, m)
    sj2 = snake(1, m)
    ampo += (-1)^cld(m.L,2) * hopping(m, m.L), "Cdagup", sj1, "Cup", sj2
    ampo += (-1)^cld(m.L,2) * hopping(m, m.L), "Cdagup", sj2, "Cup", sj1
    ampo += (-1)^cld(m.L,2) * hopping(m, m.L), "Cdagdn", sj1, "Cdn", sj2
    ampo += (-1)^cld(m.L,2) * hopping(m, m.L), "Cdagdn", sj2, "Cdn", sj1

    for j = 1:m.L
        sj1 = snake(j, m)
        ampo += potential(m, j), "Cdagup", sj1, "Cup", sj1
        ampo += potential(m, j), "Cdagdn", sj1, "Cdn", sj1
        ampo += interaction(m), "Cdagdn", sj1, "Cdn", sj1, "Cdagup", sj1, "Cup", sj1
    end
    return MPO(ampo, sites)
end

# function mpo_ham(m::IonicHubbard2Pbc, sites)

#     for j = 1:m.L-1
#         # sj1 = snake(j, m)
#         # sj2 = snake(j + 1, m)
#         ampo += hopping(m, j), "Cdagup", j, "Cup", j+1
#         ampo += hopping(m, j), "Cdagup", j+1, "Cup", j
#         ampo += hopping(m, j), "Cdagdn", j, "Cdn", j+1
#         ampo += hopping(m, j), "Cdagdn", j+1, "Cdn", j
#     end

#     # j = snake(m.L, m)
#     # j+1 = snake(1, m)
#     ampo += (-1)^cld(m.L,2) * hopping(m, m.L), "Cdagup", m.L, "Cup", 1
#     ampo += (-1)^cld(m.L,2) * hopping(m, m.L), "Cdagup", 1, "Cup", m.L
#     ampo += (-1)^cld(m.L,2) * hopping(m, m.L), "Cdagdn", m.L, "Cdn", 1
#     ampo += (-1)^cld(m.L,2) * hopping(m, m.L), "Cdagdn", 1, "Cdn", m.L

#     for j = 1:m.L
#         # j = snake(j, m)
#         ampo += potential(m, j), "Cdagup", j, "Cup", j
#         ampo += potential(m, j), "Cdagdn", j, "Cdn", j
#         ampo += interaction(m), "Cdagdn", j, "Cdn", j, "Cdagup", j, "Cup", j
#     end
#     return MPO(ampo, sites)
# end



############

function mpo_gate(m::IntRmSpinObc, sites, siterange, t)
    ampo = AutoMPO()

    for j in siterange[1:end-1]
        ampo += hopping(m, j, t), "Cdagup", 1, "Cup", 2
        ampo += hopping(m, j, t), "Cdagup", 2, "Cup", 1
        ampo += hopping(m, j, t), "Cdagdn", 1, "Cdn", 2
        ampo += hopping(m, j, t), "Cdagdn", 2, "Cdn", 1
    end

    if m.Vso != 0
        for j in siterange[1:end-1]
            ampo += spinorbitcoupling(m), "Cdagup", 1, "Cdn", 2
            ampo += spinorbitcoupling(m), "Cdagdn", 1, "Cup", 2
            ampo -= spinorbitcoupling(m), "Cdagdn", 2, "Cup", 1
            ampo -= spinorbitcoupling(m), "Cdagup", 2, "Cdn", 1
        end
    end

    for (jj, j) in enumerate(siterange)
        if j == 1
            coeff = 1
        elseif j == m.L
            coeff = 1
        else
            coeff = 0.5
        end
        ampo += coeff * potential(m, j, t), "Cdagup", jj, "Cup", jj
        ampo += -coeff * potential(m, j, t), "Cdagdn", jj, "Cdn", jj
        ampo += coeff * interaction(m), "Cdagdn", jj, "Cdn", jj, "Cdagup", jj, "Cup", jj
    end
    return MPO(ampo, sites[siterange])
end



function fermionsign(m::Model, i, j, s)
    (ii, jj) = i > j ? (j, i) : (i, j)
    if (ii == 1 && jj == m.L) || (ii == m.L + 1 && jj == 2 * m.L)
        γ = (-1)^cld(m.L,2)
    else
        γ = 1
    end
    return (-1)^(sum(s[ii + 1:jj - 1])) * γ
end

σ_z(m::IntModel1dSpinPbc,j) = j <= m.L ? 1 : -1

squircle(t) = (sign(cospi(t))/(sqrt(2)*abs(sinpi(t)))*sqrt(1-abs(cospi(2*t))),sign(sinpi(t))/(sqrt(2)*abs(cospi(t)))*sqrt(1-abs(cospi(2*t))))

"control freak function"
function cf(x)
    if 0 < mod(x, 1.) < 1 / 8.
        return 8 * mod(x, 1.)
    elseif 1 / 8. <= mod(x, 1.) < 3 / 8.
        return 1.
    elseif 3 / 8. <= mod(x, 1.) < 1 / 2.
        return 1 - 8 * (mod(x, 1.) - 3 / 8.)
    else 
        return 0.
    end
end

### generic tight-binding hopping:
"Calculate hopping for model `m` on site `j` at time `t`."
hopping(m::Model1d, j, t) = -1.0
hopping(m::RmPbc, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::IntRmPbc, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::RmSpinPbc, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::RmSpinObc, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::RmSpinObcStark, j, t) = -(1 + δ(m, t) * (-1)^(j))

function hopping(m::RmSpinObcRot, j, t)
    hop = 0.0
    hop += -(1 + δ(m, t) * (-1)^(j))
    hop += j < m.L ? spinorbitcoupling(m) : -spinorbitcoupling(m)
    return hop
end


hopping(m::IntRmSpinPbc, j, t) = -(1 + δ(m, t) * (-1)^(j))
function hopping(m::IntRmSpinPbc, i, j, s::Array{Int64,1}, t)
    (ii, jj) = i > j ? (j, i) : (i, j)
    if (ii == 1 && jj == m.L) || (ii == m.L + 1 && jj == 2 * m.L)
        γ = (-1)^cld(m.L,2)
    else
        γ = 1
    end
    return hopping(m, i, t) * (-1)^(sum(s[ii + 1:jj - 1])) * γ
end

hopping(m::IntRmPbc, j, t) = -(1 + δ(m, t) * (-1)^(j))
function hopping(m::IntRmPbc, i, j, s::Array{Int64,1}, t)
    (ii, jj) = i > j ? (j, i) : (i, j)
    if (ii == 1 && jj == m.L)
        γ = (-1)^cld(m.L,2)
    else
        γ = 1
    end
    return hopping(m, i, t) * (-1)^(sum(s[ii + 1:jj - 1])) * γ
end

function hopping(m::IonicHubbardPbc, i, j, s::Array{Int64,1}, t)
    (ii, jj) = i > j ? (j, i) : (i, j)
    if (ii == 1 && jj == m.L) || (ii == m.L + 1 && jj == 2 * m.L)
        γ = (-1)^cld(m.L,2)
    else
        γ = 1
    end
    return hopping(m, i, t) * (-1)^(sum(s[ii + 1:jj - 1])) * γ
end

function hopping(m::IonicHubbardPbcToy, i, j, s::Array{Int64,1}, t)
    (ii, jj) = i > j ? (j, i) : (i, j)
    if (ii == 1 && jj == m.L) || (ii == m.L + 1 && jj == 2 * m.L)
        γ = (-1)^cld(m.L,2)
    else
        γ = 1
    end
    return hopping(m, i, t) * (-1)^(sum(s[ii + 1:jj - 1])) * γ
end

function hopping(m::IonicHubbard2Pbc, i, j, s::Array{Int64,1})
    (ii, jj) = i > j ? (j, i) : (i, j)
    if (ii == 1 && jj == m.L) || (ii == m.L + 1 && jj == 2 * m.L)
        γ = (-1)^cld(m.L,2)
    else
        γ = 1
    end
    return hopping(m, i) * (-1)^(sum(s[ii + 1:jj - 1])) * γ
end

function hopping(m::IonicHubbard2PbcToy, i, j, s::Array{Int64,1})
    (ii, jj) = i > j ? (j, i) : (i, j)
    if (ii == 1 && jj == m.L) || (ii == m.L + 1 && jj == 2 * m.L)
        γ = (-1)^cld(m.L,2)
    else
        γ = 1
    end
    return hopping(m, i) * (-1)^(sum(s[ii + 1:jj - 1])) * γ
end

hopping(m::IntRmSpinObc, j, t) = -(1 + δ(m, t) * (-1)^(j))

hopping(m::RmSpinObcRes, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::RmPbcInterface, j, t) = j < m.L / 2 ? -(1 + δ(m, t) * (-1)^(j)) : -(1 + δ(m, t))
hopping(m::RmPbcDiagDis, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::RmPbcDiagDisInclusion, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::AA, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::RmPbcDiagDisTwist, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::RmObc, j, t) = -(1 + δ(m, t) * (-1)^(j))

hopping(m::IonicHubbard, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::IonicHubbard2, j) = -(1 + m.δ * (-1)^(j))
hopping(m::IonicHubbard2Pbc, j) = -(1 + m.δ * (-1)^(j))
hopping(m::IonicHubbard2PbcToy, j) = -(1 + m.δ * (-1)^(j))
hopping(m::IonicHubbardPbc, j, t) = -(1 + δ(m, t) * (-1)^(j))
hopping(m::IonicHubbardPbcToy, j, t) = -(1 + δ(m, t) * (-1)^(j))

# hopping(m::IonicHubbardPbcToy, j, t) = isodd(j) ? -2 * cf(t / m.T + 1 / 2 + m.ϕ₀) : - 2 * cf(t / m.T + m.ϕ₀) 
# potential(m::IonicHubbardPbcToy, j, t) = - m.RΔ * (cf(t / m.T + 1 / 4 + m.ϕ₀) - cf(t / m.T + 3 / 4. + m.ϕ₀)) * (-1)^j + m.Δc




### generic tight-binding potential:
"Calculate potential for model `m` on site `j` at time `t`."
potential(m::Model1d, j, t) = 0
potential(m::RmPbc, j, t) = Δ(m, t) / 2 * (-1)^j
potential(m::IntRmPbc, j, t) = Δ(m, t) / 2 * (-1)^j
function potential(m::RmSpinPbc, j, t)
    if j <= m.L
        return Δ(m, t) / 2 * (-1)^j
    else
        return Δ(m, t) / 2 * (-1)^(j + 1)
    end
end
function potential(m::IntRmSpinPbc, j, t)
    if j <= m.L
        return Δ(m, t) / 2 * (-1)^j
    else
        return Δ(m, t) / 2 * (-1)^(j + 1)
    end
end
function potential(m::IntRmSpinObc, j, t)
    if j <= m.L
        return Δ(m, t) / 2 * (-1)^j
    else
        return Δ(m, t) / 2 * (-1)^(j + 1)
    end
end
function potential(m::RmSpinObc, j, t)
    if j <= m.L
        return Δ(m, t) / 2 * (-1)^j
    else
        return Δ(m, t) / 2 * (-1)^(j + 1)
    end
end
function potential(m::RmSpinObcStark, j, t)
    if j <= m.L
        return Δ(m, t) / 2 * (-1)^j
    else
        return Δ(m, t) / 2 * (-1)^(j + 1)
    end
end
function potential(m::RmSpinObcRes, j, t)
    if j <= m.L + m.Lres
        return Δ(m, t) / 2 * (-1)^j
    else
        return Δ(m, t) / 2 * (-1)^(j + 1)
    end
end
potential(m::RmPbcInterface, j, t) = Δ(m, t) / 2 * (-1)^j
potential(m::RmPbcDiagDis, j, t) = Δ(m, t) / 2 * (-1)^j + v(m, j)
potential(m::RmPbcDiagDisInclusion, j, t) = Δ(m, t) / 2 * (-1)^j + v(m, j)
potential(m::AA, j, t) = Δ(m, t) / 2 * (-1)^j + v(m, j)
potential(m::RmPbcDiagDisTwist, j, t) = Δ(m, t) / 2 * (-1)^j + v(m, j)
potential(m::RmObc, j, t) = v(m, j) + Δ(m, t) / 2 * (-1)^j
potential(m::RmSpinObcRot, j, t) = v(m, j) + Δ(m, t) / 2 * (-1)^j

potential(m::IonicHubbard, j, t) = m.Δ0 * (-1)^j + Δ(m, t) * (-1)^j
potential(m::IonicHubbard2, j) = m.Δ * (-1)^j
potential(m::IonicHubbard2Pbc, j) = m.Δ * (-1)^j
potential(m::IonicHubbard2PbcToy, j) = m.Δ * (-1)^j + m.f * σ_z(m,j) * (-1)^j + m.γ * m.randpots[j]
potential(m::IonicHubbardPbc, j, t) = Δ(m, t) * (-1)^j 
potential(m::IonicHubbardPbcToy, j, t) = Δ(m, t) * (-1)^j + m.f * σ_z(m,j) * (-1)^j + m.γ * m.randpots[j] ####



#ramped up pumping speed:

"smoothly interpolates between 0 and 1."
function smoothstep(t)
    if t < 0
        return 0
    elseif t <= 1
        return 3t^2 - 2t^3
    else
        return 1
    end
end

"smoothly interpolates between 0 and 1. Both first and second derivative at edges are 0."
function smootherstep(t)
    if t < 0
        return 0
    elseif t <= 1
        return 6t^5 - 15t^4 + 10t^3
    else
        return 1
    end
end

α(m, t) = smootherstep(t / m.T) * t
β(m, t) = m.T * smootherstep(t / m.T)
transient(m,t,t_trans) = t < 0 ? -1/(2*t_trans)*t^2+t : t


### δΔ-plane:
"Calculate hopping dimerization."
δ(m::RmPbc, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmSpinPbc, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::IntRmPbc, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::IntRmSpinPbc, t) = m.Rδ * cospi(2 * transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::IntRmSpinObc, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmSpinObc, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmSpinObcStark, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmSpinObcRot, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)

δ(m::RmSpinObcRes, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmPbcInterface, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmPbcDiagDis, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmPbcDiagDisInclusion, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::AA, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmPbcDiagDisTwist, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::RmObc, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)

δ(m::IonicHubbard, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
δ(m::IonicHubbardPbc, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
# δ(m::IonicHubbardPbcToy, t) = m.Rδ * squircle(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)[1]
δ(m::IonicHubbardPbcToy, t) = m.Rδ * cospi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)





"Calculate staggering amplitude."
Δ(m::RmPbc, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmSpinPbc, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::IntRmPbc, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::IntRmSpinPbc, t) = m.RΔ * sinpi(2 *transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::IntRmSpinObc, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmSpinObcStark, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmSpinPbc, t, extrapot) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀) + extrapot
Δ(m::RmSpinObc, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmSpinObcRot, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmSpinObcRes, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmPbcInterface, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmPbcDiagDis, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmPbcDiagDisInclusion, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::AA, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmPbcDiagDisTwist, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::RmObc, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::IonicHubbard, t) = m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
Δ(m::IonicHubbardPbc, t) = m.Δc + m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)
# Δ(m::IonicHubbardPbcToy, t) = m.Δc + m.RΔ * squircle(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)[2]
Δ(m::IonicHubbardPbcToy, t) = m.Δc + m.RΔ * sinpi(2*transient(m,t,-m.T/16) / m.T + m.ϕ₀)

# Δ(m::IonicHubbardPbc, t, T) = m.Δc + m.RΔ * sinpi(2*t/ T + m.ϕ₀)







"Calculate external on-site potential."
v(m::RmObc, j) = m.V / 2.0 * (j - fld(m.L, 2.0))^2
v(m::RmSpinObcRot, j) = 0.0
v(m::RmPbcDiagDis, j) = m.w * rand() - m.w / 2
v(m::RmPbcDiagDisInclusion, j) = j > m.Linc ? m.w2 * (m.rands[j] .- 1 / 2) : 0
v(m::AA, j) = 2 * m.λ * cospi(2 * (j - 1) * round(m.α * m.L) / m.L + m.φ₀) # best approximation to golden ratio.
v(m::RmPbcDiagDisTwist, j) = m.w * rand() - m.w / 2


spinorbitcoupling(m::RmSpinPbc) = 1im * σvec([m.Vso, 0, 0])[1, 2]
spinorbitcoupling(m::RmSpinObc) = 1im * σvec([m.Vso, 0, 0])[1, 2]
spinorbitcoupling(m::RmSpinObcStark) = 1im * σvec([m.Vso, 0, 0])[1, 2]

spinorbitcoupling(m::RmSpinObcRot) = 1im * σvec([m.Vso, 0, 0])[1, 2]

spinorbitcoupling(m::IntRmSpinPbc) = 1im * σvec([m.Vso, 0, 0])[1, 2]
spinorbitcoupling(m::IntRmSpinObc) = 1im * σvec([m.Vso, 0, 0])[1, 2]

function spinorbitcoupling(m::IntRmSpinPbc, i, j, s::Array{Int64,1})
    (ii, jj) = i > j ? (j, i) : (i, j)
    return spinorbitcoupling(m) * (-1)^(sum(s[ii+1:jj-1]))
end

function spinorbitcoupling(m::IntRmSpinObc, i, j, s::Array{Int64,1})
    (ii, jj) = i > j ? (j, i) : (i, j)
    return spinorbitcoupling(m) * (-1)^(sum(s[ii+1:jj-1]))
end

spinorbitcoupling(m::RmSpinObcRes) = 1im * σvec([m.Vso, 0, 0])[1, 2]

interaction(m::IntModel) = m.U
interaction(m::IonicHubbard) = m.U



### general operators:
σx = Hermitian([0 1; 1 0])
σy = Hermitian([0 -1im; 1im 0])
σz = Hermitian([1 0; 0 -1])
σvec(v) = v[1] * σx + v[2] * σy + v[3] * σz

TR(spinor) = -σy * conj(spinor)
TR(Ψ, m::Model1d) = conj([-Ψ[m.L+1:end]; Ψ[1:m.L]])