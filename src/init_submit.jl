import DrWatson.savename
using Base.Iterators
include(srcdir("Model.jl"))
include(srcdir("Prop.jl"))

# foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.

function hmss(dt) #takes dt in seconds.
    (h, r) = divrem(dt, 60 * 60)
    (m, r) = divrem(r, 60)
    # (s,r) = divrem(r, 60)
    string(Int(h), ":", Int(m), ":", r)
end

function walltime(dt) #takes dt in seconds. Returns walltime string.
    (h, r) = divrem(dt, 60 * 60)
    (m, r) = divrem(r, 60)
    # (s,r) = divrem(r, 60)
    string(lpad("$(Int(h))", 3, "0"), ":", lpad("$(Int(m))", 2, "0"), ":", lpad("$r", 2, "0"))
end

"""
Takes dictionary containing model parameters, model itself and propagator
and returns respective model with parameters chosen accordingly.
"""
function dict2model(dict::Dict)
    return dict[:m](; dict...)
end

function dict2model(dict::Dict; kwargs...)
    return dict[:m](; dict..., kwargs...)
end


"""
Takes dict instance and creates a result dictionary that is compatible with
DrWatson.savename, filling in all default values for all parameters of the model and propagation.
"""
function resdict(d::Dict; ignores=[:filename], pops=[:filename])
    D = deepcopy(d)# result dict
    for p in pops
        try
            pop!(D, p)
        catch
        end
    end
    m  = dict2model(D)
    DD =  resdict(m)
    DDD = merge(D, DD)
    DDD[:filename] = savename(DDD, ignores=ignores)
    for p in pops
        pop!(DDD, p)
    end
    return DDD
end

function resdict(m::Model)
    D = struct2dict(m) # result dict
    DD = Dict{Symbol,Any}(:m => typeof(m))
    DD[:filename] = savename(merge(D, DD))
    return merge(D, DD)
end

function syncall()
    synccmd = `rsync -vax --exclude=".git/*" --exclude="_research/tmp" eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/ $(projectdir())/`
    run(synccmd)
end

function syncdata()
    synccmd = `rsync -vax --exclude=".git/*" --exclude="_research/*" eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/data/ $(datadir())`
    run(synccmd)
end

function pull_cluster()
     success(Cmd(`ssh eric.bertok@rocks cd mps-charge-pump \&\& git pull`))
end

function rocks(c::Cmd)
    run(Cmd(`ssh eric.bertok@rocks cd mps-charge-pump \&\& $c`))
end

function pushoutfile(path)
    mkpath((projectdir("_research","io",path)))
    synccmd = `rsync -vax $(projectdir("_research","tmp"))/ eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/_research/tmp/`
    run(synccmd)
end

function generate_outfile(runs)
    outfile = projectdir("_research", "tmp", "runs_$(runs[1]).txt")
    open(outfile, "w") do f
        for i in runs
            println(f, i)
        end
    end
    strings = splitpath(outfile)[end-2:end]
    return strings[1] * "/" * strings[2] * "/" * strings[3]
end

# function generate_jdf()
#     jdf = "
#     executable = /usr/bin/julia
#     arguments = \"--project $(dirname(PROGRAM_FILE))/runjob.jl \$(runfile) \"
#     error = _research/io/$(name)/\$(Cluster).\$(Process).err
#     log = _research/io/$(name)/\$(Cluster).log
#     output = _research/io/$(name)/\$(Cluster).\$(Process).out
#     request_memory = 10 GB
#     max_idle = 100

#     queue runfile from $outfile
#     "
# end

function generate_jdf(path, outfile, maxtime)
    jdf = "
    executable = /opt/julia/bin/julia
    arguments = \"--project scripts/$(path)/runjob.jl \$(runfile) \"
    error = _research/io/$(path)/\$(Cluster).\$(Process).err
    log = _research/io/$(path)/\$(Cluster).log
    output = _research/io/$(path)/\$(Cluster).\$(Process).out
    request_memory = 4 GB
    max_idle = 100
    +MaxRuntime = $maxtime

    queue runfile from $outfile
    "

    jdffile = projectdir("$(outfile)_jdf")
    open(jdffile, "w") do f
        println(f, jdf)
    end

    strings = splitpath(jdffile)[end-2:end]
    return strings[1] * "/" * strings[2] * "/" * strings[3]
end

function submit_jobs(path, dicts, maxtime)
    runs = tmpsave(dicts)
    println(runs)
    outfile = generate_outfile(runs)
    jdf = generate_jdf(path, outfile, maxtime)
    pushoutfile(path)
    pull_cluster()
    mkpath((projectdir("_research","io",path)))

    
    run(Cmd(`ssh eric.bertok@rocks cd mps-charge-pump source /etc/profile \&\& cd mps-charge-pump \&\& mkdir -p _research/io/$(path) \&\&condor_submit $jdf`))
end