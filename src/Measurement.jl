# Observable type, wrapping all result arrays and measurement functions.

# rewritten in dataframes!

using JLD2, FileIO, KrylovKit

"Data_Set with propagation information and all measurements."

abstract type AbstractObservable end

mutable struct Observable <: AbstractObservable
    P::Propagation # has model prop dt t Psi
    step::Int
    every::Int
    en::Array{Float64} # energy expectation ✓
    Pol::Array{Float64} # polarization ✓          
    LCM::Array{Float64} # Local Chern Marker ✓
    ent::Array{Union{Float64,Missing},2} # Entanglement spectrum ✓
    entimbal::Array{Union{Float64,Missing},2} # particle imbalances of entspec ✓
    J::Array{ComplexF64} # current ✓
    Q::Array{ComplexF64} # pumped charge ✓
    Qtmp::ComplexF64
    com::Array{Float64} # center of mass ✓
    IPR::Array{Float64,2} # inverse participation ratio ✓
    t::Array{Float64} # timerange
end

Observable(P::Propagation; maxlam=6, every=1) = Observable(P, 1, every, 
zeros(Float64, length(timerange(P, every=every))), # en
zeros(ComplexF64, length(timerange(P, every=every))), # Pol
zeros(Float64, P.m.L), # LCM
zeros(Float64, length(timerange(P, every=every)), 2^maxlam), # ent
zeros(Float64, length(timerange(P, every=every)), 2^maxlam), # entimbal
zeros(ComplexF64, length(timerange(P, every=every))), # J
zeros(ComplexF64, length(timerange(P, every=every))), # Q
0, # Qtm
zeros(Float64, length(timerange(P, every=every))),  # com
zeros(Float64, length(timerange(P, every=every)), P.occ), 
zeros(length(timerange(P, every=every)))) # IPR




mutable struct ObservableShort <: AbstractObservable
    P::Propagation # has model prop dt t Psi
    step::Int
    every::Int
    spec::Array{Float64,2} # energy spectrum ✓
    states::Array{ComplexF64,3}
    Kernel::Array{Float64,2}
    LCM::Array{Float64} # Local Chern Marker ✓
    J::Array{ComplexF64} # current ✓
    Q::Array{ComplexF64} # pumped charge ✓
    Qtmp::ComplexF64
    t::Array{Float64} # timerange
end

ObservableShort(P::Propagation; maxlam=6, every=1) = ObservableShort(P, 1, every, 
zeros(Float64, length(timerange(P, every=every)),P.m.L), # spec
zeros(Float64, length(timerange(P, every=every)),P.m.L,P.m.L), # states
zeros(Float64, length(timerange(P, every=every)),P.m.L), # Kernel
zeros(Float64, P.m.L), # LCM
zeros(ComplexF64, length(timerange(P, every=every))), # J
zeros(ComplexF64, length(timerange(P, every=every))), # Q
0, # Qtm
zeros(length(timerange(P, every=every)))) # t


mutable struct ObservableSpecLcm <: AbstractObservable
    P::Propagation # has model prop dt t Psi
    step::Int
    every::Int
    spec::Array{Float64,2} # energy spectrum ✓
    LCM::Array{Float64} # Local Chern Marker ✓
    t::Array{Float64} # timerange
end

ObservableSpecLcm(P::Propagation; maxlam=6, every=1) = ObservableSpecLcm(P, 1, every, 
zeros(Float64, length(timerange(P, every=every)),P.m.L), # spec
zeros(Float64, P.m.L), # LCM
zeros(length(timerange(P, every=every)))) # t


mutable struct ObservableQsOnly <: AbstractObservable
    P::Propagation # has model prop dt t Psi
    step::Int
    every::Int
    J::Array{ComplexF64} # current ✓
    Q::Array{ComplexF64} # pumped charge ✓
    Qtmp::ComplexF64
    t::Array{Float64} # timerange
end

ObservableQsOnly(P::Propagation; maxlam=6, every=1) = ObservableQsOnly(P, 1, every, 
zeros(ComplexF64, length(timerange(P, every=every))), # J
zeros(ComplexF64, length(timerange(P, every=every))), # Q
0, # Qtm
zeros(length(timerange(P, every=every))))

mutable struct ObservableLCM <: AbstractObservable
    P::Propagation # has model prop dt t Psi
    step::Int
    every::Int
    LCM::Array{Float64} # Local Chern Marker ✓
    t::Array{Float64} # timerange
end

ObservableLCM(P::Propagation; maxlam=6, every=1) = ObservableLCM(P, 1, every, 
zeros(Float64, P.m.L), # LCM
zeros(length(timerange(P, every=every))))


function proploop!(O::ObservableQsOnly; verbose=false)
     
    @showprogress for (i, t) in enumerate(timerange(O)[1:end - 1])
        measure!(O)
        prop!(O)
    end 
    propfinish!(O)
    measure!(O, every=false)
end

function proploop!(O::AbstractObservable; verbose=false)
     
    for (i, t) in enumerate(timerange(O)[1:end - 1])
        measure!(O)
        prop!(O)
    end 
    propfinish!(O)
    measure!(O, every=false)
    O.LCM -= LCM(O.P, O.P.occ, 0) # fix for periodic bc
end

"Measure observables and write them in the AbstractObservable-instance."
function measure!(O::Observable; every=true)

    i = O.step

    if ((O.P.timestep - 1) % O.every == 0 ) || every == false
        O.step += 1
        O.t[i] = O.P.t
        O.en[i] = energy(O.P, O.P.occ)
        O.ent[i,:], O.entimbal[i,:] = entspec(O.P, floor(Int, 1 / 4 * O.P.m.L) + 1:floor(Int, 3 / 4 * O.P.m.L), O.P.occ)
        O.Pol[i] = polarization(O.P, O.P.occ)
        O.LCM += LCM(O.P, O.P.occ, 0)
        O.J[i] = curr(O.P, O.P.occ)     
        O.Q[i] = O.Qtmp + O.J[i] * O.P.dt / O.P.occ
        O.Qtmp = O.Q[i]
        O.com[i] = com(O.P, O.P.occ)
        O.IPR[i,:] = ipr(O.P, O.P.occ)
    else
        J = curr(O.P, O.P.occ)  
        O.Qtmp = O.Qtmp + J * O.P.dt / O.P.occ
        O.LCM += LCM(O.P, O.P.occ, 0)

    end

    end

function measure!(O::ObservableShort; every=true)

    i = O.step

    if ((O.P.timestep - 1) % O.every == 0 ) || every == false
        O.step += 1
        O.t[i] = O.P.t
        O.spec[i,:], O.states[i,:,:] = eigen(hamiltonian(O.P.m, O.P.t))
        tmp = LCM(O.P, O.P.occ, 0)
        O.Kernel[i,:] = tmp
        O.LCM += tmp
        O.J[i] = curr(O.P, O.P.occ)     
        O.Q[i] = O.Qtmp + O.J[i] * O.P.dt / O.P.occ
        O.Qtmp = O.Q[i]
    else
        J = curr(O.P, O.P.occ)  
        O.Qtmp = O.Qtmp + J * O.P.dt / O.P.occ
        tmp = LCM(O.P, O.P.occ, 0)
        O.Kernel[i,:] = tmp
        O.LCM += tmp
    end

    end


function measure!(O::ObservableSpecLcm; every=true)

    i = O.step

    if ((O.P.timestep - 1) % O.every == 0 ) || every == false
        O.step += 1
        O.t[i] = O.P.t
        O.spec[i,:] = eigen(hamiltonian(O.P.m, O.P.t)).values
        O.LCM += LCM(O.P, O.P.occ, 0)
    else
        O.LCM += LCM(O.P, O.P.occ, 0)
    end

    end

function measure!(O::ObservableLCM; every=true)

    i = O.step

    if ((O.P.timestep - 1) % O.every == 0 ) || every == false
        O.step += 1
        O.t[i] = O.P.t
        O.LCM += LCM2(O.P, O.P.occ, 0.)
    else
        O.LCM += LCM2(O.P, O.P.occ, 0.)
    end

    end


function measure!(O::ObservableQsOnly; every=true)

    i = O.step

    if ((O.P.timestep - 1) % O.every == 0 ) || every == false
        O.step += 1
        O.t[i] = O.P.t
        O.J[i] = curr(O.P, O.P.occ)     
        O.Q[i] = O.Qtmp + O.J[i] * O.P.dt / O.P.occ
        O.Qtmp = O.Q[i]
    else
        J = curr(O.P, O.P.occ)  
        O.Qtmp = O.Qtmp + J * O.P.dt / O.P.occ
    end

    end

timerange(O::AbstractObservable) = timerange(O.P)

prop!(O::AbstractObservable) = prop!(O.P)
function propfinish!(O::AbstractObservable) 
    propfinish!(O.P)
end


"Site-representation of total current operator"
function Ĵ(t, m::Model1dPbc)
    dl = [hopping(m, j, t) for j in 1:m.L - 1]
    d = zeros(m.L)
    A = -1im * Tridiagonal(dl, d, -dl)  
    B = zeros(ComplexF64, m.L, m.L)
    B[1,m.L] =  -1im * hopping(m, m.L, t)
    return Hermitian(A + B)
end

function Ĵ(t, m::Model1dSpinPbc)
    dl = [[hopping(m, j, t) for j in 1:m.L - 1];0;[-hopping(m, j, t) for j in m.L + 1:2 * m.L - 1]]
    d = zeros(2 * m.L)
    A = -1im * Tridiagonal(dl, d, -dl)  
    B = zeros(ComplexF64, 2 * m.L, 2 * m.L)
    B[1,m.L] =  -1im * hopping(m, m.L, t)
    B[m.L + 1,2 * m.L] =  1im * hopping(m, m.L, t)   
    return Hermitian(A + B)
end

function Ĵ(t, m::Model1dSpinObc)
    dl = [[hopping(m, j, t) for j in 1:m.L - 1];0;[-hopping(m, j, t) for j in m.L + 1:2 * m.L - 1]]
    d = zeros(2 * m.L)
    A = -1im * Tridiagonal(dl, d, -dl)   
    return Hermitian(A)
end

function Ĵ(t, m::Model1dObc)
    dl = [hopping(m, j, t) for j in 1:m.L - 1]
    d = zeros(m.L)
    A = -1im * Tridiagonal(dl, d, -dl) 
end


"Calculate total current of slater determinant"
function curr(P::Propagation, kmax)
    res::ComplexF64 = 0.
    for k in 1:kmax
        res += P.Ψ[:,k]' * (Ĵ(P.t, P.m) * P.Ψ[:,k]) ######## Not type stable!
    end
    res
end

function curr_singlelink(P::Propagation,j, kmax)
    res::ComplexF64 = 0.
    for k in 1:kmax
        res += P.Ψ[j:j+1,k]' * (Ĵ(P.t, P.m)[j:j+1,j:j+1] * P.Ψ[j:j+1,k]) ######## Not type stable!
    end
    res
end

function curr_singlelink_variance(P::Propagation,j, kmax)
    res::ComplexF64 = 0.
    for k in 1:kmax
        res +=  (P.Ψ[j:j+1,k]' * (Ĵ(P.t, P.m)[j:j+1,j:j+1]' * Ĵ(P.t, P.m)[j:j+1,j:j+1] * P.Ψ[j:j+1,k]))  ######## Not type stable!
    end
    res
end


"Calculate total current of slater determinant"
function curr(P::Propagation)
    res::ComplexF64 = 0.
    for k in 1:P.occ
        res += P.Ψ[:,k]' * (Ĵ(P.t, P.m) * P.Ψ[:,k]) ######## Not type stable!
    end
    res
end

"Calculate the center-of-mass of a slater determinant"
com(P::Propagation, kmax) = (sum(abs2.(P.Ψ[:,1:kmax]), dims=2)[:,1] / length(1:kmax)) ⋅ collect(1:P.m.L)
comsingle(Ψ) = abs2.(Ψ) ⋅ collect(1:size(Ψ, 1))
com(m::RmSpinObc, Ψ, kmax) = (sum(abs2.(Ψ[:,1:kmax]), dims=2)[:,1] / length(1:kmax)) ⋅ [collect(1:m.L);collect(1:m.L)]
com_spin(m::RmSpinObc, Ψ, kmax) = (sum(abs2.(Ψ[:,1:kmax]), dims=2)[:,1] / length(1:kmax)) ⋅ [collect(1:m.L);-collect(1:m.L)]
com_spin(P::Propagation, kmax) = (sum(abs2.(P.Ψ[:,1:kmax]), dims=2)[:,1] / length(1:kmax)) ⋅ [collect(1:m.L);-collect(1:m.L)]


"Calculate energy expectaton value of slater detemrinant"
function energy(P::Propagation, kmax)
    res::ComplexF64 = 0.
    for k in 1:kmax
        res += P.Ψ[:,k]' * (hamiltonian(P.m, P.t)) * P.Ψ[:,k]
    end
    real.(res)
end

"Calculate inverse participation ratio"
ipr(P::Propagation, kmax) = [sum(j -> abs2.(P.Ψ[j,k]).^2, 1:P.m.L) for k in 1:kmax]

ipr(Ψ::Array{Complex{Float64},2}, kmax) = [sum(j -> abs2.(Ψ[j,k]).^2, 1:size(Ψ, 1)) for k in 1:kmax]



"Calculate correlator of slater determinant."
corr(P::Propagation, kmax) = (P.Ψ[:,1:kmax]  * P.Ψ[:,1:kmax]')

"Calculate correlation matrix for subsystem A ⊂ [1:L]"
corrmat(P::Propagation, A, kmax) = corr(P, kmax)[A,A]

"Calculate eigendecomposition of correlation matrix of subsystem A ⊂ [1:L]."
function corrmat_eigen(P::Propagation, A, kmax)
    λ, U = eigen(corrmat(P, A, kmax))
    return λ, U
end

"Calculate entanglement spectrum"
function entspec(P::Propagation, A, kmax; cutoff=12, maxlam=6)
    λ, U = corrmat_eigen(P, A, kmax) # calculate reduced density matrix of A.

    # cut system: (-B-|--A--|-B-), take only states on left edge B|A :
    coms = [comsingle(U[:,i]) for i in eachindex(A)]  
    
    isleft = coms .<= length(A) / 2 # left in subsystem A \checked.

    # println(sum(isleft))   

    λ, U, coms = λ[isleft], U[:,isleft], coms[isleft] # otherwise winding up and down and result halfed.

    # take first *maxlam eigenvalues of rho centered around 1/2 (half inside B, half inside A).
    perm = sortperm(λ, by=x -> (x - 0.5)^2) 
    λ = λ[perm][1:maxlam]
    U = U[:,perm][:,1:maxlam]
    coms = coms[perm][1:maxlam]

    sides = [  λs > 1 / 2 ? 1 : -1 for λs in λ ] # left states get label -1 and are transformed to 1-λ.
    λ = [  λs > 1 / 2 ? λs : 1 - λs  for λs in λ ] # left states get label -1 and are transformed to 1-λ.

    specs = []
    imbalance = []
    for i in 0:2^length(λ) - 1
        mask = BitArray(digits(i, base=2, pad=length(λ)))
        try         
            push!(specs, sum(-log.(λ[mask])) + sum(-log.(1 .- λ[.!mask])))
            push!(imbalance, sum(sides[mask]) - sum(sides[.!mask]))
        catch
            push!(specs, missing)
            push!(imbalance, missing)
        end
    end
    
    
        s = sortperm(specs)
    # filter!(x -> -log(1-x) < cutoff, λ)
    # filter!(x -> -log(x) < cutoff, λ)
    # return [specs[s]...,[missing for l in 1:2^maxlam-length(specs)]...], [imbalance[s]...,[missing for l in 1:2^maxlam-length(imbalance)]...]
    return specs[s], imbalance[s]

    end

"Position operator"

posmat(P::Propagation) = posmat(P, P.m)
posmat(P::Propagation, m::Model) = Diagonal([j * I[j,l] for j in 1:P.m.L ,l in 1:P.m.L]) # might want to improve this.
function posmat(P::Propagation, m::Model1dSpinPbc)
    d = [1:m.L;1:m.L]
    Diagonal(d)
end
function posmat(m::Model1dSpinPbc)
    d = [1:m.L;1:m.L]
    Diagonal(d)
end
function posmat(m::Model1dSpinObc)
    d = [1:m.L;1:m.L]
    Diagonal(d)
end

function spinmat(m::Model1dSpinPbc)
    d = [1:m.L;-(1:m.L)]
    Diagonal(d)
end



"Exponentiated position operator"
exppos(P::Propagation) = exp(1im * 2pi / P.m.L * posmat(P)) # sign error?
exppos(m::Model) = exp(1im * 2pi / m.L * posmat(m)) # sign error?
expspin(m::RmSpinPbc) = exp(1im * 2pi / m.L * spinmat(m)) # sign error?



"Calculate the polatization"
function polarization(P::Propagation, kmax)
    Ψ = eigen(hamiltonian(P.m, P.t)).vectors[:,1:kmax]  # UNITARY IF KMAX = L
    return imag.(log(det(Ψ' * exppos(P) * Ψ))) # sign error?
end

function polarization(Ψ, P, kmax)
    return imag.(log(det(Ψ[:,1:kmax]' * exppos(P) * Ψ[:,1:kmax])))
end

function polarization(Ψ, m, kmax)
    return imag.(log(det(Ψ[:,1:kmax]' * exppos(m) * Ψ[:,1:kmax])))
end

function polarization(m::RmSpinPbc, t)
    λ, U =  eigen(projspin(m, t))
    U1 = U[:,1:fld(m.L, 2)]
    U2 = U[:,3 * fld(m.L, 2) + 1:end]

    return imag.(log(det(U1' * exppos(m) * U1))), imag.(log(det(U2' * exppos(m) * U2)))
end

function polarization_nosplit(m::RmSpinPbc, t)
    λ, U =  eigen(hamiltonian(m,t))

    U = U[:,1:m.L]

    return 1/2π*imag.(log(det(U' * expspin(m) * U)))
end


function polarization(m::RmSpinObc, t, kmax, spinfactor, a)
    λ, U =  eigen(projspin(m, t, spinfactor, a))
    U1 = U[:,1:fld(m.L, 2)]
    U2 = U[:,3 * fld(m.L, 2) + 1:end]

    return com(m, U1, fld(m.L, 2)), com(m, U2, fld(m.L, 2))
end

function localdensity(m::Model1dSpinPbc, t)
    λ, U =  eigen(hamiltonian(m,t))
    Ψ = sum(abs2.(U[:,1:m.L]), dims =2)
    return Ψ
end

function localdensity(m::Model1dSpinObc, t)
    λ, U =  eigen(hamiltonian(m,t))
    Ψ = sum(abs2.(U[:,1:m.L]), dims =2)
    return Ψ
end

"Calculate the instantaneous local density of a noninteracting model `m` at time `t`."
function localdensity(m::Model1d, t)
    λ, U =  eigen(hamiltonian(m,t))
    Ψ = sum(abs2.(U[:,1:m.L]), dims =2)
    return Ψ
end

"Calculate the local density of a noninteracting model `m` for wavefunction `Ψ` with number of occupied states `occ`."
function localdensity(m::Model1d, Ψ, occ)
    n = sum(abs2.(Ψ[:,1:occ]), dims =2)
    return n
end


function localdensity(P::Propagation,m::Model1dSpinPbc, t)
    Ψ = sum(abs2.(P.Ψ[:,1:P.occ]), dims =2)
    return Ψ
end

function localdensity(P::Propagation,m::Model1dSpinObc, t, occ)
    Ψ = sum(abs2.(P.Ψ[:,1:occ]), dims =2)
    return Ψ
end

"Calculate the Local Chern Marker"
function LCM(P::Propagation, kmax, t0)
    Ψ = eigen(hamiltonian(P.m, P.t + t0)).vectors[:,1:kmax]
    P̂ = Ψ * Ψ'
    Q̂ = I - P̂
    Ψ2 = eigen(hamiltonian(P.m, P.t + P.dt + t0)).vectors[:,1:kmax]
    P̂2 = Ψ2 * Ψ2'
    return imag.(diag(exppos(P)' * (P̂ * (exppos(P) * ( Q̂ * (P̂2 * P̂) ) ) )) .* P.m.L ./ π) # last P not needed.
end

function LCM_spin(m::Model, proj1, proj2)
P̂ = proj1
    Q̂ = I - P̂
    P̂2 = proj2
    return imag.(diag(exppos(m)' * (P̂ * (exppos(m) * ( Q̂ * (P̂2 * P̂) ) ) )) .* m.L ./ (2 * π)) # last P not needed.
end

"return Chern number of model calculated from LCM."
function C_LCM(m::Model, kmax, dt)
    P = Propagation(p=Inst(), m=m, dt=dt, ϕf=1) # wraps propagation time, step, final time etc.
    c = 0.
    for (i, t) in enumerate(timerange(P)[1:end - 1]) # timerange: 0.0:0.01:1.0, dt=0.01
        # pump parameters: δ(m::AA, t) = m.Rδ * cospi(2t / m.T +  m.ϕ₀)
        Ψ = eigen(hamiltonian(P.m, P.t)).vectors[:,1:kmax] # kmax lowest eigenvectors at t. kmax = L/2
        P̂ = Ψ * Ψ' # projector onto occupied subspace at t
        Q̂ = I - P̂ # projector onto unoccupied subspace at t
        Ψ2 = eigen(hamiltonian(P.m, P.t + P.dt)).vectors[:,1:kmax] # kmax lowest eigenvectors at t+dt.
        P̂2 = Ψ2 * Ψ2' # projector onto occupied subspace at t+dt
    # println(P.t)
        c += sum(imag.(diag(exppos(P)' * (P̂ * (exppos(P) * ( Q̂ * (P̂2 * P̂) ) ) )) ./ π)) # add to clcm result.
        prop!(P) # "propagate" one time-step (here: instantaneous Propagation, just takes t -> t+dt )
    end
    return c
end

"return Chern kernel of model calculated from LCM."
function C_LCM_kernel(m::Model, t, kmax, dt)
        Ψ = eigen(hamiltonian(m, t)).vectors[:,1:kmax]
    Ψ = Ψ[:,1:kmax]
        P̂ = Ψ * Ψ'
        Q̂ = I - P̂
        Ψ2 = eigen(hamiltonian(m, t + dt)).vectors[:,1:kmax]
        P̂2 = Ψ2 * Ψ2'
        return imag.(diag(exppos(P)' * (P̂ * (exppos(P) * ( Q̂ * (P̂2 * P̂) ) ) )) ./ π)
end


"kato chern marker."
function M1(P::Propagation, j)
Ψ = eigen(hamiltonian(P.m, P.t)).vectors[:,1:P.occ]
    # Ψ = P.Ψ[:,1:P.occ]
        P̂ = Hermitian(Ψ * Ψ')
        return real.((P.Ψ' * P̂' * posmat(P) * P̂ * P.Ψ)[j,j] + (P.Ψ' * P̂' * (posmat(P) .- 1) * P̂ * P.Ψ)[j + 1,j + 1]) ./ 2
end

function M1(P::Propagation)
    Ψ = eigen(hamiltonian(P.m, P.t)).vectors[:,1:P.occ]
    # Ψ = P.Ψ[:,1:P.occ]
    P̂ = Hermitian(Ψ * Ψ')
    return diag(real.((P.Ψ' * P̂' * posmat(P) * P̂ * P.Ψ)[1:2:P.m.L,1:2:P.m.L] + (P.Ψ' * P̂' * (posmat(P) .- 1) * P̂ * P.Ψ)[2:2:P.m.L,2:2:P.m.L])) ./ 2
end

function M1_full(P_old, j)
    P = deepcopy(P_old)
    P.t = 0
    ts = timerange(P)
    P.Ψ = complex([i == j ? 1 : 0 for i in 1:P.m.L, j in 1:P.m.L]) # starting state = unity matrix for M1. 
    lcms = similar(ts)
    @showprogress  for (i, t) in enumerate(ts)
        lcms[i] = M1(P, j) # lcm for site j, j+1
        prop!(P)
    end 
    return lcms
end

function M1_full(P_old) # over all unit cells.
    P = deepcopy(P_old)
    P.t = 0
    ts = timerange(P)
    P.Ψ = complex([i == j ? 1 : 0 for i in 1:P.m.L, j in 1:P.m.L]) # starting state = unity matrix for M1. 
    lcms = zeros(length(ts), Int(P.m.L // 2))
    @showprogress  for (i, t) in enumerate(ts)
        lcms[i,:] = M1(P) # lcm for each unit cell.
        prop!(P)
    end 
    return lcms
end

function FloquetOp(P_old::Propagation)
    P = deepcopy(P_old)
    P.t = 0
    P.Ψ = complex([i == j ? 1 : 0 for i in 1:P.m.L, j in 1:P.m.L]) # starting state = unity matrix for M1. 
    @showprogress 10 "Computing Floquet Operator ... " for t in timerange(P)
        prop!(P)
    end  
    return P.Ψ
end


#### mb wavefunction:

# function diagonalmeasurement(Ψ,j::Int,basis,m::Model)

#     val  =  0.
#     for (a,s) in enumerate(int2state(basis,m))
#         if s[j] == 1
#             val += 1*abs2(Ψ[a])
#         end
#     end
#     return val
# end

function localcurrent(t, Ψ, i, j::Int, basis, m::IntModel1dSpinObc) # rename to current or Local current

    val = 0
    for (a, s) in enumerate(basis)
        ss = int2state(s, m)
        if ss[i] != ss[j] == 1
            sflip = flip(m, s, i, j)
            b = findstate(sflip, basis)
            h = 1im * hopping(m, i, t) * Ψ[b]' * Ψ[a]
            val += h
        end
        if ss[i] != ss[j] == 0
            sflip = flip(m, s, i, j)
            b = findstate(sflip, basis)
            h = -1im * hopping(m, i, t) * Ψ[b]' * Ψ[a]
            val += h
        end
    end
    return val
end

function localcurrent(t, Ψ, i, j::Int, basis, m::IntModel) # rename to current or Local current

    val = 0
    for (a, s) in enumerate(basis)
        ss = int2state(s, m)
        if ss[i] != ss[j] == 1
            sflip = flip(m, s, i, j)
            b = findstate(sflip, basis)
            h = 1im * hopping(m, i, j, ss, t) * Ψ[b]' * Ψ[a] 
            val += h
        end
        if ss[i] != ss[j] == 0
            sflip = flip(m, s, i, j)
            b = findstate(sflip, basis)
            h = -1im * hopping(m, i, j, ss, t) * Ψ[b]' * Ψ[a] 
            val += h
        end
    end
    return val
end

function localcurrent(t, Ψ, i, j::Int, basis, m::IntRmSpinPbc) # redefine

    val = 0
    for (a, s) in enumerate(basis)
        ss = int2state(s, m)
        if ss[i] != ss[j] == 1
            sflip = flip(m, s, i, j)
            b = findstate(sflip, basis)
            h = 1im * hopping(m, i, j, ss, t) * Ψ[b]' * Ψ[a]
            val += h
        end
        if ss[i] != ss[j] == 0
            sflip = flip(m, s, i, j)
            b = findstate(sflip, basis)
            h = -1im * hopping(m, i, j, ss, t) * Ψ[b]' * Ψ[a]
            val += h
        end
    end
    return val
end

"Calculates the SDI order parameter in lanczos basis."
function sdi_order(Ψ, basis, m::IntModel1dSpinPbc) # redefine

    val = 0
    for i in 1:m.L #spinup
        j = ((i+1)-1) % m.L + 1 #pbc
        for (a, s) in enumerate(basis)
            ss = int2state(s, m)
            if ss[i] != ss[j] == 1
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                h = (-1)^j *  Ψ[b]' * Ψ[a] * fermionsign(m,i,j,ss)
                val += h
            end
            if ss[i] != ss[j] == 0
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                h = (-1)^j * Ψ[b]' * Ψ[a] * fermionsign(m,i,j,ss)
                val += h
            end
        end
    end
    for i in m.L+1:2*m.L #spindn
        j = ((i-m.L+1)-1) % m.L + 1 + m.L #pbc
        for (a, s) in enumerate(basis)
            ss = int2state(s, m)
            if ss[i] != ss[j] == 1
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                h = (-1)^j * Ψ[b]' * Ψ[a] * fermionsign(m,i,j,ss)
                val += h
            end
            if ss[i] != ss[j] == 0
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                h = (-1)^j * Ψ[b]' * Ψ[a] * fermionsign(m,i,j,ss)
                val += h
            end
        end
    end
    return real.(val)/m.L
end

"Calculates the SDI order parameter in lanczos basis."
function sdi_order(Ψ, basis, m::IntModel1dSpinObc) # redefine

    val = 0
    for i in 1:m.L-1 #spinup
        j = (i+1) #pbc
        for (a, s) in enumerate(basis)
            ss = int2state(s, m)
            if ss[i] != ss[j] == 1
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                h = (-1)^j *  Ψ[b]' * Ψ[a] * fermionsign(m,i,j)
                val += h
            end
            if ss[i] != ss[j] == 0
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                h = (-1)^j * Ψ[b]' * Ψ[a] * fermionsign(m,i,j)
                val += h
            end
        end
    end
    for i in m.L+1:2*m.L-1 #spindn
        j = (i-m.L+1) + m.L #pbc
        for (a, s) in enumerate(basis)
            ss = int2state(s, m)
            if ss[i] != ss[j] == 1
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                h = (-1)^j * Ψ[b]' * Ψ[a] * fermionsign(m,i,j)
                val += h
            end
            if ss[i] != ss[j] == 0
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                h = (-1)^j * Ψ[b]' * Ψ[a] * fermionsign(m,i,j)
                val += h
            end
        end
    end
    return real.(val)
end


function localdensity(Ψ, basis, m::IntModel1dSpin)

    val  =  zeros(2 * m.L)

    for j in 1:2 * m.L
        for (a, s) in enumerate(int2state(basis, m))
            if s[j] == 1
                val[j] += 1 * abs2(Ψ[a])
            end
        end
    end
    return val
end
            
function localdensity_up(Ψ, basis, m::IntModel1dSpin)

    val  =  zeros(m.L)

    for j in 1:m.L
        for (a, s) in enumerate(int2state(basis, m))
            if s[j] == 1
                val[j] += 1 * abs2(Ψ[a])
            end
        end
    end
    return val
end
            
function localdensity_dn(Ψ, basis, m::IntModel1dSpin)

    val  =  zeros(m.L)

    for j in 1:m.L
        for (a, s) in enumerate(int2state(basis, m))
            if s[j + m.L] == 1
                val[j] += 1 * abs2(Ψ[a])
            end
        end
    end
    return val
end

function localdensity_a(Ψ, basis, m::BosonicSchwinger)

    val  =  zeros(m.L)
    for (a, s) in enumerate(int2state(basis, m))
        for j in 1:m.L
            if s[j] == 1
                val[j] += 1 * abs2(Ψ[a])
            end
        end
    end
    return val
end
            
function localdensity_b(Ψ, basis, m::BosonicSchwinger)

    val  =  zeros(m.L)
    for (a, s) in enumerate(int2state(basis, m))
        for j in 1:m.L
            if s[j + m.L] == 1
                val[j] += 1 * abs2(Ψ[a])
            end
        end
    end
    return val
end
            

function sparse_posmat(m::IntModel1dSpinPbc, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            if ss[i] == 1
                push!(rows_pot1, a)
                push!(cols_pot1, a) 
                push!(vals_pot1, i)
            end         
        end
        for i in m.L + 1:2 * m.L
            if ss[i] == 1
                push!(rows_pot1, a)
                push!(cols_pot1, a) 
                push!(vals_pot1, (i - m.L)) 
            end              
        end
    end
    return sparse(rows_pot1, cols_pot1, vals_pot1)
end

function sparse_posmat_spin(m::IntModel1dSpinPbc, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            if ss[i] == 1
                push!(rows_pot1, a)
                push!(cols_pot1, a) 
                push!(vals_pot1, i)
            end         
        end
        for i in m.L + 1:2 * m.L
            if ss[i] == 1
                push!(rows_pot1, a)
                push!(cols_pot1, a) 
                push!(vals_pot1, -(i - m.L)) 
            end              
        end
    end
    return sparse(rows_pot1, cols_pot1, vals_pot1)
end


function polarization(Ψ, basis, m::IntModel1dSpinPbc)

    X̂ = sparse_posmat(m, basis)

    val = dot(Ψ,exponentiate(X̂, 1im * 2π / m.L, Ψ)[1])

    return m.L / 2π * imag(log(val))

end

function polarization_spin(Ψ, basis, m::IntModel1dSpinPbc)

    X̂ = sparse_posmat_spin(m, basis)

    val = dot(Ψ,exponentiate(X̂, 1im * 2π / m.L, Ψ)[1])

    return m.L / 2π * imag(log(val))

end

function polarization(Ψ, basis, m::IntRmSpinPbc)

    X̂ = sparse_posmat_spin(m, basis)

    val = dot(Ψ,exponentiate(X̂, 1im * 2π / m.L, Ψ)[1])

    return m.L / 2π * imag(log(val))

end

function polarization(Ψ, basis, m::IntModel1dSpinObc)

    val  =  0.
    for (a, s) in enumerate(int2state(basis, m))
        tmp = 0.
        for j in 1:2 * m.L
            if s[j] == 1
                tmp += (abs2(Ψ[a]) * j) / m.L
                end
        end
        val += tmp
    end
            
    return val

end

function polarization_up(Ψ, basis, m::IntModel1dSpinObc)

    val  =  0.
    for (a, s) in enumerate(int2state(basis, m))
        tmp = 0.
        for j in 1:m.L
            if s[j] == 1
                tmp += (abs2(Ψ[a]) * j) / m.L
                end
        end
        val += tmp
    end
            
    return val

end

function polarization_dn(Ψ, basis, m::IntModel1dSpinObc)

    val  =  0.
    for (a, s) in enumerate(int2state(basis, m))
        tmp = 0.
        for j in m.L + 1:2 * m.L
            if s[j] == 1
                tmp += (abs2(Ψ[a]) * (j - m.L)) / m.L
            end
        end
        val += tmp
    end
            
    return val

end



"""
Calculates the derivative of two states adjacent in pumping parameter / time,
by fixing the gauge and calculating the differential quotient.
"""
function ∂(Ψ1,Ψ2,t2,t1)
    overlap = Ψ2⋅Ψ1
    γ = overlap/abs(overlap)
     (γ * Ψ2 - Ψ1)/(t2-t1)
end






        
#####################################



function projspin(m::RmSpinPbc, t)
    Ψ = eigen(hamiltonian(m, t)).vectors[:,1:m.L]
    P = Ψ * Ψ'
    Sz = Diagonal([ones(m.L); -ones(m.L)]) # Sz operator
    return (P * (Sz * P))
end

function projspin(m::RmSpinObc, t, spinfactor, a)
    Ψ = eigen(hamiltonian(m, t, spinfactor, a)).vectors[:,1:m.L]
    P = Ψ * Ψ'
    Sz = Diagonal([ones(m.L); -ones(m.L)]) # Sz operator
    return (P * (Sz * P))
end


comm(x,y) = x * y - y * x


function spinlcm(m::Model1dSpinPbc; dt=0.01)
    λs = []
    ens = []
    ts = collect(0:dt:1)[1:end - 1]

    Pplus = Array{Array{Complex{Float64},2},1}(undef, 2)
    Pminus = Array{Array{Complex{Float64},2},1}(undef, 2)

    C = 0
    X = posmat(m)
    en, Ψ = eigen(hamiltonian(m, 0))
    λ, U =  eigen(projspin(m, 0))
    Pminus[1] = U[:,1:fld(m.L, 2)] * U[:,1:fld(m.L, 2)]'
    Pplus[1] = U[:,3 * fld(m.L, 2) + 1:end] * U[:,3 * fld(m.L, 2) + 1:end]'
    @showprogress "calculating clcm... " for t in ts[2:end]
        en, Ψ = eigen(hamiltonian(m, t))
        λ, U =  eigen(projspin(m, t))
        Pminus[2] = U[:,1:fld(m.L, 2)] * U[:,1:fld(m.L, 2)]'
        Pplus[2] = U[:,3 * fld(m.L, 2) + 1:end] * U[:,3 * fld(m.L, 2) + 1:end]'
        push!(λs, real.([λ[1:fld(m.L, 2)];λ[3 * fld(m.L, 2) + 1:end]])) # ignore zero part of spectrum - this is from empty conduction states. ✓
        push!(ens, en)
        C +=  sum(LCM_spin(m, Pplus[1], Pplus[2])) / m.L
        C -=  sum(LCM_spin(m, Pminus[1], Pminus[2])) / m.L
        Pminus[1] = Pminus[2]
        Pplus[1] = Pplus[2]
    end
    return C
end



function spin_proj_spectrum(m::Model1dSpinPbc; dt=0.01)
    λs = []
    ens = []
    ts = collect(0:dt:1)[1:end - 1]

    Pplus = Array{Array{Complex{Float64},2},1}(undef, 2)
    Pminus = Array{Array{Complex{Float64},2},1}(undef, 2)

    λ, U =  eigen(projspin(m, 0))
    Pminus[1] = U[:,1:fld(m.L, 2)] * U[:,1:fld(m.L, 2)]'
    Pplus[1] = U[:,3 * fld(m.L, 2) + 1:end] * U[:,3 * fld(m.L, 2) + 1:end]'
    @showprogress "calculating clcm... " for t in ts
        en, Ψ = eigen(hamiltonian(m, t))
        λ, U =  eigen(projspin(m, t))
        Pminus[2] = U[:,1:fld(m.L, 2)] * U[:,1:fld(m.L, 2)]'
        Pplus[2] = U[:,3 * fld(m.L, 2) + 1:end] * U[:,3 * fld(m.L, 2) + 1:end]'
        push!(λs, real.([λ[1:fld(m.L, 2)];λ[3 * fld(m.L, 2) + 1:end]])) # ignore zero part of spectrum - this is from empty conduction states. ✓
        push!(ens, en)
        C +=  sum(LCM_spin(m, Pplus[1], Pplus[2])) / m.L
        C -=  sum(LCM_spin(m, Pminus[1], Pminus[2])) / m.L
        Pminus[1] = Pminus[2]
        Pplus[1] = Pplus[2]
    end
    return λs, ens
end

#####################################

function spindensity_mps(psi)
    for j = 1:m.L
        orthogonalize!(psi, j)
        psidag_j = dag(prime(psi[j], "Site"))
        dnu[j] = scalar(psidag_j * op(sites, "Nup", j) * psi[j])
        dnd[j] = scalar(psidag_j * op(sites, "Ndn", j) * psi[j])
    end
    return dnu, dnd
end

function variance(H,Ψ,en)
    phi = contract(H, Ψ, cutoff=1e-12)
    return inner(phi,phi) - en^2
end