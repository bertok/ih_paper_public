using LaTeXStrings

"""
Compares the eigenvalues of the noninteracting model to
the one from the sparse matrix for N=1 without interaction.
"""
function test_sparse_eigvals(L,Vso; U=0)
    m = IntRmSpinPbc(L=L, N=1, Vso=Vso, U=U)
    states = generatefixednstates(m)
    H = Hfromtemp(m,0,sparsehamiltonian_template(m,generatefixednstates(m))...)
    en1 = eigen(Matrix(H)).values


    m2 = RmSpinPbc(L=L, N=1, Vso=Vso)
    en2 = eigen(hamiltonian(m2,0)).values

    en1 == en2, en1, en2
end

function test_sparse_eigvals_obc(L,Vso; U=0)
    m = IntRmSpinObc(L=L, N=1, Vso=Vso, U=U)
    states = generatefixednstates(m)
    H = Hfromtemp(m,0,sparsehamiltonian_template(m,generatefixednstates(m))...)
    en1 = eigen(Matrix(H)).values


    m2 = RmSpinObc(L=L, N=1, Vso=Vso)
    en2 = eigen(hamiltonian(m2,0)).values

    en1 == en2, en1, en2
end


"""
Compares the Hamiltonian matrix from sparse construction to single particle matrix.
"""
function test_sparse_matrix(L, Vso, t)
    m = IntRmSpinPbc(L = L, N = 1, Vso = Vso)
    m2 = RmSpinPbc(L = L, N = 1, Vso = Vso)
    H = Matrix(Hfromtemp(m,t,sparsehamiltonian_template(m,generatefixednstates(m))...))
    H2 = hamiltonian(m2,t)

    H == H2
end

function test_sparse_matrix_obc(L, Vso, t)
    m = IntRmSpinObc(L = L, N = 1, Vso = Vso)
    m2 = RmSpinObc(L = L, N = 1, Vso = Vso)
    H = Matrix(Hfromtemp(m,t,sparsehamiltonian_template(m,generatefixednstates(m))...))
    H2 = hamiltonian(m2,t)

    H == H2
end


"""
Compafres sparse H from template at time t with full sparse matrix at same time.
"""
function test_sparse_matrix_template(;L = 8, Vso = 10, t = 0, U = 100, N=1)
    m = IntRmSpinPbc(L = L, N = N, Vso = Vso, U = U)
    H = Hfromtemp(m,t,sparsehamiltonian_template(m,generatefixednstates(m))...)
    H2 = sparsehamiltonian_full(m,generatefixednstates(m),t)

    H == H2
end



"""
Compares the ground state energy from a lanczos iteration of the noninteracting model to
the one from the tight binding diagonalization with U=0 and the single particle values.
"""
function test_lanczos_energy(L,Vso;N=1, U=0, Λ = 30, t=0)
    p = plot(0, xlabel = L"N", ylabel = L"E_N")
  
    ens_diag = []
    ens_lanz = []
    for i in 1:N
        m = IntRmSpinPbc(L=L, N=i, Vso=Vso, U=U)
        HΛ, phis,  ens = lanczosenergies(m, Λ)
        push!(ens_lanz,ens[end])
        states = generatefixednstates(m)
        H = Hfromtemp(m,t,sparsehamiltonian_template(m,states)...)
        push!(ens_diag,eigen(Matrix(H)).values[1])
    end
    p = scatter!(ens_lanz, marker = :cross, markersize = 4, label = "Lanczos")
    p = scatter!(ens_diag,marker = :xcross, markerstrokewidth  =2, markersize = 3, label = "Diag", legend = :top)

    m2 = RmSpinPbc(L=L, N=1, Vso=Vso, U=U)
    en2 = eigen(hamiltonian(m2,t)).values
    println(en2)
    ens_sp = [sum(en2[1:i]) for i in 1:N]

    p = scatter!(ens_sp,marker = :circle, markerstrokewidth  =2, markersize = 3, label = "SingleParticle", legend = :top)


    title!(latexstring("V_{so} = $Vso, L = $L"))
    display(p)

    # return ens_lanz[end] ≈ ens_diag[2], ens[end], ens_diag, ens[end] - ens_test
end

function test_lanczos_energy_pbc(L,Vso;N=1, U=0, Λ = 30, t=0)
    p = plot(0, xlabel = L"N", ylabel = L"E_N")
  
    ens_diag = []
    ens_lanz = []
    for i in 1:N
        m = IntRmSpinPbc(L=L, N=i, Vso=Vso, U=U, T=1)
        states = generatefixednstates(m)
        template = sparsehamiltonian_template(m,states)
        HΛ, phis,  ens = lanczosenergies(m, Λ, states, template, tol = 1e-9, t = t)
        push!(ens_lanz,ens[end][1])
        H = Hfromtemp(m,t,template...)
        push!(ens_diag,eigen(Matrix(H)).values[1])
    end
    p = scatter!(ens_lanz, marker = :cross, markersize = 4, label = "Lanczos")
    p = scatter!(ens_diag,marker = :xcross, markerstrokewidth  =2, markersize = 3, label = "Diag", legend = :top)

    m2 = RmSpinPbc(L=L, N=1, Vso=Vso, U=U, T=1)
    en2 = eigen(hamiltonian(m2,t)).values
    println(en2)
    ens_sp = [sum(en2[1:i]) for i in 1:N]

    p = scatter!(ens_sp,marker = :circle, markerstrokewidth  =2, markersize = 3, label = "SingleParticle", legend = :top)


    title!(latexstring("V_{so} = $Vso, L = $L"))
    display(p)

    # return ens_lanz[end] ≈ ens_diag[2], ens[end], ens_diag, ens[end] - ens_test
end

function test_lanczos_energy_obc(L,Vso;N=1, U=0, Λ = 30, t=0)
    p = plot(0, xlabel = L"N", ylabel = L"E_N")
  
    ens_diag = []
    ens_lanz = []
    for i in 1:N
        m = IntRmSpinObc(L=L, N=i, Vso=Vso, U=U, T=1)
        states = generatefixednstates(m)
        template = sparsehamiltonian_template(m,states)
        HΛ, phis,  ens = lanczosenergies(m, Λ, states, template, tol = 1e-9, t = t)
        push!(ens_lanz,ens[end][1])
        H = Hfromtemp(m,t,template...)
        push!(ens_diag,eigen(Matrix(H)).values[1])
    end
    p = scatter!(ens_lanz, marker = :cross, markersize = 4, label = "Lanczos")
    p = scatter!(ens_diag,marker = :xcross, markerstrokewidth  =2, markersize = 3, label = "Diag", legend = :top)

    m2 = RmSpinObc(L=L, N=1, Vso=Vso, U=U, T=1)
    en2 = eigen(hamiltonian(m2,t)).values
    println(en2)
    ens_sp = [sum(en2[1:i]) for i in 1:N]

    p = scatter!(ens_sp,marker = :circle, markerstrokewidth  =2, markersize = 3, label = "SingleParticle", legend = :top)


    title!(latexstring("V_{so} = $Vso, L = $L"))
    display(p)

    return ens_sp .- ens_diag
end

function test_lanczos_energy_nospin(L;N=1, U=0, Λ = 30)
    p = plot(0, xlabel = L"N", ylabel = L"E_N")
  
    ens_diag = []
    ens_lanz = []
    for i in 1:N
        m = IntRmPbc(L=L, N=i, U=U)
        HΛ, phis,  ens = lanczostridiag(m, Λ)
        push!(ens_lanz,ens[end])
        states = generatefixednstates(m)
        H = sparsehamiltonian(m,states,0)
        push!(ens_diag,eigen(Matrix(H)).values[1])
    end
    p = scatter!(ens_lanz, marker = :cross, markersize = 4, label = "Lanczos")
    p = scatter!(ens_diag,marker = :xcross, markerstrokewidth  =2, markersize = 3, label = "Diag", legend = :top)

    m2 = RmPbc(L=L, N=1, U=U)
    en2 = eigen(hamiltonian(m2,0)).values
    println(en2)
    ens_sp = [sum(en2[1:i]) for i in 1:N]

    p = scatter!(ens_sp,marker = :circle, markerstrokewidth  =2, markersize = 3, label = "SingleParticle", legend = :top)


    title!(latexstring("L = $L"))
    display(p)

    # return ens_lanz[end] ≈ ens_diag[2], ens[end], ens_diag, ens[end] - ens_test
end