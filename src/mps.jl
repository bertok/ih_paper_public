using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))

# Custom observer for density:

mutable struct DensityObserver <: AbstractObserver
    sites::Array{Index{Array{Pair{QN,Int64},1}},1}
    energy_tol::Float64
    last_energy::Float64
    density_tol::Float64
    last_density::Array{Float64}
 
    DensityObserver(sites, energy_tol=0.0, density_tol=0.0) = new(sites, energy_tol, 1000.0, density_tol, 1000*ones(length(sites)))
end
 
function ITensors.checkdone!(o::DensityObserver;kwargs...)
    sw = kwargs[:sweep]
    energy = kwargs[:energy]
    psi = kwargs[:psi]
    density = expect(psi, "Nup")
#    if abs(energy-o.last_energy)/abs(energy) < o.energy_tol
#      println("Stopping DMRG after sweep $sw")
#      return true
#    end
    # println(abs.(density .- o.last_density))
    if maximum(abs.(density .- o.last_density)) < o.density_tol
        println("Stopping DMRG after sweep $sw")
        return true
    end
   # Otherwise, update last_energy and keep going
    o.last_energy = energy
    o.last_density = density
    return false
end
 

function MPO2gate(m,t,dt,sites)
    L= length(sites)
    gates = ITensor[]
    for j in 1:L-1
        H = mpo_gate(m, sites, j:j+1, t)
        push!(gates,exp(-1.0im*dt/2*H[1]*H[2]))
    end
    append!(gates,reverse(gates))
    return gates
end