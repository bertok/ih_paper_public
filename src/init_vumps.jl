import DrWatson.savename
using Plots
using DataFrames
using ProgressMeter
using CSV
using ITensors
using LaTeXStrings
using ITensorInfiniteMPS


include(srcdir("Model.jl"))
include(srcdir("Prop.jl"))
include(srcdir("Measurement.jl"))
include(srcdir("simulations.jl"))
include(srcdir("lanczos.jl"))
include(srcdir("tests.jl"))

function strdict(xml_dict)
    newdict = Dict()
    for (k,v) in xml_dict
        newdict[string(k)] = v
    end
    return newdict
end

savename(d::DataFrame; kwargs...) = savename(copy(d[1,:]), ignores = [:filename, :path]; kwargs...)  #add prefix and suffix support (todo)
savename(prefix::String,d::DataFrame,;kwargs...) = savename(prefix,copy(d[1,:]), ignores = [:filename, :path]; kwargs...)  #add prefix and suffix support (todo)
savename(prefix::String,d::DataFrame,suffix::String;kwargs...) = savename(prefix,copy(d[1,:]),suffix, ignores = [:filename, :path]; kwargs...)  #add prefix and suffix support (todo)
savename(d::DataFrame,suffix::String;kwargs...) = savename(copy(d[1,:]),suffix, ignores = [:filename, :path]; kwargs...)  #add prefix and suffix support (todo)


DrWatson.default_allowed(::Dict) = (Real, String, DataType) #for savename.
DrWatson.default_allowed(::DataFrame) = (Real, String, DataType) #for savename.
DrWatson.default_allowed(::NamedTuple) = (Real, String, DataType) #for savename.


# DrWatson.default_allowed(::PropKey) = (Real, String, Model) #for savename.
Base.string(m::Model) = "$(typeof(m))" #for savename.
Base.string(p::Propagator) = "$(typeof(p))" #for savename.

#savesafe for plots:
DrWatson._wsave(filename,p::Plots.Plot) = savefig(p, filename) # ✓


#savesafe for DataFrame (as csv):
function DrWatson._wsave(filename,df::DataFrame)
    CSV.write(filename, df)
end

#automatically create CSV data tables from saved plots:
function DrWatson._wsave(filename,p::Plots.Plot)
    for (i,subplot) in enumerate(p.subplots)
        for (j,series) in enumerate(subplot.series_list)
            if series[:seriestype] == :heatmap
                df = DataFrame(Array(series[:z]),:auto)
                df2 = DataFrame(x=series[:x])
                df3 = DataFrame(x=series[:y])
                wsave(filename[1:findlast(".",filename)[1]-1]*"/subpl_$(i)_series_$(j).csv",df)
                wsave(filename[1:findlast(".",filename)[1]-1]*"/subpl_$(i)_series_$(j)_x-axis.csv",df2)
                wsave(filename[1:findlast(".",filename)[1]-1]*"/subpl_$(i)_series_$(j)_y-axis.csv",df3)
            else
                df = DataFrame(x=series[:x],y=series[:y])
                wsave(filename[1:findlast(".",filename)[1]-1]*"/subpl_$(i)_series_$(j).csv",df)
            end
        end
    end
    
    savefig(p, filename) # ✓
end


#parse array of strings in cmd line as array of strings:
function parsearr(x)
    expr = Meta.parse(x) #
    @assert expr.head == :vect
    String.(expr.args)
end

function syncall()
    synccmd = `rsync -vax --exclude=".git/*" --exclude="_research/tmp" eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/ $(projectdir())/`
    run(synccmd)
end

function syncdata()
    synccmd = `rsync -vax --exclude=".git/*" --exclude="_research/*" eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/ $(projectdir())/`
    run(synccmd)
end

name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end] #return root folder of runjob script.
mkpath("_research/io/$name")