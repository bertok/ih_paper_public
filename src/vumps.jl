import ITensors.orthogonalize
# using Pkg; Pkg.add(url="https://github.com/mtfishman/ITensorInfiniteMPS.jl.git")

using ITensorInfiniteMPS

function ITensorInfiniteMPS.InfiniteSum{T}(model::ITensorInfiniteMPS.Model, s::Vector; kwargs...) where {T}
  return ITensorInfiniteMPS.InfiniteSum{T}(model, infsiteinds(s); kwargs...)
end

function ITensorInfiniteMPS.InfiniteSum{MPO}(model::ITensorInfiniteMPS.Model, s::CelledVector; kwargs...)
  N = length(s)
  mpos = [MPO(model, s, n; kwargs...) for n in 1:N] #slightly improved version. Note: the current implementation does not really allow for staggered potentials for example
  return InfiniteSum{MPO}(mpos)
end

# MPO building version
function ITensors.MPO(model::ITensorInfiniteMPS.Model, s::CelledVector, n::Int64; kwargs...)
  n1, n2 = 1, 2
  opsum = OpSum(model, n1, n2; n=n, kwargs...)
  return ITensors.MPO(opsum, [s[x] for x in n:(n + ITensorInfiniteMPS.nrange(model) - 1)]) #modification to allow for more than two sites per term in the Hamiltonians
end

#####

function ITensorInfiniteMPS.InfiniteSum{T}(observable::ITensorInfiniteMPS.Observable, s::Vector; kwargs...) where {T}
  return ITensorInfiniteMPS.InfiniteSum{T}(Observable, infsiteinds(s); kwargs...)
end

ITensorInfiniteMPS.nrange(model::ITensorInfiniteMPS.Observable) = 2; #required to keep everything compatible with the current implementation for 2 band models
function ITensorInfiniteMPS.InfiniteSum{T}(observable::ITensorInfiniteMPS.Observable, s::Vector; kwargs...) where {T}
  return ITensorInfiniteMPS.InfiniteSum{T}(observable, infsiteinds(s); kwargs...)
end

function ITensorInfiniteMPS.InfiniteSum{MPO}(observable::ITensorInfiniteMPS.Observable, s::CelledVector; kwargs...)
  N = length(s)
  mpos = [MPO(observable, s, n; kwargs...) for n in 1:N] #slightly improved version. Note: the current implementation does not really allow for staggered potentials for example
  return InfiniteSum{MPO}(mpos)
end

# MPO building version
function ITensors.MPO(observable::ITensorInfiniteMPS.Observable, s::CelledVector, n::Int64; kwargs...)
  n1, n2 = 1, 2
  opsum = OpSum(observable, n1, n2; n=n, kwargs...)
  return ITensors.MPO(opsum, [s[x] for x in n:(n + ITensorInfiniteMPS.nrange(observable) - 1)]) #modification to allow for more than two sites per term in the Hamiltonians
end


"n: staggereing site"
function ITensors.OpSum(::Model"rmspin", n1, n2; m, n, t)
  ampo = OpSum()
  
  ampo += hopping(m, n, t), "Cdagup", n1, "Cup", n2
  ampo += hopping(m, n, t), "Cdagup", n2, "Cup", n1
  ampo += hopping(m, n, t), "Cdagdn", n1, "Cdn", n2
  ampo += hopping(m, n, t), "Cdagdn", n2, "Cdn", n1

  ampo += spinorbitcoupling(m), "Cdagup", n1, "Cdn", n2
  ampo += spinorbitcoupling(m), "Cdagdn", n1, "Cup", n2
  ampo -= spinorbitcoupling(m), "Cdagdn", n2, "Cup", n1
  ampo -= spinorbitcoupling(m), "Cdagup", n2, "Cdn", n1

  ampo += potential(m, n, t), "Cdagup", n1, "Cup", n1
  ampo += -potential(m, n, t), "Cdagdn", n1, "Cdn", n1
  ampo += interaction(m), "Cdagdn", n1, "Cdn", n1, "Cdagup", n1, "Cup", n1

  return ampo
end


"n: staggereing site"
function ITensors.OpSum(::Model"ionichubbard2", n1, n2, n; m)
  ampo = OpSum()
  
  ampo += hopping(m, n), "Cdagup", n1, "Cup", n2
  ampo += hopping(m, n), "Cdagup", n2, "Cup", n1
  ampo += hopping(m, n), "Cdagdn", n1, "Cdn", n2
  ampo += hopping(m, n), "Cdagdn", n2, "Cdn", n1


  ampo += potential(m, n), "Cdagup", n1, "Cup", n1
  ampo += potential(m, n), "Cdagdn", n1, "Cdn", n1
  ampo += interaction(m), "Cdagdn", n1, "Cdn", n1, "Cdagup", n1, "Cup", n1

  return ampo
end

function ITensors.OpSum(::Model"ionichubbard", n1, n2; m, n, t)
  ampo = OpSum()
  
  ampo += hopping(m, n, t), "Cdagup", n1, "Cup", n2
  ampo += hopping(m, n, t), "Cdagup", n2, "Cup", n1
  ampo += hopping(m, n, t), "Cdagdn", n1, "Cdn", n2
  ampo += hopping(m, n, t), "Cdagdn", n2, "Cdn", n1

  # ampo += 10, "Sz", n1, "Sz", n1
  # ampo += 10, "Sz", n2, "Sz", n2


  ampo += potential(m, n, t), "Nup", n1
  ampo += potential(m, n, t), "Ndn", n1
  ampo += interaction(m), "Nup", n1, "Ndn", n1

  return ampo
end

function ITensors.OpSum(::Model"ionichubbard_gate", n1, n2; m, n, t)
  ampo = OpSum()
  
  ampo += hopping(m, n, t), "Cdagup", n1, "Cup", n2
  ampo += hopping(m, n, t), "Cdagup", n2, "Cup", n1
  ampo += hopping(m, n, t), "Cdagdn", n1, "Cdn", n2
  ampo += hopping(m, n, t), "Cdagdn", n2, "Cdn", n1

  # ampo += 10, "Sz", n1, "Sz", n1
  # ampo += 10, "Sz", n2, "Sz", n2


  ampo += potential(m, n, t), "Nup", n1
  ampo += potential(m, n, t), "Ndn", n1
  ampo += interaction(m), "Nup", n1, "Ndn", n1

  return ampo
end


function ITensors.OpSum(::Model"ionichubbardtoy", n1, n2; m, n, t)
  ampo = OpSum()
  
  ampo += hopping(m, n, t), "Cdagup", n1, "Cup", n2
  ampo += hopping(m, n, t), "Cdagup", n2, "Cup", n1
  ampo += hopping(m, n, t), "Cdagdn", n1, "Cdn", n2
  ampo += hopping(m, n, t), "Cdagdn", n2, "Cdn", n1


  ampo += potential(m, n, t), "Cdagup", n1, "Cup", n1
  ampo += potential(m, n + m.L, t), "Cdagdn", n1, "Cdn", n1
  ampo += interaction(m), "Cdagdn", n1, "Cdn", n1, "Cdagup", n1, "Cup", n1

  ampo += m.J, "Sz", n1, "Sz", n2

  return ampo
end

function ITensors.MPO(::Model"ionichubbard", sites; m)
  ampo = AutoMPO()
  
  for j = 1:m.L - 1
      ampo += hopping(m, j), "Cdagup", j, "Cup", j + 1
      ampo += hopping(m, j), "Cdagup", j + 1, "Cup", j
      ampo += hopping(m, j), "Cdagdn", j, "Cdn", j + 1
      ampo += hopping(m, j), "Cdagdn", j + 1, "Cdn", j
  end

  for j = 1:m.L
      ampo += potential(m, j), "Cdagup", j, "Cup", j
      ampo += potential(m, j), "Cdagdn", j, "Cdn", j
      ampo += interaction(m), "Cdagdn", j, "Cdn", j, "Cdagup", j, "Cup", j
  end
  return MPO(ampo, sites)
end


"Current operator ↑"
function ITensors.OpSum(::Observable"current_up", n1, n2; m, n, t)
  ampo = OpSum()
  ampo += 1im*hopping(m, n, t), "Cdagup", n1, "Cup", n2
  ampo += -1im*hopping(m, n, t), "Cdagup", n2, "Cup", n1
  return ampo
end

"Current operator ↓"
function ITensors.OpSum(::Observable"current_dn", n1, n2; m, n ,t)
  ampo = OpSum()
  ampo += +1im*hopping(m, n, t), "Cdagdn", n1, "Cdn", n2
  ampo += -1im*hopping(m, n, t), "Cdagdn", n2, "Cdn", n1
  return ampo
end



function electron_space_shift(q̃nf, q̃sz)
    return [
      QN(("Nf", 0 - q̃nf, -1), ("Sz", 0 - q̃sz)) => 1,
      QN(("Nf", 1 - q̃nf, -1), ("Sz", 1 - q̃sz)) => 1,
      QN(("Nf", 1 - q̃nf, -1), ("Sz", -1 - q̃sz)) => 1,
      QN(("Nf", 2 - q̃nf, -1), ("Sz", 0 - q̃sz)) => 1,
    ]
end

function electron_space_shift_noSz(q̃nf)
  return [
    QN("Nf", 0 - q̃nf, -1) => 1,
    QN("Nf", 1 - q̃nf, -1) => 2,
    QN("Nf", 2 - q̃nf, -1) => 1,
  ]
end

function ITensors.expect(ψ::InfiniteCanonicalMPS,s, o, n)
    return (noprime(ψ.AL[n] * ψ.C[n] * op(o, s[n])) * dag(ψ.AL[n] * ψ.C[n]))[]
end

function expect_local(ψ::InfiniteCanonicalMPS,s, o, n)
  return ((noprime(ψ.AL[n] * ψ.C[n] * op(o, s[n])) * dag(ψ.AL[n] * ψ.C[n]))[]./(noprime(ψ.AL[n] * ψ.C[n]) * dag(ψ.AL[n] * ψ.C[n]))[])
end
  
function expect_two_site(ψ::InfiniteCanonicalMPS, h::MPO, n1n2)
    n1, n2 = n1n2
    ϕ = ψ.AL[n1] * ψ.AL[n2] * ψ.C[n2]
    return (noprime(ϕ * h[1]*h[2]) * dag(ϕ))[]
end

function expect_two_site_local(ψ::InfiniteCanonicalMPS, h::MPO, n1n2) #for nonorthogonalizeed iTEBD results.
  n1, n2 = n1n2
  ψt = ITensors.orthogonalize(InfiniteMPS(ψ[1:2]), : ; tol = 1e-9) # This returns wrong arrow directions.
  ϕ = ψt.AL[n1] * ψt.AL[n2] * ψt.C[n2]
  return (noprime(ϕ * h[1]*h[2]) * dag(ϕ))[]
end
  
function expect_two_site(ψ::MPS, h::MPO, n1n2)
    n1, n2 = n1n2
    ψ = orthogonalize(ψ, n1)
    ϕ = ψ[n1] * ψ[n2]
    return (noprime(ϕ * h[1]*h[2]) * dag(ϕ))[]
end