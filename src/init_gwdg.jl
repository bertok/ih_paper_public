import DrWatson.savename
using Plots
using DataFrames
using ProgressMeter
using CSV
using ITensors
using LaTeXStrings
using KrylovKit


include(srcdir("Model.jl"))
include(srcdir("Prop.jl"))
include(srcdir("Measurement.jl"))
include(srcdir("simulations.jl"))
include(srcdir("lanczos.jl"))
include(srcdir("tests.jl"))
# include(srcdir("unicodeanimation.jl"))


"Transform a dictionary with symbol-value pairs to a dictionary with string-value pairs."
function strdict(xml_dict)
    newdict = Dict{String,Any}()
    for (k,v) in xml_dict
        newdict[string(k)] = v
    end
    return newdict
end

savename(d::DataFrame; kwargs...) = savename(copy(d[1,:]), ignores = [:filename, :path]; kwargs...)  #add prefix and suffix support (todo)
savename(prefix::String,d::DataFrame,;kwargs...) = savename(prefix,copy(d[1,:]), ignores = [:filename, :path]; kwargs...)  #add prefix and suffix support (todo)
savename(prefix::String,d::DataFrame,suffix::String;kwargs...) = savename(prefix,copy(d[1,:]),suffix, ignores = [:filename, :path]; kwargs...)  #add prefix and suffix support (todo)
savename(d::DataFrame,suffix::String;kwargs...) = savename(copy(d[1,:]),suffix, ignores = [:filename, :path]; kwargs...)  #add prefix and suffix support (todo)


DrWatson.default_allowed(::Dict) = (Real, String, DataType) #for savename.
DrWatson.default_allowed(::DataFrame) = (Real, String, DataType) #for savename.
DrWatson.default_allowed(::NamedTuple) = (Real, String, DataType) #for savename.


# DrWatson.default_allowed(::PropKey) = (Real, String, Model) #for savename.
Base.string(m::Model) = "$(typeof(m))" #for savename.
Base.string(p::Propagator) = "$(typeof(p))" #for savename.

#savesafe for plots:
DrWatson._wsave(filename,p::Plots.Plot) = savefig(p, filename) # ✓


#savesafe for DataFrame (as csv):
function DrWatson._wsave(filename,df::DataFrame)
    CSV.write(filename, df)
end

#automatically create CSV data tables from saved plots:
function DrWatson._wsave(filename,p::Plots.Plot)
    for (i,subplot) in enumerate(p.subplots)
        for (j,series) in enumerate(subplot.series_list)
            if series[:seriestype] == :heatmap
                df = DataFrame(Array(series[:z]),:auto)
                df2 = DataFrame(x=series[:x])
                df3 = DataFrame(x=series[:y])
                wsave(filename[1:findlast(".",filename)[1]-1]*"/subpl_$(i)_series_$(j).csv",df)
                wsave(filename[1:findlast(".",filename)[1]-1]*"/subpl_$(i)_series_$(j)_x-axis.csv",df2)
                wsave(filename[1:findlast(".",filename)[1]-1]*"/subpl_$(i)_series_$(j)_y-axis.csv",df3)
            else
                df = DataFrame(x=series[:x],y=series[:y])
                wsave(filename[1:findlast(".",filename)[1]-1]*"/subpl_$(i)_series_$(j).csv",df)
            end
        end
    end
    
    savefig(p, filename) # ✓
end

"""
Takes dict instance and creates a result dictionary that is compatible with
DrWatson.savename, filling in all default values for all parameters of the model and propagation.
"""
function resdict(d::Dict; ignores=[:filename], pops=[:filename])
    D = deepcopy(d)# result dict
    for p in pops
        try
            pop!(D, p)
        catch
        end
    end
    m  = dict2model(D)
    DD =  resdict(m)
    DDD = merge(D, DD)
    DDD[:filename] = savename(DDD, ignores=ignores)
    for p in pops
        pop!(DDD, p)
    end
    return DDD
end

function resdict(m::Model)
    D = struct2dict(m) # result dict
    DD = Dict{Symbol,Any}(:m => typeof(m))
    DD[:filename] = savename(merge(D, DD))
    return merge(D, DD)
end

#parse array of strings in cmd line as array of strings:
function parsearr(x)
    expr = Meta.parse(x) #
    @assert expr.head == :vect
    String.(expr.args)
end

function syncall()
    synccmd = `rsync -vax --exclude=".git/*" --exclude="_research/tmp" eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/ $(projectdir())/`
    run(synccmd)
end

function syncdata()
    synccmd = `rsync -vax --exclude=".git/*" --exclude="_research/*" eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/data/ $(datadir())`
    run(synccmd)
    synccmd = `rsync -vax --exclude=".git/*" --exclude="_research/*" bertok@gwdg-transfer:\~/mps-charge-pump/data/ $(datadir())`
    run(synccmd)
end

function pushoutfile()
    synccmd = `rsync -vax $(projectdir("_research","tmp"))/ eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/_research/tmp/`
    run(synccmd)
end

function pull_cluster()
    success(Cmd(`ssh eric.bertok@rocks cd mps-charge-pump \&\& git pull`))
end

function rocks(c::Cmd)
   run(Cmd(`ssh eric.bertok@rocks cd mps-charge-pump \&\& $c`))
end

function pushoutfile(path)
   mkpath((projectdir("_research","io",path)))
   synccmd = `rsync -vax $(projectdir("_research","tmp"))/ eric.bertok@file.theorie.physik.uni-goettingen.de:/rocks/eric.bertok/mps-charge-pump/_research/tmp/`
   run(synccmd)
end

function generate_outfile(runs)
   outfile = projectdir("_research", "tmp", "runs_$(runs[1]).txt")
   open(outfile, "w") do f
       for i in runs
           println(f, i)
       end
   end
   strings = splitpath(outfile)[end-2:end]
   return strings[1] * "/" * strings[2] * "/" * strings[3]
end

# function generate_jdf()
#     jdf = "
#     executable = /usr/bin/julia
#     arguments = \"--project $(dirname(PROGRAM_FILE))/runjob.jl \$(runfile) \"
#     error = _research/io/$(name)/\$(Cluster).\$(Process).err
#     log = _research/io/$(name)/\$(Cluster).log
#     output = _research/io/$(name)/\$(Cluster).\$(Process).out
#     request_memory = 10 GB
#     max_idle = 100

#     queue runfile from $outfile
#     "
# end

function generate_jdf(path, outfile, maxtime)
   jdf = "
   executable = /opt/julia/bin/julia
   arguments = \"--project scripts/$(path)/runjob.jl \$(runfile) \"
   error = _research/io/$(path)/\$(Cluster).\$(Process).err
   log = _research/io/$(path)/\$(Cluster).log
   output = _research/io/$(path)/\$(Cluster).\$(Process).out
   request_memory = 10 GB
   max_idle = 100
   +MaxRuntime = $maxtime

   queue runfile from $outfile
   "

   jdffile = projectdir("$(outfile)_jdf")
   open(jdffile, "w") do f
       println(f, jdf)
   end

   strings = splitpath(jdffile)[end-2:end]
   return strings[1] * "/" * strings[2] * "/" * strings[3]
end

function submit_jobs(path, dicts, maxtime)
   runs = tmpsave(dicts)
   println(runs)
   outfile = generate_outfile(runs)
   jdf = generate_jdf(path, outfile, maxtime)
   pushoutfile(path)
   pull_cluster()
   mkpath((projectdir("_research","io",path)))

   
   run(Cmd(`ssh eric.bertok@rocks cd mps-charge-pump \&\& mkdir -p _research/io/$(path) \&\&condor_submit $jdf`))
end


