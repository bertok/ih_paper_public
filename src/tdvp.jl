using TimeEvoMPS
import TimeEvoMPS.twosite!, TimeEvoMPS.TDVP2, TimeEvoMPS.apply!, TimeEvoMPS.singlesite!, ITensors.checkdone!

checkdone!(cb::NoTEvoCallback,args...; kwargs...) = false


function tdvp_temp!(psi,m::Model,sites,dt,tf; kwargs...)
    cb = get(kwargs,:callback, NoTEvoCallback())
    hermitian = get(kwargs,:hermitian,true)
    exp_tol = get(kwargs,:exp_tol, 1e-14)
    krylovdim = get(kwargs,:krylovdim, 30 )
    maxiter = get(kwargs,:maxiter,100)
    normalize = get(kwargs,:normalize,true)
    t0 = get(kwargs,:t0,0)
    nsteps = Int((tf-t0)/dt)



    pbar = get(kwargs,:progress, true) ? Progress(nsteps, desc="Evolving state... ") : nothing
    τ = 1im*dt
    imag(τ) == 0 && (τ = real(τ))

    N = length(psi)
    ITensors.orthogonalize!(psi,1)
    t=0
    for s in 1:nsteps
        # println("ham $s")
        H = mpo_ham(m, sites, t+dt/2)
        PH = ProjMPO(H)
        position!(PH,psi,1)
        stime = @elapsed begin
        for (b,ha) in sweepnext(N)
            # println("sweep $((b,ha)): Maxlinkdim $(maxlinkdim(psi))")

            #evolve with two-site Hamiltonian
            twosite!(PH)
            ITensors.position!(PH,psi,b)
            wf = psi[b]*psi[b+1]
            wf, info = exponentiate(PH, -τ/2, wf; ishermitian=hermitian , tol=exp_tol, krylovdim=krylovdim)
            dir = ha==1 ? "left" : "right"
            info.converged==0 && throw("exponentiate did not converge")
            spec = replacebond!(psi,b,wf;normalize=normalize, ortho = dir, kwargs... )
            # normalize && ( psi[dir=="left" ? b+1 : b] /= sqrt(sum(eigs(spec))) )

            apply!(cb,psi; t=s*dt,
                   bond=b,
                   sweepend= ha==2,
                   sweepdir= ha==1 ? "right" : "left",
                   spec=spec,
                   alg=TDVP2())

            # evolve with single-site Hamiltonian backward in time.
            # In the case of imaginary time-evolution this step
            # is not necessary (see Ref. [1])
            i = ha==1 ? b+1 : b
            if 1<i<N && !(dt isa Complex)
                singlesite!(PH)
                ITensors.position!(PH,psi,i)
                psi[i], info = exponentiate(PH,τ/2,psi[i]; ishermitian=hermitian, tol=exp_tol, krylovdim=krylovdim,
                                            maxiter=maxiter)
                info.converged==0 && throw("exponentiate did not converge")
            elseif i==1 && dt isa Complex
                # TODO not sure if this is necessary anymore
                psi[i] /= sqrt(real(scalar(dag(psi[i])*psi[i])))
            end

        end
        end
        t+=dt
        !isnothing(pbar) && ProgressMeter.next!(pbar, showvalues=[("t", dt*s),
                                                                  ("dt step time", round(stime,digits=3)),
                                                                  ("Max bond-dim", maxlinkdim(psi))])
        checkdone!(cb) && break
    end
end