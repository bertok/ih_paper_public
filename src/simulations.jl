using ITensors
using KrylovKit
using DrWatson
using TimeEvoMPS

"""
Takes dictionary containing model parameters, model itself and propagator
and returns respective model with parameters chosen accordingly.
"""
function dict2model(dict::Dict)
    return dict[:m](;dict...)
end

function dict2model(dict::Dict; kwargs...)
    return dict[:m](;dict..., kwargs...)
end

"""
Takes dictionary containing model and/or propagator parameters, model itself and propagator
and returns respective Propagation with parameters chosen accordingly.
"""
function dict2prop(dict::Dict)
    return Propagation(m=dict[:m](;dict...);dict...)
end

"""
Takes Propagation instance and creates a result dictionary that is compatible with
DrWatson.savename, filling in all default values for all parameters of the model and propagation.
"""
# function resdict(P::Propagation)
#     D = merge(struct2dict(P.m), struct2dict(PropKey(P))) # result dict
#     D[:m] = typeof(D[:m])
#     D[:p] = typeof(D[:p])
#     D[:filename] = savename(D)
#     return strdict(D)
# end





function makesim_floquet_ipr(d::Dict)
    m = d[:model](L=d[:L],  RΔ=d[:RΔ], Rδ=d[:Rδ], T=d[:T], w=d[:w], seed=d[:s])
    P = Propagation(m=m, p=d[:prop](), dt=d[:dt], ϕf=1, occ=1 / 2)
    D = merge(struct2dict(m), struct2dict(PropKey(P))) # result array
    D[:m] = typeof(D[:m])
    D[:p] = typeof(D[:p])
    D[:filename] = savename(D)
    E, U = eigen(FloquetOp(P))
    D[:ipr]  = ipr(U, P.occ)
    return strdict(D)
end


function makesim_floquet_ipr_modesweep(d::Dict)
    m = d[:model](L=d[:L],  RΔ=d[:RΔ], Rδ=d[:Rδ], T=d[:T], w=d[:w], seed=d[:s][end])
    P = Propagation(m=m, p=d[:prop](), dt=d[:dt], ϕf=1, occ=1 / 2)
    D = merge(struct2dict(m), struct2dict(PropKey(P))) # result array
    D[:m] = typeof(D[:m])
    D[:p] = typeof(D[:p])
    D[:filename] = savename(D)
    iprs = zeros(length(d[:s]), ceil(Int, d[:L] * 1 / 2))
    for (i, s) in enumerate(d[:s])
        m = d[:model](L=d[:L],  RΔ=d[:RΔ], Rδ=d[:Rδ], T=d[:T], w=d[:w], seed=s)
        P = Propagation(m=m, p=d[:prop](), dt=d[:dt], ϕf=1, occ=1 / 2)
        E, U = eigen(FloquetOp(P))
        iprs[i,:] = ipr(U, P.occ)
    end
    D[:ipr] = iprs
    return strdict(D)
end


function makesim_spinpump_current(d::Dict)
    P = dict2prop(d)
    D = resdict(P)

    evr = 1000
    Ψ = zeros(ComplexF64, length(timerange(P, every=evr)), size(hamiltonian(P.m, 0))...)
    Js = zeros(ComplexF64, length(timerange(P)))

    j = 1
    @showprogress  for (i, t) in enumerate(timerange(P))
        Js[i] = curr(P)
        if t in timerange(P, every=evr)
            Ψ[j,:,:] = P.Ψ
            j += 1
        end
        prop!(P)
    end
    D[:J] = Js
    D[:Q] = real.([sum(Js[1:i]) * P.dt ./ P.occ for i in eachindex(timerange(P))])
    D[:Ψ] = Ψ
    return strdict(D)
end


function makesim_spinpump_lanczos_gap(d::Dict; ts=0:0.25:1)
    m = dict2model(d)
    D = resdict(d)
    d[:N] = d[:L]

    basis = generatefixednstates(m)
    Htemplate = sparsehamiltonian_template(m, basis)

    D[:ts] = ts
    intgaps = zeros(length(ts))
    if m.Vso == 0
        spingaps = zeros(length(ts))
        gsspin0 = zeros(length(ts))
        gsspin1 = zeros(length(ts))
    else
        
        spingaps = missing
        gsspin0 = missing
        gsspin1 = missing
    end

    degs = zeros(length(ts))
    dens = zeros(length(ts), 2 * m.L)
    gsint = zeros(length(ts))
    polup = zeros(length(ts))
    poldn = zeros(length(ts))

   
    gsNplus2 = zeros(length(ts))
    gsNminus2 = zeros(length(ts))
    
    for (i, t) in enumerate(ts)
        H, Φs, ens = lanczosenergies(m::IntModel, t, 100, basis, Htemplate; energyoffset=0, energynum=2, tol=1e-9)
        intgaps[i] = ens[end][2] - ens[end][1]
        degs[i] = ens[end][3] - ens[end][2]
        gsint[i] = ens[end][1]
        ground = lanczosgroundstate(H, Φs; statenum=1)
        dens[i,:] = localdensity(ground, basis, m)

        polup[i] = sum([(j - m.L / 2) * dens[i,j]] for j in 1:m.L)[1] / m.L
        poldn[i] = sum([(j - m.L / 2 - m.L) * dens[i,j]] for j in m.L + 1:2 * m.L)[1] / m.L

    end
    D[:intgaps] = intgaps
    D[:degs] = degs # was error here. old data useless.
    D[:dens] = dens
    D[:gsint] = gsint
    D[:polup] = polup
    D[:poldn] = poldn

    if m.Vso == 0
        basis_sz0 = generatefixedSzstates(m, Sz=0)
        Htemplate_sz0 = sparsehamiltonian_template(m, basis_sz0)

        for (i, t) in enumerate(ts)
            H, Φs, ens = lanczosenergies(m::IntModel, t, 100, basis_sz0, Htemplate_sz0; energyoffset=0, energynum=3, tol=1e-9)
            gsspin0[i] = ens[end][1]
        end
        D[:gsspin0] = gsspin0

        basis_sz1 = generatefixedSzstates(m, Sz=1)
        Htemplate_sz1 = sparsehamiltonian_template(m, basis_sz1)

        for (i, t) in enumerate(ts)
            H, Φs, ens = lanczosenergies(m::IntModel, t, 100, basis_sz1, Htemplate_sz1; energyoffset=0, energynum=3, tol=1e-9)
            gsspin1[i] = ens[end][1]
        end
        D[:gsspin1] = gsspin1
    end


    d[:N] = d[:N] + 2
    m = dict2model(d)
    basis_Nplus2 = generatefixednstates(m) ###fixed N ?
    Htemplate_Nplus2 = sparsehamiltonian_template(m, basis_Nplus2)

    for (i, t) in enumerate(ts)
        H, Φs, ens = lanczosenergies(m::IntModel, t, 100, basis_Nplus2, Htemplate_Nplus2; energyoffset=0, energynum=1, tol=1e-9)
        gsNplus2[i] = ens[end][1]
    end
    D[:gsNplus2] = gsNplus2

    d[:N] = d[:N] - 4
    m = dict2model(d)
    basis_Nminus2 = generatefixednstates(m)
    Htemplate_Nminus2 = sparsehamiltonian_template(m, basis_Nminus2)

    for (i, t) in enumerate(ts)
        H, Φs, ens = lanczosenergies(m::IntModel, t, 100, basis_Nminus2, Htemplate_Nminus2; energyoffset=0,energynum=1, tol=1e-9)
        gsNminus2[i] = ens[end][1]
    end
    D[:gsNminus2] = gsNminus2

    D[:chargegaps] = (gsNplus2 .+ gsNminus2 .- 2 * gsint) / 2
    D[:spingaps] = gsspin1 .- gsspin0

    pop!(d, :N)
    return strdict(D)

end

function makesim_spinpump_spin_polarization(d::Dict)
    m = dict2model(d)
    D = resdict(d)

    gap_theta(dt) = fld(cld(1, dt), 4) * dt - dt, fld(cld(1, dt), 4) * dt + dt, fld(3 * cld(1, dt), 4) * dt - dt, fld(3 * cld(1, dt), 4) * dt + dt

    Vsos = 0:0.01:7
    res = []
    @showprogress for Vso in Vsos
        m = dict2model(d, Vso=Vso)
        P1 = []
        P2 = []
        for t in gap_theta(1e-9)
            push!.((P1, P2), polarization(m, t) ./ (2π))
        end
    
        push!(res, ((P2[1] - P2[2] + P2[3] - P2[4]) - (P1[1] - P1[2] + P1[3] - P1[4])))
    end

    D[:Ps] = res
    D[:Vsos] = Vsos

    
    return strdict(D)

end

function makesim_spinpump_nonint_gap(d::Dict)
    m = dict2model(d)
    D = resdict(m)

    Vsos  =  d[:Vsos]

    ts = [0,0.25]
    mingaps = []
    @showprogress for Vso in Vsos
        m = dict2model(d, Vso=Vso)
        gaps = []
        for t in ts
            λ, U = eigen(hamiltonian(m, t))
            push!(gaps, λ[m.L + 1] - λ[m.L])   
        end
        push!(mingaps, minimum(gaps))
    end

    D[:mingap] = mingaps
    D[:Vso] = Vsos

    return strdict(D)

end


function lanczos_current(d::Dict)
    m = dict2model(d)
    P = LPropagation(m=m, dt=0.1, ϵ=1e-6, Λ=200)

    basis = generatefixednstates(m)
    H = sparsehamiltonian_template(m, basis)
    HΛ, Φs, ens = lanczosenergies(m, 0,100, basis, H, tol = 1e-6,  energynum=1)
    P.Ψ = lanczosgroundstate(HΛ, Φs)

    scurr = []
    scurr2 = []
    qs1 = [0.]
    qs2 = [0.]
    dup = []
    ddn = []
    ts = []
    pol = []

    while P.t < m.T
        ttmp = P.t
        prop!(P, H)
        push!(scurr, real(localcurrent(P.t, P.Ψ, 1, 2, basis, m)))
        push!(scurr2, real(localcurrent(P.t, P.Ψ, 2, 3, basis, m)))
        push!(qs1, qs1[end] + (P.t - ttmp) * scurr[end])
        push!(dup,localdensity_up(P.Ψ,basis,m))
        push!(ddn,localdensity_dn(P.Ψ,basis,m))
        push!(qs2, qs2[end] + (P.t - ttmp) * scurr2[end])
        push!(ts, P.t)
        push!(pol,polarization(P.Ψ,basis,m))
    end
    # qs1 = real.([sum(scurr[1:i] .* (ts[i+1]-ts[i])) for i in eachindex(scurr[1:end-1])])
    # qs2 = real.([sum(scurr2[1:i] .* (ts[i+1]-ts[i])) for i in eachindex(scurr2[1:end-1])])

    

    return Dict(:q => qs1 .+ qs2, :t => ts, :pol => pol)
end

function lanczos_current_kryl(d::Dict; conserve_sz = true, calculate_inst = false, periods = 1)
    m = dict2model(d)
    mC1 = dict2model(d,N = d[:L]+2)
    mC2 = dict2model(d,N = d[:L]-2)
    dt = d[:dt]
    D = resdict(m)
    P = LPropagation(m=m, dt=dt, ϵ=1e-9, Λ=200)

    if conserve_sz == true
        basis = generatefixedSzstates(m, Sz = 0)
        basisS = generatefixedSzstates(m, Sz = 1)
        basisC1 = generatefixedSzstates(mC1, Sz = 0)
        basisC2 = generatefixedSzstates(mC2, Sz = 0)
    else
        basis = generatefixednstates(m)
        basisS = generatefixedSzstates(m, Sz = 1)
        basisC1 = generatefixedSzstates(mC1, Sz = 0)
        basisC2 = generatefixedSzstates(mC2, Sz = 0)
    end
    Htemp = sparsehamiltonian_template(m, basis)
    HtempS = sparsehamiltonian_template(m, basisS)
    HtempC1 = sparsehamiltonian_template(m, basisC1)
    HtempC2 = sparsehamiltonian_template(m, basisC2)


    H = Hfromtemp(P.m,-m.T,Htemp...)
    P.Ψ = complex.(eigsolve(H, 3, :SR)[2])[1]
    # HΛ, Φs, ens = lanczosenergies(m, 0,100, basis, Htemp, tol = 1e-6,  energynum=1)
    # P.Ψ = lanczosgroundstate(HΛ, Φs)

    ts = 0:P.dt:(m.T*periods)-P.dt
    D[:scurr] = zeros(length(ts))
    D[:scurr2] = zeros(length(ts))
    D[:scurr3] = zeros(length(ts))
    D[:scurr4] = zeros(length(ts))
    D[:qs1] = zeros(length(ts)+1)
    D[:qs2] = zeros(length(ts)+1)
    D[:qs3] = zeros(length(ts)+1)
    D[:qs4] = zeros(length(ts)+1)
    D[:qs] = zeros(length(ts))
    D[:qsS] = zeros(length(ts))
    D[:ts] = ts
    D[:pol] = zeros(Union{Float64,Missing},length(ts))
    D[:pol2] = zeros(Union{Float64,Missing},length(ts))
    D[:pol3] = zeros(Union{Float64,Missing},length(ts))
    D[:polspin] = zeros(Union{Float64,Missing},length(ts))
    D[:polspin2] = zeros(Union{Float64,Missing},length(ts))
    D[:polspin3] = zeros(Union{Float64,Missing},length(ts))


    D[:polS] = zeros(Union{Float64,Missing},length(ts))
    D[:polspinS] = zeros(Union{Float64,Missing},length(ts))
    D[:polC1] = zeros(Union{Float64,Missing},length(ts))
    D[:polspinC1] = zeros(Union{Float64,Missing},length(ts))
    D[:polC2] = zeros(Union{Float64,Missing},length(ts))
    D[:polspinC2] = zeros(Union{Float64,Missing},length(ts))

    D[:en0] = zeros(Union{Float64,Missing},length(ts))
    D[:enS] = zeros(Union{Float64,Missing},length(ts))
    D[:enS2] = zeros(Union{Float64,Missing},length(ts))
    D[:enC1] = zeros(Union{Float64,Missing},length(ts))
    D[:enC2] = zeros(Union{Float64,Missing},length(ts))
    D[:en1] = zeros(Union{Float64,Missing},length(ts))
    D[:en2] = zeros(Union{Float64,Missing},length(ts))
    D[:en3] = zeros(Union{Float64,Missing},length(ts))
    D[:adpar] = zeros(Union{Float64,Missing},length(ts))
    D[:derivative] = zeros(Union{Float64,Missing},length(ts))
    D[:derivative2] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_exc] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_exc2] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_exc3] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_dyn] = zeros(Union{Float64,Missing},length(ts))
    D[:sdiS] = zeros(Union{Float64,Missing},length(ts))
    D[:sdiS2] = zeros(Union{Float64,Missing},length(ts))
    D[:adiabaticity] = zeros(Union{Float64,Missing},length(ts))

    H = Hfromtemp(P.m,-m.T/16,Htemp...)
    ens, vecs, info = eigsolve(H, 4, :SR, tol = 1e-9)
    P.t = -m.T/16
    P.Ψ = vecs[1]
    @showprogress "Einschwingvorgang" for (i,t) in enumerate(-m.T/16:P.dt:0-P.dt) #Einschwingvorgang
        ttmp = P.t
        H = Hfromtemp(P.m,P.t+P.dt/2,Htemp...)
        P.Ψ, info = exponentiate(H,-1im * P.dt,P.Ψ, tol=1e-9)
        P.t += P.dt
    end

    @showprogress "Messung" for (i,t) in enumerate(0:P.dt:(m.T*periods)-P.dt) #Messung
        ttmp = P.t
        H = Hfromtemp(P.m,P.t+P.dt/2,Htemp...)
        P.Ψ, info = exponentiate(H,-1im * P.dt,P.Ψ, tol=1e-9)
        P.t += P.dt
        D[:scurr][i] = real(localcurrent(P.t, P.Ψ, 1, 2, basis, m)) #curr spin up link 1
        D[:scurr2][i] = real(localcurrent(P.t, P.Ψ, 2, 3, basis, m)) #curr spin up link 2
        D[:scurr3][i] = real(localcurrent(P.t, P.Ψ, 1+m.L, 2+m.L, basis, m)) #curr spin dn link 1
        D[:scurr4][i] = real(localcurrent(P.t, P.Ψ, 2+m.L, m.L+3, basis, m)) #curr spin dn link 2
        D[:qs1][i+1] = D[:qs1][i] + (P.t - ttmp) * D[:scurr][i]
        D[:qs2][i+1] = D[:qs2][i] + (P.t - ttmp) * D[:scurr2][i]
        D[:qs3][i+1] = D[:qs3][i] + (P.t - ttmp) * D[:scurr3][i]
        D[:qs4][i+1] = D[:qs4][i] + (P.t - ttmp) * D[:scurr4][i]
        D[:sdi_dyn][i] = sdi_order(P.Ψ,basis,m) #curr spin dn link 2

        if calculate_inst 
        
        # try           
            H = Hfromtemp(P.m,P.t,Htemp...)
            ens, vecs = eigsolve(H, 4, :SR, tol = 1e-9)[1:3]
            D[:en0][i] = ens[1]
            D[:en1][i] = ens[2]
            D[:en2][i] = ens[3]
            D[:en3][i] = ens[4]
            D[:pol][i] = polarization(complex.(vecs[1]),basis,m)/m.L
            D[:polspin][i] = polarization_spin(complex.(vecs[1]),basis,m)/m.L
            D[:pol2][i] = polarization(complex.(vecs[2]),basis,m)/m.L
            D[:polspin2][i] = polarization_spin(complex.(vecs[2]),basis,m)/m.L
            D[:pol3][i] = polarization(complex.(vecs[3]),basis,m)/m.L
            D[:polspin3][i] = polarization_spin(complex.(vecs[3]),basis,m)/m.L

            Ψ1 = complex.(vecs[1])
            Ψ1_exc = complex.(vecs[2])
            Ψ1_exc2 = complex.(vecs[3])
            Ψ1_exc3 = complex.(vecs[4])


            D[:sdi][i] = sdi_order(Ψ1,basis,m)
            D[:sdi_exc][i] = sdi_order(Ψ1_exc,basis,m)
            D[:sdi_exc2][i] = sdi_order(Ψ1_exc2,basis,m)
            D[:sdi_exc3][i] = sdi_order(Ψ1_exc3,basis,m)

            D[:adiabaticity][i] = norm(Ψ1 ⋅ P.Ψ)


            # H = Hfromtemp(P.m,P.t+P.dt,Htemp...)
            # Ψ2 = complex.(eigsolve(H, 3, :SR)[2])[1]

            # Ψ̇ = ∂(Ψ1,Ψ2,P.t,P.t+P.dt)

            # D[:derivative][i] = Ψ1_exc ⋅ Ψ̇
            # D[:derivative2][i] = Ψ1_exc2 ⋅ Ψ̇

            H = Hfromtemp(P.m,P.t,HtempS...)
            ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
            D[:enS][i] = ens[1]
            D[:enS2][i] = ens[2]


            D[:sdiS][i] = sdi_order(vecs[1],basisS,m)
            D[:sdiS2][i] = sdi_order(vecs[2],basisS,m)

            D[:polS][i] = polarization(complex.(vecs[1]),basisS,m)/m.L
            D[:polspinS][i] = polarization_spin(complex.(vecs[1]),basisS,m)/m.L

            H = Hfromtemp(mC1,P.t,HtempC1...)
            ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
            D[:enC1][i] = ens[1]

            D[:polC1][i] = polarization(complex.(vecs[1]),basisC1,m)/m.L
            D[:polspinC1][i] = polarization_spin(complex.(vecs[1]),basisC1,m)/m.L


            H = Hfromtemp(mC2,P.t,HtempC2...)
            ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
            D[:enC2][i] = ens[1]
            D[:polC2][i] = polarization(complex.(vecs[1]),basisC2,m)/m.L
            D[:polspinC2][i] = polarization_spin(complex.(vecs[1]),basisC2,m)/m.L



        # catch
        #     ens = [missing, missing, missing]
        #     println("exception $(P.t)") 
        #     D[:en0][i] = missing
        #     D[:en1][i] = missing
        #     D[:en2][i] = missing
        #     D[:enS][i] = missing
        #     D[:enC1][i] = missing
        #     D[:enC2][i] = missing

        # end
       
            D[:adpar][i] = (2π/m.T)/(D[:en1][i]-D[:en0][i])^2
        end

    end

    D[:qs] = (D[:qs1] + D[:qs2] + D[:qs3] + D[:qs4])[2:end] / 2
    D[:qsS] = (D[:qs1] + D[:qs2] - D[:qs3] - D[:qs4])[2:end] / 2

    D[:intgap] = D[:en1] - D[:en0]
    D[:intgap2] = D[:en2] - D[:en0]
    D[:intgap3] = D[:en3] - D[:en0]
    D[:chargegap] = (D[:enC1] .+ D[:enC2] .- 2 * D[:en0]) ./ 2
    D[:spingap] = D[:enS] .- D[:en0]

    return strdict(D)
end

function lanczos_current_tdvp(d::Dict; conserve_sz = true, dt = 0.1, einschwing = true)
    m = dict2model(d)
    D = resdict(d)
    ts = 0:dt:m.T-dt

    D[:scurr] = zeros(length(ts))
    D[:scurr2] = zeros(length(ts))
    D[:scurr3] = zeros(length(ts))
    D[:scurr4] = zeros(length(ts))
    D[:qs1] = zeros(length(ts)+1)
    D[:qs2] = zeros(length(ts)+1)
    D[:qs3] = zeros(length(ts)+1)
    D[:qs4] = zeros(length(ts)+1)
    D[:qs] = zeros(length(ts)+1)
    D[:qsS] = zeros(length(ts)+1)
    D[:ts] = ts
    D[:pol] = zeros(Union{Float64,Missing},length(ts))
    D[:en0] = zeros(Union{Float64,Missing},length(ts))
    D[:enS] = zeros(Union{Float64,Missing},length(ts))
    D[:enS2] = zeros(Union{Float64,Missing},length(ts))
    D[:enC1] = zeros(Union{Float64,Missing},length(ts))
    D[:enC2] = zeros(Union{Float64,Missing},length(ts))
    D[:en1] = zeros(Union{Float64,Missing},length(ts))
    D[:en2] = zeros(Union{Float64,Missing},length(ts))
    D[:en3] = zeros(Union{Float64,Missing},length(ts))
    D[:adpar] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_exc] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_exc2] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_exc3] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_dyn] = zeros(Union{Float64,Missing},length(ts))
    D[:sdiS] = zeros(Union{Float64,Missing},length(ts))
    D[:sdiS2] = zeros(Union{Float64,Missing},length(ts))

    sites = siteinds("Electron", D[:L];conserve_qns=true)

    state0, state1 = fillparticles(D[:N], D[:L]) # resume here.
    # state0_plus2, state1_plus2 = fillparticles(D[:N] + 2, D[:L])
    # state0_minus2, state1_minus2 = fillparticles(D[:N] - 2, D[:L])

    # state0_flip = fillparticles(D[:N], D[:L])[1]
    # if state0_flip[cld(D[:L], 2)] == "Dn"
    #     state0_flip[cld(D[:L], 2)] = "Up"
    # elseif state0_flip[cld(D[:L], 2)+1] == "Dn"
    #     state0_flip[cld(D[:L], 2)+1] = "Up"
    # end

    psi0 = 1 / sqrt(4) * (MPS(sites, state0) + MPS(sites, reverse(state0)) + MPS(sites, reverse(state1)) + MPS(sites, state1))
    # psi0_plus2 = 1 / sqrt(4) * (MPS(sites, state0_plus2) + MPS(sites, reverse(state0_plus2)) + MPS(sites, reverse(state1_plus2)) + MPS(sites, state1_plus2))
    # psi0_minus2 = 1 / sqrt(4) * (MPS(sites, state0_minus2) + MPS(sites, reverse(state0_minus2)) + MPS(sites, reverse(state1_minus2)) + MPS(sites, state1_minus2))
    # psi0_flip = 1 / sqrt(2) * (MPS(sites, state0_flip) + MPS(sites, reverse(state0_flip)))
        
    println(flux(psi0))
    # println(flux(psi0_plus2))
    # println(flux(psi0_minus2))
    # println(flux(psi0_flip))

    if D[:maxdim] == "increasing" D[:maxdim] = Int.([5,5,10,20,200]) end

    if einschwing == true
        H = mpo_ham(m, sites, -m.T/16)
        obs = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-9)
        sweeps = Sweeps(200)
        maxdim!(sweeps, D[:maxdim]...)
        # noise!(sweeps,1E-5)
        cutoff!(sweeps, 1E-8)
        en, psi = dmrg(H, psi0, sweeps, observer=obs)

        tdvp_temp!(psi,m,sites,dt,m.T,cutoff = 1e-8, maxdim = 20, exp_tol = 1e-9, t0 = -m.T/16)

        # @showprogress for (i,t) in enumerate(-m.T:dt:0-dt) #Einschwingvorgang
        #     println(i)
        #     H = mpo_ham(m, sites, t+dt/2)
        #     tdvp!(psi,H,dt,dt,cutoff = 1e-8, exp_tol = 1e-9)
        # end
    else
    ## calculate initial state
        H = mpo_ham(m, sites, 0)
        obs = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-5)
        sweeps = Sweeps(200)
        maxdim!(sweeps, D[:maxdim]...)
        # noise!(sweeps,1E-5)
        cutoff!(sweeps, 1E-8)

        en, psi = dmrg(H, psi0, sweeps, observer=obs)
    end

    @showprogress for (i,t) in enumerate(0:dt:m.T-dt) #Einschwingvorgang
        H = mpo_ham(m, sites, t+dt/2)
        tdvp!(psi,H,dt,dt,cutoff = 1e-4, exp_tol = 1e-3)
        currmat = correlation_matrix(psi,"Cdagup","Cup",site_range = 1:snake(3,m))
        D[:scurr][i] = real(1im*hopping(m,1,t)*currmat[snake(1,m),snake(2,m)]-1im*hopping(m,1,t)*currmat[snake(2,m),snake(1,m)]) #curr spin up link 1
        D[:scurr2][i] = real(1im*hopping(m,2,t)*currmat[snake(2,m),snake(3,m)]-1im*hopping(m,2,t)*currmat[snake(3,m),snake(2,m)]) #curr spin up link 2
        D[:qs1][i+1] = D[:qs1][i] + dt * D[:scurr][i]
        D[:qs2][i+1] = D[:qs2][i] + dt * D[:scurr2][i]
        currmat = correlation_matrix(psi,"Cdagdn","Cdn",site_range = 1:snake(3,m))
        D[:scurr3][i] = real(1im*hopping(m,1,t)*currmat[snake(1,m),snake(2,m)]-1im*hopping(m,1,t)*currmat[snake(2,m)snake(1,m)]) #curr spin dn link 1
        D[:scurr4][i] = real(1im*hopping(m,2,t)*currmat[snake(2,m),snake(3,m)]-1im*hopping(m,2,t)*currmat[snake(3,m),snake(2,m)]) #curr spin dn link 2     
        D[:qs3][i+1] = D[:qs3][i] + dt * D[:scurr3][i]
        D[:qs4][i+1] = D[:qs4][i] + dt * D[:scurr4][i]
        # D[:sdi_dyn][i] = sdi_order(psi,basis,m) #curr spin dn link 2
        D[:dup][i] = expect(psi,"Nup")[cld(m.L,2)]
    end

    D[:qs] = (D[:qs1] + D[:qs2] + D[:qs3] + D[:qs4]) / 2
    D[:qsS] = (D[:qs1] + D[:qs2] - D[:qs3] - D[:qs4]) / 2  

    return strdict(D)
end

function lanczos_current_mpsRK4(d::Dict; conserve_sz = true, dt = 0.1)
    m = dict2model(d)
    D = resdict(d)
    ts = 0:dt:m.T-dt

    D[:scurr] = zeros(length(ts))
    D[:scurr2] = zeros(length(ts))
    D[:scurr3] = zeros(length(ts))
    D[:scurr4] = zeros(length(ts))
    D[:qs1] = zeros(length(ts)+1)
    D[:qs2] = zeros(length(ts)+1)
    D[:qs3] = zeros(length(ts)+1)
    D[:qs4] = zeros(length(ts)+1)
    D[:qs] = zeros(length(ts)+1)
    D[:qsS] = zeros(length(ts)+1)
    D[:ts] = ts
    D[:pol] = zeros(Union{Float64,Missing},length(ts))
    D[:en0] = zeros(Union{Float64,Missing},length(ts))
    D[:enS] = zeros(Union{Float64,Missing},length(ts))
    D[:enC1] = zeros(Union{Float64,Missing},length(ts))
    D[:enC2] = zeros(Union{Float64,Missing},length(ts))
    D[:en1] = zeros(Union{Float64,Missing},length(ts))
    D[:en2] = zeros(Union{Float64,Missing},length(ts))
    D[:adpar] = zeros(Union{Float64,Missing},length(ts))
    D[:derivative] = zeros(Union{Float64,Missing},length(ts))
    D[:derivative2] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi_dyn] = zeros(Union{Float64,Missing},length(ts))
    D[:adiabaticity] = zeros(Union{Float64,Missing},length(ts))
    sites = siteinds("Electron", D[:L];conserve_qns=true)

    state0, state1 = fillparticles(D[:N], D[:L])
    # state0_plus2, state1_plus2 = fillparticles(D[:N] + 2, D[:L])
    # state0_minus2, state1_minus2 = fillparticles(D[:N] - 2, D[:L])

    # state0_flip = fillparticles(D[:N], D[:L])[1]
    # if state0_flip[cld(D[:L], 2)] == "Dn"
    #     state0_flip[cld(D[:L], 2)] = "Up"
    # elseif state0_flip[cld(D[:L], 2)+1] == "Dn"
    #     state0_flip[cld(D[:L], 2)+1] = "Up"
    # end

    psi0 = 1 / sqrt(4) * (MPS(sites, state0) + MPS(sites, reverse(state0)) + MPS(sites, reverse(state1)) + MPS(sites, state1))
    # psi0_plus2 = 1 / sqrt(4) * (MPS(sites, state0_plus2) + MPS(sites, reverse(state0_plus2)) + MPS(sites, reverse(state1_plus2)) + MPS(sites, state1_plus2))
    # psi0_minus2 = 1 / sqrt(4) * (MPS(sites, state0_minus2) + MPS(sites, reverse(state0_minus2)) + MPS(sites, reverse(state1_minus2)) + MPS(sites, state1_minus2))
    # psi0_flip = 1 / sqrt(2) * (MPS(sites, state0_flip) + MPS(sites, reverse(state0_flip)))
        
    println(flux(psi0))
    # println(flux(psi0_plus2))
    # println(flux(psi0_minus2))
    # println(flux(psi0_flip))

    if D[:maxdim] == "increasing" D[:maxdim] = Int.([5,5,10,20,200]) end


    ## calculate initial state
    H = mpo_ham(m, sites, 0)
    obs = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-5)
    sweeps = Sweeps(200)
    maxdim!(sweeps, D[:maxdim]...)
    # noise!(sweeps,1E-5)
    cutoff!(sweeps, 1E-8)

    en, psi = dmrg(H, psi0, sweeps, observer=obs)

    # nsteps = 100
    # tdvp!(psi0,H,-1im*dt,-1im*nsteps*dt;progress = true,exp_tol = 1e-6, cutoff = 1e-5, maxdim = 20)

    # return psi, psi0

    @showprogress "Einschwingvorgang" for (i,t) in enumerate(0:dt:D[:T]-dt) #Einschwingvorgang
        H = mpo_ham(m, sites, t)
        k1 = noprime(-1im*contract(H,psi,cutoff = 1e-6))
        k2 = noprime(-1im*contract(H,psi+dt/2 *k1,cutoff = 1e-6))
        k3 = noprime(-1im*contract(H,psi+dt/2 *k2,cutoff = 1e-6))
        k4 = noprime(-1im*contract(H,psi+dt*k3,cutoff = 1e-6))

        psi = psi + dt/6 * (k1 + 2*k2 +2*k3 + k4)
    end

    @showprogress "Messung" for (i,t) in enumerate(m.T:dt:2*m.T-dt) #Einschwingvorgang
        H = mpo_ham(m, sites, t)
        k1 = noprime(-1im*contract(H,psi,cutoff = 1e-6))
        k2 = noprime(-1im*contract(H,psi+dt/2 *k1,cutoff = 1e-6))
        k3 = noprime(-1im*contract(H,psi+dt/2 *k2,cutoff = 1e-6))
        k4 = noprime(-1im*contract(H,psi+dt*k3,cutoff = 1e-6))

        psi = psi + dt/6 * (k1 + 2*k2 +2*k3 + k4)

        currmat = correlation_matrix(psi,"Cdagup","Cup",site_range = 1:snake(3,m))
        D[:scurr][i] = real(1im*hopping(m,1,t)*currmat[snake(1,m),snake(2,m)]-1im*hopping(m,1,t)*currmat[snake(2,m),snake(1,m)]) #curr spin up link 1
        D[:scurr2][i] = real(1im*hopping(m,2,t)*currmat[snake(2,m),snake(3,m)]-1im*hopping(m,2,t)*currmat[snake(3,m),snake(2,m)]) #curr spin up link 2
        D[:qs1][i+1] = D[:qs1][i] + dt * D[:scurr][i]
        D[:qs2][i+1] = D[:qs2][i] + dt * D[:scurr2][i]
        currmat = correlation_matrix(psi,"Cdagdn","Cdn",site_range = 1:snake(3,m))
        D[:scurr3][i] = real(1im*hopping(m,1,t)*currmat[snake(1,m),snake(2,m)]-1im*hopping(m,1,t)*currmat[snake(2,m)snake(1,m)]) #curr spin dn link 1
        D[:scurr4][i] = real(1im*hopping(m,2,t)*currmat[snake(2,m),snake(3,m)]-1im*hopping(m,2,t)*currmat[snake(3,m),snake(2,m)]) #curr spin dn link 2     
        D[:qs3][i+1] = D[:qs3][i] + dt * D[:scurr3][i]
        D[:qs4][i+1] = D[:qs4][i] + dt * D[:scurr4][i]
        # D[:sdi_dyn][i] = sdi_order(psi,basis,m) #curr spin dn link 2
    end

    # @showprogress "Messung" for (i,t) in enumerate(m.T:P.dt:2*m.T-P.dt) #Messung
    #     ttmp = P.t
    #     H = Hfromtemp(P.m,P.t+P.dt/2,Htemp...)
    #     P.Ψ, info = exponentiate(H,-1im * P.dt,P.Ψ, tol=1e-9)
    #     P.t += P.dt
    #     D[:scurr][i] = real(localcurrent(P.t, P.Ψ, 1, 2, basis, m)) #curr spin up link 1
    #     D[:scurr2][i] = real(localcurrent(P.t, P.Ψ, 2, 3, basis, m)) #curr spin up link 2
    #     D[:scurr3][i] = real(localcurrent(P.t, P.Ψ, 1+m.L, 2+m.L, basis, m)) #curr spin dn link 1
    #     D[:scurr4][i] = real(localcurrent(P.t, P.Ψ, 2+m.L, m.L+3, basis, m)) #curr spin dn link 2
    #     D[:qs1][i+1] = D[:qs1][i] + (P.t - ttmp) * D[:scurr][i]
    #     D[:qs2][i+1] = D[:qs2][i] + (P.t - ttmp) * D[:scurr2][i]
    #     D[:qs3][i+1] = D[:qs3][i] + (P.t - ttmp) * D[:scurr3][i]
    #     D[:qs4][i+1] = D[:qs4][i] + (P.t - ttmp) * D[:scurr4][i]
    #     D[:sdi_dyn][i] = sdi_order(P.Ψ,basis,m) #curr spin dn link 2

        
    #     try
    #         H = Hfromtemp(P.m,P.t,Htemp...)
    #         ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:3]
    #         D[:en0][i] = ens[1]
    #         D[:en1][i] = ens[2]
    #         D[:en2][i] = ens[3]
    #         D[:pol][i] = polarization(complex.(vecs[1]),basis,m)/m.L
    #         Ψ1 = complex.(vecs[1])
    #         Ψ1_exc = complex.(vecs[2])
    #         Ψ1_exc2 = complex.(vecs[2])

    #         D[:sdi][i] = sdi_order(Ψ1,basis,m)
    #         D[:adiabaticity][i] = norm(Ψ1 ⋅ P.Ψ)


    #         H = Hfromtemp(P.m,P.t+P.dt,Htemp...)
    #         Ψ2 = complex.(eigsolve(H, 3, :SR)[2])[1]

    #         Ψ̇ = ∂(Ψ1,Ψ2,P.t,P.t+P.dt)

    #         D[:derivative][i] = Ψ1_exc ⋅ Ψ̇
    #         D[:derivative2][i] = Ψ1_exc2 ⋅ Ψ̇

    #         H = Hfromtemp(P.m,P.t,HtempS...)
    #         ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
    #         D[:enS][i] = ens[1]

    #         H = Hfromtemp(P.m,P.t,HtempC1...)
    #         ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
    #         D[:enC1][i] = ens[1]

    #         H = Hfromtemp(P.m,P.t,HtempC2...)
    #         ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
    #         D[:enC2][i] = ens[1]


    #     catch
    #         ens = [missing, missing, missing]
    #         println("exception $(P.t)") 
    #         D[:en0][i] = missing
    #         D[:en1][i] = missing
    #         D[:en2][i] = missing
    #         D[:enS][i] = missing
    #         D[:enC1][i] = missing
    #         D[:enC2][i] = missing

    #     end
       
    #     D[:adpar][i] = (2π/m.T)/(D[:en1][i]-D[:en0][i])^2
    # end

    D[:qs] = (D[:qs1] + D[:qs2] + D[:qs3] + D[:qs4]) / 2
    D[:qsS] = (D[:qs1] + D[:qs2] - D[:qs3] - D[:qs4]) / 2

    # D[:intgap] = D[:en1] - D[:en0]
    # D[:chargegap] = (D[:enC1] .+ D[:enC2] .- 2 * D[:en0]) ./ 2
    # D[:spingap] = D[:enS] .- D[:en0]



    

    return strdict(D)
end


function lanczos_pol_kryl(d::Dict; conserve_sz = true, dt = 0.1)
    m = dict2model(d)
    mC1 = dict2model(d,N = d[:L]+2)
    mC2 = dict2model(d,N = d[:L]-2)
    D = resdict(m)
    P = LPropagation(m=m, dt=dt, ϵ=1e-9, Λ=200)

    if conserve_sz == true
        basis = generatefixedSzstates(m, Sz = 0)
        basisS = generatefixedSzstates(m, Sz = 1)
        basisC1 = generatefixedSzstates(mC1, Sz = 0)
        basisC2 = generatefixedSzstates(mC2, Sz = 0)
    else
        basis = generatefixednstates(m)
    end
    Htemp = sparsehamiltonian_template(m, basis)
    HtempS = sparsehamiltonian_template(m, basisS)
    HtempC1 = sparsehamiltonian_template(m, basisC1)
    HtempC2 = sparsehamiltonian_template(m, basisC2)


    H = Hfromtemp(P.m,0,Htemp...)
    P.Ψ = complex.(eigsolve(H, 3, :SR)[2])[1]
    # HΛ, Φs, ens = lanczosenergies(m, 0,100, basis, Htemp, tol = 1e-6,  energynum=1)
    # P.Ψ = lanczosgroundstate(HΛ, Φs)

    ts = 0:P.dt:m.T-P.dt
    D[:scurr] = zeros(length(ts))
    D[:scurr2] = zeros(length(ts))
    D[:scurr3] = zeros(length(ts))
    D[:scurr4] = zeros(length(ts))
    D[:ts] = ts
    D[:pol] = zeros(Union{Float64,Missing},length(ts))
    D[:en0] = zeros(Union{Float64,Missing},length(ts))
    D[:enS] = zeros(Union{Float64,Missing},length(ts))
    D[:enC1] = zeros(Union{Float64,Missing},length(ts))
    D[:enC2] = zeros(Union{Float64,Missing},length(ts))
    D[:en1] = zeros(Union{Float64,Missing},length(ts))
    D[:en2] = zeros(Union{Float64,Missing},length(ts))
    D[:adpar] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi] = zeros(Union{Float64,Missing},length(ts))

    @showprogress "Messung" for (i,t) in enumerate(m.T:P.dt:2*m.T-P.dt) #Messung

        
        
        H = Hfromtemp(m,t,Htemp...)
        ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:3]
        D[:en0][i] = ens[1]
        D[:en1][i] = ens[2]
        D[:en2][i] = ens[3]
        D[:pol][i] = polarization(complex.(vecs[1]),basis,m)/m.L
        Ψ1 = complex.(vecs[1])
        Ψ1_exc = complex.(vecs[2])
        Ψ1_exc2 = complex.(vecs[2])

        D[:sdi][i] = sdi_order(Ψ1,basis,m)


        H = Hfromtemp(P.m,P.t+P.dt,Htemp...)
        Ψ2 = complex.(eigsolve(H, 3, :SR)[2])[1]

        H = Hfromtemp(P.m,P.t,HtempS...)
        ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
        D[:enS][i] = ens[1]

        H = Hfromtemp(P.m,P.t,HtempC1...)
        ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
        D[:enC1][i] = ens[1]

        H = Hfromtemp(P.m,P.t,HtempC2...)
        ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
        D[:enC2][i] = ens[1]

    end

    D[:intgap] = D[:en1] - D[:en0]
    D[:chargegap] = (D[:enC1] .+ D[:enC2] .- 2 * D[:en0]) ./ 2
    D[:spingap] = D[:enS] .- D[:en0]


    return strdict(D)
end

function lanczos_tower_of_states(d::Dict; conserve_sz = true, dt = 0.01)
    m = dict2model(d)
    mC1 = dict2model(d,N = d[:L]+2)
    mC2 = dict2model(d,N = d[:L]-2)
    D = resdict(m)
    P = LPropagation(m=m, dt=dt, ϵ=1e-9, Λ=200)

    if conserve_sz == true
        basis = generatefixedSzstates(m, Sz = 0)
        basisS = generatefixedSzstates(m, Sz = 1)
        basisC1 = generatefixedSzstates(mC1, Sz = 0)
        basisC2 = generatefixedSzstates(mC2, Sz = 0)
    else
        basis = generatefixednstates(m)
    end
    Htemp = sparsehamiltonian_template(m, basis)
    HtempS = sparsehamiltonian_template(m, basisS)
    HtempC1 = sparsehamiltonian_template(m, basisC1)
    HtempC2 = sparsehamiltonian_template(m, basisC2)


    H = Hfromtemp(P.m,0,Htemp...)
    P.Ψ = complex.(eigsolve(H, 3, :SR)[2])[1]
    # HΛ, Φs, ens = lanczosenergies(m, 0,100, basis, Htemp, tol = 1e-6,  energynum=1)
    # P.Ψ = lanczosgroundstate(HΛ, Φs)

    ts = 0:P.dt:m.T-P.dt
  
    D[:ts] = ts
    D[:en] = zeros(Union{Float64,Missing},50,length(ts))
    D[:enS] = zeros(Union{Float64,Missing},50,length(ts))
    D[:enC1] = zeros(Union{Float64,Missing},length(ts))
    D[:enC2] = zeros(Union{Float64,Missing},length(ts))
    D[:adpar] = zeros(Union{Float64,Missing},length(ts))
    D[:sdi] = zeros(Union{Float64,Missing},length(ts))

    @showprogress "Messung" for (i,t) in enumerate(m.T:P.dt:2*m.T-P.dt) #Messung

        H = Hfromtemp(m,t,Htemp...)
        ens, vecs, info = eigsolve(H, 50, :SR, krylovdim = 100, tol = 1e-13)
        D[:en][:,i] .= ens[1:50]
        if info.converged < 50
            error("could not converge all 50 states")
        end

        H = Hfromtemp(m,t,HtempS...)
        ens, vecs, info = eigsolve(H, 50, :SR, krylovdim = 100, tol = 1e-13)
        D[:enS][:,i] .= ens[1:50]
        if info.converged < 50
            error("could not converge all 50 states")
        end
    end

    return strdict(D)
end


function lanczos_current_pol(d::Dict; conserve_sz = false)
    m = dict2model(d)
    D = resdict(m)
    P = LPropagation(m=m, dt=0.1, ϵ=1e-9, Λ=200)

    if conserve_sz == true
        basis = generatefixedSzstates(m, Sz = 0)
    else
        basis = generatefixednstates(m)
    end
    Htemp = sparsehamiltonian_template(m, basis)
    H = Hfromtemp(P.m,0,Htemp...)
    P.Ψ = complex.(eigsolve(H, 3, :SR)[2])[1]
    # HΛ, Φs, ens = lanczosenergies(m, 0,100, basis, Htemp, tol = 1e-6,  energynum=1)
    # P.Ψ = lanczosgroundstate(HΛ, Φs)

    D[:scurr] = []
    D[:scurr2] = []
    D[:qs1] = [0.]
    D[:qs2] = [0.]
    D[:ts] = []
    D[:pol] = []

    @showprogress for t in 0:P.dt:m.T-P.dt
        ttmp = P.t
        H = Hfromtemp(P.m,P.t,Htemp...)
        P.Ψ = complex.(eigsolve(H, 3, :SR)[2])[1]
        P.t += P.dt
        push!(D[:scurr], real(localcurrent(P.t, P.Ψ, 1, 2, basis, m)))
        push!(D[:scurr2], real(localcurrent(P.t, P.Ψ, 2, 3, basis, m)))
        push!(D[:qs1], D[:qs1][end] + (P.t - ttmp) * D[:scurr][end])
        push!(D[:qs2], D[:qs2][end] + (P.t - ttmp) * D[:scurr2][end])
        push!(D[:ts], P.t)
        push!(D[:pol],polarization(P.Ψ,basis,m)/m.L)
    end

    

    return strdict(D)
end

function lanczos_pol_pbc(d,m::IntModel1dSpinPbc)
    D = resdict(d, pops = [:δ,:Δ])
    D[:pol] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en0] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en1] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en2] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:enS] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:enC1] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:enC2] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:enC1new] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:enC2new] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en1] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en2] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    m = dict2model(d, Δ = 1, δ = 0.5) #arbitrary
    mC1 = dict2model(d,N = d[:L]+2)
    mC2 = dict2model(d,N = d[:L]-2)
    # basis = generatefixednstates(m)
    basis = generatefixedSzstates(m, Sz = 0)
    basisS = generatefixedSzstates(m, Sz = 1)
    basisC1 = generatefixedSzstates(mC1, Sz = 0)
    basisC2 = generatefixedSzstates(mC2, Sz = 0)
    Htemp = sparsehamiltonian_template(m, basis)
    HtempS = sparsehamiltonian_template(m, basisS)
    HtempC1 = sparsehamiltonian_template(m, basisC1)
    HtempC2 = sparsehamiltonian_template(m, basisC2)
    @showprogress for (i,Δ) in enumerate(d[:Δs]), (j,δ) in enumerate(d[:δs])
        m = dict2model(d, Δ = Δ, δ = δ)
        mC1 = dict2model(d,N = d[:L]+2,Δ = Δ, δ = δ)
        mC2 = dict2model(d,N = d[:L]-2,Δ = Δ, δ = δ)

        H = Hfromtemp(m,Htemp...)
        
        vals, vecs, info = eigsolve(H, 3, :SR, tol = 1e-9)
        D[:en0][i,j] = vals[1]
        D[:en1][i,j] = vals[2]
        D[:en2][i,j] = vals[3]

        Ψ = complex.(vecs[1])
        D[:pol][i,j] = polarization(Ψ, basis, m)/m.L

        H = Hfromtemp(m,HtempS...)
        ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
        D[:enS][i,j] = ens[1]

        H = Hfromtemp(mC1,HtempC1...)
        ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
        D[:enC1][i,j] = ens[1]

        H = Hfromtemp(mC2,HtempC2...)
        ens, vecs = eigsolve(H, 3, :SR, tol = 1e-9)[1:2]
        D[:enC2][i,j] = ens[1]        
     
    end


    D[:intgap] = D[:en1] - D[:en0]
    D[:intgap2] = D[:en2] - D[:en0]
    D[:chargegap] = (D[:enC1] .+ D[:enC2] .- 2 * D[:en0]) ./ 2
    D[:spingap] = D[:enS] .- D[:en0]

    return strdict(D)
end

function lanczos_pol_only_pbc(d,m::IntModel1dSpinPbc)
    D = resdict(d, pops = [:δ,:Δ])
    D[:pol] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:polS] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en0] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en1] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en2] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))

    m = dict2model(d, Δ = 1, δ = 0.5) #arbitrary
    # basis = generatefixednstates(m)
    basis = generatefixedSzstates(m, Sz = 0)
    Htemp = sparsehamiltonian_template(m, basis)
    @showprogress for (i,Δ) in enumerate(d[:Δs]), (j,δ) in enumerate(d[:δs])
        m = dict2model(d, Δ = Δ, δ = δ)

        H = Hfromtemp(m,Htemp...)
        
        vals, vecs, info = eigsolve(H, 3, :SR, tol = 1e-9)
        D[:en0][i,j] = vals[1]
        D[:en1][i,j] = vals[2]
        D[:en2][i,j] = vals[3]

        Ψ = complex.(vecs[1])
        D[:pol][i,j] = polarization(Ψ, basis, m)/m.L
        D[:polS][i,j] = polarization_spin(Ψ, basis, m)/m.L

     
    end


    D[:intgap] = D[:en1] - D[:en0]
    D[:intgap2] = D[:en2] - D[:en0]
    return strdict(D)
end


function lanczos_pol(Vso)
    m = IntRmSpinObc(L=6, U=0, T=1, Vso=Vso)
    basis = generatefixednstates(m)
    H = sparsehamiltonian_template(m, basis)
    HΛ, Φs, ens = lanczosenergies(m, 100, basis, H)
    Ψ = lanczosgroundstate(HΛ, Φs)

    ts = 0:0.01:1
    pol = []

    @showprogress for t in ts
        HΛ, Φs, tmpen = lanczosenergies(m, 100, basis, H, t=t)
        push!(pol, polarization(lanczosgroundstate(HΛ, Φs), basis, m))
    end

    return ts, pol
end

function mps_com(d::Dict)
    m = dict2model(d)
    D = resdict(m)

    sites = siteinds("Electron", m.L;conserve_qns=true)

    ts = 0:0.01:1
    pol1 = similar(ts)
    pol2 = similar(ts)
    res1 = []
    res2 = []

    state = [isodd(n) ? "Emp" : "UpDn"  for n = 1:m.L]
    state2 = [isodd(n) ? "UpDn" : "Emp"  for n = 1:m.L]

    psi0 = randomMPS(sites, state)
    psi02 = randomMPS(sites, state2)
    @showprogress for (tind, t) in enumerate(ts)
        H = mpo_ham(m, sites, t)

        

        sweeps = Sweeps(7)
        maxdim!(sweeps, 5, 5, 5, 5, 50, 100, 500)
        cutoff!(sweeps, 1E-12)

        en, psi = dmrg(H, psi0 + psi02, sweeps)

        dnu = zeros(m.L)
        dnd = zeros(m.L)

        for j = 1:m.L
            orthogonalize!(psi, j)
            psidag_j = dag(prime(psi[j], "Site"))
            dnu[j] = scalar(psidag_j * op(sites, "Nup", j) * psi[j])
            dnd[j] = scalar(psidag_j * op(sites, "Ndn", j) * psi[j])
        end
        push!(res1, dnu)
        push!(res2, dnd)


        pol1[tind] = sum([(j - m.L / 2) * dnu[j]] for j in 1:m.L)[1] / m.L
        pol2[tind] = sum([(j - m.L / 2) * dnd[j]] for j in 1:m.L)[1] / m.L
    end

    D[:polup] = pol1
    D[:poldn] = pol2
    D[:ts] = ts

    return strdict(D)

end

function mps_sweeps_ionic_hubbard(d::Dict)
    m = dict2model(d)
    D = resdict(d)
    
    
    ts = 0:0.1:1
    D[:polup] = similar(ts)
    D[:poldn] = similar(ts)
    D[:dup] = []
    D[:ddn] = []
    D[:cSz] = []
    D[:cNup] = []
    D[:cNdn] = []
    D[:cNupdn] = []
    D[:en] = []
    D[:obsSz] = []
    D[:obsNup] = []
    D[:obsNdn] = []
    D[:var] = []

    sites = siteinds("Electron", m.L;conserve_qns=true)
  

    state = ["Emp" for i in 1:D[:L]]
    for i in 1:D[:N] ## fill particles depending on L and N.
        if i <= D[:L]
            state[i] = isodd(i) ? "Up" : "Dn"
        else
            state[i - D[:L]] = "UpDn"
        end
    end
        
    psi = randomMPS(sites, state, 10)

    if D[:maxdim] == "increasing" D[:maxdim] = Int.([10,20,50,100,200]) end

    @showprogress for (tind, t) in enumerate(ts)
        H = mpo_ham(m, sites, t)
        obs = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-9)

        sweeps = Sweeps(200)
        maxdim!(sweeps, D[:maxdim]...)
        # noise!(sweeps,1E-5)
        cutoff!(sweeps, 1E-8)

        en, psi = dmrg(H, truncate(psi, maxdim=10), sweeps, observer=obs)
     
        push!(D[:en], en)
        push!(D[:dup], expect(psi, "Nup"))
        push!(D[:ddn], expect(psi, "Ndn"))
        push!(D[:cSz], correlation_matrix(psi, "Sz", "Sz"))
        push!(D[:cNup], correlation_matrix(psi, "Nup", "Nup"))
        push!(D[:cNdn], correlation_matrix(psi, "Ndn", "Ndn"))
        push!(D[:cNupdn], correlation_matrix(psi, "Nup", "Ndn"))
        push!(D[:obsSz], measurements(obs)["Sz"])
        push!(D[:obsNup], measurements(obs)["Nup"])
        push!(D[:obsNdn], measurements(obs)["Ndn"])


        D[:polup][tind] = sum([(j - m.L / 2) * D[:dup][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn][tind] = sum([(j - m.L / 2) * D[:ddn][tind][j]] for j in 1:m.L)[1] / m.L

       
        push!(D[:var], variance(H,psi,en))
    end
    D[:ts] = ts

    return strdict(D)

end

function mps_ionic_hubbard2(d::Dict) # δ sweeps
    D = resdict(d, pops = [:δ,:Δ])

    D[:en0] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en1] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:en2] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:enS] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:enC1] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:enC2] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:var0] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:var1] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:var2] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:varS] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:varC1] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))
    D[:varC2] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),length(d[:δs]))

    D[:dup] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),d[:L])
    D[:ddn] = Array{Union{Float64,Nothing}}(UndefInitializer(),length(d[:Δs]),d[:L])

    m = dict2model(d, Δ = 1, δ = 0.5) #arbitrary

    sites = siteinds("Electron", D[:L];conserve_qns=true)

    state0, state1 = fillparticles(D[:N], D[:L]) # resume here.
    state0_plus2, state1_plus2 = fillparticles(D[:N] + 2, D[:L])
    state0_minus2, state1_minus2 = fillparticles(D[:N] - 2, D[:L])

    state0_flip = fillparticles(D[:N], D[:L])[1]
    if state0_flip[cld(D[:L], 2)] == "Dn"
        state0_flip[cld(D[:L], 2)] = "Up"
    elseif state0_flip[cld(D[:L], 2)+1] == "Dn"
        state0_flip[cld(D[:L], 2)+1] = "Up"
    end


        
    # psi0 = 1 / sqrt(2) * (MPS(sites, state0) + MPS(sites, reverse(state0)))
    # psi0_plus2 = 1 / sqrt(2) * (MPS(sites, state0_plus2) + MPS(sites, reverse(state0_plus2)))
    # psi0_minus2 = 1 / sqrt(2) * (MPS(sites, state0_minus2) + MPS(sites, reverse(state0_minus2)))

    # psi0 = 1 / sqrt(4) * (MPS(sites, state0) + MPS(sites, reverse(state0)) + MPS(sites, reverse(state1)) + MPS(sites, state1))
    # psi0_plus2 = 1 / sqrt(4) * (MPS(sites, state0_plus2) + MPS(sites, reverse(state0_plus2)) + MPS(sites, reverse(state1_plus2)) + MPS(sites, state1_plus2))
    # psi0_minus2 = 1 / sqrt(4) * (MPS(sites, state0_minus2) + MPS(sites, reverse(state0_minus2)) + MPS(sites, reverse(state1_minus2)) + MPS(sites, state1_minus2))
    # psi0_flip = 1 / sqrt(2) * (MPS(sites, state0_flip) + MPS(sites, reverse(state0_flip)))

    psi0 = randomMPS(sites, state0, linkdims = 5)
    psi0_plus2 = randomMPS(sites, state0_plus2, linkdims = 5)
    psi0_minus2 = randomMPS(sites, state0_minus2, linkdims = 5)
    psi0_flip = randomMPS(sites, state0_flip, linkdims = 5)

        
    println(flux(psi0))
    println(flux(psi0_plus2))
    println(flux(psi0_minus2))
    println(flux(psi0_flip))


    D[:maxdim] = Int.([5,5,5,10,20,40,80,D[:maxdim]])

    @showprogress for (i,Δ) in enumerate(d[:Δs]), (j,δ) in enumerate(d[:δs])
        m = dict2model(D, δ=δ, Δ=Δ)
        H = mpo_ham(m, sites)

        sweeps = Sweeps(200)
        maxdim!(sweeps, D[:maxdim]...)
        # noise!(sweeps,1E-5)
        cutoff!(sweeps, 1E-9)

        D[:en0][i,j], psi = dmrg(H, psi0, sweeps, observer=DMRGObserver(energy_tol=1E-5, minsweeps = 20))
        D[:en1][i,j], psi2 = dmrg(H, [psi], psi0, sweeps, observer=DMRGObserver(energy_tol=1E-5, minsweeps = 20))
        D[:en2][i,j], psi3 = dmrg(H, [psi,psi2], psi0, sweeps, observer=DMRGObserver(energy_tol=1E-5, minsweeps = 20))

        D[:enC1][i,j], psi_plus2 = dmrg(H, psi0_plus2, sweeps, observer=DMRGObserver(energy_tol=1E-5, minsweeps = 20))
        D[:enC2][i,j], psi_minus2 = dmrg(H, psi0_minus2, sweeps, observer=DMRGObserver(energy_tol=1E-5, minsweeps = 20))
        D[:enS][i,j], psi_flip = dmrg(H, psi0_flip, sweeps, observer=DMRGObserver(energy_tol=1E-5, minsweeps = 20))

     
        D[:dup][i,:] = expect(psi, "Nup")
        D[:ddn][i,:] = expect(psi, "Ndn")
        # cSz = correlation_matrix(psi, "Sz", "Sz")
        # cNup = correlation_matrix(psi, "Nup", "Nup")
        # cNdn =  correlation_matrix(psi, "Ndn", "Ndn")
        # cNupdn = correlation_matrix(psi, "Nup", "Ndn")
        # push!(D[:cSz], sum([cSz[i,i + 1] for i in 1:2:(size(cSz, 1) - 1)]) - sum([cSz[i + 1,i + 2] for i in 1:2:size(cSz, 1) - 2]))
        # push!(D[:cNup], sum([cNup[i,i + 1] for i in 1:2:(size(cNup, 1) - 1)]) - sum([cNup[i + 1,i + 2] for i in 1:2:size(cNup, 1) - 2]))
        # push!(D[:cNdn], sum([cNdn[i,i + 1] for i in 1:2:(size(cNdn, 1) - 1)]) - sum([cNdn[i + 1,i + 2] for i in 1:2:size(cNdn, 1) - 2]))
        # push!(D[:cNupdn], sum([cNupdn[i,i + 1] for i in 1:2:(size(cNupdn, 1) - 1)]) - sum([cNupdn[i + 1,i + 2] for i in 1:2:size(cNupdn, 1) - 2]))

        # push!(D[:en_plus2], en_plus2)
        # push!(D[:dup_plus2], expect(psi_plus2, "Nup"))
        # push!(D[:ddn_plus2], expect(psi_plus2, "Ndn"))
        # cSz_plus2 = correlation_matrix(psi_plus2, "Sz", "Sz")
        # cNup_plus2 = correlation_matrix(psi_plus2, "Nup", "Nup")
        # cNdn_plus2 =  correlation_matrix(psi_plus2, "Ndn", "Ndn")
        # cNupdn_plus2 = correlation_matrix(psi_plus2, "Nup", "Ndn")
        # push!(D[:cSz_plus2], sum([cSz_plus2[i,i + 1] for i in 1:2:(size(cSz_plus2, 1) - 1)]) - sum([cSz_plus2[i + 1,i + 2] for i in 1:2:size(cSz_plus2, 1) - 2]))
        # push!(D[:cNup_plus2], sum([cNup_plus2[i,i + 1] for i in 1:2:(size(cNup_plus2, 1) - 1)]) - sum([cNup_plus2[i + 1,i + 2] for i in 1:2:size(cNup_plus2, 1) - 2]))
        # push!(D[:cNdn_plus2], sum([cNdn_plus2[i,i + 1] for i in 1:2:(size(cNdn_plus2, 1) - 1)]) - sum([cNdn_plus2[i + 1,i + 2] for i in 1:2:size(cNdn_plus2, 1) - 2]))
        # push!(D[:cNupdn_plus2], sum([cNupdn_plus2[i,i + 1] for i in 1:2:(size(cNupdn_plus2, 1) - 1)]) - sum([cNupdn_plus2[i + 1,i + 2] for i in 1:2:size(cNupdn_plus2, 1) - 2]))

        # push!(D[:en_minus2], en_minus2)
        # push!(D[:dup_minus2], expect(psi_minus2, "Nup"))
        # push!(D[:ddn_minus2], expect(psi_minus2, "Ndn"))
        # cSz_minus2 = correlation_matrix(psi_minus2, "Sz", "Sz")
        # cNup_minus2 = correlation_matrix(psi_minus2, "Nup", "Nup")
        # cNdn_minus2 =  correlation_matrix(psi_minus2, "Ndn", "Ndn")
        # cNupdn_minus2 = correlation_matrix(psi_minus2, "Nup", "Ndn")
        # push!(D[:cSz_minus2], sum([cSz_minus2[i,i + 1] for i in 1:2:(size(cSz_minus2, 1) - 1)]) - sum([cSz_minus2[i + 1,i + 2] for i in 1:2:size(cSz_minus2, 1) - 2]))
        # push!(D[:cNup_minus2], sum([cNup_minus2[i,i + 1] for i in 1:2:(size(cNup_minus2, 1) - 1)]) - sum([cNup_minus2[i + 1,i + 2] for i in 1:2:size(cNup_minus2, 1) - 2]))
        # push!(D[:cNdn_minus2], sum([cNdn_minus2[i,i + 1] for i in 1:2:(size(cNdn_minus2, 1) - 1)]) - sum([cNdn_minus2[i + 1,i + 2] for i in 1:2:size(cNdn_minus2, 1) - 2]))
        # push!(D[:cNupdn_minus2], sum([cNupdn_minus2[i,i + 1] for i in 1:2:(size(cNupdn_minus2, 1) - 1)]) - sum([cNupdn_minus2[i + 1,i + 2] for i in 1:2:size(cNupdn_minus2, 1) - 2]))

        # push!(D[:en_flip], en_flip)
        # push!(D[:dup_flip], expect(psi_flip, "Nup"))
        # push!(D[:ddn_flip], expect(psi_flip, "Ndn"))
        # cSz_flip = correlation_matrix(psi_flip, "Sz", "Sz")
        # cNup_flip = correlation_matrix(psi_flip, "Nup", "Nup")
        # cNdn_flip =  correlation_matrix(psi_flip, "Ndn", "Ndn")
        # cNupdn_flip = correlation_matrix(psi_flip, "Nup", "Ndn")
        # push!(D[:cSz_flip], sum([cSz_flip[i,i + 1] for i in 1:2:(size(cSz_flip, 1) - 1)]) - sum([cSz_flip[i + 1,i + 2] for i in 1:2:size(cSz_flip, 1) - 2]))
        # push!(D[:cNup_flip], sum([cNup_flip[i,i + 1] for i in 1:2:(size(cNup_flip, 1) - 1)]) - sum([cNup_flip[i + 1,i + 2] for i in 1:2:size(cNup_flip, 1) - 2]))
        # push!(D[:cNdn_flip], sum([cNdn_flip[i,i + 1] for i in 1:2:(size(cNdn_flip, 1) - 1)]) - sum([cNdn_flip[i + 1,i + 2] for i in 1:2:size(cNdn_flip, 1) - 2]))
        # push!(D[:cNupdn_flip], sum([cNupdn_flip[i,i + 1] for i in 1:2:(size(cNupdn_flip, 1) - 1)]) - sum([cNupdn_flip[i + 1,i + 2] for i in 1:2:size(cNupdn_flip, 1) - 2]))


        # D[:polup][δind] = sum([(j - m.L / 2) * D[:dup][δind][j]] for j in 1:m.L)[1] / m.L
        # D[:poldn][δind] = sum([(j - m.L / 2) * D[:ddn][δind][j]] for j in 1:m.L)[1] / m.L

        # D[:polup_plus2][δind] = sum([(j - m.L / 2) * D[:dup_plus2][δind][j]] for j in 1:m.L)[1] / m.L
        # D[:poldn_plus2][δind] = sum([(j - m.L / 2) * D[:ddn_plus2][δind][j]] for j in 1:m.L)[1] / m.L

        # D[:polup_minus2][δind] = sum([(j - m.L / 2) * D[:dup_minus2][δind][j]] for j in 1:m.L)[1] / m.L
        # D[:poldn_minus2][δind] = sum([(j - m.L / 2) * D[:ddn_minus2][δind][j]] for j in 1:m.L)[1] / m.L

        # D[:polup_flip][δind] = sum([(j - m.L / 2) * D[:dup_flip][δind][j]] for j in 1:m.L)[1] / m.L
        # D[:poldn_flip][δind] = sum([(j - m.L / 2) * D[:ddn_flip][δind][j]] for j in 1:m.L)[1] / m.L

        D[:var0][i,j] = variance(H,psi,D[:en0][i,j])
        D[:var1][i,j] = variance(H,psi2,D[:en1][i,j])
        D[:var2][i,j] = variance(H,psi3,D[:en2][i,j])
        D[:varC1][i,j] = variance(H,psi_plus2,D[:enC1][i,j])
        D[:varC2][i,j] = variance(H,psi_minus2,D[:enC2][i,j])
        D[:varS][i,j] = variance(H,psi_flip,D[:enS][i,j])



    end

    D[:intgap] = D[:en1] .- D[:en0]
    D[:intgap2] = D[:en2] .- D[:en0]
    D[:chargegap] = (D[:enC1] .+ D[:enC2] .- 2 * D[:en0]) ./ 2
    D[:intgapobc] = D[:enC1] .- D[:enC2]
    D[:spingap] = D[:enS] .- D[:en0]

    return strdict(D)

end

function mps_ionic_hubbard2_lanczos(d::Dict) # δ sweeps
    D = resdict(d, ignores=[:filename, :δ], pops=[:δ])

    D[:polup] = similar(D[:δs])
    D[:poldn] = similar(D[:δs])
    D[:polup_exc] = similar(D[:δs])
    D[:poldn_exc] = similar(D[:δs])
    D[:dup] = []
    D[:ddn] = []
    D[:dup_exc] = []
    D[:ddn_exc] = []
    # D[:cSz] = []
    # D[:cNup] = []
    # D[:cNdn] = []
    # D[:cNupdn] = []
    D[:en] = []
    D[:en_exc] = []
    # D[:var] = []

    m = dict2model(D, δ=0)
    basis = generatefixedSzstates(m, Sz=0)
    Htemp = sparsehamiltonian_template(m, basis)
    @showprogress for (δind, δ) in enumerate(D[:δs])
        m = dict2model(d, δ=δ)
        

        HΛ, Φs, ens = lanczosenergies(m, 400, basis, Htemp, energynum=3, tol=1e-12)

        Ψ = lanczosgroundstate(HΛ, Φs)
        Ψ_exc = lanczosgroundstate(HΛ, Φs, statenum=2)


     
        push!(D[:en], ens[end][1])
        push!(D[:en_exc], ens[end][2])
        push!(D[:dup], localdensity_up(Ψ, basis, m))
        push!(D[:ddn], localdensity_dn(Ψ, basis, m))
        push!(D[:dup_exc], localdensity_up(Ψ_exc, basis, m))
        push!(D[:ddn_exc], localdensity_dn(Ψ_exc, basis, m))
        # push!(D[:cSz], correlation_matrix(psi, "Sz", "Sz"))
        # push!(D[:cNup], correlation_matrix(psi, "Nup", "Nup"))
        # push!(D[:cNdn], correlation_matrix(psi, "Ndn", "Ndn"))
        # push!(D[:cNupdn], correlation_matrix(psi, "Nup", "Ndn"))

        D[:polup][δind] = sum([(j - m.L / 2) * D[:dup][δind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn][δind] = sum([(j - m.L / 2) * D[:ddn][δind][j]] for j in 1:m.L)[1] / m.L
        D[:polup_exc][δind] = sum([(j - m.L / 2) * D[:dup_exc][δind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn_exc][δind] = sum([(j - m.L / 2) * D[:ddn_exc][δind][j]] for j in 1:m.L)[1] / m.L

    end

    return strdict(D)

end


function continuedfraction(H, psi, en, j, jj, sites, numstates, η;opr="Cup", ωs=0:0.01:3)

    f = []
    a = []
    b = []
    f0 = deepcopy(psi)

    orthogonalize!(f0, j)
    f0[j] = noprime(op(sites, opr, j) * f0[j]) # apply local op to MPS
    f0 = f0 * norm(f0)^-1
    # Hf = contract(H,f0, cutoff = 1e-12)
    push!(a, dot(f0, H, f0) / dot(f0, f0)) # <psi|H|psi>
    push!(b, 0)
    
    Hf = noprime(contract(H, f0, cutoff=1e-12))
    push!(f, f0)
    ftmp = Hf - a[1] * f0
    btmp = norm(ftmp)
    push!(f, ftmp * btmp^-1)
    push!(b, btmp)
    @showprogress for i in 2:numstates
        Hf = noprime(contract(H, f[i], cutoff=1e-12))
        push!(a, dot(f[i], H, f[i]))
        ftmp = Hf - a[i] * f[i] - b[i - 1]^2 * f[i - 1]
        btmp = norm(ftmp)
        push!(f, ftmp * btmp^-1)
        println("test : ", dot(f[i + 1], f[i]))

        push!(b, btmp)
    end

    function rec(i, z, a, b, numstates)
        if i < numstates
            # println(i)
            return z - a[i] - (b[i + 1]^2 / rec(i + 1, z, a, b, numstates))
        else
            return Inf
        end
    end 

    f0prime = deepcopy(psi)
    orthogonalize!(f0prime, j)
    f0prime[jj] = noprime(op(sites, opr, jj) * f0prime[jj]) # apply local op to MPS
    f0prime = f0prime * norm(f0prime)^-1

    G(ω, η, E0, a, b) = dot(f0prime, H, f0) / rec(1, ω + 1im * η + E0, a, b, numstates)
    println("a : ", a)
    println("b : ", b)



    return ωs, [imag.(G(ω, η, en, a, b)) for ω in ωs]
end


function topoham_test(d::Dict)
    m = dict2model(d)
    D = resdict(d)
    
    D[:var] = []
    D[:res] = []

    ts = [0]

    if m.Vso == 0
        sites = siteinds("Electron", m.L;conserve_qns=true)
    else
        sites = siteinds("Electron", m.L;conserve_nf=true,
        conserve_nfparity=true)
    end

    state0 = ["Emp" for i in 1:D[:L]]
    for i in 1:D[:N] ## fill particles depending on L and N.
        if i <= D[:L]
            state0[i] = isodd(i) ? "Up" : "Dn"
        else
            state0[i - D[:L]] = "UpDn"
        end
    end
        
    psi = randomMPS(sites, state0, 10)

    if D[:maxdim] == "increasing" D[:maxdim] = Int.([10,20,50,100,200]) end

    if D[:reverse] == true ts = reverse(ts) end
    @showprogress for (tind, t) in enumerate(ts)
        H = mpo_ham(m, sites, t)
        obs = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-12)

        sweeps = Sweeps(200)
        maxdim!(sweeps, D[:maxdim]...)
        # noise!(sweeps,1E-5)
        cutoff!(sweeps, 1E-12)

        en, psi = dmrg(H, truncate(psi, maxdim=10), sweeps, observer=obs)

        push!(D[:var], norm(contract(H, psi, cutoff=1e-12)) - abs(en))
        push!(D[:res], continuedfraction(H, psi, en, 1, 1, sites, 9, 0.0000001))
    end
    D[:ts] = ts

    return strdict(D)
end


function fillparticles(N, L) # works.
    state0 = ["Emp" for i in 1:L]
    state1 = ["Emp" for i in 1:L]


    for i in 1:L ## fill particles depending on L and N.
        state0[i] = isodd(i) ? "Up" : "Dn"
    end
    a = N - L
    for i in sort(1:L, by=x -> (x - L / 2)^2)## fill particles depending on L and N.
        if isodd(i)
             if a < 0 
                a += 2
             else
                state1[i] = "UpDn"
             end
        elseif a > 0 
            state1[i] = "UpDn"
            a -= 2          
        end
    end

    a = N - L
    for i in sort(1:L, by=x -> (x - L / 2)^2)## fill particles depending on L and N.
        if a < 0
            state0[i] = "Emp"
            a += 1          
        elseif a > 0 
            state0[i] = "UpDn"
a -= 1         
        end
    end


    return state0, state1
end


function mps_sweeps_spinpump_gap(d;ts=0:0.25:1)
    m = dict2model(d)
    D = resdict(d)
    
    
    # return ts
    D[:polup] = similar(ts)
    D[:poldn] = similar(ts)
    D[:dup] = []
    D[:ddn] = []

    D[:en] = []
    D[:obsSz] = []
    D[:obsNup] = []
    D[:obsNdn] = []
    D[:var] = []

    D[:polup_exc] = similar(ts)
    D[:poldn_exc] = similar(ts)
    D[:dup_exc] = []
    D[:ddn_exc] = []

    D[:en_exc] = []
    D[:obsSz_exc] = []
    D[:obsNup_exc] = []
    D[:obsNdn_exc] = []
    D[:var_exc] = []

    D[:exc_overlap] = []

    D[:polup_plus2] = similar(ts)
    D[:poldn_plus2] = similar(ts)
    D[:dup_plus2] = []
    D[:ddn_plus2] = []

    D[:en_plus2] = []
    D[:obsSz_plus2] = []
    D[:obsNup_plus2] = []
    D[:obsNdn_plus2] = []
    D[:var_plus2] = []

    D[:polup_minus2] = similar(ts)
    D[:poldn_minus2] = similar(ts)
    D[:dup_minus2] = []
    D[:ddn_minus2] = []
  
    D[:en_minus2] = []
    D[:obsSz_minus2] = []
    D[:obsNup_minus2] = []
    D[:obsNdn_minus2] = []
    D[:var_minus2] = []

    D[:polup_flip] = similar(ts)
    D[:poldn_flip] = similar(ts)
    D[:dup_flip] = []
    D[:ddn_flip] = []

    D[:en_flip] = []
    D[:obsSz_flip] = []
    D[:obsNup_flip] = []
        D[:obsNdn_flip] = []
    D[:var_flip] = []

    if m.Vso == 0
        sites = siteinds("Electron", m.L;conserve_qns=true)
    else
        sites = siteinds("Electron", m.L;conserve_nf=true,
        conserve_nfparity=true)
    end

    state0, state1 = fillparticles(D[:N], D[:L]) # resume here.
    state0_plus2, state1_plus2 = fillparticles(D[:N] + 2, D[:L])
    state0_minus2, state1_minus2 = fillparticles(D[:N] - 2, D[:L])

    state0_flip = fillparticles(D[:N], D[:L])[1]
    state0_flip[cld(D[:L], 2)] = "Dn"

        
    psi0 = 1 / sqrt(4) * (MPS(sites, state0) + MPS(sites, reverse(state0)) + MPS(sites, reverse(state1)) + MPS(sites, state1))
    psi0_exc = psi0
    psi0_plus2 = 1 / sqrt(4) * (MPS(sites, state0_plus2) + MPS(sites, reverse(state0_plus2)) + MPS(sites, reverse(state1_plus2)) + MPS(sites, state1_plus2))
    psi0_minus2 = 1 / sqrt(4) * (MPS(sites, state0_minus2) + MPS(sites, reverse(state0_minus2)) + MPS(sites, reverse(state1_minus2)) + MPS(sites, state1_minus2))
    psi0_flip = 1 / sqrt(2) * (MPS(sites, state0_flip) + MPS(sites, reverse(state0_flip)))

    if D[:maxdim] == "increasing" D[:maxdim] = Int.([10,20,50,100,200]) end


    if D[:reverse] == true ts = reverse(ts) end
    @showprogress for (tind, t) in enumerate(ts)
        H = mpo_ham(m, sites, t)
        obs = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-9)
        obs_exc = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-9)
        obs_plus2 = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-9)
        obs_minus2 = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-9)
        obs_flip = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1E-9)

        sweeps = Sweeps(200)
        maxdim!(sweeps, D[:maxdim]...)

        noise!(sweeps, 1E-3, 1E-6, 1E-8, 1E-10)
        cutoff!(sweeps, 1E-9)
        println("test $tind")

        en, psi = dmrg(H, psi0, sweeps, observer=obs)
        en_exc, psi_exc = dmrg(H, [psi], psi0_exc, sweeps, observer=obs_exc, weight=20.)
        push!(D[:exc_overlap], dot(psi, psi_exc))
        en_plus2, psi_plus2 = dmrg(H, psi0_plus2, sweeps, observer=obs_plus2)
        en_minus2, psi_minus2 = dmrg(H, psi0_minus2, sweeps, observer=obs_minus2)
        en_flip, psi_flip = dmrg(H, psi0_flip, sweeps, observer=obs_flip)

        push!(D[:en], en)
        push!(D[:en_exc], en_exc)
        push!(D[:en_plus2], en_plus2)
        push!(D[:en_minus2], en_minus2)
        push!(D[:en_flip], en_flip)

        push!(D[:dup], expect(psi, "Nup"))
        push!(D[:ddn], expect(psi, "Ndn"))
        push!(D[:dup_exc], expect(psi_exc, "Nup"))
        push!(D[:ddn_exc], expect(psi_exc, "Ndn"))


        push!(D[:dup_plus2], expect(psi_plus2, "Nup"))
        push!(D[:ddn_plus2], expect(psi_plus2, "Ndn"))
        push!(D[:dup_minus2], expect(psi_minus2, "Nup"))
        push!(D[:ddn_minus2], expect(psi_minus2, "Ndn"))


        push!(D[:dup_flip], expect(psi_flip, "Nup"))
        push!(D[:ddn_flip], expect(psi_flip, "Ndn"))


        D[:polup][tind] = sum([(j - m.L / 2) * D[:dup][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn][tind] = sum([(j - m.L / 2) * D[:ddn][tind][j]] for j in 1:m.L)[1] / m.L
        D[:polup_exc][tind] = sum([(j - m.L / 2) * D[:dup_exc][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn_exc][tind] = sum([(j - m.L / 2) * D[:ddn_exc][tind][j]] for j in 1:m.L)[1] / m.L
        D[:polup_plus2][tind] = sum([(j - m.L / 2) * D[:dup_plus2][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn_plus2][tind] = sum([(j - m.L / 2) * D[:ddn_plus2][tind][j]] for j in 1:m.L)[1] / m.L
        D[:polup_minus2][tind] = sum([(j - m.L / 2) * D[:dup_minus2][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn_minus2][tind] = sum([(j - m.L / 2) * D[:ddn_minus2][tind][j]] for j in 1:m.L)[1] / m.L
        D[:polup_flip][tind] = sum([(j - m.L / 2) * D[:dup_flip][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn_flip][tind] = sum([(j - m.L / 2) * D[:ddn_flip][tind][j]] for j in 1:m.L)[1] / m.L

        push!(D[:var], norm(contract(H, psi, cutoff=1e-12)) - abs(en))
        push!(D[:var_exc], norm(contract(H, psi_exc, cutoff=1e-12)) - abs(en_exc))
        push!(D[:var_plus2], norm(contract(H, psi_plus2, cutoff=1e-12)) - abs(en_plus2))
        push!(D[:var_minus2], norm(contract(H, psi_minus2, cutoff=1e-12)) - abs(en_minus2))
        push!(D[:var_flip], norm(contract(H, psi_flip, cutoff=1e-12)) - abs(en_flip))


    end
    D[:ts] = ts

    return strdict(D)
end


function mps_sweeps_spinpump_gap_noSz(d;ts=0:0.25:1, err=1E-9)
    m = dict2model(d)
    D = resdict(d)
    
    
    # return ts
    D[:polup] = similar(ts)
    D[:poldn] = similar(ts)
    D[:dup] = []
    D[:ddn] = []

    D[:en] = []
    D[:var] = []

    D[:polup_exc] = similar(ts)
    D[:poldn_exc] = similar(ts)
    D[:dup_exc] = []
    D[:ddn_exc] = []

    D[:en_exc] = []
    D[:var_exc] = []

    D[:exc_overlap] = []

    D[:polup_plus2] = similar(ts)
    D[:poldn_plus2] = similar(ts)
    D[:dup_plus2] = []
    D[:ddn_plus2] = []

    D[:en_plus2] = []
    D[:var_plus2] = []

    D[:polup_minus2] = similar(ts)
    D[:poldn_minus2] = similar(ts)
    D[:dup_minus2] = []
    D[:ddn_minus2] = []
  
    D[:en_minus2] = []
    D[:var_minus2] = []

    # D[:polup_flip] = similar(ts)
    # D[:poldn_flip] = similar(ts)
    # D[:dup_flip] = []
    # D[:ddn_flip] = []

    # D[:en_flip] = []
    # D[:var_flip] = []

 
    sites = siteinds("Electron", m.L;conserve_nf=true,
    conserve_nfparity=true)

    state0, state1 = fillparticles(D[:N], D[:L]) # resume here.
    state0_plus2, state1_plus2 = fillparticles(D[:N] + 2, D[:L])
    state0_minus2, state1_minus2 = fillparticles(D[:N] - 2, D[:L])

    # state0_flip = fillparticles(D[:N],D[:L])[1]
    # state0_flip[cld(D[:L],2)] = "Up" ###might need to check flux of this.

        
    psi0 = 1 / sqrt(4) * (MPS(sites, state0) + MPS(sites, reverse(state0)) + MPS(sites, reverse(state1)) + MPS(sites, state1))
    psi0_exc = psi0
    psi0_plus2 = 1 / sqrt(4) * (MPS(sites, state0_plus2) + MPS(sites, reverse(state0_plus2)) + MPS(sites, reverse(state1_plus2)) + MPS(sites, state1_plus2))
    psi0_minus2 = 1 / sqrt(4) * (MPS(sites, state0_minus2) + MPS(sites, reverse(state0_minus2)) + MPS(sites, reverse(state1_minus2)) + MPS(sites, state1_minus2))
    # psi0_flip = 1/sqrt(2)*(MPS(sites, state0_flip)+MPS(sites, reverse(state0_flip)))

    D[:maxdim] = Int.([10,20,50,100,500])


    # if D[:reverse] == true ts = reverse(ts) end
    @showprogress for (tind, t) in enumerate(ts)
        H = mpo_ham(m, sites, t)
        obs = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1e-9)
        obs_exc = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1e-9)
        obs_plus2 = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1e-9)
        obs_minus2 = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1e-9)
        # obs_flip = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1e-9)

        sweeps = Sweeps(200)
        maxdim!(sweeps, D[:maxdim]...)

        noise!(sweeps, 1E-3, 1E-6, 1E-8, 1E-10)
        cutoff!(sweeps, err)

        en, psi = dmrg(H, psi0, sweeps, observer=obs)
        en_exc, psi_exc = dmrg(H, [psi], psi0_exc, sweeps, observer=obs_exc, weight=1.0e4)
        push!(D[:exc_overlap], dot(psi, psi_exc))
        en_plus2, psi_plus2 = dmrg(H, psi0_plus2, sweeps, observer=obs_plus2)
        en_minus2, psi_minus2 = dmrg(H, psi0_minus2, sweeps, observer=obs_minus2)
        # en_flip, psi_flip = dmrg(H, psi0_flip, sweeps, observer=obs_flip)

        push!(D[:en], en)
        push!(D[:en_exc], en_exc)
        push!(D[:en_plus2], en_plus2)
        push!(D[:en_minus2], en_minus2)
        # push!(D[:en_flip], en_flip)

        push!(D[:dup], expect(psi, "Nup"))
        push!(D[:ddn], expect(psi, "Ndn"))
        push!(D[:dup_exc], expect(psi_exc, "Nup"))
        push!(D[:ddn_exc], expect(psi_exc, "Ndn"))


        push!(D[:dup_plus2], expect(psi_plus2, "Nup"))
        push!(D[:ddn_plus2], expect(psi_plus2, "Ndn"))
        push!(D[:dup_minus2], expect(psi_minus2, "Nup"))
        push!(D[:ddn_minus2], expect(psi_minus2, "Ndn"))


        # push!(D[:dup_flip], expect(psi_flip, "Nup"))
        # push!(D[:ddn_flip], expect(psi_flip, "Ndn"))


        D[:polup][tind] = sum([(j - m.L / 2) * D[:dup][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn][tind] = sum([(j - m.L / 2) * D[:ddn][tind][j]] for j in 1:m.L)[1] / m.L
        D[:polup_exc][tind] = sum([(j - m.L / 2) * D[:dup_exc][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn_exc][tind] = sum([(j - m.L / 2) * D[:ddn_exc][tind][j]] for j in 1:m.L)[1] / m.L
        D[:polup_plus2][tind] = sum([(j - m.L / 2) * D[:dup_plus2][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn_plus2][tind] = sum([(j - m.L / 2) * D[:ddn_plus2][tind][j]] for j in 1:m.L)[1] / m.L
        D[:polup_minus2][tind] = sum([(j - m.L / 2) * D[:dup_minus2][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn_minus2][tind] = sum([(j - m.L / 2) * D[:ddn_minus2][tind][j]] for j in 1:m.L)[1] / m.L
        # D[:polup_flip][tind] = sum([(j - m.L / 2) * D[:dup_flip][tind][j]] for j in 1:m.L)[1] / m.L
        # D[:poldn_flip][tind] = sum([(j - m.L / 2) * D[:ddn_flip][tind][j]] for j in 1:m.L)[1] / m.L

        push!(D[:var], norm(contract(H, psi, cutoff=1e-12)) - abs(en))
        push!(D[:var_exc], norm(contract(H, psi_exc, cutoff=1e-12)) - abs(en_exc))
        push!(D[:var_plus2], norm(contract(H, psi_plus2, cutoff=1e-12)) - abs(en_plus2))
        push!(D[:var_minus2], norm(contract(H, psi_minus2, cutoff=1e-12)) - abs(en_minus2))
        # push!(D[:var_flip], norm(contract(H, psi_flip, cutoff=1e-12)) - abs(en_flip))


    end
    D[:chargegap] = (D[:en_plus2] .+ D[:en_minus2] .- 2 * D[:en]) ./ 2
    # D[:spingap] = D[:en_flip] .- D[:en]
    D[:intgap] = D[:en_exc] .- D[:en]


    D[:ts] = ts

    return strdict(D)
end


function tebd_test(d;ts=0:0.25:1, err=1E-9)
# begin
    m = dict2model(d, T=100, U=6, Vso = 0.6, L= 20)
    D = resdict(m)

    sites = siteinds("Electron", m.L;conserve_nf=true,conserve_nfparity=true)

    state0, state1 = fillparticles(D[:N], D[:L]) # resume here.
        
    psi0 = 1 / sqrt(4) * (MPS(sites, state0) + MPS(sites, reverse(state0)) + MPS(sites, reverse(state1)) + MPS(sites, state1))

    D[:maxdim] = Int.([10,20,50,100,500])

    #calculate initial (ground) state:
    H = mpo_ham(m, sites, 0)
    obs = DMRGObserver(["Sz","Nup","Ndn"], sites, energy_tol=1e-13)
    sweeps = Sweeps(200)
    maxdim!(sweeps, D[:maxdim]...)
    noise!(sweeps, 1E-3, 1E-6, 1E-8, 1E-10)
    cutoff!(sweeps, 1e-10)
    en, psi = dmrg(H, psi0, sweeps, observer=obs)
    phi = contract(H, psi, cutoff=1e-12)
    D[:var] = inner(phi,phi) - en^2

    ts = 0:0.1:100
    D[:ts] = ts
    D[:dup] = []
    D[:ddn] = []
    D[:polup] = similar(ts)
    D[:poldn] = similar(ts)
    ens = []
    norms = []
    overlaps = []
    psitmp = psi

    opr = op(sites[div(L,2)],"Cup")
    psi = apply(opr,psi)

    @showprogress for (tind, t) in enumerate(ts)

        gates = MPO2gate(m,t,0.1,sites)

        psi = apply(gates, psi; cutoff=1e-4)
    
        push!(D[:dup], expect(psi, "Nup"))
        push!(D[:ddn], expect(psi, "Ndn"))
        push!(norms, norm(psi))
        D[:polup][tind] = sum([(j - m.L / 2) * D[:dup][tind][j]] for j in 1:m.L)[1] / m.L
        D[:poldn][tind] = sum([(j - m.L / 2) * D[:ddn][tind][j]] for j in 1:m.L)[1] / m.L
        push!(ens,inner(psi,contract(H, psi, cutoff=1e-12)))
        push!(overlaps,inner(psitmp,psi))
    end

    dup = hcat(D[:dup]...)'
    ddn = hcat(D[:ddn]...)'
    polup = D[:polup]
    poldn = D[:poldn]
    maximum(real.(ens.-en)) #max error in energy:



    # return strdict(D)
end
