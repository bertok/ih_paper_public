using Plots
using UnicodePlots

function move_up(s::AbstractString)
    move_up_n_lines(n) = "\u1b[$(n)F"
    # actually string_height - 1, but we're assuming cursor is on the last line
    string_height = length(collect(eachmatch(r"\n", s)))
    print(move_up_n_lines(string_height))
    nothing
end

function animate(frames; frame_delay = 0)
    print("\u001B[?25l") # hide cursor
    for frame in frames[1:end-1]
        print(frame)
        sleep(frame_delay)
        move_up(string(frame))
    end
    print(frames[end])
    print("\u001B[?25h") # visible cursor
    nothing
end

showcursor() = print("\u001B[?25h") # hide cursor
hidecursor() = print("\u001B[?25l")
function addplot(frame)
    print(frame)
    move_up(string(frame))
end
