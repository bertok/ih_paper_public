using DrWatson
using SparseArrays

include(srcdir("tests.jl"))



basissize(m::Model1d) = (m.L, m.L)
basissize(m::Model1dSpinObc) = (m.L, m.L)
# basissize(m::IntModel1dSpinObc) = (m.L, m.L)
# basissize(m::IonicHubbard) = (m.L, m.L)
# basissize(m::IonicHubbard2) = (m.L, m.L)
basissize(m::Model1dSpinPbc) = (m.L, m.L)


basissize(m::IntModel1dSpinPbc) = Int(factorial(big(2 * m.L)) // (factorial(big(m.N)) * factorial(big(2 * m.L - m.N))))
basissize(m::IntModel1dSpinObc) = Int(factorial(big(2 * m.L)) // (factorial(big(m.N)) * factorial(big(2 * m.L - m.N))))

basissize(m::IntModel1dSpinPbc, nup, ndown) = Int(factorial(big(m.L)) // (factorial(big(nup)) * factorial(big(m.L - nup)))) * Int(factorial(big(m.L)) // (factorial(big(ndown)) * factorial(big(m.L - ndown))))
basissize(m::IntModel1dSpinObc, nup, ndown) = Int(factorial(big(m.L)) // (factorial(big(nup)) * factorial(big(m.L - nup)))) * Int(factorial(big(m.L)) // (factorial(big(ndown)) * factorial(big(m.L - ndown))))


basissize(m::IntModel1dPbc) = Int(factorial(big(m.L)) // (factorial(big(m.N)) * factorial(big(m.L - m.N))))

basissize(m::BosonicSchwinger) = sum(x->binomial(m.L,x)^2,0:m.L)

function generatefixednstates(m::IntModel1dSpinPbc)
    res = Array{Int}(undef, basissize(m))
    j = 1
    @showprogress "building basis... " for i in 0:2^(2 * m.L) - 1
        s = digits(i, base=2, pad=2 * m.L)
        if sum(s) == m.N
            res[j] = i
            j += 1
        end
    end
    return res
end

function generatefixednstates(m::IntModel1dSpinObc)
    res = Array{Int}(undef, basissize(m))
    j = 1
    @showprogress "building basis... " for i in 0:2^(2 * m.L) - 1
        s = digits(i, base=2, pad=2 * m.L)
        if sum(s) == m.N
            res[j] = i
            j += 1
        end
    end
    return res
end

function generatefixednstates(m::IntModel1dObc)
    res = Array{Int}(undef, basissize(m))
    j = 1
    @showprogress "building basis... " for i in 0:2^(m.L) - 1
        s = digits(i, base=2, pad=m.L)
        if sum(s) == m.N
            res[j] = i
            j += 1
        end
    end
    return res
end

function generatefixednstates(m::IntModel1dPbc)
    res = Array{Int}(undef, basissize(m))
    j = 1
    @showprogress "building basis... " for i in 0:2^(m.L) - 1
        s = digits(i, base=2, pad=m.L)
        if sum(s) == m.N
            res[j] = i
            j += 1
        end
    end
    return res
end


function generatefixedSzstates(m::IntModel1dSpinPbc; Sz=0)
    nup = fld(2 * Sz + m.N, 2) 
    ndown = m.N - nup
    res = Array{Int}(undef, basissize(m, nup, ndown))
    j = 1
    @showprogress "building basis... " for i in 0:2^(2 * m.L) - 1
        s = digits(i, base=2, pad=2 * m.L)
        if sum(s) == m.N && fld(sum(s[1:m.L]) - sum(s[m.L + 1:2 * m.L]), 2) == Sz
            res[j] = i
            j += 1
        end
    end
    return res
end

function generatefixedSzstates(m::IntModel1dSpinObc; Sz=0)
    nup = fld(2 * Sz + m.N, 2) 
    ndown = m.N - nup
    res = Array{Int}(undef, basissize(m, nup, ndown))
    j = 1
    @showprogress "building basis... " for i in 0:2^(2 * m.L) - 1
        s = digits(i, base=2, pad=2 * m.L)
        if sum(s) == m.N && fld(sum(s[1:m.L]) - sum(s[m.L + 1:2 * m.L]), 2) == Sz
            res[j] = i
            j += 1
        end
    end
    return res
end

function generatefixedQstates(m::BosonicSchwinger; Q = 0)
    res = Array{Int}(undef, basissize(m))
    j = 1
    @showprogress "building basis... " for i in 0:2^(2*m.L) - 1
        s = digits(i, base=2, pad=2*m.L)
        if sum(s[1:m.L]) == sum(s[m.L+1:2*m.L])
            res[j] = i
            j += 1
        end
    end
    return res
end

function findstate(s::BitArray, states)
    return(findfirst(x -> x == s, states))
    # return(searchsortedfirst(states,s))
end

function findstate(s::Int, states)
    # return(findfirst(x -> x == s, states))
    return(searchsortedfirst(states, s))
end


function flip(m::IntModel1dSpinPbc, os::Int, i, j)
    s = BitArray(digits(os, base=2, pad=2 * m.L))
    s[i] = !s[i]
    s[j] = !s[j]
    return sum([s[k] * 2^(k - 1) for k = 1:length(s)])
end

function flip(m::IntModel1dSpinObc, os::Int, i, j)
    s = BitArray(digits(os, base=2, pad=2 * m.L))
    s[i] = !s[i]
    s[j] = !s[j]
    return sum([s[k] * 2^(k - 1) for k = 1:length(s)])
end

function flip(m::IntModel1dPbc, os::Int, i, j)
    s = BitArray(digits(os, base=2, pad=m.L))
    s[i] = !s[i]
    s[j] = !s[j]
    return sum([s[k] * 2^(k - 1) for k = 1:length(s)])
end

function flip(m::IntModel1dObc, os::Int, i, j)
    s = BitArray(digits(os, base=2, pad=m.L))
    s[i] = !s[i]
    s[j] = !s[j]
    return sum([s[k] * 2^(k - 1) for k = 1:length(s)])
end

function flip(m::BosonicSchwinger, os::Int, i, j)
    s = BitArray(digits(os, base=2, pad= 2* m.L))
    s[i] = !s[i]
    s[j] = !s[j]
    return sum([s[k] * 2^(k - 1) for k = 1:length(s)])
end

function paircreation(m::BosonicSchwinger, os::Int, i, j)
    s = BitArray(digits(os, base=2, pad=2*m.L))
    s[i] = !s[i]
    s[j] = !s[j]
    return sum([s[k] * 2^(k - 1) for k = 1:length(s)])
end

int2state(i::Int, m::IntModel1dSpinPbc) = digits(i, base=2, pad=2 * m.L)
int2state(i::Int, m::IntModel1dSpinObc) = digits(i, base=2, pad=2 * m.L)
int2state(s::Array{Int}, m::IntModel1dSpinPbc) = [digits(i, base=2, pad=2 * m.L)  for i in s]
int2state(s::Array{Int}, m::IntModel1dSpinObc) = [digits(i, base=2, pad=2 * m.L)  for i in s]


int2state(i::Int, m::IntModel1dPbc) = digits(i, base=2, pad=m.L)
int2state(i::Int, m::IntModel1dObc) = digits(i, base=2, pad=m.L)
int2state(s::Array{Int}, m::IntModel1dPbc) = [digits(i, base=2, pad=m.L)  for i in s]

int2state(i::Int, m::BosonicSchwinger) = digits(i, base=2, pad=2*m.L)
int2state(s::Array{Int}, m::BosonicSchwinger) = [digits(i, base=2, pad=2*m.L)  for i in s]

function sparsehamiltonian_template(m::IntRmPbc, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_hop1pbc = Int[]
    cols_hop1pbc = Int[]
    vals_hop1pbc = Float64[]
    rows_hop2pbc = Int[]
    cols_hop2pbc = Int[]
    vals_hop2pbc = Float64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i % m.L + 1 # PBC
            k = i % m.L + 1 + m.L # PBC
            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss, 0)
                    if i % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
            
            if ss[i] == 1 == ss[j]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m))  
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_hop1pbc, cols_hop1pbc, vals_hop1pbc), (rows_hop2pbc, cols_hop2pbc, vals_hop2pbc), (rows_int, cols_int, vals_int)
end


function sparsehamiltonian_template(m::IntRmSpinPbc, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_hop1pbc = Int[]
    cols_hop1pbc = Int[]
    vals_hop1pbc = Float64[]
    rows_hop2pbc = Int[]
    cols_hop2pbc = Int[]
    vals_hop2pbc = Float64[]
    rows_so = Int[]
    cols_so = Int[]
    vals_so = ComplexF64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i % m.L + 1 # PBC
            k = i % m.L + 1 + m.L # PBC
            u = i + m.L# PBC
            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss, 0)
                    if i % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
            if ss[i] == 0 != ss[k]
                sflip = flip(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_so, b)
                    push!(cols_so, a) 
                    push!(vals_so, spinorbitcoupling(m, i, k, ss))  
                end
            end
            if ss[i] == 1 != ss[k]
                sflip = flip(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_so, b)
                    push!(cols_so, a) 
                    push!(vals_so, -spinorbitcoupling(m, i, k, ss))  
                end
            end
            if ss[i] == 1 == ss[u]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m))  
            end
        end
        for i in m.L + 1:2 * m.L
            j = (i - m.L) % m.L + 1 + m.L # PBC
            k = (i - m.L) % m.L + 1 # PBC
            if ss[i] == 1
                if (i-m.L) % 2 == 1
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1) 
                else
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1) 
                end
            end          
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss, 0)
                    if (i-m.L) % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)  
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)  
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
            if ss[i] == 0 != ss[k]
                sflip = flip(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_so, b)
                    push!(cols_so, a) 
                    push!(vals_so, spinorbitcoupling(m, i, k, ss)) 
                end
            end
            if ss[i] == 1 != ss[k]
                sflip = flip(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_so, b)
                    push!(cols_so, a) 
                    push!(vals_so, -spinorbitcoupling(m, i, k, ss)) 
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_hop1pbc, cols_hop1pbc, vals_hop1pbc), (rows_hop2pbc, cols_hop2pbc, vals_hop2pbc), (rows_so, cols_so, vals_so), (rows_int, cols_int, vals_int)
end

function sparsehamiltonian_template(m::IntRmSpinObc, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_so = Int[]
    cols_so = Int[]
    vals_so = ComplexF64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset

        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i + 1
            k = i + 1 + m.L
            u = i + m.L
            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if (j <= m.L) && (ss[i] != ss[j])
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    if i % 2 == 1
                        push!(rows_hop1, a)
                        push!(cols_hop1, b) 
                        push!(vals_hop1, 1)
                    else
                        push!(rows_hop2, a)
                        push!(cols_hop2, b) 
                        push!(vals_hop2, 1)                         
                    end
                end
            end
            if (k <= 2*m.L) && (ss[i] == 0 != ss[k])
                sflip = flip(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_so, b)
                    push!(cols_so, a) 
                    push!(vals_so, spinorbitcoupling(m, i, k, ss))  
                else
                    println("warning")
                end
            end
            if (k <= 2*m.L) && (ss[i] == 1 != ss[k])
                sflip = flip(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_so, b)
                    push!(cols_so, a) 
                    push!(vals_so, -spinorbitcoupling(m, i, k, ss))  
                else
                    println("warning")
                end
            end
            if ss[i] == 1 == ss[u]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m))  
            end
        end
        for i in m.L + 1:2 * m.L
            j = i + 1
            k = i - m.L + 1
            if ss[i] == 1
                if (i-m.L) % 2 == 1
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1) 
                else
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1) 
                end
            end          
            if (j <= 2*m.L) && (ss[i] != ss[j])
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    if (i-m.L) % 2 == 1
                        push!(rows_hop1, a)
                        push!(cols_hop1, b) 
                        push!(vals_hop1, 1)  
                    else
                        push!(rows_hop2, a)
                        push!(cols_hop2, b) 
                        push!(vals_hop2, 1)  
                    end
                end
            end
            if (k <= m.L) && (ss[i] == 0 != ss[k])
                sflip = flip(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_so, b)
                    push!(cols_so, a) 
                    push!(vals_so, spinorbitcoupling(m, i, k, ss)) 
                else
                    println("warning")
                end
            end
            if (k <= m.L) && (ss[i] == 1 != ss[k])
                sflip = flip(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_so, b)
                    push!(cols_so, a) 
                    push!(vals_so, -spinorbitcoupling(m, i, k, ss)) 
                else
                    println("warning")
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_so, cols_so, vals_so), (rows_int, cols_int, vals_int)
end

function sparsehamiltonian_template(m::IonicHubbard, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset

        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i + 1
            u = i + m.L
            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if (j <= m.L) && (ss[i] != ss[j])
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    if i % 2 == 1
                        push!(rows_hop1, a)
                        push!(cols_hop1, b) 
                        push!(vals_hop1, 1)
                    else
                        push!(rows_hop2, a)
                        push!(cols_hop2, b) 
                        push!(vals_hop2, 1)                         
                    end
                end
            end
            if ss[i] == 1 == ss[u]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m))  
            end
        end
        for i in m.L + 1:2 * m.L
            j = i + 1
            if ss[i] == 1
                if (i-m.L) % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1) 
                else
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1) 
                end
            end          
            if (j <= 2*m.L) && (ss[i] != ss[j])
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    if (i-m.L) % 2 == 1
                        push!(rows_hop1, a)
                        push!(cols_hop1, b) 
                        push!(vals_hop1, 1)  
                    else
                        push!(rows_hop2, a)
                        push!(cols_hop2, b) 
                        push!(vals_hop2, 1)  
                    end
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_int, cols_int, vals_int)
end

function sparsehamiltonian_template(m::IonicHubbard2, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset

        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i + 1
            u = i + m.L
            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if (j <= m.L) && (ss[i] != ss[j])
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    if i % 2 == 1
                        push!(rows_hop1, a)
                        push!(cols_hop1, b) 
                        push!(vals_hop1, 1)
                    else
                        push!(rows_hop2, a)
                        push!(cols_hop2, b) 
                        push!(vals_hop2, 1)                         
                    end
                end
            end
            if ss[i] == 1 == ss[u]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m))  
            end
        end
        for i in m.L + 1:2 * m.L
            j = i + 1
            k = i - m.L + 1
            if ss[i] == 1
                if (i-m.L) % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1) 
                else
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1) 
                end
            end          
            if (j <= 2*m.L) && (ss[i] != ss[j])
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    if (i-m.L) % 2 == 1
                        push!(rows_hop1, a)
                        push!(cols_hop1, b) 
                        push!(vals_hop1, 1)  
                    else
                        push!(rows_hop2, a)
                        push!(cols_hop2, b) 
                        push!(vals_hop2, 1)  
                    end
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_int, cols_int, vals_int)
end

function sparsehamiltonian_template(m::IonicHubbardPbc, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_hop1pbc = Int[]
    cols_hop1pbc = Int[]
    vals_hop1pbc = Float64[]
    rows_hop2pbc = Int[]
    cols_hop2pbc = Int[]
    vals_hop2pbc = Float64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i % m.L + 1 # PBC
            u = i + m.L# PBC
            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss, 0)
                    if i % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
            if ss[i] == 1 == ss[u]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m))  
            end
        end
        for i in m.L + 1:2 * m.L
            j = (i - m.L) % m.L + 1 + m.L # PBC
            if ss[i] == 1
                if (i - m.L) % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1) 
                else
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1) 
                end
            end          
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss, 0)
                    if (i-m.L) % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)  
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)  
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_hop1pbc, cols_hop1pbc, vals_hop1pbc), (rows_hop2pbc, cols_hop2pbc, vals_hop2pbc),  (rows_int, cols_int, vals_int)
end


function sparsehamiltonian_template(m::IonicHubbardPbcToy, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_pot3 = Int[]
    cols_pot3 = Int[]
    vals_pot3 = Float64[]
    rows_pot4 = Int[]
    cols_pot4 = Int[]
    vals_pot4 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_hop1pbc = Int[]
    cols_hop1pbc = Int[]
    vals_hop1pbc = Float64[]
    rows_hop2pbc = Int[]
    cols_hop2pbc = Int[]
    vals_hop2pbc = Float64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    rows_SzSz = Int[]
    cols_SzSz = Int[]
    vals_SzSz = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i % m.L + 1 # PBC
            k = i % m.L + 1 + m.L # PBC Sz Sz
            u = i + m.L# PBC


            push!(rows_SzSz, a)
            push!(cols_SzSz, a) 
            push!(vals_SzSz, m.J*(ss[i]-ss[u])*(ss[j]-ss[k])/4)

            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss, 0)
                    if i % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
            if ss[i] == 1 == ss[u]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m))  
            end
        end
        for i in m.L + 1:2 * m.L
            j = (i - m.L) % m.L + 1 + m.L # PBC
            if ss[i] == 1
                if (i - m.L) % 2 == 1
                    push!(rows_pot3, a)
                    push!(cols_pot3, a) 
                    push!(vals_pot3, 1) 
                else
                    push!(rows_pot4, a)
                    push!(cols_pot4, a) 
                    push!(vals_pot4, 1) 
                end
            end          
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss, 0)
                    if (i-m.L) % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)  
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)  
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_pot3, cols_pot3, vals_pot3), (rows_pot4, cols_pot4, vals_pot4), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_hop1pbc, cols_hop1pbc, vals_hop1pbc), (rows_hop2pbc, cols_hop2pbc, vals_hop2pbc),  (rows_int, cols_int, vals_int), (rows_SzSz, cols_SzSz, vals_SzSz)
end


function sparsehamiltonian_template(m::IonicHubbard2Pbc, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_hop1pbc = Int[]
    cols_hop1pbc = Int[]
    vals_hop1pbc = Float64[]
    rows_hop2pbc = Int[]
    cols_hop2pbc = Int[]
    vals_hop2pbc = Float64[]
    rows_so = Int[]
    cols_so = Int[]
    vals_so = ComplexF64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i % m.L + 1 # PBC
            u = i + m.L# PBC
            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss)
                    if i % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
            if ss[i] == 1 == ss[u]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m)) 
                # println(ss,"i: ",i,"u: ",u,"a: ",a)
            end
        end
        for i in m.L + 1:2 * m.L
            j = (i - m.L) % m.L + 1 + m.L # PBC
            if ss[i] == 1
                if (i - m.L) % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1) 
                else
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1) 
                end
            end          
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss)
                    if (i-m.L) % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)  
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)  
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_hop1pbc, cols_hop1pbc, vals_hop1pbc), (rows_hop2pbc, cols_hop2pbc, vals_hop2pbc),  (rows_int, cols_int, vals_int)
end

function sparsehamiltonian_template(m::IonicHubbard2PbcToy, basis; energyoffset=0, init=nothing)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_pot2 = Int[]
    cols_pot2 = Int[]
    vals_pot2 = Float64[]
    rows_pot3 = Int[]
    cols_pot3 = Int[]
    vals_pot3 = Float64[]
    rows_pot4 = Int[]
    cols_pot4 = Int[]
    vals_pot4 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_hop2 = Int[]
    cols_hop2 = Int[]
    vals_hop2 = Float64[]
    rows_hop1pbc = Int[]
    cols_hop1pbc = Int[]
    vals_hop1pbc = Float64[]
    rows_hop2pbc = Int[]
    cols_hop2pbc = Int[]
    vals_hop2pbc = Float64[]
    rows_int = Int[]
    cols_int = Int[]
    vals_int = Float64[]
    rows_SzSz = Int[]
    cols_SzSz = Int[]
    vals_SzSz = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i % m.L + 1 # PBC
            k = i % m.L + 1 + m.L # PBC Sz Sz
            u = i + m.L# PBC


            push!(rows_SzSz, a)
            push!(cols_SzSz, a) 
            push!(vals_SzSz, m.J*(ss[i]-ss[u])*(ss[j]-ss[k])/4)

            if ss[i] == 1
                if i % 2 == 1
                    push!(rows_pot1, a)
                    push!(cols_pot1, a) 
                    push!(vals_pot1, 1)
                else 
                    push!(rows_pot2, a)
                    push!(cols_pot2, a) 
                    push!(vals_pot2, 1)
                end
            end         
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss)
                    if i % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
            if ss[i] == 1 == ss[u]
                push!(rows_int, a)
                push!(cols_int, a) 
                push!(vals_int, interaction(m))  
            end
        end
        for i in m.L + 1:2 * m.L
            j = (i - m.L) % m.L + 1 + m.L # PBC
            if ss[i] == 1
                if (i - m.L) % 2 == 1
                    push!(rows_pot3, a)
                    push!(cols_pot3, a) 
                    push!(vals_pot3, 1) 
                else
                    push!(rows_pot4, a)
                    push!(cols_pot4, a) 
                    push!(vals_pot4, 1) 
                end
            end          
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    h = hopping(m, i, j, ss)
                    if (i-m.L) % 2 == 1
                        if h < 0
                            push!(rows_hop1, a)
                            push!(cols_hop1, b) 
                            push!(vals_hop1, 1)  
                        else 
                            push!(rows_hop1pbc, a)
                            push!(cols_hop1pbc, b) 
                            push!(vals_hop1pbc, 1)  
                        end
                    else
                        if h < 0
                            push!(rows_hop2, a)
                            push!(cols_hop2, b) 
                            push!(vals_hop2, 1)  
                        else
                            push!(rows_hop2pbc, a)
                            push!(cols_hop2pbc, b) 
                            push!(vals_hop2pbc, 1)  
                        end
                    end
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_pot2, cols_pot2, vals_pot2), (rows_pot3, cols_pot3, vals_pot3), (rows_pot4, cols_pot4, vals_pot4), (rows_hop1, cols_hop1, vals_hop1), (rows_hop2, cols_hop2, vals_hop2), (rows_hop1pbc, cols_hop1pbc, vals_hop1pbc), (rows_hop2pbc, cols_hop2pbc, vals_hop2pbc),  (rows_int, cols_int, vals_int), (rows_SzSz, cols_SzSz, vals_SzSz)
end


function sparsehamiltonian_template(m::BosonicSchwinger, basis)
    l = length(basis)
    rows_pot1 = Int[]
    cols_pot1 = Int[]
    vals_pot1 = Float64[]
    rows_hop1 = Int[]
    cols_hop1 = Int[]
    vals_hop1 = Float64[]
    rows_pair1 = Int[]
    cols_pair1 = Int[]
    vals_pair1 = Float64[]
    rows_offset = Int[]
    cols_offset = Int[]
    vals_offset = Float64[]
    @showprogress "building H... " for (a, s) in enumerate(basis)
        ss = digits(s, base=2, pad=2 * m.L)
        for i in 1:m.L            
            j = i + 1 #hopping
            k = i + 1 + m.L #pair1
            if ss[i] == 1
                push!(rows_pot1, a)
                push!(cols_pot1, a) 
                push!(vals_pot1, m.K)
            end         
            if (j <= m.L) && (ss[i] != ss[j])
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_hop1, a)
                    push!(cols_hop1, b) 
                    push!(vals_hop1, m.B)
                else
                    println("warning: out of basis")
                end
            end
            if (k <= 2*m.L) && (ss[i] == ss[k])
                sflip = paircreation(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_pair1, b)
                    push!(cols_pair1, a) 
                    push!(vals_pair1, m.B)  
                else
                    println("warning: out of basis")
                end
            end
        end
        for i in m.L + 1:2 * m.L
            j = i + 1
            k = i - m.L + 1
            if ss[i] == 1
                push!(rows_pot1, a)
                push!(cols_pot1, a) 
                push!(vals_pot1, -m.K)     
            end         
            if (j <= 2*m.L) && (ss[i] != ss[j])
                sflip = flip(m, s, i, j)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_hop1, a)
                    push!(cols_hop1, b) 
                    push!(vals_hop1, m.B)
                else
                    println("warning: out of basis")
                end            
            end
            if (k <= m.L) && (ss[i] == ss[k])
                sflip = paircreation(m, s, i, k)
                b = findstate(sflip, basis)
                if b <= l # filter out matrix elements that bring you out of the basis.
                    push!(rows_pair1, b)
                    push!(cols_pair1, a) 
                    push!(vals_pair1, m.B)  
                else
                    println("warning: out of basis")
                end
            end
        end
    end
    return (rows_pot1, cols_pot1, vals_pot1), (rows_hop1, cols_hop1, vals_hop1), (rows_pair1, cols_pair1, vals_pair1), (rows_offset, cols_offset, vals_offset)
end


function makeHtemplate(m::IntModel1dSpinPbc, states)
    temp = sparsehamiltonian_template(m::IntRmSpinPbc, states)
    wsave("_research/hamiltonians/Hamiltonian_$(typeof(m))_$(savename(m)).jld2", "temp", temp)
end

function loadHtemplate(m::IntModel1dSpinPbc)
    return load("_research/hamiltonians/Hamiltonian_$(typeof(m))_$(savename(m)).jld2", "temp")
end

function Hfromtemp(m::IntModel1dSpinPbc, t, Hpot1, Hpot2, Hhop1, Hhop2, Hhop1pbc, Hhop2pbc, Hso, Hint)

    rows = [Hpot1[1];Hpot2[1];Hhop1[1];Hhop2[1];Hhop1pbc[1];Hhop2pbc[1];Hso[1];Hint[1]]
    cols = [Hpot1[2];Hpot2[2];Hhop1[2];Hhop2[2];Hhop1pbc[2];Hhop2pbc[2];Hso[2];Hint[2]]
    vals = [potential(m, 1, t) .* Hpot1[3];potential(m, 2, t) .* Hpot2[3];hopping(m, 1, t) .* Hhop1[3];hopping(m, 2, t) .* Hhop2[3]; -hopping(m, 1, t) .* Hhop1pbc[3];- hopping(m, 2, t) .* Hhop2pbc[3];Hso[3];Hint[3]]

    return sparse(rows, cols, vals)
end

function Hfromtemp(m::IntModel1dSpinObc, t, Hpot1, Hpot2, Hhop1, Hhop2, Hso, Hint)

    rows = [Hpot1[1];Hpot2[1];Hhop1[1];Hhop2[1];Hso[1];Hint[1]]
    cols = [Hpot1[2];Hpot2[2];Hhop1[2];Hhop2[2];Hso[2];Hint[2]]
    vals = [potential(m, 1, t) .* Hpot1[3];potential(m, 2, t) .* Hpot2[3];hopping(m, 1, t) .* Hhop1[3];hopping(m, 2, t) .* Hhop2[3];Hso[3];Hint[3]]

    return sparse(rows, cols, vals)
end

function Hfromtemp(m::IonicHubbard, t, Hpot1, Hpot2, Hhop1, Hhop2, Hint)

    rows = [Hpot1[1];Hpot2[1];Hhop1[1];Hhop2[1];Hint[1]]
    cols = [Hpot1[2];Hpot2[2];Hhop1[2];Hhop2[2];Hint[2]]
    vals = [potential(m, 1, t) .* Hpot1[3];potential(m, 2, t) .* Hpot2[3];hopping(m, 1, t) .* Hhop1[3];hopping(m, 2, t) .* Hhop2[3];Hint[3]]

    return sparse(rows, cols, vals)
end


function Hfromtemp(m::IonicHubbard2, Hpot1, Hpot2, Hhop1, Hhop2, Hint)

    rows = [Hpot1[1];Hpot2[1];Hhop1[1];Hhop2[1];Hint[1]]
    cols = [Hpot1[2];Hpot2[2];Hhop1[2];Hhop2[2];Hint[2]]
    vals = [potential(m, 1) .* Hpot1[3];potential(m, 2) .* Hpot2[3];hopping(m, 1) .* Hhop1[3];hopping(m, 2) .* Hhop2[3];Hint[3]]

    return sparse(rows, cols, vals)
end

function Hfromtemp(m::IonicHubbardPbc, t, Hpot1, Hpot2, Hhop1, Hhop2, Hhop1pbc, Hhop2pbc, Hint)

    rows = [Hpot1[1];Hpot2[1];Hhop1[1];Hhop2[1];Hhop1pbc[1];Hhop2pbc[1];Hint[1]]
    cols = [Hpot1[2];Hpot2[2];Hhop1[2];Hhop2[2];Hhop1pbc[2];Hhop2pbc[2];Hint[2]]
    vals = [potential(m, 1, t) .* Hpot1[3];potential(m, 2, t) .* Hpot2[3];hopping(m, 1, t) .* Hhop1[3];hopping(m, 2, t) .* Hhop2[3]; -hopping(m, 1, t) .* Hhop1pbc[3];- hopping(m, 2, t) .* Hhop2pbc[3];Hint[3]]

    return sparse(rows, cols, vals)
end

function Hfromtemp(m::IonicHubbardPbcToy, t, Hpot1, Hpot2, Hpot3, Hpot4, Hhop1, Hhop2, Hhop1pbc, Hhop2pbc, Hint, HSzSz)

    rows = [Hpot1[1];Hpot2[1];Hpot3[1];Hpot4[1];Hhop1[1];Hhop2[1];Hhop1pbc[1];Hhop2pbc[1];Hint[1];HSzSz[1]]
    cols = [Hpot1[2];Hpot2[2];Hpot3[2];Hpot4[2];Hhop1[2];Hhop2[2];Hhop1pbc[2];Hhop2pbc[2];Hint[2];HSzSz[2]]
    vals = [potential(m, 1, t) .* Hpot1[3];potential(m, 2, t) .* Hpot2[3];potential(m, 1+m.L, t) .* Hpot3[3];potential(m, 2+m.L, t) .* Hpot4[3];hopping(m, 1, t) .* Hhop1[3];hopping(m, 2, t) .* Hhop2[3]; -hopping(m, 1, t) .* Hhop1pbc[3];- hopping(m, 2, t) .* Hhop2pbc[3];Hint[3];HSzSz[3]]

    return sparse(rows, cols, vals)
end



function Hfromtemp(m::IonicHubbard2Pbc, Hpot1, Hpot2, Hhop1, Hhop2, Hhop1pbc, Hhop2pbc, Hint)

    rows = [Hpot1[1];Hpot2[1];Hhop1[1];Hhop2[1];Hhop1pbc[1];Hhop2pbc[1];Hint[1]]
    cols = [Hpot1[2];Hpot2[2];Hhop1[2];Hhop2[2];Hhop1pbc[2];Hhop2pbc[2];Hint[2]]
    vals = [potential(m, 1) .* Hpot1[3];potential(m, 2) .* Hpot2[3];hopping(m, 1) .* Hhop1[3];hopping(m, 2) .* Hhop2[3]; -hopping(m, 1) .* Hhop1pbc[3];- hopping(m, 2) .* Hhop2pbc[3];Hint[3]]

    return sparse(rows, cols, vals)
end

function Hfromtemp(m::IonicHubbard2PbcToy, Hpot1, Hpot2, Hpot3, Hpot4, Hhop1, Hhop2, Hhop1pbc, Hhop2pbc, Hint, HSzSz)

    rows = [Hpot1[1];Hpot2[1];Hpot3[1];Hpot4[1];Hhop1[1];Hhop2[1];Hhop1pbc[1];Hhop2pbc[1];Hint[1];HSzSz[1]]
    cols = [Hpot1[2];Hpot2[2];Hpot3[2];Hpot4[2];Hhop1[2];Hhop2[2];Hhop1pbc[2];Hhop2pbc[2];Hint[2];HSzSz[2]]
    vals = [potential(m, 1) .* Hpot1[3];potential(m, 2) .* Hpot2[3];potential(m, 1+m.L) .* Hpot3[3];potential(m, 2+m.L) .* Hpot4[3];hopping(m, 1) .* Hhop1[3];hopping(m, 2) .* Hhop2[3]; -hopping(m, 1) .* Hhop1pbc[3];- hopping(m, 2) .* Hhop2pbc[3];Hint[3];HSzSz[3]]

    return sparse(rows, cols, vals)
end

function Hfromtemp(m::BosonicSchwinger, Hpot1, Hhop1, Hpair1, Hoffset)

    rows = [Hpot1[1];Hhop1[1];Hpair1[1];Hoffset[1]]
    cols = [Hpot1[2];Hhop1[2];Hpair1[2];Hoffset[2]]
    vals = [Hpot1[3];Hhop1[3];Hpair1[3];Hoffset[3]]

    return sparse(rows, cols, vals)
end

function Hfromtemp(m::IntRmPbc, t, Hpot1, Hpot2, Hhop1, Hhop2, Hhop1pbc, Hhop2pbc, Hint)

    rows = [Hpot1[1];Hpot2[1];Hhop1[1];Hhop2[1];Hhop1pbc[1];Hhop2pbc[1];Hint[1]]
    cols = [Hpot1[2];Hpot2[2];Hhop1[2];Hhop2[2];Hhop1pbc[2];Hhop2pbc[2];Hint[2]]
    vals = [potential(m, 1, t) .* Hpot1[3];potential(m, 2, t) .* Hpot2[3];hopping(m, 1, t) .* Hhop1[3];hopping(m, 2, t) .* Hhop2[3]; -hopping(m, 1, t) .* Hhop1pbc[3];- hopping(m, 2, t) .* Hhop2pbc[3];Hint[3]]

    return sparse(rows, cols, vals)
end

function sparsehamiltonian(m::IntModel1dPbc, states, t; energyoffset=0, init=nothing)
    l = length(states)
    rows = Int[]
    cols = Int[]
    vals = ComplexF64[]
    @showprogress "building H... " for (a, s) in enumerate(states)
        # H[a,a] -= energyoffset
        ss = digits(s, base=2, pad=m.L)
        for i in 1:m.L            
            j = i % m.L + 1 # PBC
            if ss[i] == 1
                push!(rows, a)
                push!(cols, a) 
                push!(vals, potential(m, i, t))    
            end         
            if ss[i] != ss[j]
                sflip = flip(m, s, i, j)
                b = findstate(sflip, states)
                push!(rows, a)
                push!(cols, b) 
                push!(vals, hopping(m, i, j, ss, t))  
            end
            if ss[i] == 1 == ss[j] ## NN - interaction
                push!(rows, a)
                push!(cols, a) 
                push!(vals, interaction(m))  
            end
        end
    end
    return sparse(rows, cols, vals)
end

function lanczostridiag(m::IntModel, Λ; energyoffset=0, t=0)
    basis = generatefixednstates(m)
    H = Hfromtemp(m, t, sparsehamiltonian_template(m, basis)...)

    Φs = Array{Complex{Float64},2}(undef, Λ, basissize(m))
    Φs[1,:] = normalize!(rand(ComplexF64, basissize(m)))
    as = zeros(Float64, Λ)
    ns = zeros(Float64, Λ)
    ens = zeros(Float64, Λ)


    Φs[2,:] = H * Φs[1,:]
    as[1] = real(Φs[1,:]' * Φs[2,:])
    ens[1] = as[1]
    Φs[2,:] = Φs[2,:] - as[1] * Φs[1,:]
    ns[2] = norm(Φs[2,:])
    Φs[2,:] = Φs[2,:] ./ ns[2]
    @showprogress "building lanczos basis..." for i in 2:Λ - 1
        Φs[i + 1,:] = H * Φs[i,:]
        as[i] = real(Φs[i,:]' * Φs[i + 1,:])
        ens[i] = eigen(SymTridiagonal(as[1:i], ns[2:i])).values[1]
        Φs[i + 1,:] = Φs[i + 1,:] - as[i] * Φs[i,:] - ns[i] * Φs[i - 1,:]
        ns[i + 1] = norm(Φs[i + 1,:])
        Φs[i + 1,:] = Φs[i + 1,:] ./ ns[i + 1]


        for m in 1:i # reorthogonalization
            q = (Φs[m,:]' * Φs[i + 1,:])
            Φs[i + 1, :] = (Φs[i + 1, :] - Φs[m, :] * q) ./ (1 - abs2(q))
            if !(norm(Φs[i+1,:]) ≈ 1.) 
                println(m,"  ",norm(Φs[i+1,:]),"  ", (Φs[m,:]' * Φs[i + 1,:]))
                return HΛ, Φs[1:i,:], ens
            end
        end

    end
    Φtmp = H * Φs[Λ,:]
    as[Λ] = real(Φs[Λ,:]' * Φtmp)
    ens[Λ] = eigen(SymTridiagonal(as[1:Λ], ns[2:Λ])).values[1]

    return SymTridiagonal(as, ns[2:end]), Φs, ens
end


function lanczosenergies(m::IntModel, t, Λ, basis, Htemplate ; energyoffset=0, energynum=1, tol=1e-5)
    H = Hfromtemp(m, t, Htemplate...)

    Φs = Array{Complex{Float64},2}(undef, Λ, length(basis))
    Φs[1,:] = normalize!(rand(ComplexF64, length(basis)))
    # Φs[1,:] = normalize!(ones(ComplexF64, length(basis)))

    as = zeros(ComplexF64, Λ)
    ns = zeros(Float64, Λ)
    ens = Array{Float64}[]

    Φs[2,:] = H * Φs[1,:]
    as[1] = real(Φs[1,:]' * Φs[2,:])
    # ens[1] = as[1]
    push!(ens, [as[1]])
    Φs[2,:] = Φs[2,:] - as[1] * Φs[1,:]
    ns[2] = norm(Φs[2,:])
    Φs[2,:] = Φs[2,:] ./ ns[2]
    @showprogress "building lanczos basis..." for i in 2:Λ - 1
        Φs[i + 1,:] = H * Φs[i,:]
        as[i] = Φs[i,:]' * Φs[i + 1,:]
        HΛ = SymTridiagonal(real.(as[1:i]), ns[2:i])
        push!(ens, eigen(HΛ).values)
        # println(ens[i][energynum] - ens[i-1][energynum])
        if i > energynum && abs.(ens[i][energynum] - ens[i - 1][energynum]) < tol
            return HΛ, Φs[1:i,:], ens
        end
        Φs[i + 1,:] = Φs[i + 1,:] - Φs[i,:] * as[i] - ns[i] * Φs[i - 1,:]
        ns[i + 1] = norm(Φs[i + 1,:])
        Φs[i + 1,:] = Φs[i + 1,:] ./ ns[i + 1]

        for m in 1:i # reorthogonalization
            q = (Φs[m,:]' * Φs[i + 1,:])
            Φs[i + 1, :] = (Φs[i + 1, :] - Φs[m, :] * q) ./ (1 - abs2(q))
            if !(norm(Φs[i+1,:]) ≈ 1.) 
                println(m,"  ",norm(Φs[i+1,:]),"  ", (Φs[m,:]' * Φs[i + 1,:]))
                return HΛ, Φs[1:i,:], ens
            end
        end
    end
    Φtmp = H * Φs[Λ,:]
    as[Λ] = real(Φs[Λ,:]' * Φtmp)
    push!(ens, eigen(SymTridiagonal(real.(as[1:Λ]), ns[2:Λ])).values)

    return SymTridiagonal(real.(as), ns[2:end]), Φs, ens
end

function lanczosenergies(m::IntModel, Λ, basis, Htemplate ; energyoffset=0, energynum=1, tol=1e-5)
    H = Hfromtemp(m, Htemplate...)

    Φs = Array{Complex{Float64},2}(undef, Λ, length(basis))
    Φs[1,:] = normalize!(rand(ComplexF64, length(basis)))
    # Φs[1,:] = normalize!(ones(ComplexF64, length(basis)))

    as = zeros(ComplexF64, Λ)
    ns = zeros(Float64, Λ)
    ens = Array{Float64}[]

    Φs[2,:] = H * Φs[1,:]
    as[1] = real(Φs[1,:]' * Φs[2,:])
    # ens[1] = as[1]
    push!(ens, [as[1]])
    Φs[2,:] = Φs[2,:] - as[1] * Φs[1,:]
    ns[2] = norm(Φs[2,:])
    Φs[2,:] = Φs[2,:] ./ ns[2]
    @showprogress "building lanczos basis..." for i in 2:Λ - 1
        Φs[i + 1,:] = H * Φs[i,:]
        as[i] = Φs[i,:]' * Φs[i + 1,:]
        HΛ = SymTridiagonal(real.(as[1:i]), ns[2:i])
        push!(ens, eigen(HΛ).values)
        # println(ens[i][energynum] - ens[i-1][energynum])
        if i > energynum && abs.(ens[i][energynum] - ens[i - 1][energynum]) < tol
            return HΛ, Φs[1:i,:], ens
        end
        Φs[i + 1,:] = Φs[i + 1,:] - Φs[i,:] * as[i] - ns[i] * Φs[i - 1,:]
        ns[i + 1] = norm(Φs[i + 1,:])
        Φs[i + 1,:] = Φs[i + 1,:] ./ ns[i + 1]

        for m in 1:i # reorthogonalization
            q = (Φs[m,:]' * Φs[i + 1,:])
            Φs[i + 1, :] = (Φs[i + 1, :] - Φs[m, :] * q) ./ (1 - abs2(q))
            if !(norm(Φs[i+1,:]) ≈ 1.) 
                println(m,"  ",norm(Φs[i+1,:]),"  ", (Φs[m,:]' * Φs[i + 1,:]))
                return HΛ, Φs[1:i,:], ens
            end
        end
    end
    Φtmp = H * Φs[Λ,:]
    as[Λ] = real(Φs[Λ,:]' * Φtmp)
    push!(ens, eigen(SymTridiagonal(real.(as[1:Λ]), ns[2:Λ])).values)

    return SymTridiagonal(real.(as), ns[2:end]), Φs, ens
end


function lanczosgroundstate(HΛ, Φs; statenum=1) #no longer needed  due to eigsolve
    Φs' * eigen(HΛ).vectors[:,statenum] 
end


