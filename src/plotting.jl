using CSV

"""save data from current plot into csv files."""
function splot()
    dfs =[]
    p = current() #get current plot object
    for (i,subplot) in enumerate(p.subplots)
        for (j,series) in enumerate(subplot.series_list)
            push!(dfs,DataFrame(x=series[:x],y=series[:y]))
        end
    end
    return dfs
end

