# MPS charge pump

This code base is using the Julia Language and [DrWatson](https://juliadynamics.github.io/DrWatson.jl/stable/)
to make a reproducible scientific project named
> MPS charge pump

It is authored by Eric Bertok.

To (locally) reproduce this project, do the following:

0. Download this code base. Notice that raw data are typically not included in the
   git-history and may need to be downloaded independently.
1. Open a Julia console and do:
   ```
   julia> using Pkg
   julia> Pkg.activate("path/to/this/project")
   julia> Pkg.instantiate()
   ```

This will install all necessary packages for you to be able to run the scripts and
everything should work out of the box.


# Eric Bertok's notes:

1. MKL is preferred over Over OpenBLAS for the ITensors package.

   You can check it via
      ```
      julia> using LinearAlgebra
      julia> BLAS.vendor()

      :mkl
      ```
   install via:
   ```
      julia>] add MKL
      julia>] build MKL

   ```
   from a Julia REPL outside of the project (environment should be 1.5, not MPS charge pump during the installation). Open a terminal outside of this folder and open Julia there. Then type the above commands.

