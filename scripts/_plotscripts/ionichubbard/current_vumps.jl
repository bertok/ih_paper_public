using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
size = (350,350/φ)
default(xlabel = L"t/T",ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4)


d = filter(d->d.T == 1000,collect_results!(datadir("sims","ionichubbard","VUMPS_test")))
maxdims = sort(d.maxdim)


##


df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim)
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 5,:],:err)

plot(0:0.0001:1,(df1.qsdn1+df1.qsdn2+df1.qsup1+df1.qsup2)/2,
label = df1.maxdim',
legend_title = L"\chi",
ylabel = L"Q",
xlabel = L"t/T",
ylims = (-0.2,2.1))


annotate!(0.4,-0.05,text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"),8))
wsave(plotsdir("ionichubbard","VUMPS_current",savename(df1)*"_.pdf"),current())



## Test of spin imbalance


df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim)
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 0.2,:],:err)

plot()
for df in eachrow(df1)
    y = abs.(((df.dup -df.ddn)/2))[1:100:end,1]
    plot!(0:0.01:0.99,y,
    label = df.maxdim',
    yscale = :log10,
    legend_title = L"\chi",
    ylabel = L"n_\downarrow - n_\uparrow",
    xlabel = L"t/T",
    legend = :outertopright,
    legend_columns=1,
    ylims = (1e-12,1))
end

annotate!(0.4,1e-10,text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"),8))
wsave(plotsdir("ionichubbard","VUMPS_current_imbalance",savename(df1)*"_.pdf"),current())


## test spin current

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim)
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1,:],:err)

plot()
for df in eachrow(df1)
    y = abs.((df.qsdn1+df.qsdn2-df.qsup1-df.qsup2)/2)[1:100:end,1]
    plot!(0:0.01:1,y,
    label = df.maxdim',
    yscale = :log10,
    legend_title = L"\chi",
    ylabel = L"Q_S",
    xlabel = L"t/T",
    legend = :outertopright,
    legend_columns=1,
    ylims = (1e-12,10))
end

annotate!(0.4,1e-10,text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"),8))
wsave(plotsdir("ionichubbard","VUMPS_current_spincurr",savename(df1)*"_.pdf"),current())




## All converged currents on one plot:

d = filter(d->d.T == 1000,collect_results!(datadir("sims","ionichubbard","VUMPS_test")))

df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 2,:],:err)[1,:] #lowest error
plot(0:0.01:1,((df1.qsdn1+df1.qsdn2+df1.qsup1+df1.qsup2)/2)[1:100:end],
label = L"\mathcal{C}_{\mathrm{RM}, 1}",
ylabel = L"Q",
xlabel = L"t/T",
c = 1,
lw=1.5,
ls = :solid,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1.075,:],:err)[1,:] #lowest error
plot!(0:0.01:1,((df1.qsdn1+df1.qsdn2+df1.qsup1+df1.qsup2)/2)[1:100:end],
label =  L"\mathcal{C}_{\mathrm{RM}, 2}",
ylabel = L"Q",
xlabel = L"t/T",
c = 2,
ls = :dash,
lw=1.5,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1,:],:err)[1,:] #lowest error
plot!(0:0.01:1,((df1.qsdn1+df1.qsdn2+df1.qsup1+df1.qsup2)/2)[1:100:end],
label = L"\mathcal{C}_{\mathrm{IH}, 1}",
ylabel = L"Q",
xlabel = L"t/T",
c = 3,
ls = :dot,
lw = 1.5,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 0.2,:],:err)[1,:] #lowest error
plot!(0:0.01:1,((df1.qsdn1+df1.qsdn2+df1.qsup1+df1.qsup2)/2)[1:100:end],
label = L"\mathcal{C}_{\mathrm{IH}, 2}",
ylabel = L"Q(t)",
xlabel = L"t/T",
c = 4,
ls = :dashdot,
lw = 1.5,
ylims = (-0.2,2.5))

wsave(plotsdir("ionichubbard","VUMPS_current_best",savename(df) * "_.tikz"),current())
wsave(plotsdir("ionichubbard","VUMPS_current_best",savename(df) * "_.pdf"),current())

## spin current:

d = filter(d->d.T == 1000,collect_results!(datadir("sims","ionichubbard","VUMPS_test")))

df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 2,:],:err)[1,:] #lowest error
plot(0:0.01:1,((df1.qsdn1+df1.qsdn2-df1.qsup1-df1.qsup2)/2)[1:100:end],
label = L"\mathcal{C}_{\mathrm{RM}, 1}",
ylabel = L"Q",
xlabel = L"t/T",
c = 1,
lw=1.5,
ls = :solid,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1.075,:],:err)[1,:] #lowest error
plot!(0:0.01:1,((df1.qsdn1+df1.qsdn2-df1.qsup1-df1.qsup2)/2)[1:100:end],
label =  L"\mathcal{C}_{\mathrm{RM}, 2}",
ylabel = L"Q",
xlabel = L"t/T",
c = 2,
ls = :dash,
lw=1.5,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1,:],:err)[1,:] #lowest error
plot!(0:0.01:1,((df1.qsdn1+df1.qsdn2-df1.qsup1-df1.qsup2)/2)[1:100:end],
label = L"\mathcal{C}_{\mathrm{IH}, 1}",
ylabel = L"Q",
xlabel = L"t/T",
c = 3,
ls = :dot,
lw = 1.5,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 0.2,:],:err)[1,:] #lowest error
plot!(0:0.01:1,((df1.qsdn1+df1.qsdn2-df1.qsup1-df1.qsup2)/2)[1:100:end],
label = L"\mathcal{C}_{\mathrm{IH}, 2}",
ylabel = L"Q_S(t)",
xlabel = L"t/T",
c = 4,
ls = :dashdot,
lw = 1.5,
ylims = (-0.3,0.3))

wsave(plotsdir("ionichubbard","VUMPS_current_best","spin_"*savename(df) * "_.tikz"),current())
wsave(plotsdir("ionichubbard","VUMPS_current_best","spin_"*savename(df) * "_.pdf"),current())



### spin imbalance 

d = filter(d->d.T == 1000,collect_results!(datadir("sims","ionichubbard","VUMPS_test")))

df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 2,:],:err)[1,:] #lowest error
plot(0:0.01:0.99,((df1.dup -df1.ddn)/2)[1:100:end,1],
label = L"\mathcal{C}_{\mathrm{RM}, 1}",
ylabel = L"Q",
xlabel = L"t/T",
c = 1,
lw=1.5,
ls = :solid,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1.075,:],:err)[1,:] #lowest error
plot!(0:0.01:0.99,((df1.dup -df1.ddn)/2)[1:100:end,1],
label =  L"\mathcal{C}_{\mathrm{RM}, 2}",
ylabel = L"Q",
xlabel = L"t/T",
c = 2,
ls = :dash,
lw=1.5,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1,:],:err)[1,:] #lowest error
plot!(0:0.01:0.99,((df1.dup -df1.ddn)/2)[1:100:end,1],
label = L"\mathcal{C}_{\mathrm{IH}, 1}",
ylabel = L"Q",
xlabel = L"t/T",
c = 3,
ls = :dot,
lw = 1.5,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 0.2,:],:err)[1,:] #lowest error
plot!(0:0.01:0.99,df1.Sz[1:100:end,1],
label = L"\mathcal{C}_{\mathrm{IH}, 2}",
ylabel = L"n_\downarrow - n_\uparrow",
xlabel = L"t/T",
c = 4,
ls = :dashdot,
lw = 1.5,
ylims = (-0.07,0.07))

wsave(plotsdir("ionichubbard","VUMPS_current_best","spin_bal_"*savename(df) * "_.tikz"),current())
wsave(plotsdir("ionichubbard","VUMPS_current_best","spin_bal_"*savename(df) * "_.pdf"),current())


d = filter(d->d.T == 1000,collect_results!(datadir("sims","ionichubbard","VUMPS_test")))

df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 2,:],:err)[1,:] #lowest error
plot(0:0.01:0.99,(df1.Sz)[1:100:end,2],
label = L"\mathcal{C}_{\mathrm{RM}, 1}",
ylabel = L"Q",
xlabel = L"t/T",
c = 1,
lw=1.5,
ls = :solid,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 0 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1.075,:],:err)[1,:] #lowest error
plot!(0:0.01:0.99,((df1.dup -df1.ddn)/2)[1:100:end,2],
label =  L"\mathcal{C}_{\mathrm{RM}, 2}",
ylabel = L"Q",
xlabel = L"t/T",
c = 2,
ls = :dash,
lw=1.5,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 1,:],:err)[1,:] #lowest error
plot!(0:0.01:0.99,((df1.dup -df1.ddn)/2)[1:100:end,2],
label = L"\mathcal{C}_{\mathrm{IH}, 1}",
ylabel = L"Q",
xlabel = L"t/T",
c = 3,
ls = :dot,
lw = 1.5,
ylims = (-0.2,2.1))

df = sort(filter(d->d.Δc == 1.2 && d.Rδ .== 0.2,d),:maxdim) # 0.9 has larger errors
RΔs = sort(collect(Set(df.RΔ)))
df1 = sort(df[df.RΔ .== 0.2,:],:err)[1,:] #lowest error
plot!(0:0.01:0.99,((df1.dup -df1.ddn)/2)[1:100:end,2],
label = L"\mathcal{C}_{\mathrm{IH}, 2}",
ylabel = L"n_\downarrow - n_\uparrow",
xlabel = L"t/T",
c = 4,
ls = :dashdot,
lw = 1.5,
ylims = (-0.07,0.07))

wsave(plotsdir("ionichubbard","VUMPS_current_best","spin_bal2_"*savename(df) * "_.tikz"),current())
wsave(plotsdir("ionichubbard","VUMPS_current_best","spin_bal2_"*savename(df) * "_.pdf"),current())
