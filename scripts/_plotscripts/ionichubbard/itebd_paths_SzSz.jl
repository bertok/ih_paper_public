using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
size = (350, 350 / φ)
default(xlabel = L"t/T", ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4)


d = sort(collect_results!(datadir("interactive", "ionichubbard", "itebd_szsz_")),[:T,:maxdim])
maxdims = sort(d.maxdim)


## 

for g in groupby(d,[:maxdim])
    plot()
    for df in groupby(g,:J)
    
        ts = [df.ts[1]; df.ts[1][end] + df.dt[1]][1:100:end]
        plot!(ts./ts[end], df.qs[1][1:100:end],
            label = df.J',
            legend_title = L"J",
            ylabel = L"Q",
            xlabel = L"t/T",
            ylims = (-0.2, 2.1))

    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","J", savename(DataFrame(g)) * "_charge" * "_.pdf"), current())
    # annotate!(0.4, -0.05, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df.RΔ[1]),$(df.Rδ[1]),$(df.Δc[1]))"), 8))

    plot()
    for df in groupby(g,:J)
    
        ts = [df.ts[1]; df.ts[1][end] + df.dt[1]][1:100:end]
        plot!(ts./ts[end], df.qsS[1][1:100:end],
            label = df.J',
            legend_title = L"J",
            ylabel = L"Q_S",
            xlabel = L"t/T")

    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","J", savename(DataFrame(g)) * "_spin" * "_.pdf"), current())

    plot()
    for df in groupby(g,:J)
    
        ts = df.ts[1][1:100:end]
        plot!(ts./ts[end], df.lambdamin[1][1:100:end],
            label = df.J',
            legend_title = L"J",
            ylabel = L"\lambda_\mathrm{max}",
            xlabel = L"t/T")
    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","J", savename(DataFrame(g)) * "_cutoff" * "_.pdf"), current())

    plot()
    for df in groupby(g,:J)
    
        ts = df.ts[1][1:100:end]
        plot!(ts./ts[end], df.dup[1][1:100:end,:] - df.ddn[1][1:100:end,:],
            label = df.J',
            legend_title = L"J",
            ylabel = L"n_\uparrow - n_\downarrow",
            xlabel = L"t/T")

    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","J", savename(DataFrame(g)) * "_localS" * "_.pdf"), current())

    plot()
    for df in groupby(g,:J)
    
        ts = df.ts[1][1:100:end]
        plot!(ts./ts[end], df.dim[1][1:100:end],
            label = df.J',
            legend_title = L"J",
            ylabel = L"\chi_\mathrm{i}",
            xlabel = L"t/T")

    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","J", savename(DataFrame(g)) * "_dim" * "_.pdf"), current())

end  

##

for g in groupby(d,[:J])
    plot()
    for df in groupby(g,:maxdim)
    
        ts = [df.ts[1]; df.ts[1][end] + df.dt[1]][1:100:end]
        plot!(ts./ts[end], df.qs[1][1:100:end],
            label = df.maxdim',
            legend_title = L"\chi_i",
            ylabel = L"Q",
            xlabel = L"t/T",
            ylims = (-0.2, 2.1))

    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","maxdim", savename(DataFrame(g)) * "_charge" * "_.pdf"), current())
    # annotate!(0.4, -0.05, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df.RΔ[1]),$(df.Rδ[1]),$(df.Δc[1]))"), 8))

    plot()
    for df in groupby(g,:maxdim)
    
        ts = [df.ts[1]; df.ts[1][end] + df.dt[1]][1:100:end]
        plot!(ts./ts[end], df.qsS[1][1:100:end],
            label = df.maxdim',
            legend_title = L"\chi_i",
            ylabel = L"Q_S",
            xlabel = L"t/T")

    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","maxdim", savename(DataFrame(g)) * "_spin" * "_.pdf"), current())

    plot()
    for df in groupby(g,:maxdim)
    
        ts = df.ts[1][1:100:end]
        plot!(ts./ts[end], df.lambdamin[1][1:100:end],
            label = df.maxdim',
            legend_title = L"\chi_i",
            ylabel = L"\lambda_\mathrm{max}",
            xlabel = L"t/T")
    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","maxdim", savename(DataFrame(g)) * "_cutoff" * "_.pdf"), current())

    plot()
    for df in groupby(g,:maxdim)
    
        ts = df.ts[1][1:100:end]
        plot!(ts./ts[end], df.dup[1][1:100:end,:] - df.ddn[1][1:100:end,:],
            label = df.maxdim',
            legend_title = L"\chi_i",
            ylabel = L"n_\uparrow - n_\downarrow",
            xlabel = L"t/T")

    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","maxdim", savename(DataFrame(g)) * "_localS" * "_.pdf"), current())

    plot()
    for df in groupby(g,:maxdim)
    
        ts = df.ts[1][1:100:end]
        plot!(ts./ts[end], df.dim[1][1:100:end],
            label = df.maxdim',
            legend_title = L"\chi_i",
            ylabel = L"\chi_\mathrm{i}",
            xlabel = L"t/T")

    end
    wsave(plotsdir("ionichubbard", "itebd_szsz","maxdim", savename(DataFrame(g)) * "_dim" * "_.pdf"), current())

end 