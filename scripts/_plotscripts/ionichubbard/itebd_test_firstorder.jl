using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
size = (350, 350 / φ)
default(xlabel = L"t/T", ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4)


d = sort(collect_results!(datadir("sims", "ionichubbard", "tebd_test")),[:T,:maxdim])
maxdims = sort(d.maxdim)


## 

for g in groupby(d,:T)
    for df in groupby(g,:dt)
        plot()

        ts = [df.ts[1]; df.ts[1][end] + df.dt[1]]
        plot!(ts, df.qs,
            label = df.maxdim',
            legend_title = L"\chi",
            ylabel = L"Q",
            xlabel = L"t/T",
            ylims = (-0.2, 2.1))
        wsave(plotsdir("ionichubbard", "itebd_test", savename(DataFrame(df)) * "_.pdf"), current())

    end
    # annotate!(0.4, -0.05, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df.RΔ[1]),$(df.Rδ[1]),$(df.Δc[1]))"), 8))

end  