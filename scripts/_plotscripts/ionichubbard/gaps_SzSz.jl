using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
size = (350, 350 / φ)
default(framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4)

## Gaps δ=0 plot.

d = collect_results(datadir("sims", "ionichubbard", "lanczos_SzSz_gap"))

##

df = filter(d -> d.J == 2, d)

for g1 in groupby(df, :δs)
    for g2 in groupby(g1, :L)

        # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
        plot(g2.Δs, g2.intgap, size = size, xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_\mathrm{exc}", color = :auto, linestyle = :auto)
        # plot!(g2[:Δs],abs.(g2[:chargegap])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C", color = :auto, linestyle = :auto)
        # plot!(g2[:Δs],abs.(g2[:chargegapnew])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C new", color = :auto, linestyle = :auto)
        plot!(g2.Δs, g2.spingap, xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_S", color = :auto, linestyle = :dashdot, linewidth = 2)
        plot!(g2.Δs, g2.intgap2, xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_2", color = :auto, linestyle = :auto, legend = :top, legend_columns = 4)
        ylims!(0, 2.3)

        wsave(plotsdir("ionichubbard", "gaps_SzSz_lanczos" , "gaps",savename(DataFrame(g2)) * ".tikz"), current())
        wsave(plotsdir("ionichubbard", "gaps_SzSz_lanczos" ,"gaps",savename(DataFrame(g2)) * ".pdf"), current())
    end
end

## L scaling:

for g1 in groupby(df, :δs)
    x = [g.L[1] for g in groupby(g1,:L)]
    y = [g.intgap[1][60] for g in groupby(g1,:L)]
    y2 = [g.spingap[1][60] for g in groupby(g1,:L)]
    y3 = [g.intgap2[1][60] for g in groupby(g1,:L)]


    scatter(x,y, c = 1, label = L"\Delta E_\mathrm{int}", xlabel = L"L", ylabel = L"\Delta E", m = :auto)
    scatter!(x,y2, c = 2, label = L"\Delta E_S", xlabel = L"L", ylabel = L"\Delta E", m = :auto)
    scatter!(x,y3, c = 3, label = L"\Delta E_2", xlabel = L"L", ylabel = L"\Delta E", m = :auto, legend = :topright)

    wsave(plotsdir("ionichubbard", "gaps_SzSz_lanczos" , "Ls",savename(DataFrame(g1)) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "gaps_SzSz_lanczos" ,"Ls",savename(DataFrame(g1)) * ".pdf"), current())


end

## interactive:

plotlyjs()


df = sort(filter(d -> d.J == 2 && d.δs == 0, d),:L)


plot(df.Δs, df.intgap, size = size, xlabel = "δ", ylabel = "E", label = df.L', color = :auto, linestyle = :auto)
ylims!(0, 2.3)


