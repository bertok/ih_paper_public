using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))
using DelimitedFiles
φ = Base.MathConstants.φ
size = (350,350/φ)

pgfplotsx(framestyle = :box, grid = :false)

#allow for adding text as legend image in pgfplotsx:
Pre = raw"\pgfplotsset{
    legend image with text/.style={
        legend image code/.code={%
            \node[anchor=center] at (0.3cm,0cm) {#1};
        }
    },
}
\definecolor{cust4}{rgb}{0.1216,0.4667,0.7059}
\definecolor{cust7}{rgb}{1.0,0.498,0.0549}
\definecolor{cust6}{rgb}{0.1725,0.6275,0.1725}
"
push!(PGFPlotsX.CUSTOM_PREAMBLE, Pre)



## berry phase plots
    each = 2
    
    pgfplotsx(framestyle = :box, grid = false, legend = :top) 
    δs =  readdlm(datadir("Armando", "Delta0", "delta.txt"), Float64)[:,1]
    Δs =  readdlm(datadir("Armando", "Delta0", "Del.txt"), Float64)[:,1]
    Cs =  readdlm(datadir("Armando", "Delta0", "Bp.txt"), Float64)[:,1]
    Cs = reshape(Cs, length(Δs), length(δs))
    Δc = Δs[findfirst(x->x>0.5,Cs[:,201])]
    ts = 0:0.001:1
    C1(t) = (0.2*cos(2π * t),2*sin(2π * t))
    C2(t) = (0.2*cos(2π * t),1.2*sin(2π * t))
    C3(t) = (0.2*cos(2π * t),1.4 + 1 * sin(2π * t))
    C4(t) = (0.2*cos(2π * t),1.4 + 0.2 * sin(2π * t))
    
    ### L = 6 !!!
    heatmap((δs[1:each:end],Δs[1:each:end],(Cs[1:each:end,1:each:end] .+ 2) .% 2) , size=(320,350/φ), clims = (0,2), colorbar=true, xlabel=L"\delta", ylabel=L"\Delta/J",colorbar_title=L"\gamma/\pi", framestyle = :box, grid = false, c = cgrad(:greys, rev=true));
    scatter!([(0,Δc),(0,-Δc)],marker = :circ, color = :white, markersize =3,label = false);
    plot!(C1.(ts), c = :black, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},1}");
    plot!(C2.(ts), c = :black, ls = :dash, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},2}");
    plot!(C3.(ts), c= :black, ls = :dot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},1}");
    plot!(C4.(ts), c= :black, ls = :dashdot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},2}" ,legendcolumns = 4);
    ylims!(-2.1,3.5)


    wsave(plotsdir("ionichubbard","berryphase", "berry2.pdf"),current())
    wsave(plotsdir("ionichubbard","berryphase", "berry2.tikz"),current())

##

    xlims!(-0.5,0.5)
   
    Ds = [0,1.288,5]
    RDs = [1,2,3,4]

    m = IonicHubbardPbc(L=6,T=1,U=4)
    ts = 0:0.01:1
    lines = [:solid, :dash, :dot, :dashdot, :dashdotdot]
    pal = palette(:tab10)
    for (c,D) in enumerate(Ds), (i,RD) in enumerate(RDs)
        m = IonicHubbardPbc(Δc = D, RΔ = RD, L=6,T=1,U=4)
        plot!(t->δ(m,t),t->Δ(m,t),ts, legend = :outertop, 
        extra_kwargs = :subplot, 
        label = false,
        legend_columns = 5,
        add = 
        [raw"\addlegendimage{legend image with text = {$~_{\Delta_C/J} \setminus ^{R_\Delta/J}$}}
        \addlegendentry{{}} \addlegendimage{legend image with text = {1}}
        \addlegendentry{{}} \addlegendimage{legend image with text = {2}}
        \addlegendentry{{}} \addlegendimage{legend image with text = {3}}
        \addlegendentry{{}} \addlegendimage{legend image with text = {4}}
        \addlegendentry{{}} \addlegendimage{legend image with text = {0}}
        \addlegendentry{{}} \addlegendimage{line legend,cust4}
        \addlegendentry{{}} \addlegendimage{cust4,dashed}
        \addlegendentry{{}} \addlegendimage{cust4,dotted}
        \addlegendentry{{}} \addlegendimage{cust4,dashdotted}
        \addlegendentry{{}} \addlegendimage{legend image with text = {1.288}}
        \addlegendentry{{}} \addlegendimage{cust7}
        \addlegendentry{{}} \addlegendimage{cust7,dashed}
        \addlegendentry{{}} \addlegendimage{cust7,dotted}
        \addlegendentry{{}} \addlegendimage{cust7,dashdotted}
        \addlegendentry{{}} \addlegendimage{legend image with text = {5}}
        \addlegendentry{{}} \addlegendimage{cust6}
        \addlegendentry{{}} \addlegendimage{cust6,dashed}
        \addlegendentry{{}} \addlegendimage{cust6,dotted}
        \addlegendentry{{}} \addlegendimage{cust6,dashdotted}
        \addlegendentry{{}}"
        ],
        c = pal[c],linestyle = lines[i], framestyle = :box, grid = false,tex_output_standalone = false)
    end



    wsave(plotsdir("ionichubbard","berryphase", "Delta0.pdf"),current())
    wsave(plotsdir("ionichubbard","berryphase", "Delta0.tikz"),current())

    
##
    