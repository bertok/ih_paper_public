using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
size = (350, 350 / φ)
default(xlabel = L"t/T", ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4)


d = filter(d -> d.T == 2000, collect_results!(datadir("sims", "ionichubbard", "VUMPS_test_2")))
maxdims = sort(d.maxdim)


## 

for RΔ ∈ [1, 0.2]
    df = sort(filter(d -> d.Δc == 1.2 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot(0:0.00005:1, (df1.qsdn1 + df1.qsdn2 + df1.qsup1 + df1.qsup2) / 2,
        label = df1.maxdim',
        legend_title = L"\chi",
        ylabel = L"Q",
        xlabel = L"t/T",
        ylims = (-0.2, 2.1))


    annotate!(0.4, -0.05, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", savename(df1) * "_.pdf"), current())
end


## Test of spin imbalance

for RΔ ∈ [1, 0.2]

    df = sort(filter(d -> d.Δc == 1.2 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = abs.(((df.dup - df.ddn) / 2))[1:100:end, 1]
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            yscale = :log10,
            legend_title = L"\chi",
            ylabel = L"|n_\downarrow - n_\uparrow|",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (1e-12, 1))
    end
    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "imbalance_" * savename(df1) * "_.pdf"), current())

end



## test spin current

for RΔ ∈ [1, 0.2]

    df = sort(filter(d -> d.Δc == 1.2 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = abs.((df.qsdn1 + df.qsdn2 - df.qsup1 - df.qsup2) / 2)[2:100:end, 1]
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            yscale = :log10,
            legend_title = L"\chi",
            ylabel = L"|Q_S|",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (1e-12, 10))
    end

    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "spincurr_" * savename(df1) * "_.pdf"), current())

end

## cutoff

for RΔ ∈ [1, 0.2]

    df = sort(filter(d -> d.Δc == 1.2 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = abs.((df.cutoff)[1:100:end, 1])
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            yscale = :log10,
            legend_title = L"\chi",
            ylabel = L"|\lambda_\mathrm{min}|",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (1e-12, 10))
    end

    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "cutoff_" * savename(df1) * "_.pdf"), current())

end

## energy

for RΔ ∈ [1, 0.2]

    df = sort(filter(d -> d.Δc == 1.2 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = real.((df.en01)[1:100:end, :])
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            legend_title = L"\chi",
            ylabel = L"\epsilon_0",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (-3, 1))
    end
    for df in eachrow(df1)
        y = real.((df.en02)[1:100:end, :])
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            legend_title = L"\chi",
            ylabel = L"\epsilon_0",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (-3, 1))
    end

    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "energy_" * savename(df1) * "_.pdf"), current())

end

## density

for RΔ ∈ [1, 0.2]

    df = sort(filter(d -> d.Δc == 1.2 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = (df.dup)[1:100:end, :]
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            legend_title = L"\chi",
            ylabel = L"n_\uparrow",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (-0.1, 0.9))
            
    end

    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "density_" * savename(df1) * "_.pdf"), current())

end


#### RM ##################################

## 

for RΔ ∈ [1.075,2]
    df = sort(filter(d -> d.Δc == 0 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot(0:0.00005:1, (df1.qsdn1 + df1.qsdn2 + df1.qsup1 + df1.qsup2) / 2,
        label = df1.maxdim',
        legend_title = L"\chi",
        ylabel = L"Q",
        xlabel = L"t/T",
        ylims = (-0.2, 2.1))


    annotate!(0.4, -0.05, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", savename(df1) * "_.pdf"), current())
end


## Test of spin imbalance

for RΔ ∈ [1.075,2]

    df = sort(filter(d -> d.Δc == 0 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = abs.(((df.dup - df.ddn) / 2))[1:100:end, 1]
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            yscale = :log10,
            legend_title = L"\chi",
            ylabel = L"|n_\downarrow - n_\uparrow|",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (1e-12, 1))
    end
    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "imbalance_" * savename(df1) * "_.pdf"), current())

end



## test spin current

for RΔ ∈ [1.075,2]

    df = sort(filter(d -> d.Δc == 0 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = abs.((df.qsdn1 + df.qsdn2 - df.qsup1 - df.qsup2) / 2)[2:100:end, 1]
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            yscale = :log10,
            legend_title = L"\chi",
            ylabel = L"|Q_S|",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (1e-12, 10))
    end

    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "spincurr_" * savename(df1) * "_.pdf"), current())

end

## cutoff

for RΔ ∈ [1.075,2]

    df = sort(filter(d -> d.Δc == 0 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = abs.((df.cutoff)[1:100:end, 1])
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            yscale = :log10,
            legend_title = L"\chi",
            ylabel = L"|\lambda_\mathrm{min}|",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (1e-12, 10))
    end

    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "cutoff_" * savename(df1) * "_.pdf"), current())

end

## energy

for RΔ ∈ [1.075,2]

    df = sort(filter(d -> d.Δc == 0 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = real.((df.en01)[1:100:end, :])
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            legend_title = L"\chi",
            ylabel = L"\epsilon_0",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (-3, 1))
    end
    for df in eachrow(df1)
        y = real.((df.en02)[1:100:end, :])
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            legend_title = L"\chi",
            ylabel = L"\epsilon_0",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (-3, 1))
    end

    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "energy_" * savename(df1) * "_.pdf"), current())

end

## density

for RΔ ∈ [1.075,2]

    df = sort(filter(d -> d.Δc == 0 && d.Rδ .== 0.2, d), :maxdim)
    RΔs = sort(collect(Set(df.RΔ)))
    df1 = sort(df[df.RΔ.==RΔ, :], :err)

    plot()
    for df in eachrow(df1)
        y = (df.dup)[1:100:end, :]
        plot!(0:0.005:0.995, y,
            label = df.maxdim',
            legend_title = L"\chi",
            ylabel = L"n_\uparrow",
            xlabel = L"t/T",
            legend = :outertopright,
            legend_columns = 1,
            ylims = (-0.1, 0.9))
            
    end

    annotate!(0.4, 1e-10, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df1.RΔ[1]),$(df1.Rδ[1]),$(df1.Δc[1]))"), 8))
    wsave(plotsdir("ionichubbard", "VUMPS_current2", "density_" * savename(df1) * "_.pdf"), current())

end