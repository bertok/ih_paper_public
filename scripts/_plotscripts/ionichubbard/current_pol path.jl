using DrWatson
@quickactivate
include(srcdir("init.jl"))
φ = Base.MathConstants.φ
size = (350,350/φ)
using Plots.PlotMeasures

d = collect_results(datadir("sims", "ionichubbard", "lanczos_current_path_order"))

sort!(d,:L)
Ls = sort(collect(Set(d.L)))
Δcs = sort(collect(Set(d.Δc)))
RΔs = sort(collect(Set(d.RΔ)))


## polar plot pol
begin
    pgfplotsx(grid=false,size=(263, 250), framestyle=:box)
    lss = [:dot, :dashdot]
    lbls = [L"\mathcal C_\mathrm{IH,1}";L"\mathcal C_\mathrm{IH,2}"]
    cs = [3,4]

    plot()
    for (c, RΔ) in enumerate(reverse(RΔs))
        df = filter(d -> d.RΔ == RΔ && d.T == 1000 && d.L == 8, d)

        x = [0.25 .+ (df.ts[i][1:10:end] ./ df.T[i]) for i in eachindex(df.ts)]
        y = [(df.pol[i] * 2π)[1:10:end] for i in eachindex(df.ts)]
        plot!(y, x,
    label=lbls[c],
    yticks=([0.25,1.25], ["0",L"2\pi"]),
    xticks=([0,0,0,π / 2,π,1.5π], ["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    xformatter=:radians, 
    legend=:outertopright,
    ylabel=L"\theta",
    xlabel=L"P",
    ls = lss[c],
    c = cs[c],
    legend_columns = 1,
    proj=:polar,linestyle=:auto, linewidth=1.5)

    end

    wsave(plotsdir("ionichubbard", "path_order", "pol_polar_" * savename(df) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "path_order", "pol_polar_" * savename(df) * ".pdf"), current())

end


## polar plot pol interactive
begin
    f = load(datadir("interactive","ionichubbard","pols_" * "L=8_RΔ=2_Rδ=0.2_T=100_U=4_m=IonicHubbardPbc_Δc=0_ϕ₀=0" * ".jld2"),"f")
    f2 = load(datadir("interactive","ionichubbard","pols_" * "L=8_RΔ=1.2_Rδ=0.2_T=100_U=4_m=IonicHubbardPbc_Δc=0_ϕ₀=0" * ".jld2"),"f")
    pgfplotsx(grid=false,size=(263, 250), framestyle=:box)

    fs = [f,f2]
    lss = [:solid, :dash]
    lbls = [L"\mathcal C_\mathrm{RM,1}";L"\mathcal C_\mathrm{RM,2}"]

    plot()
    for (i,f) in enumerate(fs)
    plot!()

        x = 0.25 .+ (0:0.001:0.999)
        y = (f[:pol] * 2π)
        plot!(y, x,
    label=lbls[i],
    yticks=([0.25,1.25], ["0",L"2\pi"]),
    xticks=([0,0,0,π / 2,π,1.5π], ["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    xformatter=:radians,
    legend=:outertopright,
    ylabel=L"\theta",
    xlabel=L"P",
    proj=:polar,linestyle=:auto, color=:auto, linewidth=1.5, legend_columns=1)

    end
    wsave(plotsdir("ionichubbard", "path_order", "pol_polar_" * savename(f) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "path_order", "pol_polar_" * savename(f) * ".pdf"), current())

end



## all pols in one:

## polar plot pol
begin
   

    f = load(datadir("interactive","ionichubbard","pols_" * "L=8_RΔ=2_Rδ=0.2_T=100_U=4_m=IonicHubbardPbc_Δc=0_ϕ₀=0" * ".jld2"),"f")
    f2 = load(datadir("interactive","ionichubbard","pols_" * "L=8_RΔ=1.2_Rδ=0.2_T=100_U=4_m=IonicHubbardPbc_Δc=0_ϕ₀=0" * ".jld2"),"f")
    pgfplotsx(grid=false,size=(263, 250), framestyle=:box)

    fs = [f,f2]
    lss = [:solid, :dash]
    lbls = [L"\mathcal C_\mathrm{RM,1}";L"\mathcal C_\mathrm{RM,2}"]
    plot()
    for (i,f) in enumerate(fs)
    plot!()

        x = 0.25 .+ (0:0.001:0.999)
        y = (f[:pol] * 2π)
        plot!(y, x,
    label=lbls[i],
    yticks=([0.25,1.25], ["0",L"2\pi"]),
    xticks=([0,0,0,π / 2,π,1.5π], ["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    xformatter=:radians,
    legend=:outertopright,
    ylabel=L"\theta",
    xlabel=L"P",
    proj=:polar,linestyle=:auto, color=:auto, linewidth=1.5, legend_columns=1)

    end

    pgfplotsx(grid=false,size=(263, 250), framestyle=:box)
    lss = [:dot, :dashdot]
    lbls = [L"\mathcal C_\mathrm{IH,1}";L"\mathcal C_\mathrm{IH,2}"]
    cs = [3,4]


    for (c, RΔ) in enumerate(reverse(RΔs))
        df = filter(d -> d.RΔ == RΔ && d.T == 1000 && d.L == 8, d)

        x = [0.25 .+ (df.ts[i][1:10:end] ./ df.T[i]) for i in eachindex(df.ts)]
        y = [(df.pol[i] * 2π)[1:10:end] for i in eachindex(df.ts)]
        plot!(y, x,
    label=lbls[c],
    yticks=([0.25,1.25], ["0",L"2\pi"]),
    xticks=([0,0,0,π / 2,π,1.5π], ["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    xformatter=:radians, 
    legend=:outertopright,
    ylabel=L"\theta",
    xlabel=L"P",
    ls = lss[c],
    c = cs[c],
    legend_columns = 1,
    proj=:polar,linestyle=:auto, linewidth=1.5)

    end

    wsave(plotsdir("ionichubbard", "path_order", "all_pol_polar_" * savename(f) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "path_order", "all_pol_polar_" * savename(f) * ".pdf"), current())

end


## regular pols
begin 
    pgfplotsx(grid=false)
    # gr(grid = false)
    plot()
    for RΔ in RΔs
        default(size=(300, 200), framestyle=:box)
        df = filter(d -> d.RΔ == RΔ && d.T == 1000 && d.L == 8, d)
    
        x = [(df.ts[i] ./ df.T[i])[1:100:end] for i in eachindex(df.ts)]
        y = [(df.pol[i])[1:100:end] for i in eachindex(df.ts)]

        plot!(x, y,
        label=string.((df.RΔ)') .* ["; " "; " "; "] .* string.((df.L)'), 
        legend_title=L"\quad\quad R_\Delta/J;\; L",
        legend=:outertopright,
        xlabel=L"t/T",
        ylabel=L"P",
        linestyle=:auto,
        linewidth=1.5,
        ylims=(-0.5, 0.5))
    
    end
    df = filter(d -> d.T == 5000 && d.L == 10, d)
    # wsave(plotsdir("ionichubbard", "path_order", "pol_" * savename(df) * ".tikz"), current())
    # wsave(plotsdir("ionichubbard", "path_order", "pol_" * savename(df) * ".pdf"), current())
end

## pumped charge RM

d = filter(d->d.T == 5000 && d.Δc == 0,collect_results!(datadir("sims", "ionichubbard", "lanczos_current_path_order2")))
Ls = sort(collect(Set(d.L)))
Δcs = sort(collect(Set(d.Δc)))
RΔs = sort(collect(Set(d.RΔ)))
Rδs = sort(collect(Set(d.Rδ)))

pgfplotsx(grid=false)
# gr(grid = false)
default(size=size, framestyle=:box)
plot()
L=6
Rδ=0.2
df = filter(d -> d.Rδ == Rδ && d.L == L, d)
for RΔ in RΔs
    df = filter(d -> d.RΔ == RΔ && d.Rδ == Rδ && d.L == L, d)

    x = [(df.ts[i] ./ df.T[i])[1:100:end] for i in eachindex(df.ts)]
    y = [(df.qs[i][1:end - 1])[1:100:end]  for i in eachindex(df.ts)]

    plot!(x, y,
    label=string.((round.(df.RΔ,digits=2))'), 
    legend_title=L"\quad\quad R_\Delta/J",
    legend=:bottom,
    legend_columns = 5,
    xlabel=L"t/T",
    annotation=(0.25,4,text(latexstring("L=$L,\\;R_\\delta = $(Rδ)"),8)),
    ylabel=L"\Delta Q",
    linestyle=:auto,
    linewidth=1.5)

end
wsave(plotsdir("ionichubbard", "path_order2", "charge_" * savename(df) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "path_order2", "charge_" * savename(df) * ".pdf"), current())
current()



## pumped charge Ionic Hubbard
d = filter(d->d.T == 5000 && d.Δc != 0,collect_results!(datadir("sims", "ionichubbard", "lanczos_current_path_order2")))
Ls = sort(collect(Set(d.L)))
Δcs = sort(collect(Set(d.Δc)))
RΔs = sort(collect(Set(d.RΔ)))
Rδs = sort(collect(Set(d.Rδ)))

begin 
    pgfplotsx(grid=false)
    # gr(grid = false)
    default(size=(300, 200), framestyle=:box)
    plot()
    for RΔ in RΔs
        for Rδ in Rδs
        df = filter(d -> d.RΔ == RΔ, d)
    
        x = [(df.ts[i] ./ df.T[i])[1:100:end] for i in eachindex(df.ts)]
        y = [(df.qs[i][1:end - 1])[1:100:end]  for i in eachindex(df.ts)]

        plot!(x, y,
        # label=string.((df.RΔ)') .* ["; " "; "] .* string.((df.L)'), 
        legend_title=L"R_\Delta/J;\; L",
        legend=:outertopright,
        xlabel=L"t/T",
        ylabel=L"\Delta Q",
        linestyle=:auto,
        linewidth=1.5)
        end
    end
    # wsave(plotsdir("ionichubbard", "path_order", "charge_" * savename(df) * ".tikz"), current())
    # wsave(plotsdir("ionichubbard", "path_order", "charge_" * savename(df) * ".pdf"), current())
    current()
end


## exc_gap
begin 
    pgfplotsx(grid=false)
    # gr(grid = false)
    plot()
    for RΔ in RΔs
        default(size=(300, 200), framestyle=:box)
        df = filter(d -> d.RΔ == RΔ && d.T == 5000, d)
    
        x = [(df.ts[i] ./ df.T[i])[1:100:end] for i in eachindex(df.ts)]
        y = [(df.intgap[i][1:end])[1:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label=string.((df.RΔ)') .* ["; " "; " "; "] .* string.((df.L)'), 
        legend_title=L"\quad\quad R_\Delta/J;\; L",
        legend=:outertopright,
        xlabel=L"t/T",
        ylabel=L"\Delta E_\mathrm{exc}",
        linestyle=:auto,
        ylims = (0,1.5),
    linewidth=1.5)
    wsave(plotsdir("ionichubbard", "path_order", "gap_exc_" * savename(df) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "path_order", "gap_exc_" * savename(df) * ".pdf"), current())

    end    
    
end


## charge_gap
begin 
    pgfplotsx(grid=false)
    # gr(grid = false)
    plot()
    for RΔ in RΔs
        default(size=(300, 200), framestyle=:box)
        df = filter(d -> d.RΔ == RΔ && d.T == 5000, d)
    
        x = [(df.ts[i] ./ df.T[i])[1:100:end] for i in eachindex(df.ts)]
        y = [(df.chargegap[i][1:end])[1:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label=string.((df.RΔ)') .* ["; " "; " "; "] .* string.((df.L)'), 
        legend_title=L"\quad\quad R_\Delta/J;\; L",
        legend=:outertopright,
        xlabel=L"t/T",
        ylabel=L"\Delta E_C",
        linestyle=:auto,
        linewidth=1.5)
        wsave(plotsdir("ionichubbard", "path_order", "gap_charge_" * savename(df) * ".tikz"), current())
        wsave(plotsdir("ionichubbard", "path_order", "gap_charge_" * savename(df) * ".pdf"), current())    

    end
end


## spin_gap
begin 
    pgfplotsx(grid=false)
    # gr(grid = false)
    plot()
    for RΔ in RΔs
        default(size=(300, 200), framestyle=:box)
        df = filter(d -> d.RΔ == RΔ && d.T == 5000, d)
    
        x = [(df.ts[i] ./ df.T[i])[1:100:end] for i in eachindex(df.ts)]
        y = [(df.spingap[i][1:end])[1:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label=string.((df.RΔ)') .* ["; " "; " "; "] .* string.((df.L)'), 
        legend_title=L"\quad\quad R_\Delta/J;\; L",
        legend=:outertopright,
        xlabel=L"t/T",
        ylabel=L"\Delta E_S",
        linestyle=:auto,
        linewidth=1.5)
        wsave(plotsdir("ionichubbard", "path_order", "gap_spin_" * savename(df) * ".tikz"), current())
        wsave(plotsdir("ionichubbard", "path_order", "gap_spin_" * savename(df) * ".pdf"), current())
    
    end
end

## adiabaticity
begin 
    pgfplotsx(grid=false)
    # gr(grid = false)
    plot()
    for RΔ in RΔs
        default(size=(300, 200), framestyle=:box)
        df = filter(d -> d.RΔ == RΔ && d.T == 1000, d)
    
        x = [(df.ts[i] ./ df.T[i])[1:10:end] for i in eachindex(df.ts)]
        y = [(df.adiabaticity[i][1:end])[1:10:end] for i in eachindex(df.ts)]

        plot(x, y,
        label=string.((df.RΔ)') .* ["; " "; " "; "] .* string.((df.L)'), 
        legend_title=L"R_\Delta/J;\; L",
        legend=:outertopright,
        xlabel=L"t/T",
        ylabel=L"\alpha",
        linestyle=:auto,
        linewidth=1.5,
        color = :auto)
        # wsave(plotsdir("ionichubbard", "path_order", "adiabaticity_" * savename(df) * ".tikz"), current())
        # wsave(plotsdir("ionichubbard", "path_order", "adiabaticity_" * savename(df) * ".pdf"), current())
    
    end
end

## sdi
begin 
    pgfplotsx(grid=false)
    # gr(grid = false)
    plot()
    for RΔ in RΔs
        default(size=(300, 200), framestyle=:box)
        df = filter(d -> d.RΔ == RΔ && d.T == 5000, d)
    
        x = [(df.ts[i] ./ df.T[i])[1:100:end] for i in eachindex(df.ts)]
        y = [(df.sdi[i][1:end]./df.L[i])[1:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label=string.((df.RΔ)') .* ["; " "; " "; "] .* string.((df.L)'), 
        legend_title=L"R_\Delta/J;\; L",
        legend=:outertopright,
        xlabel=L"t/T",
        ylabel=L"\mathcal O_\mathrm{SDI}",
        linestyle=:auto,
        linewidth=1.5,
        color = :auto)

        wsave(plotsdir("ionichubbard", "path_order", "sdi_" * savename(df) * ".tikz"), current())
        wsave(plotsdir("ionichubbard", "path_order", "sdi_" * savename(df) * ".pdf"), current())
    

    end
end


## allgaps
begin 
    pgfplotsx(grid=false)
    # gr(grid = false)
    plot()
    L = 10
    for RΔ in RΔs
        default(size=(300, 200), framestyle=:box)
        df = filter(d -> d.RΔ == RΔ && d.T == 5000 && d.L == L, d)
    
        x = [(df.ts[i] ./ df.T[i])[1:100:end] for i in eachindex(df.ts)]
        y = [[(df.intgap[i][1:end])[1:100:end] for i in eachindex(df.ts)]]
        push!(y, [(df.chargegap[i][1:end])[1:100:end] for i in eachindex(df.ts)])
        push!(y, [(df.spingap[i][1:end])[1:100:end] for i in eachindex(df.ts)])

        plot(x, y,
        label=[L"\Delta E_\mathrm{exc}" L"\Delta E_C" L"\Delta E_S"], 
        legend_title=latexstring("L = $L"),
        legend=:outertopright,
        xlabel=L"t/T",
        ylabel=L"\mathcal O_\mathrm{SDI}",
        linestyle=:auto,
        linewidth=1.5,
        ylims = (0,3),
        color = :auto)

        wsave(plotsdir("ionichubbard", "path_order", "allgaps_" * savename(df) * ".tikz"), current())
        wsave(plotsdir("ionichubbard", "path_order", "allgaps_" * savename(df) * ".pdf"), current())
    

    end
end