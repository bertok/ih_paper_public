using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

using Plots.PlotMeasures
pgfplotsx(grid = false)
##

fs = load(datadir("interactive","ionichubbard","gapstest.jld2"),"fs")
f = load(datadir("interactive","ionichubbard","mps_pbc_L=6_N=6_U=4.0_filename=L=6_N=6_U=4.0_m=IonicHubbard2Pbc_maxdim=1000_Δ=2.0_δ=0.25_δs=0_m=IonicHubbard2Pbc_δs=0.jld2"),"f")

##

f6 = load(datadir("interactive","ionichubbard","new_inst_6.jld2"),"f")
f8 = load(datadir("interactive","ionichubbard","new_inst_8.jld2"),"f")
f10 = load(datadir("interactive","ionichubbard","new_inst_10.jld2"),"f")

pgfplotsx(legend = :top, framestyle = :box,xticks = ([0,π/2,π,3π/2,2π],[0,L"\pi/2",L"\pi",L"3\pi/2",L"2\pi"]),size = (300,200),linewidth = 1.2, xlabel = L"\theta", ylabel = L"\Delta E_\mathrm{exc}", linestyle = :auto, legend_columns = 4)
plot(x,(f6[:intgap]) ,label = L"L=6")
plot!(x,(f8[:intgap]),label = L"L=8")
plot!(x,(f10[:intgap]),label = L"L=10")
ylims!(0,2)
wsave(plotsdir("ionichubbard", "gaps_path", "int_" * savename(f) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "gaps_path", "int_" * savename(f) * ".pdf"), current())


##

f6 = load(datadir("interactive","ionichubbard","new_inst_6.jld2"),"f")
f8 = load(datadir("interactive","ionichubbard","new_inst_8.jld2"),"f")
f10 = load(datadir("interactive","ionichubbard","new_inst_10.jld2"),"f")

pgfplotsx(legend = :top, framestyle = :box,xticks = ([0,π/2,π,3π/2,2π],[0,L"\pi/2",L"\pi",L"3\pi/2",L"2\pi"]),size = (300,200),linewidth = 1.2, xlabel = L"\theta", ylabel = L"\Delta E_S", linestyle = :auto, legend_columns = 4)
plot(x,(f6[:spingap]) ,label = L"L=6")
plot!(x,(f8[:spingap]),label = L"L=8")
plot!(x,(f10[:spingap]),label = L"L=10")
ylims!(0,2.1)
wsave(plotsdir("ionichubbard", "gaps_path", "spin_" * savename(f) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "gaps_path", "spin_" * savename(f) * ".pdf"), current())

##




##

f6 = load(datadir("interactive","ionichubbard","new_inst2_6.jld2"),"f")
f8 = load(datadir("interactive","ionichubbard","new_inst2_8.jld2"),"f")
f10 = load(datadir("interactive","ionichubbard","new_inst2_10.jld2"),"f")

x = f6[:ts]/f6[:T]*2π
pgfplotsx(legend = :top, framestyle = :box,xticks = ([0,π/2,π,3π/2,2π],[0,L"\pi/2",L"\pi",L"3\pi/2",L"2\pi"]),size = (300,200),linewidth = 1.2, xlabel = L"\theta", ylabel = L"\Delta E_\mathrm{exc}", linestyle = :auto, legend_columns = 4)
plot(x,(f6[:intgap]) ,label = L"L=6")
plot!(x,(f8[:intgap]),label = L"L=8")
plot!(x,(f10[:intgap]),label = L"L=10")
ylims!(0,2)
wsave(plotsdir("ionichubbard", "gaps_path", "int2_" * savename(f) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "gaps_path", "int2_" * savename(f) * ".pdf"), current())


##

f6 = load(datadir("interactive","ionichubbard","new_inst2_6.jld2"),"f")
f8 = load(datadir("interactive","ionichubbard","new_inst2_8.jld2"),"f")
f10 = load(datadir("interactive","ionichubbard","new_inst2_10.jld2"),"f")

x = f6[:ts]/f6[:T]*2π
pgfplotsx(legend = :top, framestyle = :box,xticks = ([0,π/2,π,3π/2,2π],[0,L"\pi/2",L"\pi",L"3\pi/2",L"2\pi"]),size = (300,200),linewidth = 1.2, xlabel = L"\theta", ylabel = L"\Delta E_S", linestyle = :auto, legend_columns = 4)
plot(x,(f6[:spingap]) ,label = L"L=6")
plot!(x,(f8[:spingap]),label = L"L=8")
plot!(x,(f10[:spingap]),label = L"L=10")
ylims!(0,2.1)
wsave(plotsdir("ionichubbard", "gaps_path", "spin2_" * savename(f) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "gaps_path", "spin2_" * savename(f) * ".pdf"), current())

##

f6 = load(datadir("interactive","ionichubbard","new_inst_6.jld2"),"f")
f8 = load(datadir("interactive","ionichubbard","new_inst_8.jld2"),"f")
f10 = load(datadir("interactive","ionichubbard","new_inst_10.jld2"),"f")

x = f6[:ts]/f6[:T]*2π
pgfplotsx(legend = :bottom, framestyle = :box,xticks = ([0,π/2,π,3π/2,2π],[0,L"\pi/2",L"\pi",L"3\pi/2",L"2\pi"]),size = (300,200),linewidth = 1.2, xlabel = L"\theta", ylabel = L"\Delta E_C", linestyle = :auto, legend_columns = 4)
plot(x,(f6[:chargegap]) ,label = L"L=6")
plot!(x,(f8[:chargegap]),label = L"L=8")
plot!(x,(f10[:chargegap]),label = L"L=10")
ylims!(0,2.6)
wsave(plotsdir("ionichubbard", "gaps_path", "charge_" * savename(f) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "gaps_path", "charge_" * savename(f) * ".pdf"), current())


##

f6 = load(datadir("interactive","ionichubbard","new_inst2_6.jld2"),"f")
f8 = load(datadir("interactive","ionichubbard","new_inst2_8.jld2"),"f")
f10 = load(datadir("interactive","ionichubbard","new_inst2_10.jld2"),"f")


x = f6[:ts]/f6[:T]*2π
pgfplotsx(legend = :bottom, framestyle = :box,xticks = ([0,π/2,π,3π/2,2π],[0,L"\pi/2",L"\pi",L"3\pi/2",L"2\pi"]),size = (300,200),linewidth = 1.2, xlabel = L"\theta", ylabel = L"\Delta E_C", linestyle = :auto, legend_columns = 4)
plot(x,(f6[:chargegap]) ,label = L"L=6")
plot!(x,(f8[:chargegap]),label = L"L=8")
plot!(x,(f10[:chargegap]),label = L"L=10")
ylims!(0,2.6)
wsave(plotsdir("ionichubbard", "gaps_path", "charge2_" * savename(f6) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "gaps_path", "charge2_" * savename(f6) * ".pdf"), current())

##

f6 = load(datadir("interactive","ionichubbard","new_inst2_6.jld2"),"f")
f8 = load(datadir("interactive","ionichubbard","new_inst2_8.jld2"),"f")
f10 = load(datadir("interactive","ionichubbard","new_inst2_10.jld2"),"f")
x = f6[:ts]/f6[:T]*2π
pgfplotsx(legend = :top, framestyle = :box,xticks = ([0,π/2,π,3π/2,2π],[0,L"\pi/2",L"\pi",L"3\pi/2",L"2\pi"]),size = (300,200),linewidth = 1.2, xlabel = L"\theta", ylabel = L"\mathcal O _\mathrm{SDI}", linestyle = :auto, legend_columns = 4)
plot(x,(f8[:sdi]) ,label = L"0")
plot!(x,(f8[:sdi_exc]),label = L"\mathrm{exc}")
plot!(x,(f8[:sdi_exc2]),label = L"2")
plot!(x,(f8[:sdiS]),label = L"S")
ylims!(-0.8,1)
wsave(plotsdir("ionichubbard", "gaps_path", "sdi2_" * savename(f8) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "gaps_path", "sdi2_" * savename(f8) * ".pdf"), current())

##

f6 = load(datadir("interactive","ionichubbard","new_inst_6.jld2"),"f")
f8 = load(datadir("interactive","ionichubbard","new_inst_8.jld2"),"f")
f10 = load(datadir("interactive","ionichubbard","new_inst_10.jld2"),"f")
x = f8[:ts]/f8[:T]*2π
pgfplotsx(legend = :top, framestyle = :box,xticks = ([0,π/2,π,3π/2,2π],[0,L"\pi/2",L"\pi",L"3\pi/2",L"2\pi"]),size = (300,200),linewidth = 1.2, xlabel = L"\theta", ylabel = L"\mathcal O _\mathrm{SDI}", linestyle = :auto, legend_columns = 4)
plot(x,(f8[:sdi]) ,label = L"0")
plot!(x,(f8[:sdi_exc]),label = L"\mathrm{exc}")
plot!(x,(f8[:sdi_exc2]),label = L"2")
plot!(x,(f8[:sdiS]),label = L"S")
ylims!(-0.8,1)
wsave(plotsdir("ionichubbard", "gaps_path", "sdi2_" * savename(f8) * ".tikz"), current())
wsave(plotsdir("ionichubbard", "gaps_path", "sdi2_" * savename(f8) * ".pdf"), current())

##


