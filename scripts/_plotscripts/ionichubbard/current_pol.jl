using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

using Plots.PlotMeasures

d = filter(x -> x.T == 500,collect_results(datadir("sims","ionichubbard","lanczos_current_path","U=4")))

sort!(d,:L)
Ls = sort(collect(Set(d.L)))
Δcs = sort(collect(Set(d.Δc)))
RΔs = sort(collect(Set(d.RΔ)))


#polar plot pol
begin
pgfplotsx()

pal = palette(:tab10)
default(size = (263,250), framestyle = :box, color = :tab10)

for (c,Δc) in enumerate(Δcs)
    df = filter(d -> d.Δc == Δc && d.L == 10,d)

    x = [0.25 .+ (df.ts[i][1:10:end]./df.T[i]) for i in eachindex(df.ts)]
    y = [(df.pol[i]*2π)[1:10:end] for i in eachindex(df.ts)]
    plot(y, x,
    label = (df.RΔ)',
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    xformatter = :radians, 
    legend_title = L"R_\Delta/J",
    legend = :outertopright,
    ylabel = L"t/T",
    xlabel = L"P",
    proj = :polar,linestyle = :auto, color = pal[c])

    wsave(plotsdir("ionichubbard","pols",savename(df)*".pdf"),current())
    wsave(plotsdir("ionichubbard","pols",savename(df)*".tikz"),current())


end

end

# regular pol plot
begin 
    gr()
    for Δc in Δcs
        default(size = (300,200), framestyle = :box)
        df = filter(d -> d.Δc == Δc && d.L == 10,d)
    
        x = [(df.ts[i][1:100:end]./df.T[i]) for i in eachindex(df.ts)]
        y = [(df.pol[i])[1:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label = (df.RΔ)', 
        legend_title = L"R_\Delta/J",
        legend = :outertopright,
        xlabel = L"t/T",
        ylabel = L"P",
        ylims = (-0.5,0.5),
        linestyle = :auto)
    
        wsave(plotsdir("ionichubbard","pols_reg",savename(df)*".pdf"),current())
    
    end
    
end

# regular current
begin 
    gr()
    for Δc in Δcs
        default(size = (300,200), framestyle = :box)
        df = filter(d -> d.Δc == Δc && d.L == 10,d)
    
        x = [(df.ts[i][1:100:end]./df.T[i]) for i in eachindex(df.ts)]
        y = [(df.qs[i])[2:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label = (df.RΔ)', 
        legend_title = L"R_\Delta/J",
        legend = :outertopright,
        xlabel = L"t/T",
        ylabel = L"Q",
        ylims = (0,2),
        linestyle = :auto)
    
        wsave(plotsdir("ionichubbard","pols_reg","current_"*savename(df)*".pdf"),current())
    
    end
    
end


#current Ls
begin 
    gr()
    for Δc in Δcs, RΔ in RΔs
        default(size = (300,200), framestyle = :box)
        df = filter(d -> d.Δc == Δc && d.RΔ == RΔ,d)
    
        x = [(df.ts[i][1:100:end]./df.T[i]) for i in eachindex(df.ts)]
        y = [(df.qs[i])[2:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label = (df.L)', 
        legend_title = L"L",
        legend = :outertopright,
        xlabel = L"t/T",
        ylabel = L"Q",
        linestyle = :auto)
    
        wsave(plotsdir("ionichubbard","pols_reg","current_"*savename(df)*".pdf"),current())
    
    end
    
end

### energy
begin 
    gr()
    for Δc in Δcs, RΔ in RΔs
        default(size = (300,200), framestyle = :box)
        df = filter(d -> d.Δc == Δc && d.RΔ == RΔ,d)
    
        x = [(df.ts[i][1:100:end]./df.T[i]) for i in eachindex(df.ts)]
        y = [(df.en1[i])[1:100:end] .- (df.en0[i])[1:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label = (df.L)', 
        legend_title = L"L",
        legend = :outertopright,
        xlabel = L"t/T",
        ylabel = L"E_1-E_0",
        ylims = (0,maximum(maximum.(y))),
        linestyle = :auto)
    
        wsave(plotsdir("ionichubbard","pols_reg","energy_"*savename(df)*".pdf"),current())
    
    end
    
end


### deg
begin 
    gr()
    for Δc in Δcs, RΔ in RΔs
        default(size = (300,200), framestyle = :box)
        df = filter(d -> d.Δc == Δc && d.RΔ == RΔ,d)
    
        x = [(df.ts[i][1:100:end]./df.T[i]) for i in eachindex(df.ts)]
        y = [(df.en2[i])[1:100:end] .- (df.en1[i])[1:100:end] for i in eachindex(df.ts)]

        plot(x, y,
        label = (df.L)', 
        legend_title = L"L",
        legend = :outertopright,
        xlabel = L"t/T",
        ylabel = L"E_2-E_1",
        ylims = (0,maximum(maximum.(y))),
        linestyle = :auto)
    
        wsave(plotsdir("ionichubbard","pols_reg","energy_deg_"*savename(df)*".pdf"),current())
    
    end
    
end