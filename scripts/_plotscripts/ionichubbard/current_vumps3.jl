using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
size = (350, 350 / φ)
default(xlabel = L"t/T", ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4)


d = sort(collect_results!(datadir("sims", "ionichubbard", "VUMPS_test_3")),:maxdim)
maxdims = sort(d.maxdim)


## 

for g in groupby(d,:RΔ)
    for gg in groupby(g,:ϕ₀)
        for df in groupby(gg,:dt)

            ts = [df.ts[1]; df.ts[1][end] + df.dt[1]]
            plot(ts, df.qs,
                label = df.maxdim',
                legend_title = L"\chi",
                ylabel = L"Q",
                xlabel = L"t/T",
                ylims = (-0.2, 2.1))
            # annotate!(0.4, -0.05, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df.RΔ[1]),$(df.Rδ[1]),$(df.Δc[1]))"), 8))
            wsave(plotsdir("ionichubbard", "VUMPS_current3", savename(DataFrame(df)) * "_.pdf"), current())

            plot(ts, df.qsS,
                label = df.maxdim',
                legend_title = L"\chi",
                ylabel = L"Qs",
                xlabel = L"t/T")
            # annotate!(0.4, 0.05, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df.RΔ[1]),$(df.Rδ[1]),$(df.Δc[1]))"), 8))
            wsave(plotsdir("ionichubbard", "VUMPS_current3", "spincurr_" * savename(DataFrame(df)) * "_.pdf"), current())

            plot(df.ts, df.cutoff,
                label = df.maxdim',
                legend_title = L"\chi",
                ylabel = L"\lambda_\mathrm{min}",
                xlabel = L"t/T")
            # annotate!(0.4, 0.05, text(latexstring("(R_\\Delta,R_\\delta,\\Delta_c)=($(df.RΔ[1]),$(df.Rδ[1]),$(df.Δc[1]))"), 8))
            wsave(plotsdir("ionichubbard", "VUMPS_current3", "cutoff_" * savename(DataFrame(df)) * "_.pdf"), current())

        end
    end
end  