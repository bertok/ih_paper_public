using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

using Plots.PlotMeasures

d = collect_results(datadir("sims","ionichubbard","mps_pbc_delta0"))

##
df = sort!(d[d.L .== 60,:],:maxdim)

plot()
for i in 1:size(df,1)
    plot!(df.var1[i][:,1], label = (df[i,:].maxdim))
end
current()