using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
φ = Base.MathConstants.φ
size = (350,350/φ)

using Plots.PlotMeasures
pgfplotsx(framestyle = :box, grid = :false)

## Gaps δ=0 plot.

fs = load(datadir("interactive","ionichubbard","gapstest.jld2"),"fs")
Δc = [1.4,1.33,1.28]
Δs = [1.12,1.015,0.96]
for (i,f) in enumerate(fs)
    f[:Δs] = -2:0.01:2
    # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
    plot(f[:Δs],abs.(f[:intgap])[:,1],size = size, xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_\mathrm{exc}", color = :auto, linestyle = :auto)
    plot!(f[:Δs],abs.(f[:chargegap])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C", color = :auto, linestyle = :auto)
    # plot!(f[:Δs],abs.(f[:chargegapnew])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C new", color = :auto, linestyle = :auto)
    plot!(f[:Δs],abs.(f[:spingap])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_S", color = :auto, linestyle = :dashdot, linewidth = 2)
    plot!(f[:Δs],abs.(f[:en2] - f[:en0])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_2", color = :auto, linestyle = :auto, legend = :top, legend_columns = 4)


    vline!(Δc[i:i], c = :black, label = false)
    vline!(Δs[i:i], linestyle = :dash, c = :black, label = false)
    vline!(-Δc[i:i], c = :black, label = false)
    vline!(-Δs[i:i], c = :black, linestyle = :dash, label = false)
    ylims!(0,2.3)

    wsave(plotsdir("ionichubbard", "delta0", "gaps_" * savename(f) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "delta0", "gaps_" * savename(f) * ".pdf"), current())

end
current()