using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
size = (350, 350 / φ)
default(framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4)

## Gaps δ=0 plot.

d = collect_results(datadir("sims", "ionichubbard", "lanczos_SzSz_path"))

##

df = filter(d -> (d.J == 2), d)
# df = combine(df, :, :Δc .=> (Δc -> (Δc == 0 ? "rm0" : "ih")) => :type) ##continue

for g1 in groupby(df, [:RΔ])
    plot(size = size, xlabel = L"t/T", ylabel = L"\Delta E")
    for (c,g2) in enumerate(groupby(g1, :L))

        # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:Δc])"), legend_title = L"L", color = i)
        plot!(g2.ts[1][1:100:end]./g2.ts[1][end], g2.intgap[1][1:100:end], label = L"\Delta E_\mathrm{int}"  , color = c, linestyle = :solid)
        plot!(g2.ts[1][1:100:end]./g2.ts[1][end], g2.spingap[1][1:100:end], label = L"\Delta E_S"  , color = c, linestyle = :dash)
        plot!(g2.ts[1][1:100:end]./g2.ts[1][end], g2.intgap2[1][1:100:end], label = L"\Delta E_2"  , color = c, linestyle = :dashdot)
    end
    wsave(plotsdir("ionichubbard", "path_SzSz_lanczos" , "gaps",savename(DataFrame(g1)) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "path_SzSz_lanczos" ,"gaps",savename(DataFrame(g1)) * ".pdf"), current())

end

## pumped charge

df = filter(d -> (d.J == 2), d)

for g1 in groupby(df, [:RΔ])
    plot(size = size, xlabel = L"t/T", ylabel = L"\Delta Q")
    for (c,g2) in enumerate(groupby(g1, :L))

        # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:Δc])"), legend_title = L"L", color = i)
        plot!(g2.ts[1][1:100:end]./g2.ts[1][end], g2.qs[1][2:100:end], label = g2.L  , color = c, linestyle = :solid)
    end
    wsave(plotsdir("ionichubbard", "path_SzSz_lanczos" , "current",savename(DataFrame(g1)) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "path_SzSz_lanczos" ,"current",savename(DataFrame(g1)) * ".pdf"), current())

end

## BOW parameter

df = filter(d -> (d.J == 2), d)

for g1 in groupby(df, [:RΔ])
    plot(size = size, xlabel = L"t/T", ylabel = L"\Delta Q")
    for (c,g2) in enumerate(groupby(g1, :L))

        # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:Δc])"), legend_title = L"L", color = i)
        plot!(g2.ts[1][1:100:end]./g2.ts[1][end], g2.sdi[1][1:100:end], label = g2.L  , color = c, linestyle = :solid)
    end
    wsave(plotsdir("ionichubbard", "path_SzSz_lanczos" , "BOW",savename(DataFrame(g1)) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "path_SzSz_lanczos" ,"BOW",savename(DataFrame(g1)) * ".pdf"), current())

end


## loschmidt echo rate function λ = -1/N log(||<Ψ|U|Ψ>||^2)

df = filter(d -> (d.J == 2), d)

for g1 in groupby(df, [:RΔ])
    plot(size = size, xlabel = L"t/T", ylabel = L"\lambda(t)")
    for (c,g2) in enumerate(groupby(g1, :L))

        # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
        plot!(g2.ts[1][1:100:end]./g2.ts[1][end], -1/g2.L[1] * log.((g2.adiabaticity[1][1:100:end]).^2), label = g2.L  , color = c, linestyle = :solid)
    end
    wsave(plotsdir("ionichubbard", "path_SzSz_lanczos" , "loschmidt",savename(DataFrame(g1)) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "path_SzSz_lanczos" ,"loschmidt",savename(DataFrame(g1)) * ".pdf"), current())

end



