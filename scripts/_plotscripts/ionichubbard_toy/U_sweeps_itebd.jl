######################### TEBD: ####################################
##

using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
using Plots.PlotMeasures

include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))
include(srcdir("itebd.jl"))

size = (350, 350 / φ)
default(xlabel=L"t/T", ylabel=L"\mathcal O", framestyle=:box, size=size, grid=false, legend=:top, legend_columns=4, widen=false)

##

d = collect_results!(datadir("sims", "ionichubbard_toy", "tebd_U_sweep"))


d = sort(d[d.periods .==1,:],[:U, :cutoff, :maxdim])

##

d = d[d.maxdim .==200,:]

## params
t = 403 #Hz
δt = 118 #t±δt
ΔΔ = 847
δδ = 460

Δc = 4t #chosen, origin of shifted pump cycle.

#constraints:
U = 32 * t
T = 100 / t # 2π ?

Δ0 = ΔΔ / t
δΔ01 = ΔΔ / (t + δt)
δΔ02 = ΔΔ / (t - δt)

δ0 = δδ / (t + δt)


Uc1 = (1.6 * t + 2 * (Δc - ΔΔ)) / t
Us1 = (2.2 * t + 2 * (Δc - ΔΔ)) / t

Uc2 = (1.6 * t + 2 * (Δc + ΔΔ)) / t
Us2 = (2.2 * t + 2 * (Δc + ΔΔ)) / t


## U sweep

function color(cutoff)
    if maxdim == 1e-6
        return 1
    elseif maxdim == 1e-7
        return 2
    elseif maxdim == 1e-8
        return 3
    end
end

for g in groupby(d,[:cutoff,:T])

    delqs = [r.qs[end] for r in eachrow(g)]

    scatter(g.U, delqs,
    markersize = 1.6, marker = :circle, c = color.(g.cutoff)
    ,label=latexstring("L=\\infty, TJ=$(round(g.T[1];digits = 2))"), legend=:topright)
    xlabel!(L"U/J")
    ylabel!(L"\Delta Q")

    vline!([Uc1, Uc2], ls=:solid, label=false)
    vline!([Us1, Us2], ls=:dash, label=false)
    ylims!(-0.2,1)

    wsave(plotsdir("ionichubbard_toy", "tebd_U_sweep_cutoffs", savename(g) * ".pdf" ), current())

end

# ## single U

# for g in groupby(d,:T)

#     df = g[g.U .== 1, :]

#     qs = [r.qs[2:end] for r in eachrow(df)]

#     plot(df.ts ./T, qs, label=latexstring("L=\\infty, TJ=$(round(df.T[1];digits = 2)), U/J = 1"), legend=:topright)
#     xlabel!(L"t/T")
#     ylabel!(L"Q")
#     ylims!(-0.5,1)

#     wsave(plotsdir("ionichubbard_toy", "tebd_U_sweep", "single_" * savename(df) * ".pdf" ), current())

# end

# for g in groupby(d,:T)
#     Us = 6:13
#     df = []
#     Usingle = 0

#     try
#         for U in Us
#             df = g[g.U .== U, :]
#             Usingle = U
#         end
#     catch
#         println("no U found")
#     end
#     qs = [r.qs[2:end] for r in eachrow(df)]

#     plot(df.ts[1]./T, qs, label=latexstring("L=\\infty, TJ=$(round(df.T[1];digits = 2)), U/J = $Usingle"), legend=:bottomright)
#     xlabel!(L"t/T")
#     ylabel!(L"Q")
#     ylims!(-0.5,1)

#     wsave(plotsdir("ionichubbard_toy", "tebd_U_sweep", "single_" * savename(df) * ".pdf" ), current())

# end

# for g in groupby(d,:T)
#     Us = 20:30
#     df = []
#     Usingle = 0
#     try
#         for U in Us
#             df = g[g.U .== U, :]
#             Usingle = U
#         end
#     catch
#         println("no U found")
#     end
#     qs = [r.qs[2:end] for r in eachrow(df)]

#     plot(df.ts[1]./T, qs, label=latexstring("L=\\infty, TJ=$(round(df.T[1];digits = 2)), U/J = $Usingle"), legend=:topright )
#     xlabel!(L"t/T")
#     ylabel!(L"Q")
#     ylims!(-0.5,1)

#     wsave(plotsdir("ionichubbard_toy", "tebd_U_sweep", "single_" * savename(df) * ".pdf" ), current())

# end


