using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
using Plots.PlotMeasures

size = (350, 350 / φ)
default(xlabel=L"t/T", ylabel=L"\mathcal O", framestyle=:box, size=size, grid=false, legend=:top, legend_columns=1, widen=false)

##

d = collect_results!(datadir("sims", "ionichubbard_toy", "lanczos_U_sweep"))


d8 = sort(d[d.L.==8, :], :U)

## params
t = 403 #Hz
δt = 118 #t±δt
ΔΔ = 847
δδ = 460

Δc = 4t #chosen, origin of shifted pump cycle.

#constraints:
U = 32 * t
T = 100 / t # 2π ?

Δ0 = ΔΔ / t
δΔ01 = ΔΔ / (t + δt)
δΔ02 = ΔΔ / (t - δt)

δ0 = δδ / (t + δt)


Uc1 = (1.6 * t + 2 * (Δc - ΔΔ)) / t
Us1 = (2.2 * t + 2 * (Δc - ΔΔ)) / t

Uc2 = (1.6 * t + 2 * (Δc + ΔΔ)) / t
Us2 = (2.2 * t + 2 * (Δc + ΔΔ)) / t


## U sweep

for T in collect(Set(d8.T))

    df = d8[d8.T.==T, :]
    delqs = [r.qs[end] for r in eachrow(df)]

    scatter(d10.U, delqs, label=latexstring("L=8, TJ=$(round(T;digits = 2))"), legend=:topright)
    xlabel!(L"U/J")
    ylabel!(L"\Delta Q")

    vline!([Uc1, Uc2], ls=:solid, label=false)
    vline!([Us1, Us2], ls=:dash, label=false)

    wsave(plotsdir("ionichubbard_toy", "U_sweep", savename(df) * ".pdf" ), current())

end

## single U

for T in collect(Set(d8.T))

    df = d8[d8.T.==T .&& d8.U .== 1, :]

    plot(df.ts[1]./T, df.qs[1], label=latexstring("L=8, TJ=$(round(T;digits = 2)), U/J = 1"), legend=:bottomright)
    xlabel!(L"t/T")
    ylabel!(L"Q")

    wsave(plotsdir("ionichubbard_toy", "U_sweep", "single_" * savename(df) * ".pdf" ), current())

    df = d8[d8.T.==T .&& d8.U .== 10, :]

    plot(df.ts[1]./T, df.qs[1], label=latexstring("L=8, TJ=$(round(T;digits = 2)), U/J = 10"), legend=:bottomright)
    xlabel!(L"t/T")
    ylabel!(L"Q")

    wsave(plotsdir("ionichubbard_toy", "U_sweep", "single_" * savename(df) * ".pdf" ), current())


    df = d8[d8.T.==T .&& d8.U .== 20, :]

    plot(df.ts[1]./T, df.qs[1], label=latexstring("L=8, TJ=$(round(T;digits = 2)), U/J = 20"), legend=:bottomright)
    xlabel!(L"t/T")
    ylabel!(L"Q")

    wsave(plotsdir("ionichubbard_toy", "U_sweep", "single_" * savename(df) * ".pdf" ), current())

end


