using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
φ = Base.MathConstants.φ
size = (350,350/φ)

using Plots.PlotMeasures
default()
pgfplotsx(framestyle = :box, grid = :false,foreground_color_legend = nothing,background_color_legend = nothing)

## Gaps δ=0 plot Staggered field

d = collect_results!(datadir("sims","ionichubbard_toy","gap_delta0"))


##

for g in groupby(d,:L)
    # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
    plot(g.Δs,abs.(g.intgap[1])[:,1],size = size, xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_\mathrm{exc}", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.chargegap[1])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C", color = :auto, linestyle = :auto)
    # plot!(f.Δs,abs.(f[:chargegapnew])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C new", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.spingap[1])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_S", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.intgap2[1])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_2", color = :auto, linestyle = :auto, legend = :top, legend_columns = 4)
    ylims!(0,4.6)
    wsave(plotsdir("ionichubbard_toy", "delta0", "gaps_" * savename(g) * ".tikz"), current())
    wsave(plotsdir("ionichubbard_toy", "delta0", "gaps_" * savename(g) * ".pdf"), current())

end
current()


## Gaps δ=0 plot SzSz

dSz = collect_results!(datadir("sims","ionichubbard_toy","gap_delta0_Sz"))
df = collect_results!(datadir("sims","ionichubbard_toy","gap_delta0"))

plot(layout = (3,1), size = (350,1.5*350/φ), bottom_margin = -16.5mm)

for g in groupby(df[df.L .== 12,:],:L)
    # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
    plot!(g.Δs,abs.(g.intgap[1])[:,1],subplot = 1, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_\mathrm{int}", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.chargegap[1])[:,1],subplot = 1, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_C", color = :auto, linestyle = :auto)
    # plot!(f.Δs,abs.(f[:chargegapnew])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_C new", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.spingap[1])[:,1],subplot = 1, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_S", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.intgap2[1])[:,1],subplot = 1, xlabel = "", ylabel = L"\Delta E/J", label = L"\Delta E_2", color = :auto, linestyle = :auto, legend = :false, legend_columns = 4,xformatter = _ -> " ")
    ylims!(0,4.6, subplot =1)
end

for g in groupby(dSz[dSz.L .== 12 .&& dSz.J .!= 0,:],:L)
    # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E/J", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
    plot!(g.Δs,abs.(g.intgap[1])[:,1],subplot = 2, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_\mathrm{int}", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.chargegap[1])[:,1],subplot = 2, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_C", color = :auto, linestyle = :auto)
    # plot!(f.Δs,abs.(f[:chargegapnew])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_C new", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.spingap[1])[:,1],subplot = 2, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_S", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.intgap2[1])[:,1],subplot = 2, xlabel = "", ylabel = L"\Delta E/J", label = L"\Delta E_2", color = :auto, linestyle = :auto, legend = :false, legend_columns = 4, xformatter = _ -> " ")
    ylims!(0,4.6, subplot = 2)

end

for g in groupby(dSz[dSz.L .== 12 .&& dSz.J .== 0.,:],:L)
    # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E/J", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
    plot!(g.Δs,abs.(g.intgap[1])[:,1],subplot = 3, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_\mathrm{int}", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.chargegap[1])[:,1],subplot = 3, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_C", color = :auto, linestyle = :auto)
    # plot!(f.Δs,abs.(f[:chargegapnew])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_C new", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.spingap[1])[:,1],subplot = 3, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = L"\Delta E_S", color = :auto, linestyle = :auto)
    plot!(g.Δs,abs.(g.intgap2[1])[:,1],subplot = 3, xlabel = "", ylabel = L"\Delta E/J", label = L"\Delta E_2", color = :auto, linestyle = :auto, legend = :top, legend_columns = 4, )
    ylims!(0,4.6, subplot =3)
    xlabel!(L"\Delta/J",subplot = 3)
end

    annotate!(-1.5,3.8,text(L"(a)\;\hat H_\mathrm{IHB}(\delta=0)",10), subplot = 1)
    annotate!(-1.5,3.8,text(L"(b)\;\hat H_\mathrm{IHZ}(\delta=0)",10), subplot = 2)
    annotate!(-1.5,3,text(L"(c)\;\hat H_\mathrm{IH}(\delta=0)",10), subplot = 3)


##

wsave(plotsdir("ionichubbard_toy", "delta0_all", "gaps_" * savename(dSz[dSz.L .== 12,:]) * ".tikz"), current())
wsave(plotsdir("ionichubbard_toy", "delta0_all", "gaps_" * savename(dSz[dSz.L .== 12,:]) * ".pdf"), current())

current()



## Gaps Ls

dSz = collect_results!(datadir("sims","ionichubbard_toy","gap_delta0_Sz"))
df = collect_results!(datadir("sims","ionichubbard_toy","gap_delta0"))

plot(layout = (3,1), size = (350,1.5*350/φ), bottom_margin = -16.5mm)

markers = [:cross, :xcross, :star, :hline]
for (i,g) in enumerate(groupby(df,:L))
    scatter!((g.L[1],abs.(g.intgap[1])[300,1]),subplot = 1, markerstrokecolor = 1,xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_\mathrm{int}" : "", color = 1, linestyle = :auto, marker = markers[1])
    scatter!((g.L[1],abs.(g.chargegap[1])[300,1]),subplot = 1, markerstrokecolor = 2,xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_C" : "", color = 2, linestyle = :auto, marker = markers[2])
    scatter!((g.L[1],abs.(g.spingap[1])[300,1]),subplot = 1, markerstrokecolor = 3,xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_S" : "", color = 3, linestyle = :auto, marker = markers[3])
    scatter!((g.L[1],abs.(g.intgap2[1])[300,1]),subplot = 1,markerstrokecolor = 4, xlabel = "", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_2" : "", color = 4, linestyle = :auto, legend = :false, legend_columns = 4,xformatter = _ -> " ", marker = markers[4])
    ylims!(-0.5,5, subplot =1)
end

for (i,g) in enumerate(groupby(dSz[dSz.J .!= 0,:],:L))
    scatter!((g.L[1],abs.(g.intgap[1])[300,1]),subplot = 2, markerstrokecolor = 1, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_\mathrm{int}" : "", color = 1, linestyle = :auto, marker = markers[1])
    scatter!((g.L[1],abs.(g.chargegap[1])[300,1]),subplot = 2,markerstrokecolor = 2, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_C" : "", color = 2, linestyle = :auto, marker = markers[2])
    scatter!((g.L[1],abs.(g.spingap[1])[300,1]),subplot = 2, markerstrokecolor = 3,xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_S" : "", color = 3, linestyle = :auto, marker = markers[3])
    scatter!((g.L[1],abs.(g.intgap2[1])[300,1]),subplot = 2,markerstrokecolor = 4, xlabel = "", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_2" : "", color = 4, linestyle = :auto, legend = :false, legend_columns = 4,xformatter = _ -> " ", marker = markers[4])
    ylims!(-0.5,5, subplot = 2)
end

for (i,g) in enumerate(groupby(dSz[dSz.J .== 0.,:],:L))
    scatter!((g.L[1],abs.(g.intgap[1])[300,1]),subplot = 3,markerstrokecolor = 1, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_\mathrm{int}" : "", color = 1, linestyle = :auto, marker = markers[1])
    scatter!((g.L[1],abs.(g.chargegap[1])[300,1]),subplot = 3,markerstrokecolor = 2, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_C" : "", color = 2, linestyle = :auto, marker = markers[2])
    scatter!((g.L[1],abs.(g.spingap[1])[300,1]),subplot = 3,markerstrokecolor = 3, xlabel = L"\Delta/J", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_S" : "", color = 3, linestyle = :auto, marker = markers[3])
    scatter!((g.L[1],abs.(g.intgap2[1])[300,1]),subplot = 3,markerstrokecolor = 4, xlabel = "", ylabel = L"\Delta E/J", label = i ==1 ? L"\Delta E_2" : "", color = 4, linestyle = :auto, legend = :top, legend_columns = 4, marker = markers[4])
    ylims!(-0.5,5, subplot =3)
    xlabel!(L"L",subplot = 3)
end

    annotate!(6,4.5,text(L"(a)\;\hat H_{IHB}(\delta=0)",10), subplot = 1)
    annotate!(6,4.5,text(L"(b)\;\hat H_{IHZ}(\delta=0)",10), subplot = 2)
    annotate!(6,3.3,text(L"(c)\;\hat H_{IH}(\delta=0)",10), subplot = 3)

    current()
##

wsave(plotsdir("ionichubbard_toy", "delta0_all", "Ls_" * savename(dSz[dSz.L .== 12,:]) * ".tikz"), current())
wsave(plotsdir("ionichubbard_toy", "delta0_all", "Ls_" * savename(dSz[dSz.L .== 12,:]) * ".pdf"), current())

