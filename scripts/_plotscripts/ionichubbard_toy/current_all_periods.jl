using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
using Plots.PlotMeasures

size = (350, 350 / φ)
default(xlabel = L"t/T", ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4, widen = false)


dinf = sort(collect_results(datadir("sims","ionichubbard_toy", "tebd")),[:T,:maxdim])
maxdims = sort(dinf.maxdim)


## C2
df = d[d.Δc .== 0 .&& d.T .== 50 .&& d.RΔ .== 4,:]
df2 = d2[d2.Δc .== 0 .&& d2.T .== 50 .&& d.RΔ .== 4,:]

# delq2 =[df2.qs[i][end] for i in 1:5]
delq1 =[df.qs[i][end] for (i,_) in enumerate(eachrow(df))]
delqS1 =[df.qsS[i][end] for (i,_) in enumerate(eachrow(df))]


# delqS2 =[df2.qsS[i][end] for i in 1:5]
delq2 =[df2.qs[i][end] for (i,_) in enumerate(eachrow(df2))]
delqS2 =[df2.qsS[i][end] for (i,_) in enumerate(eachrow(df2))]

plot(
layout = (2,1),
top_margin = -17mm,
widen = true
)

scatter!(df2.L,abs.(delq2), subplot = 1,ylabel = L"\Delta Q", label = L"\hat H_\mathrm{IHB}", marker = :square, markersize = 3)
scatter!(df.L,abs.(delq1), subplot = 1,ylabel = L"\Delta Q", label = L"\hat H_\mathrm{IHZ}", marker = :circle, markersize = 3)

scatter!(df2.L,abs.(delqS2), subplot = 2, xlims = (4,10),ylabel = L"\Delta Q_S", label = L"\hat H_\mathrm{IHB}", marker = :square, markersize = 3)
scatter!(df.L,abs.(delqS1), ylims = (0,21), subplot = 2, xlims = (4,10),ylabel = L"\Delta Q_S", label = L"\hat H_\mathrm{IHZ}", marker = :circle, markersize = 3)

xlabel!(L"L")

annotate!(4.5,22,text(L"(a)",10), subplot = 1)
annotate!(4.5,18,text(L"(b)",10), subplot = 2)

wsave(plotsdir("ionichubbard_toy", "charge_Ls_periods_new",savename(df) * "_spin_RM" * "_.pdf"), current())
wsave(plotsdir("ionichubbard_toy", "charge_Ls_periods_new", savename(df) * "_spin_RM" * "_.tikz"), current())


## C1
df = d[d.Δc .== 2  .&& d.T .== 50 ,:]
df2 = d2[d2.Δc .== 2.24 .&& d2.T .== 50 ,:]

# delq2 =[df2.qs[i][end] for i in 1:5]
delq1 =[df.qs[i][end] for (i,_) in enumerate(eachrow(df))]
delqS1 =[df.qsS[i][end] for (i,_) in enumerate(eachrow(df))]


# delqS2 =[df2.qsS[i][end] for i in 1:5]
delq2 =[df2.qs[i][end] for (i,_) in enumerate(eachrow(df2))]
delqS2 =[df2.qsS[i][end] for (i,_) in enumerate(eachrow(df2))]

plot(
layout = (2,1),
top_margin = -17mm,
widen = true
)

scatter!(df2.L,abs.(delq2), subplot = 1,ylabel = L"\Delta Q", label = L"\hat H_\mathrm{IHB}", marker = :square, markersize = 3)
scatter!(df.L,abs.(delq1), subplot = 1,ylabel = L"\Delta Q", label = L"\hat H_\mathrm{IHZ}", marker = :circle, markersize = 3)

scatter!(df2.L,abs.(delqS2), subplot = 2, xlims = (4,10),ylabel = L"\Delta Q_S", label = L"\hat H_\mathrm{IHB}", marker = :square, markersize = 3)
scatter!(df.L,abs.(delqS1), ylims = (0,21), subplot = 2, xlims = (4,10),ylabel = L"\Delta Q_S", label = L"\hat H_\mathrm{IHZ}", marker = :circle, markersize = 3)

xlabel!(L"L")

annotate!(4.5,11,text(L"(a)",10), subplot = 1)
annotate!(4.5,18,text(L"(b)",10), subplot = 2)

wsave(plotsdir("ionichubbard_toy", "charge_Ls_periods_new",savename(df) * "_spin_IH" * "_.pdf"), current())
wsave(plotsdir("ionichubbard_toy", "charge_Ls_periods_new", savename(df) * "_spin_IH" * "_.tikz"), current())


## infinite system

## C1
df = dinf[dinf.J .== 0,:]
df2 = dinf[dinf.J .== 2,:]

for T in 1:10

# delq2 =[df2.qs[i][end] for i in 1:5]
delq1 =[df.qs[i][10000*T] for (i,_) in enumerate(eachrow(df))]
delqS1 =[df.qsS[i][10000*T] for (i,_) in enumerate(eachrow(df))]


# delqS2 =[df2.qsS[i][end] for i in 1:5]
delq2 =[df2.qs[i][10000*T] for (i,_) in enumerate(eachrow(df2))]
delqS2 =[df2.qsS[i][10000*T] for (i,_) in enumerate(eachrow(df2))]

plot(
layout = (2,1),
top_margin = -17mm,
widen = true
)

scatter!(df2.maxdim,abs.(delq2)./T, ylims = (0,1), subplot = 1,ylabel = L"\Delta Q/T", label = L"\hat H_\mathrm{IHZ}", marker = :square,marker_z = Int.(-log10.(df2.cutoff)), markersize = 3, alpha = 0.5, c = :cool)
scatter!(df.maxdim,abs.(delq1)./T, ylims = (0,1), subplot = 1,ylabel = L"\Delta Q/T", label = L"\hat H_\mathrm{IH}",marker_z = Int.(-log10.(df.cutoff)), marker = :circle, markersize = 3, alpha = 0.5, c = :cool)

scatter!(df2.maxdim,abs.(delqS2)./T, ylims = (0,1), subplot = 2,ylabel = L"\Delta Q_S/T", label = L"\hat H_\mathrm{IHZ}",marker_z = Int.(-log10.(df2.cutoff)), marker = :square, markersize = 3, alpha = 0.5, colorbar = false, c = :cool)
scatter!(df.maxdim,abs.(delqS1)./T, ylims = (0,1), subplot = 2, ylabel = L"\Delta Q_S/T", label = L"\hat H_\mathrm{IH}",marker_z = Int.(-log10.(df.cutoff)), marker = :circle, markersize = 3, alpha = 0.5, colorbar = false, c = :cool)

xlabel!(L"\mathrm{maxdim}")

# annotate!(4.5,11,text(L"(a)",10), subplot = 1)
# annotate!(4.5,0.75,text(L"(b)",10), subplot = 2)
annotate!(350,0.74,text(latexstring("T=$T"),10), subplot = 2)


wsave(plotsdir("ionichubbard_toy", "charge_Ls_periods_inf",savename(df) * "_$T" * "_.pdf"), current())
wsave(plotsdir("ionichubbard_toy", "charge_Ls_periods_inf", savename(df) * "_$T" * "_.tikz"), current())

end