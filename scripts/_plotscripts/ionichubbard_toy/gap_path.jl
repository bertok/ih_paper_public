using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
using Plots.PlotMeasures

size = (350, 350 / φ)
default(xlabel = L"t/T", ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4,foreground_color_legend = nothing,background_color_legend = nothing, guidefontvalign = :bottom)


d = sort(collect_results!(datadir("sims","ionichubbard_toy","pol_path")),:L)
d = d[d.L .==10,:]

d2 = sort(collect_results!(datadir("sims","ionichubbard_toy","pol_path_RM")),:L)
d2 = d2[d2.L .==10,:]

d = d[[2,3,1],:]
d2 = d2[[2,3,1],:]
##polar plot pol
pgfplotsx()

pal = palette(:tab10)
default(size = (263,250), framestyle = :box)



# x = [0.25 .+ (df.ts[i][1:10:end]./df.T[i]) for i in eachindex(df.ts)]
# y = [(df.pol[i]*2π)[1:10:end] for i in eachindex(df.ts)]

plot(layout = (2,2), margins = -3mm, size = (350,350))
markers = [:circle, :square, :utriangle]

for i in 1:3

    plot!(d.pol[i][1:1:end] .* 2π, d.ts[i][1:1:end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 1,
    xformatter = :radians, 
    legend = :false,
    label = "",
    ylabel = "",
    xlabel = L"\gamma_C\hspace{4mm}",
    ylims = (0,1.5), 
    lw = 0.8,
    proj = :polar,linestyle = :solid, c = i)


end
labs = [L"\hat H_\mathrm{IHB}",L"\hat H_\mathrm{IHZ}",L"\hat H_\mathrm{IH}"]
for i in 1:3
    scatter!(d.pol[i][(1+i*4):12:end] .* 2π, d.ts[i][(1+i*4):12:end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 1,
    xformatter = :radians, 
    legend = (0.7,1.2),
    ylabel = "",
    xlabel = L"\gamma_C\hspace{4mm}",
    ylims = (0,1.5),
    label = labs[i],
    marker = (markers[i],2),
    markerstrokewidth = 0.4,
    legend_columns = 3,
    title =L"(a)\:\mathcal C_1",
    titlefontsize = 10,
    titleposition = (0,0.7),
    proj = :polar,linestyle = :auto, c = i)
end


i1 = [findfirst(abs.(d.polspin[i]) .≈ 0.5) for i in 1:3]
i2 = [findlast(abs.(d.polspin[i]) .≈ 0.5) for i in 1:3]


plot!(d.polspin[1][1:1:end] .* 2π, d.ts[1][1:1:end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 2,
    xformatter = :radians, 
    legend = :false,
    label = "",
    ylabel = "",
    xlabel = L"P\gamma_S\hspace{4mm}",
    ylims = (0,1.5), 
    lw = 0.8,
    proj = :polar,linestyle = :solid, c = 1)



for i in 2:3

    plot!(d.polspin[i][1:1:(i1[i]-1)] .* 2π, d.ts[i][1:1:(i1[i]-1)] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 2,
    xformatter = :radians, 
    legend = :false,
    label = "",
    ylabel = "",
    xlabel = L"\gamma_S\hspace{4mm}",
    ylims = (0,1.5), 
    lw = 0.8,
    proj = :polar,linestyle = :solid, c = i)

    plot!(d.polspin[i][(i2[i]+1):end] .* 2π, d.ts[i][(i2[i]+1):end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 2,
    xformatter = :radians, 
    legend = :false,
    label = "",
    ylabel = "",
    xlabel = L"\gamma_S\hspace{4mm}",
    ylims = (0,1.5), 
    lw = 0.8,
    proj = :polar,linestyle = :solid, c = i)


end

scatter!(d.polspin[1][(1+1*4):12:end] .* 2π, d.ts[1][(1+1*4):12:end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 2,
    xformatter = :radians, 
    legend = :false,
    ylabel = "",
    xlabel = L"\gamma_S\hspace{4mm}",
    ylims = (0,1.5),
    marker = (markers[1],2),
    markerstrokewidth = 0.4,
    proj = :polar,linestyle = :auto, c = 1)

for i in 2:3

    scatter!(d.polspin[i][(1+i*4):15:(i1[i]-1)] .* 2π, d.ts[i][(1+i*4):15:(i1[i]-1)] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 2,
    xformatter = :radians, 
    legend = :false,
    ylabel = "",
    xlabel = L"\gamma_S\hspace{4mm}",
    ylims = (0,1.5), 
    marker = (markers[i],2),
    markerstrokewidth = 0.4,
    lw = 0.8,
    proj = :polar,linestyle = :solid, c = i)

    scatter!(d.polspin[i][(i2[i]+1):15:(end-i*4)] .* 2π, d.ts[i][(i2[i]+1):15:(end-i*4)] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 2,
    xformatter = :radians, 
    legend = :false,
    ylabel = "",
    xlabel = L"\gamma_S\hspace{4mm}",
    ylims = (0,1.5), 
    marker = (markers[i],2),
    markerstrokewidth = 0.4,
    lw = 0.8,
    proj = :polar,linestyle = :solid, c = i)

    scatter!(d.polspin[i][i1[i]:4:i2[i]] .* 2π, d.ts[i][i1[i]:4:i2[i]] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 2,
    xformatter = :radians, 
    legend = :false,
    ylabel = "",
    xlabel = L"\gamma_S\hspace{4mm}",
    ylims = (0,1.5), 
    marker = (markers[i],2),
    markerstrokewidth = 0.4,
    lw = 0.8,
    title =L"(b)\:\mathcal C_1",
    titlefontsize = 10,
    titleposition = (0,0.7),
    proj = :polar,linestyle = :solid, c = i)


end


for i in 1:3

    plot!(d2.pol[i][1:1:end] .* 2π, d.ts[i][1:1:end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 3,
    xformatter = :radians, 
    legend = :false,
    label = "",
    ylabel = "",
    xlabel = L"\gamma_C\hspace{4mm}",
    ylims = (0,1.5), 
    lw = 0.8,
    proj = :polar,linestyle = :solid, c = i)


end
for i in 1:3
    scatter!(d2.pol[i][(1+i*4):12:end] .* 2π, d.ts[i][(1+i*4):12:end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 3,
    xformatter = :radians, 
    legend = :false,
    ylabel = "",
    xlabel = L"\gamma_C\hspace{4mm}",
    ylims = (0,1.5),
    marker = (markers[i],2),
    markerstrokewidth = 0.4,
    title =L"(c)\:\mathcal C_2",
    titlefontsize = 10,
    titleposition = (0,0.7),
    proj = :polar,linestyle = :auto, c = i)
end


for i in 1:3

    plot!(d2.polspin[i][1:1:end] .* 2π, d.ts[i][1:1:end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 4,
    xformatter = :radians, 
    legend = :false,
    label = "",
    ylabel = "",
    xlabel = L"P_S\hspace{4mm}",
    ylims = (0,1.5), 
    lw = 0.8,
    proj = :polar,linestyle = :solid, c = i)


end
for i in 1:3
    scatter!(d2.polspin[i][(1+i*4):12:end] .* 2π, d.ts[i][(1+i*4):12:end] .+ 0.25,
    yticks = ([0.25,1.25],["0","1"]),
    xticks = ([0,0,0,π/2,π,1.5π],["","","0",L"\pi/2",L"\pi",L"3\pi/2"]),
    subplot = 4,
    xformatter = :radians, 
    ylabel = "",
    xlabel = L"\gamma_S\hspace{4mm}",
    ylims = (0,1.5),
    marker = (markers[i],2),
    markerstrokewidth = 0.4,
    title =L"(d)\:\mathcal C_2",
    titlefontsize = 10,
    titleposition = (0,0.7),
    proj = :polar,linestyle = :auto, c = i, legend = :false)
end

annotate!(20.25,0.85,text(L"\theta",8), subplot = 1)
annotate!(20,0.85,text(L"\theta",8), subplot = 2)
annotate!(20,0.85,text(L"\theta",8), subplot = 3)
annotate!(20,0.85,text(L"\theta",8), subplot = 4)


wsave(plotsdir("ionichubbard_toy","pols_pol",savename(d)*".pdf"),current())
wsave(plotsdir("ionichubbard_toy","pols_pol",savename(d)*".tikz"),current())

