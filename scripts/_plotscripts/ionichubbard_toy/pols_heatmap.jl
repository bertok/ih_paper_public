using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))
using DelimitedFiles
using Plots.PlotMeasures
φ = Base.MathConstants.φ
size = (350,2*350/φ)

default()
pgfplotsx(framestyle = :box, grid = :false)

#allow for adding text as legend image in pgfplotsx:
Pre = raw"\pgfplotsset{
    legend image with text/.style={
        legend image code/.code={%
            \node[anchor=center] at (0.3cm,0cm) {#1};
        }
    },
}
\definecolor{cust4}{rgb}{0.1216,0.4667,0.7059}
\definecolor{cust7}{rgb}{1.0,0.498,0.0549}
\definecolor{cust6}{rgb}{0.1725,0.6275,0.1725}
"
push!(PGFPlotsX.CUSTOM_PREAMBLE, Pre)


##

d = sort!(collect_results!(datadir("sims","ionichubbard_toy","pol_heatmap")),:δs)



## berry phase plots
    
    ev = 1

    df = d[d.J .== 0 .&& d.f .==0 .&& d.L .== 10,:]   
    δs = df.δs[1:ev:end]
    Δs = (df.Δs[1])[1:ev:end]
    ts = 0:0.01:1 
    pgfplotsx(framestyle = :box, grid = false, size = size, c = :RdBu, clims = (0,1), xlims = (-1,1), ylims = (-5,5), legend_columns = 3) 
    plot(layout = (3,2), bottom_margin = -17mm, right_margin = -23mm)


    C1(t) = (0.9*cos(2π * t),sin(2π * t))
    C2(t) = (0.9*cos(2π * t),4*sin(2π * t))
    C3(t) = (0.9*cos(2π * t),2.24 + 2 * sin(2π * t))
    # C4(t) = (0.2*cos(2π * t),1.4 + 0.2 * sin(2π * t))


    df = d[d.J .== 0 .&& d.f .== 0.5 .&& d.L .== 10,:]    
    pols = hcat(df.pol...)[1:ev:end,1:ev:end]
    polsS = hcat(df.polS...)[1:ev:end,1:ev:end]

    δs = df.δs[1:ev:end]
    Δs = (df.Δs[1])[1:ev:end]
    ts = 0:0.01:1

    heatmap!(δs,Δs,(pols.+1).%1,subplot = 1, colorbar_title = L"\gamma / 2\pi", colorbar = false, xlabel = false, xformatter = _ -> "",ylabel = L"\Delta/J",yticks = [-2.5,0,2.5,5]);
    heatmap!(δs,Δs,(polsS.+1).%1,subplot = 2, colorbar_title = L"\gamma / 2\pi", xlabel = false, xformatter = _ -> "", yformatter = _ -> "", colorbar = :top);

    df = d[d.J .== 2 .&& d.f .== 0 .&& d.L .== 10,:]    
    pols = hcat(df.pol...)[1:ev:end,1:ev:end]
    polsS = hcat(df.polS...)[1:ev:end,1:ev:end]
    polsS = (polsS.+1).%1
    polsS = ifelse.(polsS .> 0.9, polsS .-1, polsS)

    δs = df.δs[1:ev:end]
    Δs = (df.Δs[1])[1:ev:end]
    ts = 0:0.01:1

    heatmap!(δs,Δs,(pols.+1).%1,subplot = 3, colorbar_title = L"\gamma / 2\pi", colorbar = false, xlabel = false,ylabel = L"\Delta/J", yticks = [-2.5,0,2.5,5]);
    heatmap!(δs,Δs,polsS,subplot = 4, colorbar_title = L"\gamma / 2\pi", yformatter = _ -> "",xformatter = _ -> "", colorbar = false, xticks = [-0.5,0,0.5,1]);

    df = d[d.J .== 0 .&& d.f .==0 .&& d.L .== 10,:]   
    pols = hcat(df.pol...)[1:ev:end,1:ev:end]
    polsS = hcat(df.polS...)[1:ev:end,1:ev:end]
    polsS = (polsS.+1).%1
    polsS = ifelse.(polsS .> 0.9, polsS .-1, polsS)

    heatmap!(δs,Δs,(pols.+1).%1,subplot = 5, colorbar_title = L"\gamma / 2\pi", colorbar = false, xlabel = false,ylabel = L"\Delta/J", yticks = [-5,-2.5,0,2.5,5]);
    heatmap!(δs,Δs,polsS.%1,subplot = 6, colorbar_title = L"\gamma / 2\pi", xlabel = false, yformatter = _ -> "", colorbar = false, xticks = [-0.5,0,0.5,1]);



    # plot!(C1.(ts), c = :black, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},1}", legend = :top);
    # plot!(C2.(ts), c = :black, ls = :dash, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},2}");
    # plot!(C3.(ts), c = :black, ls = :dot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},1}");
    # plot!(C4.(ts), c= :black, ls = :dashdot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},2}" ,legendcolumns = 4);

    # ylabel!(L"\Delta/J");
    xlabel!(L"\delta");

    annotate!(-0.5,4,text(L"(a)\;\gamma_C, \hat H_\mathrm{IHB}",:white,11), subplot = 1)
    annotate!(-0.5,4,text(L"(b)\;\gamma_S, \hat H_\mathrm{IHB}",:white,11), subplot = 2)
    annotate!(-0.5,4,text(L"(c)\;\gamma_C, \hat H_\mathrm{IHZ}",:white,11), subplot = 3)
    annotate!(-0.5,4,text(L"(d)\;\gamma_S, \hat H_\mathrm{IHZ}",:white,11), subplot = 4)
    annotate!(-0.5,4,text(L"(e)\;\gamma_C, \hat H_\mathrm{IH}",:white,11), subplot = 5)
    annotate!(-0.5,4,text(L"(f)\;\gamma_S, \hat H_\mathrm{IH}",:white,11), subplot = 6)
    

    wsave(plotsdir("ionichubbard_toy","berryphase", "berry.pdf"),current())
    wsave(plotsdir("ionichubbard_toy","berryphase", "berry.tikz"),current())


##

## berry phase plots L=8
    
ev = 1

df = d[d.J .== 0 .&& d.f .==0 .&& d.L .== 8,:]   
δs = df.δs[1:ev:end]
Δs = (df.Δs[1])[1:ev:end]
ts = 0:0.01:1 
pgfplotsx(framestyle = :box, grid = false, size = size, c = :RdBu, clims = (0,1), xlims = (-1,1), ylims = (-5,5), legend_columns = 3) 
plot(layout = (3,2), bottom_margin = -17mm, right_margin = -23mm)


C1(t) = (0.9*cos(2π * t),sin(2π * t))
C2(t) = (0.9*cos(2π * t),4*sin(2π * t))
C3(t) = (0.9*cos(2π * t),2.24 + 2 * sin(2π * t))
# C4(t) = (0.2*cos(2π * t),1.4 + 0.2 * sin(2π * t))


df = d[d.J .== 0 .&& d.f .== 0.5 .&& d.L .== 8,:]    
pols = hcat(df.pol...)[1:ev:end,1:ev:end]
polsS = hcat(df.polS...)[1:ev:end,1:ev:end]

δs = df.δs[1:ev:end]
Δs = (df.Δs[1])[1:ev:end]
ts = 0:0.01:1

heatmap!(δs,Δs,(pols.+1).%1,subplot = 1, colorbar_title = L"\gamma / 2\pi", colorbar = false, xlabel = false, xformatter = _ -> "",ylabel = L"\Delta/J",yticks = [-2.5,0,2.5,5]);
heatmap!(δs,Δs,(polsS.+1).%1,subplot = 2, colorbar_title = L"\gamma / 2\pi", xlabel = false, xformatter = _ -> "", yformatter = _ -> "", colorbar = :top);

df = d[d.J .== 2 .&& d.f .== 0 .&& d.L .== 8,:]    
pols = hcat(df.pol...)[1:ev:end,1:ev:end]
polsS = hcat(df.polS...)[1:ev:end,1:ev:end]
polsS = (polsS.+1).%1
polsS = ifelse.(polsS .> 0.9, polsS .-1, polsS)

δs = df.δs[1:ev:end]
Δs = (df.Δs[1])[1:ev:end]
ts = 0:0.01:1

heatmap!(δs,Δs,(pols.+1).%1,subplot = 3, colorbar_title = L"\gamma / 2\pi", colorbar = false, xlabel = false,ylabel = L"\Delta/J", yticks = [-2.5,0,2.5,5]);
heatmap!(δs,Δs,polsS,subplot = 4, colorbar_title = L"\gamma / 2\pi", yformatter = _ -> "",xformatter = _ -> "", colorbar = false, xticks = [-0.5,0,0.5,1]);

df = d[d.J .== 0 .&& d.f .==0 .&& d.L .== 8,:]   
pols = hcat(df.pol...)[1:ev:end,1:ev:end]
polsS = hcat(df.polS...)[1:ev:end,1:ev:end]
polsS = (polsS.+1).%1
polsS = ifelse.(polsS .> 0.9, polsS .-1, polsS)

heatmap!(δs,Δs,(pols.+1).%1,subplot = 5, colorbar_title = L"\gamma / 2\pi", colorbar = false, xlabel = false,ylabel = L"\Delta/J", yticks = [-5,-2.5,0,2.5,5]);
heatmap!(δs,Δs,polsS.%1,subplot = 6, colorbar_title = L"\gamma / 2\pi", xlabel = false, yformatter = _ -> "", colorbar = false, xticks = [-0.5,0,0.5,1]);



# plot!(C1.(ts), c = :black, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},1}", legend = :top);
# plot!(C2.(ts), c = :black, ls = :dash, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},2}");
# plot!(C3.(ts), c = :black, ls = :dot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},1}");
# plot!(C4.(ts), c= :black, ls = :dashdot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},2}" ,legendcolumns = 4);

# ylabel!(L"\Delta/J");
xlabel!(L"\delta");

annotate!(-0.8,4,text(L"(a)\;\gamma, \hat H_\mathrm{IHB}",:white,11), subplot = 1)
annotate!(-0.8,4,text(L"(b)\;\gamma_S, \hat H_\mathrm{IHB}",:white,11), subplot = 2)
annotate!(-0.8,4,text(L"(c)\;\gamma, \hat H_\mathrm{IHZ}",:white,11), subplot = 3)
annotate!(-0.8,4,text(L"(d)\;\gamma_S, \hat H_\mathrm{IHZ}",:white,11), subplot = 4)
annotate!(-0.8,4,text(L"(e)\;\gamma, \hat H_\mathrm{IH}",:white,11), subplot = 5)
annotate!(-0.8,4,text(L"(f)\;\gamma_S, \hat H_\mathrm{IH}",:white,11), subplot = 6)


wsave(plotsdir("ionichubbard_toy","berryphase", "berry8.pdf"),current())
wsave(plotsdir("ionichubbard_toy","berryphase", "berry8.tikz"),current())


##



## berry phase plots gap comp
    
df = d[d.J .== 0 .&& d.f .==0 .&& d.L .== 10,:]    
ev = 10
gaps1 = hcat((df.en1.-df.en0)...)[1:ev:end,1:ev:end]
gaps2 = hcat((df.en2.-df.en1)...)[1:ev:end,1:ev:end]



δs = df.δs[1:ev:end]
Δs = (df.Δs[1])[1:ev:end]
ts = 0:0.01:1

C1(t) = (0.9*cos(2π * t),sin(2π * t))
C2(t) = (0.9*cos(2π * t),4*sin(2π * t))
C3(t) = (0.9*cos(2π * t),2.24 + 2 * sin(2π * t))
# C4(t) = (0.2*cos(2π * t),1.4 + 0.2 * sin(2π * t))

pgfplotsx(framestyle = :box, grid = false, size = size, c = :RdBu, clims = (0,1), xlims = (-1,1), ylims = (-5,5), legend_columns = 3) 
plot(layout = (3,2), bottom_margin = -17mm, right_margin = -23mm)
heatmap!(δs,Δs,gaps1,subplot = 1, colorbar_title = L"E_{i+1}-E_i", colorbar = false, xlabel = false, xformatter = _ -> "",ylabel = L"\Delta/J",yticks = [-2.5,0,2.5,5]);
heatmap!(δs,Δs,gaps2,subplot = 2, colorbar_title = L"E_{i+1}-E_i", xlabel = false, xformatter = _ -> "", yformatter = _ -> "", colorbar = :top);

df = d[d.J .== 0 .&& d.f .== 0.5 .&& d.L .== 10,:]    
ev = 10
gaps1 = hcat((df.en1.-df.en0)...)[1:ev:end,1:ev:end]
gaps2 = hcat((df.en2.-df.en1)...)[1:ev:end,1:ev:end]

δs = df.δs[1:ev:end]
Δs = (df.Δs[1])[1:ev:end]
ts = 0:0.01:1

heatmap!(δs,Δs,gaps1,subplot = 3, colorbar_title = L"E_{i+1}-E_i", colorbar = false, xformatter = _ -> "",ylabel = L"\Delta/J", yticks = [-2.5,0,2.5,5]);
heatmap!(δs,Δs,gaps2,subplot = 4, colorbar_title = L"E_{i+1}-E_i", colorbar = false, xlabel = false,xformatter = _ -> "", yformatter = _ -> "");

df = d[d.J .== 2 .&& d.f .== 0 .&& d.L .== 10,:]    
ev = 10
gaps1 = hcat((df.en1.-df.en0)...)[1:ev:end,1:ev:end]
gaps2 = hcat((df.en2.-df.en1)...)[1:ev:end,1:ev:end]

δs = df.δs[1:ev:end]
Δs = (df.Δs[1])[1:ev:end]
ts = 0:0.01:1

heatmap!(δs,Δs,gaps1,subplot = 5, colorbar_title = L"E_{i+1}-E_i", colorbar = false, xlabel = false,ylabel = L"\Delta/J", yticks = [-5,-2.5,0,2.5,5]);
heatmap!(δs,Δs,gaps2,subplot = 6, colorbar_title = L"E_{i+1}-E_i", yformatter = _ -> "", colorbar = false, xticks = [-0.5,0,0.5,1]);

# plot!(C1.(ts), c = :black, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},1}", legend = :top);
# plot!(C2.(ts), c = :black, ls = :dash, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},2}");
# plot!(C3.(ts), c = :black, ls = :dot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},1}");
# plot!(C4.(ts), c= :black, ls = :dashdot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},2}" ,legendcolumns = 4);

# ylabel!(L"\Delta/J");
xlabel!(L"\delta");


wsave(plotsdir("ionichubbard_toy","berryphase", "gaps.pdf"),current())
wsave(plotsdir("ionichubbard_toy","berryphase", "gaps.tikz"),current())


##

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>





#     pgfplotsx(framestyle = :box, grid = false, legend = :top) 
#     δs =  readdlm(datadir("Armando", "Delta0", "delta.txt"), Float64)[:,1]
#     Δs =  readdlm(datadir("Armando", "Delta0", "Del.txt"), Float64)[:,1]
#     Cs =  readdlm(datadir("Armando", "Delta0", "Bp.txt"), Float64)[:,1]
#     Cs = reshape(Cs, length(Δs), length(δs))
#     Δc = Δs[findfirst(x->x>0.5,Cs[:,201])]
#     ts = 0:0.001:1
#     # C1(t) = (0.2*cos(2π * t),2*sin(2π * t))
#     # C2(t) = (0.2*cos(2π * t),1.2*sin(2π * t))
#     # C3(t) = (0.2*cos(2π * t),1.4 + 1 * sin(2π * t))
#     # C4(t) = (0.2*cos(2π * t),1.4 + 0.2 * sin(2π * t))
    
#     heatmap((δs[1:each:end],Δs[1:each:end],(Cs[1:each:end,1:each:end] .+ 2) .% 2) , size=(320,350/φ), clims = (0,2), colorbar=true, xlabel=L"\delta", ylabel=L"\Delta/J",colorbar_title=L"\gamma/\pi", framestyle = :box, grid = false, c = cgrad(:greys, rev=true));
#     scatter!([(0,Δc),(0,-Δc)],marker = :circ, color = :white, markersize =3,label = false);
#     plot!(C1.(ts), c = :black, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},1}");
#     plot!(C2.(ts), c = :black, ls = :dash, lw = 1.4, label = L"\mathcal C_{\mathrm{RM},2}");
#     plot!(C3.(ts), c= :black, ls = :dot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},1}");
#     plot!(C4.(ts), c= :black, ls = :dashdot, lw = 1.4, label = L"\mathcal C_{\mathrm{IH},2}" ,legendcolumns = 4);


#     wsave(plotsdir("ionichubbard","berryphase", "berry2.pdf"),current())
#     wsave(plotsdir("ionichubbard","berryphase", "berry2.tikz"),current())

# ##

#     xlims!(-0.5,0.5)
   
#     Ds = [0,1.288,5]
#     RDs = [1,2,3,4]

#     m = IonicHubbardPbc(L=6,T=1,U=4)
#     ts = 0:0.01:1
#     lines = [:solid, :dash, :dot, :dashdot, :dashdotdot]
#     pal = palette(:tab10)
#     for (c,D) in enumerate(Ds), (i,RD) in enumerate(RDs)
#         m = IonicHubbardPbc(Δc = D, RΔ = RD, L=6,T=1,U=4)
#         plot!(t->δ(m,t),t->Δ(m,t),ts, legend = :outertop, 
#         extra_kwargs = :subplot, 
#         label = false,
#         legend_columns = 5,
#         add = 
#         [raw"\addlegendimage{legend image with text = {$~_{\Delta_C/J} \setminus ^{R_\Delta/J}$}}
#         \addlegendentry{{}} \addlegendimage{legend image with text = {1}}
#         \addlegendentry{{}} \addlegendimage{legend image with text = {2}}
#         \addlegendentry{{}} \addlegendimage{legend image with text = {3}}
#         \addlegendentry{{}} \addlegendimage{legend image with text = {4}}
#         \addlegendentry{{}} \addlegendimage{legend image with text = {0}}
#         \addlegendentry{{}} \addlegendimage{line legend,cust4}
#         \addlegendentry{{}} \addlegendimage{cust4,dashed}
#         \addlegendentry{{}} \addlegendimage{cust4,dotted}
#         \addlegendentry{{}} \addlegendimage{cust4,dashdotted}
#         \addlegendentry{{}} \addlegendimage{legend image with text = {1.288}}
#         \addlegendentry{{}} \addlegendimage{cust7}
#         \addlegendentry{{}} \addlegendimage{cust7,dashed}
#         \addlegendentry{{}} \addlegendimage{cust7,dotted}
#         \addlegendentry{{}} \addlegendimage{cust7,dashdotted}
#         \addlegendentry{{}} \addlegendimage{legend image with text = {5}}
#         \addlegendentry{{}} \addlegendimage{cust6}
#         \addlegendentry{{}} \addlegendimage{cust6,dashed}
#         \addlegendentry{{}} \addlegendimage{cust6,dotted}
#         \addlegendentry{{}} \addlegendimage{cust6,dashdotted}
#         \addlegendentry{{}}"
#         ],
#         c = pal[c],linestyle = lines[i], framestyle = :box, grid = false,tex_output_standalone = false)
#     end



#     wsave(plotsdir("ionichubbard","berryphase", "Delta0.pdf"),current())
#     wsave(plotsdir("ionichubbard","berryphase", "Delta0.tikz"),current())

    
# ##
    