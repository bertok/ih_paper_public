using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
using Plots.PlotMeasures

size = (350, 350 / φ)
default(xlabel = L"t/T", ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4, widen = false)


dinf = sort(collect_results(datadir("interactive", "ionichubbard", "itebd_szsz_")),[:T,:maxdim])
maxdims = sort(dinf.maxdim)
dinf = dinf[dinf.maxdim .== 200 .&& dinf.J .== 2,:]

d = sort(collect_results!(datadir("sims","ionichubbard_toy","current_path_Sz")),:L)
d2 = sort(collect_results!(datadir("sims","ionichubbard_toy","current_path")),:L)
d3 = sort(collect_results(datadir("interactive", "ionichubbard", "itebd_szsz_")),[:T,:maxdim])
df3 = d3[d3.maxdim .== 200,:]

qsinf = df3[df3.J .== 0,:].qs[1]
qsinfS = df3[df3.J .== 0,:].qsS[1]


df = d[d.Δc .== 2 .&& d.T .== 50,:]
df2 = d2[d2.Δc .== 2.24 .&& d2.T .== 50 ,:]
## 

# delq2 =[df2.qs[i][end] for i in 1:5]
delq1 =[df.qs[i][end] for i in 1:5]

# delqS2 =[df2.qsS[i][end] for i in 1:5]
delqS1 =[df.qsS[i][end] for i in 1:5]

plot(
layout = (2,1),
top_margin = -17mm,
)


scatter(df.L,abs.(delq1.-1), subplot = 1,ylabel = L"\Delta Q - 1", ylims = (1e-4,1.2), label = L"\hat H_\mathrm{IHZ}", marker = :square, yscale = :log10, markersize = 3)
# scatter!(df2.L,delq2.-1,subplot = 1,ylabel = L"\Delta Q - 1", ylims = (1e-4,1.2), label = L"\hat H_\mathrm{IHB}", marker = :circle, yscale = :log10, xformatter = _ -> "", markersize = 3)

scatter!(df.L,delqS1, subplot = 2, ylabel = L"\Delta Q_S",label = L"\hat H_\mathrm{IHZ}", marker = :square, markersize = 3)
# scatter!(df2.L,delqS2, subplot = 2,ylabel = L"\Delta Q_S", label = L"\hat H_\mathrm{IHB}", marker = :circle, ylims = (-1.25,0.39), legend = false, markersize = 3)
xlabel!(L"L")

annotate!(4.5,0.4,text(L"(a)",10), subplot = 1)
annotate!(4.5,0.2,text(L"(b)",10), subplot = 2)

# wsave(plotsdir("ionichubbard_toy", "charge_Ls",savename(df) * "_spin" * "_.pdf"), current())
# wsave(plotsdir("ionichubbard_toy", "charge_Ls", savename(df) * "_spin" * "_.tikz"), current())



##
plot(
ylabel = L"\vspace{1cm} Q(t)",
xlabel = L"t/T",
layout = (2,1),
top_margin = -17mm
)

plot!(df3.ts[4][1:4:end]/df3.ts[4][end], qsinf[2:4:end],subplot = 1 ,label =  L"\hat H_\mathrm{IH}", ls = :solid, lw = 0.5,linealpha =0.7)
plot!(df3.ts[4][1:4:end]/df3.ts[4][end], qsinfS[2:4:end],subplot = 2 ,label =  L"\hat H_\mathrm{IH}", ls = :solid, legend = false, lw = 0.5,linealpha =0.7, inset = (1, bbox(0.52, -0.33, 0.44, 1.2)))

plot!(df2.ts[4]/df2.ts[4][end], df2.qs[4],subplot = 1 ,label =  L"\hat H_\mathrm{IHB}", legend = :topleft, legend_columns =1,foreground_color_legend = nothing,background_color_legend = nothing, ylims = (-0.1,1), ls = :dash, xformatter = _ -> "")
plot!(df.ts[4]/df.ts[4][end], df.qs[4],subplot = 1 ,label =  L"\hat H_\mathrm{IHZ}", ls = :dot,inset = (2, bbox(0.1, -0.33, 0.44, 1.2)), yticks = ([0,0.25,0.5,0.75,1],[L"\hphantom{-}0.00","0.25","0.50","0.75","1.00"]))

plot!(df2.ts[4]/df2.ts[4][end], df2.qsS[4],subplot = 2, label =  L"\hat H_\mathrm{IHB}", legend = false, ylims = (-1,0.1), ls = :dash, ylabel = L"Q_S(t)")
plot!(df.ts[4]/df.ts[4][end], df.qsS[4],subplot = 2, label =  L"\hat H_\mathrm{IHZ}", legend = false, ls = :dot)

plot!(df3.ts[4][1:4:end]/df3.ts[4][end], qsinf[2:4:end],subplot =3, xlims = (0.9,1), ylims = (0.85,1), legend = false, lw = 0.5, alpha = 0.7)
plot!(df2.ts[4]/df2.ts[4][end], df2.qs[4],subplot =3, xlims = (0.9,1), ylims = (0.85,1), ls = :dash,legend = false)
plot!(df.ts[4]/df.ts[4][end], df.qs[4],subplot = 3,xlims = (0.9,1), ylims = (0.85,1), ls = :dot,legend = false)

yticks!([0.85,1], subplot = 3) 
xticks!([0.9,1], subplot = 3) 
xlabel!("", subplot = 3) 
ylabel!("", subplot = 3) 

plot!(df3.ts[4][1:4:end]/df3.ts[4][end],700* qsinfS[2:4:end],subplot =4, xlims = (0.5,1), ylims = (-1,1), legend = false, lw = 0.5, alpha = 0.7, xticks = [0.5,1], yticks = [-1,1], ylabel = "", xlabel = "")
plot!(df2.ts[4]/df2.ts[4][end],700* df2.qsS[4],subplot =4, xlims = (0.5,1), ylims = (-1,1), ls = :dash,legend = false, xticks = [0.5,1], yticks = [-1,1], ylabel = "", xlabel = "")
plot!(df.ts[4]/df.ts[4][end], 700* df.qsS[4],subplot = 4,xlims = (0.5,1), ylims = (-1,1), ls = :dot,legend = false, xticks = [0.5,0.75,1], yticks = ([-0.7,0.7],["-0.001","0.001"]), ylabel = "", xlabel = "")


annotate!(0.07,0.37,text(L"(a)",10), subplot = 1)
annotate!(0.07,-0.2,text(L"(b)",10), subplot = 2)

wsave(plotsdir("ionichubbard_toy", "charge", savename(df) * "_charge" * "_.pdf"), current())
wsave(plotsdir("ionichubbard_toy", "charge", savename(df) * "_charge" * "_.tikz"), current())

##

# convergence of oscillations:

plot()

for g in groupby(d3,:maxdim)
    qsinfS = g[g.J .== 0,:].qs[1]
    plot!(g.ts[4][1:4:end]/g.ts[4][end], qsinfS[2:4:end],xlims = (0.9,1), ylims = (0.85,1), legend = false, lw = 0.5, alpha = 0.7)
end
# plot!(df2.ts[4]/df2.ts[4][end], df2.qs[4], xlims = (0.9,1), ylims = (0.85,1), ls = :dash,legend = false)
# plot!(df.ts[4]/df.ts[4][end], df.qs[4],xlims = (0.9,1), ylims = (0.85,1), ls = :dot,legend = false)

current()
## iTEBD results:

d = sort(collect_results(datadir("interactive", "ionichubbard", "itebd_szsz_")),[:T,:maxdim])
df = d[d.maxdim .== 200,:]

##

qsinf = df[df.J .== 0,:].qs[1]
qsinf2 = df[df.J .== 2,:].qs[1]
qsinfS = df[df.J .== 0,:].qsS[1]
qsinf2S = df[df.J .== 2,:].qsS[1]


plot(
ylabel = L"Q(t)",
xlabel = L"t/T",
layout = (2,1),
top_margin = -17mm
)

plot!(df.ts[4][1:100:end]/df.ts[4][end], qsinf[2:100:end],subplot = 1 ,label =  L"\hat H_\mathrm{IH}", ls = :solid)
plot!(df.ts[4][1:100:end]/df.ts[4][end], qsinf2[2:100:end],subplot = 1 ,label =  L"\hat H_\mathrm{IHZ}", ls = :dash, legend = :bottomright, xformatter = _->"", ylims = (0,1),xlims=(0,1))

plot!(df.ts[4][1:100:end]/df.ts[4][end], qsinfS[2:100:end],subplot = 2 ,label =  L"\hat H_\mathrm{IH}", ls = :solid, legend = false)
plot!(df.ts[4][1:100:end]/df.ts[4][end], qsinf2S[2:100:end],subplot = 2 ,label =  L"\hat H_\mathrm{IHZ}", ls = :dash, legend = false,ylabel = L"Q_S(t)", xlims = (0,1))

annotate!(0.07,0.9,text(L"(a)",10), subplot = 1)
annotate!(0.07,0.0002,text(L"(b)",10), subplot = 2)

wsave(plotsdir("ionichubbard_toy", "charge_infinite", savename(df) * "_charge" * "_.pdf"), current())
wsave(plotsdir("ionichubbard_toy", "charge_infinite", savename(df) * "_charge" * "_.tikz"), current())


## RM paths:

d = sort(collect_results!(datadir("sims","ionichubbard_toy","RM_path")),:L)

d = d[d.L .== 10,:]

plot(
xlabel = L"t/T",
layout = (2,1),
top_margin = -17mm
)

labs = [L"\hat H_\mathrm{IH}" L"\hat H_\mathrm{IHB}" L"\hat H_\mathrm{IHZ}"]

plot!(d.ts/d.ts[1][end], d.qs,subplot = 1 ,ylabel = L"Q(t)",label =  labs, ls = :auto, foreground_color_legend = nothing,background_color_legend = nothing, xformatter = _ -> "", ylims = (-0.25,2),yticks = ([0,0.5,1,1.5,2],[L"\hphantom{-}0.0","0.5","1.0","1.5","2.0"]))

plot!(d.ts/d.ts[1][end], d.qsS,subplot = 2,ylabel = L"Q_S(t)", label =  labs, legend = false, ylims = (-0.6,0.05), ls = :auto, foreground_color_legend = nothing,background_color_legend = nothing)


annotate!(0.07,1.7,text(L"(a)",10), subplot = 1)
annotate!(0.07,-0.2,text(L"(b)",10), subplot = 2)

wsave(plotsdir("ionichubbard_toy", "charge_RM", savename(d) * "_charge" * "_.pdf"), current())
wsave(plotsdir("ionichubbard_toy", "charge_RM", savename(d) * "_charge" * "_.tikz"), current())

