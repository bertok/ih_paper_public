using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
φ = Base.MathConstants.φ
size = (350,350/φ)

using Plots.PlotMeasures
default()
pgfplotsx(framestyle = :box, grid = :false, widen = false, tickfontvalign = :bottom, foreground_color_legend = nothing,background_color_legend = nothing)

## Gaps δ=0 plot Staggered field

d = collect_results!(datadir("sims","ionichubbard_toy","gap_path_tower"))


##

gs = groupby(d[d.L .== 12 .&& d.Δc .!= 0,:], [:f,:J])
gs = gs[[2,3,1]]
anns = [L"\hat H_\mathrm{IHB}",L"\hat H_\mathrm{IHZ}",L"\hat H_\mathrm{IH}"]
plot(size = (270,350/φ), layout = (1,3), legend = false, right_margin = -16.5mm)
for (i,g) in enumerate(gs)
    plot!((g.ts[1]./g.ts[1][end]),g.en[1]',xticks = i == 1 ? ([0.5,0.75,1],[L"\pi",L"3\pi/2",L"2\pi"]) : ([0.75,1],[L"3\pi/2",L"2\pi"]),xlims = (0.5,1), ylims = (-15,-4), subplot = i, c = 1, yformatter = (i == 1 ? :plain : _ -> ""), label = :false)
    plot!((g.ts[1]./g.ts[1][end]),g.enS[1]',xticks = i == 1 ? ([0.5,0.75,1],[L"\pi",L"3\pi/2",L"2\pi"]) : ([0.75,1],[L"3\pi/2",L"2\pi"]),xlims = (0.5,1), ylims = (-15,-4), subplot = i, c = 2, yformatter = (i == 1 ? :plain : _ -> ""), ls = :dot, xlabel = L"\theta", ylabel = i == 1 ? L"E(\theta)/J" : "", label = :false)
    annotate!(0.75,-13.5,text(anns[i],10), subplot = i)
end
plot!((gs[1].ts[1]./gs[1].ts[1][end]),gs[1].en[1][1,:], c = 1, ls = :solid, legend = :top, label = L"S_z = 0")
plot!((gs[1].ts[1]./gs[1].ts[1][end]),gs[1].enS[1][1,:], c = 2, subplot = 2, ls = :dot ,legend = :top, label = L"S_z = 1")


annotate!(0.6,-6,text(L"(a)",10), subplot = 1)
annotate!(0.6,-6,text(L"(b)",10), subplot = 2)
annotate!(0.6,-6,text(L"(c)",10), subplot = 3)

wsave(plotsdir("ionichubbard_toy","tower","towers.tikz"),current())
wsave(plotsdir("ionichubbard_toy","tower","towers.pdf"),current())
current()
