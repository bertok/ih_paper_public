using DrWatson
@quickactivate
include(srcdir("init.jl"))
using Plots
using Plots.PlotMeasures
using LaTeXStrings
φ = Base.MathConstants.φ
size = (350,350/φ)
default()
pgfplotsx(framestyle = :origin, size = size,ticks = false, grid = false,foreground_color_border=:grey, foreground_color_guide=:black, lw = 1.5,xguidefonthalign = :right, yguidefontvalign = :top, legend = (-0.35,1.2), foreground_color_legend = nothing,background_color_legend = nothing)

## paths cartoon:

t = 0:0.01:1

f(t) = (0.5*cospi(2*t),2+sinpi(2*t))
g(t) = (cospi(2*t),4*sinpi(2*t))
h(t) = (0.4*cospi(2*t),0.7*sinpi(2*t))


plot(layout = (1,2), left_margin = -12mm, bottom_margin = -3mm)
plot!(f.(t),subplot = 1,label = L"\mathcal{C}_1")
plot!(g.(t),subplot = 1, ls = :dash,label = L"\mathcal{C}_2")
plot!(h.(t),subplot = 1, ls = :dot,label = L"\tilde{\mathcal{C}}_2")
scatter!((0,0),subplot = 1, c = :red, legend = false, title = L"U/J=0", title_position = (0.5,-0.3))

plot!(f.(t),subplot = 2,label = L"\mathcal{C}_1")
plot!(g.(t),subplot = 2, ls = :dash,label = L"\mathcal{C}_2")
plot!(h.(t),subplot = 2, ls = :dot,label = L"\tilde{\mathcal{C}}_2")
scatter!((0,2),subplot = 2, c = :red, label = false)
scatter!((0,-2),subplot = 2, c = :red, label = false, title = L"U/J>0", title_position = (0.5,-0.3))
xlabel!(L"\hspace{0.5cm}\delta")
ylabel!(L"\hspace{5cm}\Delta/J", align = :top)

annotate!(-0.8,3.5,text(L"(a)",10), subplot = 1)
annotate!(-0.8,3.5,text(L"(b)",10), subplot = 2)

wsave(plotsdir("ionichubbard_toy","cartoon","paths.pdf"),current())
wsave(plotsdir("ionichubbard_toy","cartoon","paths.tikz"),current())

