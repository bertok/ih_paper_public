using DrWatson
@quickactivate
include(srcdir("init.jl"))
pgfplotsx()
φ = Base.MathConstants.φ
using Plots.PlotMeasures
size = (350, 350 / φ)
default(xlabel = L"t/T", ylabel = L"\mathcal O", framestyle = :box, size = size, grid = false, legend = :top, legend_columns = 4, widen = true)

## C1

m = RmPbc(L=100,T=50,RΔ=3*2,Rδ=0.9,ϕ₀ = 0)

potential(m::RmPbc, j, t) = Δ(m, t) / 2 * (-1)^j

potential(m::RmPbc, j, t) = Δ(m, t) / 2 * (-1)^j + 4*(-1)^j


P = Propagation(m = m, dt = 0.1, p =Crank())
ts = timerange(P)
ts2 = collect(-m.T/16:P.dt:-P.dt)
Js = zeros(Float64, length(ts))
Jsvar = zeros(Float64, length(ts))
dens = zeros(Float64, length(ts),2)

P.t = -m.T/16
P.Ψ = eigen(hamiltonian(m,-m.T/16)).vectors
@showprogress "Einschwing " for (i,t)  in enumerate(ts2)
    P.t = t
    prop!(P)
end

P.t = 0
@showprogress "Messung " for (i,t)  in enumerate(ts)
    prop!(P)
    Js[i] = real.(curr_singlelink(P,1,P.occ))
    Jsvar[i] = real.(curr_singlelink_variance(P,1,P.occ)-(Js[i])^2)
    dens[i,:] = localdensity(m,P.Ψ,cld(m.L,2))[:,1][1:2]
end

Qs = [sum(Js[1:i])*P.dt for i in eachindex(ts)]  #✓
Qsvar = [sum(sqrt.(Jsvar[1:i]))*P.dt for i in eachindex(ts)]  #✓

plot(ts./m.T,2*Qs, label = L"\mathcal C_1",ylims=(0,2), ylabel = L"Q(t/T)")

## C2:

m = RmPbc(L=100,T=50,RΔ=3*2,Rδ=0.9,ϕ₀ = 0)

potential(m::RmPbc, j, t) = Δ(m, t) / 2 * (-1)^j

P = Propagation(m = m, dt = 0.1, p =Crank())
ts = timerange(P)
ts2 = collect(-m.T/16:P.dt:-P.dt)
Js = zeros(Float64, length(ts))
dens = zeros(Float64, length(ts),2)

P.t = -m.T/16
P.Ψ = eigen(hamiltonian(m,-m.T/16)).vectors
@showprogress "Einschwing " for (i,t)  in enumerate(ts2)
    P.t = t
    prop!(P)
end

P.t = 0
@showprogress "Messung " for (i,t)  in enumerate(ts)
    prop!(P)
    Js[i] = real.(curr_singlelink(P,1,P.occ))
    dens[i,:] = localdensity(m,P.Ψ,cld(m.L,2))[:,1][1:2]
end

Qs = [sum(Js[1:i])*P.dt for i in eachindex(ts)]  #✓
plot!(ts./m.T,2*Qs, label = L"\mathcal C_2",ylims=(0,2), ylabel = L"Q(t/T)" ,ls = :dash, legend = :topleft)


## C2 tilde:

m = RmPbc(L=100,T=50,RΔ=3,Rδ=0.5,ϕ₀ = 0)

potential(m::RmPbc, j, t) = Δ(m, t) / 2 * (-1)^j

P = Propagation(m = m, dt = 0.1, p =Crank())
ts = timerange(P)
ts2 = collect(-m.T/16:P.dt:-P.dt)
Js = zeros(Float64, length(ts))
dens = zeros(Float64, length(ts),2)

P.t = -m.T/16
P.Ψ = eigen(hamiltonian(m,-m.T/16)).vectors
@showprogress "Einschwing " for (i,t)  in enumerate(ts2)
    P.t = t
    prop!(P)
end

P.t = 0
@showprogress "Messung " for (i,t)  in enumerate(ts)
    prop!(P)
    Js[i] = real.(curr_singlelink(P,1,P.occ))
    dens[i,:] = localdensity(m,P.Ψ,cld(m.L,2))[:,1][1:2]
end

Qs = [sum(Js[1:i])*P.dt for i in eachindex(ts)]  #✓
plot!(ts./m.T,2*Qs, label = L"\tilde{\mathcal C}_2",ylims=(0,2), ylabel = L"Q(t/T)" ,ls = :dot, legend = :topleft)


wsave(plotsdir("ionichubbard_toy", "damop_talk",  "RM_.pdf"), current())


##########

##

d = sort(collect_results!(datadir("sims","ionichubbard_toy","current_path")),:L)
dRM = sort(collect_results!(datadir("sims","ionichubbard_toy","RM_path_talk")),:L)

d2 = dRM[dRM.f .== 0.5 .&& dRM.RΔ .== 4,:]
d2tilde = dRM[dRM.f .== 0.5 .&& dRM.RΔ .== 0.5,:]
d1 = d[d.L .== 10 .&& d.RΔ .== 2 .&& d.Δc .== 2.24,:]


plot(d1.ts./50,d1.qs1[1][2:end]+d1.qs3[1][2:end], label = L"\mathcal C_1",ylims=(0,2), ylabel = L"Q(t/T)" ,ls = :solid, legend = :topleft)
plot!(d2.ts[1][1:10:end]./100,d2.qs1[1][2:10:end]+d2.qs3[1][2:10:end], label = L"\mathcal C_2",ylims=(0,2), ylabel = L"Q(t/T)" ,ls = :dash, legend = :topleft)
plot!(d2tilde.ts./100,d2tilde.qs1[1][2:end]+d2tilde.qs3[1][2:end], label = L"\tilde{\mathcal C}_2",ylims=(0,2), ylabel = L"Q(t/T)" ,ls = :dot, legend = :topleft)


wsave(plotsdir("ionichubbard_toy", "damop_talk",  "U>0.pdf"), current())



## infinite system current:


d3 = sort(collect_results(datadir("interactive", "ionichubbard", "itebd_szsz_")),[:T,:maxdim])
df3 = d3[d3.maxdim .== 200,:]
d = sort(collect_results!(datadir("sims","ionichubbard_toy","RM_path")),:L)
d = d[d.L .== 10,:]
##

plot(df3.ts[4]./1000,df3.qs[4][2:end], label = L"\mathcal C_1",ylims=(0,2), ylabel = L"Q(t/T)" ,ls = :solid, legend = :topleft)
plot!(d.ts[1]./50,d.qs[1], label = L"\mathcal C_2",ylims=(0,2), ylabel = L"Q(t/T)" ,ls = :dash, legend = :topleft)


wsave(plotsdir("ionichubbard_toy", "damop_talk",  "inf.pdf"), current())
