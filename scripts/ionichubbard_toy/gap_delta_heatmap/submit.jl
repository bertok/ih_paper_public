using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init_submit.jl"))

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.

# create dic list of parameters:
allparams = Dict(
    :L => [10],   
    :U => [4],
    :m => [IonicHubbard2PbcToy],
    :Δs => [(-5+0.001):0.01:5],
    :δs => collect(-1+0.001:0.01:1),
    :J => [0],
    :f => [0.5],
)
#
dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:U])
##

submit_jobs("ionichubbard_toy/gap_delta_heatmap", dicts, 3600*4)
