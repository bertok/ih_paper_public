using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))
include(srcdir("itebd.jl"))
# create dic list of parameters:
allparams = Dict(
    :T => [100,100*2π,100/2π],
    :U => collect(1:0.5:32),
    :m => [IonicHubbardPbcToy],
    :Δc => [4],
    :RΔ => [2.1017],
    :Rδ => 0.8829,
    :ϕ₀ => 0,
    :dt => [0.1],
    :maxdim => [200],
    :cutoff => [1e-7,1e-8],
    :md => Model"ionichubbardtoy"(),
    :J => [0],
    :f => 0,
    :periods => 1
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:maxdim])

##

submit_jobs("ionichubbard_toy/tebd_U_sweep", dicts, 3600*24*6)

