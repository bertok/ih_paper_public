using DrWatson
@quickactivate "mps-charge-pump"

using Distributed, ClusterManagers

addprocs_slurm(20, exeflags="--project",mem_per_cpu="4G")

##
@everywhere begin
    using DrWatson
    include(srcdir("init.jl"))
    include(srcdir("vumps.jl"))
    include(srcdir("vumpstest_path.jl"))
end

##

allparams = Dict(   #free fermion calculation.
    :T => [50,100],
    :U => [0],
    :m => [IonicHubbard],
    :Δ0 => [0],
    :RΔ => [3],
    :Rδ => 0.9,
    :ϕ₀ => 0,
    :maxdim => [40,60,80,100,200,400],
    :dt => 0.1
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:U])

## run  all simulations in parallel:

@distributed for d in dicts
    produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_current_test2"),d,run_vumps_evo)[1]
end
