using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))


# create dic list of parameters:
allparams = Dict(
    :L => [8],   
    :T => [50],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [0],
    :RΔ => [4],
    :Rδ => [0.9],
    :ϕ₀ => 0, 
    :dt => 0.01,
    :J => [0,2],
    :f => [@onlyif(:J ==0,0.5),@onlyif(:J !=0,0),@onlyif(:J ==0,0)]
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

##

d = dicts[3]

r, t = @timed lanczos_current_kryl(d)

##

plot(f["en"]', c = :black, legend = false)
plot!(f["enS"]', c = :red, legend = false)

