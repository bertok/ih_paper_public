using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))


# create dic list of parameters:
allparams = Dict(
    :L => [8],   
    :T => [1],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [0,2.24],
    :RΔ => [@onlyif(:Δc != 0,2),
    @onlyif(:Δc == 0,4),
    @onlyif(:Δc == 0,1)],
    :Rδ => [0.9],
    :ϕ₀ => 0, 
    :dt => 0.01,
    :J => 2,
    :f => 0
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

##

d = dicts[3]

f, t = @timed lanczos_tower_of_states(d)

##

plot(f["en"]', c = :black, legend = false)
plot!(f["enS"]', c = :red, legend = false)

