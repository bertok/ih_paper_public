using DrWatson
@quickactivate
include(srcdir("init_submit.jl"))

Δcs = Dict(6=>1.975,:8=>1.9,:10=>1.875,:12=>1.85)
Δss = Dict(:6=>1.9,:8=>1.85,:10=>1.8, :12=>1.8)

# create dic list of parameters:
allparams = Dict(
    :L => [8],   
    :T => [100],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [0],
    :RΔ => [0.5,4],
    :Rδ => [0.9],
    :ϕ₀ => 0, 
    :dt => 0.01,
    :J => [0,2],
    :f => [@onlyif(:J ==0,0.5),@onlyif(:J !=0,0)]
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

## 
submit_jobs("ionichubbard_toy/RM_path_talk", dicts, 3600*2)