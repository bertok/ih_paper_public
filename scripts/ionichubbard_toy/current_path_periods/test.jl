using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))


# create dic list of parameters:
allparams = Dict(
    :L => [6],   
    :T => [50],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [0,2.24],
    :RΔ => [@onlyif(:Δc != 0,2),
    @onlyif(:Δc == 0,4),
    @onlyif(:Δc == 0,1)],
    :Rδ => [0.9],
    :ϕ₀ => 0, 
    :dt => 0.01,
    :J => 2,
    :f => 0,
    :periods => 10
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

##

d = dicts[1]

f, t = @timed lanczos_current_kryl(d, conserve_sz = true, periods = d[:periods])

##

# plot(f["ts"],f["qs"],label = "qs")
plot(f["ts"],((f["qs1"]+f["qs2"])/2)[2:end],label = "qs_up")
plot!(f["ts"],((f["qs3"]+f["qs4"])/2)[2:end],label = "qs_dn")
xlabel!("t")
ylabel!("ΔQ")



##
plot!(f["ts"],f["intgap"],label = "intgap")
plot!(f["ts"],f["intgap2"],label = "intgap2")

## tower of states

d = dicts[1]

f, t = @timed lanczos_tower_of_states(d, conserve_sz = true)

##