using DrWatson
@quickactivate
include(srcdir("init_submit.jl"))

Δcs = Dict(6=>1.975,:8=>1.9,:10=>1.875,:12=>1.85)
Δss = Dict(:6=>1.9,:8=>1.85,:10=>1.8, :12=>1.8)

# create dic list of parameters:
allparams = Dict(
    :L => [4,6,8,10,12],   
    :T => [1],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [2.24],
    :RΔ => [2],
    :Rδ => [0.9],
    :ϕ₀ => 0, 
    :dt => 0.01,
    :J => [0,2],
    :f => [@onlyif(:J == 0, 0.5),@onlyif(:J == 2, 0),@onlyif(:J == 0, 0)],

)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

## 
submit_jobs("ionichubbard_toy/pol_path", dicts, 3600*12)