using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))


allparams = Dict(
    :L => [10],   
    :T => [1],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [0],
    :RΔ => [4],
    :Rδ => [0.9],
    :ϕ₀ => 0, 
    :dt => 0.01,
    :J => [0,2],
    :f => [@onlyif(:J == 0, 0.5),@onlyif(:J == 2, 0),@onlyif(:J == 0, 0)]
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

##

for d in dicts

produce_or_load(datadir("sims","ionichubbard_toy","pol_path_RM"),d,d->lanczos_current_kryl(d, conserve_sz = true, calculate_inst = true))

end
##