using DrWatson
@quickactivate
include(srcdir("init_submit.jl"))

# create dic list of parameters:
allparams = Dict(
    :L => [4,6,8,10],   
    :T => [100,100*2π,100/2π],
    :U => collect(1:0.5:32),
    :m => [IonicHubbardPbcToy],
    :Δc => [4],
    :RΔ => [2.1017],
    :Rδ => [0.8829],
    :ϕ₀ => 0, 
    :dt => 0.01,
    :J => 0,
    :f => 0,
    :periods => 10
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

## 
submit_jobs("ionichubbard_toy/lanczos_U_sweep", dicts, 3600*6*1)