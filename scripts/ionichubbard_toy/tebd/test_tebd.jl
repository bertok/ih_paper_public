using DrWatson

@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))
include(srcdir("itebd.jl"))

##
allparams = Dict(
    :T => [1000],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [1.2],
    :RΔ => [1],
    :Rδ => 0.2,
    :ϕ₀ => 0,
    :dt => [0.1],
    :maxdim => [60,100,200,400],
    :cutoff => [1e-4,1e-5,1e-6,1e-9],
    :md => Model"ionichubbardtoy"(),
    :J => [0,2],
    :periods => 10
)
dicts = resdict.(dict_list(allparams))

##
sort!(dicts, by = x -> x[:U])

rs = []
for d in dicts
    r = run_itebd(d;loading = false, model = d[:md])
    push!(rs,r)
end 
# r = lanczos_current_kryl(d)
# r1 = produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_energy_test"),dicts[1],run_vumps_evo)[1]
# r2 = produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_current_test.jld2"),dicts[2],d->run_vumps_evo(d,alg=d[:alg]))[1]
