using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))
include(srcdir("itebd.jl"))
# create dic list of parameters:
allparams = Dict(
    :T => [1000],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [1.2],
    :RΔ => [1],
    :Rδ => 0.2,
    :ϕ₀ => 0,
    :dt => [0.1],
    :maxdim => [60,100,200,400],
    :cutoff => [1e-4,1e-5,1e-6,1e-9],
    :md => Model"ionichubbardtoy"(),
    :J => [0,2],
    :periods => 10
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:maxdim])

##

submit_jobs("ionichubbard_toy/tebd", dicts, 3600*24*28)

