using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init_submit.jl"))

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.

# create dic list of parameters:
allparams = Dict(
    :L => [4,6,8,10,12],   
    :U => [4],
    :m => [IonicHubbard2PbcToy],
    :Δs => [-3:0.01:3],
    :δs => [0],
    :J => [0],
    :f => [0],
)
#
dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:U])

dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

##

submit_jobs("ionichubbard_toy/gap_delta0_Sz", dicts, 3600*24*1)
