using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.

# create dic list of parameters:
allparams = Dict(
    :L => [4,6,8,10],   
    :U => [4],
    :m => [IonicHubbard2PbcToy],
    :Δs => [-3:0.01:3],
    :δs => [0],
    :J => [0.5],
    :f => [0],
)
#

dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

##

d = dicts[1]

f, t = @timed lanczos_pol_pbc(d, d[:m]())


##
plot(f["Δs"],f["spingap"],label = "spingap, L=10", ls = :solid, c = 1)
plot!(f["Δs"],f["intgap"],label = "intgap, L=10", ls = :solid, c = 2)
plot!(f["Δs"],f["intgap2"],label = "intgap2, L=10", ls = :solid, c = 3)
plot!(f["Δs"],f["chargegap"],label = "chargegap, L=10", ls = :solid, c = 4)


##
d = dicts[1]

f, t = @timed lanczos_pol_pbc(d, d[:m]())

plot!(f["Δs"],f["spingap"],label = "spingap L=4", ls = :dash, c = 1)
plot!(f["Δs"],f["intgap"],label = "intgap L=4", ls = :dash, c = 2)
plot!(f["Δs"],f["intgap2"],label = "intgap2 L=4", ls = :dash, c = 3)
plot!(f["Δs"],f["chargegap"],label = "chargegap L=4", ls = :dash, c = 4)

xlabel!("Δ")
ylabel!("ΔE")
