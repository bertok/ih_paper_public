using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.

# create dic list of parameters:
allparams = Dict(
    :L => [10],   
    :U => [4],
    :m => [IonicHubbard2PbcToy],
    :Δs => [(-5+0.001):0.01:5],
    :δs => collect(-1+0.001:0.01:1),
    :J => [0,2],
    :f => [@onlyif(:J == 0, 0.5),@onlyif(:J == 2, 0),@onlyif(:J == 0, 0)],
)
#

dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

##

d = dicts[3]

r, t = @timed lanczos_pol_only_pbc(d, d[:m]())


##
heatmap(f["δs"],f["Δs"],f["spingap"],clims = (0,maximum(f["spingap"])))
heatmap(f["δs"],f["Δs"],f["intgap"],clims = (0,maximum(f["intgap"])))
heatmap(f["δs"],f["Δs"],f["intgap2"],clims = (0,maximum(f["intgap2"])))
heatmap(f["δs"],f["Δs"],f["pol"])


# plot!(f["Δs"],f["intgap"],label = "intgap, L=10", ls = :solid, c = 2)
# plot!(f["Δs"],f["intgap2"],label = "intgap2, L=10", ls = :solid, c = 3)
# plot!(f["Δs"],f["chargegap"],label = "chargegap, L=10", ls = :solid, c = 4)


##
d = dicts[1]

f, t = @timed lanczos_pol_pbc(d, d[:m]())

plot!(f["Δs"],f["spingap"],label = "spingap L=4", ls = :dash, c = 1)
plot!(f["Δs"],f["intgap"],label = "intgap L=4", ls = :dash, c = 2)
plot!(f["Δs"],f["intgap2"],label = "intgap2 L=4", ls = :dash, c = 3)
plot!(f["Δs"],f["chargegap"],label = "chargegap L=4", ls = :dash, c = 4)

xlabel!("Δ")
ylabel!("ΔE")
