using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init_submit.jl"))

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.


mkpath("_research/io/$name")

## create dic list of parameters:
allparams = Dict(
    :L => [6,8,10],   
    :T => [1000],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [@onlyif(:L == 6,1.40),@onlyif(:L == 8,1.33),@onlyif(:L == 10,1.28)],
    :RΔ => [0.2,1],
    :Rδ => 0.2 ,
    :ϕ₀ => 0,
    :maxdim => ["increasing"]
)

dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:U])

runs = tmpsave(dicts)

outfile  = projectdir("_research","tmp","runs.txt")
open(outfile, "w") do f
    for i in runs
      println(f, i)
    end
end 

jdf = "
executable = /usr/bin/julia
arguments = \"--project $(dirname(PROGRAM_FILE))/runjob.jl \$(runfile) \"
error = _research/io/$(name)/\$(Cluster).\$(Process).err
log = _research/io/$(name)/\$(Cluster).log
output = _research/io/$(name)/\$(Cluster).\$(Process).out
request_memory = 10 GB
max_idle = 100

queue runfile from $outfile
"

submit = pipeline(`echo $jdf`, `condor_submit`)

run(submit)
