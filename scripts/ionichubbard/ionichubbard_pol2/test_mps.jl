using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))
using DelimitedFiles
using ITensors


AutoMPO()

##

println("test")
println("hel;low")


## create dic list of parameters:
allparams = Dict(
:m => [IonicHubbard2Pbc],
:L => [8],
:Δs => [collect(-2:0.1:2)],
:δs => 0,     
:U => [4],
:maxdim => ["increasing"]
)

dicts = dict_list(allparams)

fs = []
totaltime = @elapsed for d in dicts
    f, time = @timed mps_ionic_hubbard2(d)
    push!(fs, f)
    println("Time Taken: $time seconds.")
end
println("Total Time Taken: $(totaltime / (60 * 60)) hours.")


##  plots

pgfplotsx(framestyle = :box, grid = false)
Δc = [1.4,1.33,1.28]
Δs = [1.12,1.015,0.96]

# fs = load(datadir("interactive","ionichubbard","gapstest.jld2"),"fs")
for (i,f) in enumerate(fs)
    # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
    plot(f[:Δs],abs.(f[:intgap])[:,1],size = (300,200), xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_\mathrm{exc}", legend_title = L"\delta = 0", color = i, linestyle = :auto)
    plot!(f[:Δs],abs.(f[:chargegap])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C", color = i, linestyle = :auto)
    # plot!(f[:Δs],abs.(f[:chargegapnew])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C new", color = i, linestyle = :auto)
    plot!(f[:Δs],abs.(f[:spingap])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_S", color = i, linestyle = :auto)
    plot!(f[:Δs],abs.(f[:en2] - f[:en0])[:,1],size = (300,200), xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_2", color = i, linestyle = :auto)


    vline!(Δc[i:i], c = :black, label = false)
    vline!(Δs[i:i], linestyle = :dash, c = :black, label = false)
    vline!(-Δc[i:i], c = :black, label = false)
    vline!(-Δs[i:i], c = :black, linestyle = :dash, label = false)
    ylims!(0,3)

    wsave(plotsdir("ionichubbard", "delta0", "gaps_mps_" * savename(f) * ".tikz"), current())
    wsave(plotsdir("ionichubbard", "delta0", "gaps_mps_" * savename(f) * ".pdf"), current())

end
    
current()

