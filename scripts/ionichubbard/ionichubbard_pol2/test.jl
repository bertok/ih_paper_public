using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))
using DelimitedFiles

begin####################
# create dic list of parameters:
    allparams = Dict(
    :m => [IonicHubbard2],
    :L => [4,6,8],
    :Δ => 1.8,
    :δs => [collect(-1:0.01:1)],     
    :U => [4],
    :maxdim => ["increasing"],
)

    dicts = dict_list(allparams)

    fs = []
    totaltime = @elapsed for d in dicts
        f, time = @timed mps_ionic_hubbard2_lanczos(d)
        push!(fs, f)
        println("Time Taken: $time seconds.")
    end
    println("Total Time Taken: $(totaltime / (60 * 60)) hours.")

end###################

begin################
    gr()
    dimerization(C, δs) = [sum([C[j][i,i + 1] for i in 1:2:(size(C[j], 1) - 1)]) - sum([C[j][i + 1,i + 2] for i in 1:2:size(C[j], 1) - 2]) for j in eachindex(δs)]

    
    p = plot(legend=:outerright, xlabel=L"\delta", ylabel=L"E_0", layout=(4, 1), size=(500, 600))
    for f in fs
        p = scatter!(f[:δs], f[:en], subplot=1, markersize=2, markerstrokewidth=0, label=f[:L])
        p = scatter!(f[:δs], f[:polup], subplot=2, markersize=2, label=f[:L], markerstrokewidth=0, ylabel=L"P_\uparrow = P_\downarrow", )
        p = scatter!(f[:δs], dimerization(f[:cNup], f[:δs]), subplot=3, markersize=2, markerstrokewidth=0, label=f[:L], ylabel=L"c_{N_\uparrow}")
        p = scatter!(f[:δs], dimerization(f[:cSz], f[:δs]), subplot=4, markersize=2, markerstrokewidth=0, label=f[:L], ylabel=L"c_{Sz}")


    end
    p
end##################
wsave(plotsdir("ionichubbard_convergence_test", savename(fs[1], ignores=[:filename]) * ".pdf"),current())



# LANCZOS:
begin################
    gr()
dimerization(C, δs) = [sum([C[j][i,i + 1] for i in 1:2:(size(C[j], 1) - 1)]) - sum([C[j][i + 1,i + 2] for i in 1:2:size(C[j], 1) - 2]) for j in eachindex(δs)]

    
    p = plot(legend=:outerright, xlabel=L"\delta", ylabel=L"E_0", layout=(4, 1), size=(500, 600))
    for f in fs
        p = scatter!(f[:δs], f[:en], subplot=1, markersize=2, markerstrokewidth=0, label=f[:L])
        p = scatter!(f[:δs], f[:polup], subplot=2, markersize=2, label=f[:L], markerstrokewidth=0, ylabel=L"P_\uparrow = P_\downarrow", )
        theme_palette(:auto)
        p = scatter!(f[:δs], f[:en_exc], subplot=1, markersize=2, marker=:xcross, markerstrokewidth=0, label=f[:L])
        p = scatter!(f[:δs], f[:polup_exc], subplot=2, markersize=2, marker=:xcross, markerstrokewidth=0, label=f[:L])

    end
    p
end##################
wsave(plotsdir("ionichubbard_convergence_test", savename(fs[1], ignores=[:filename]) * "_lanczos_.pdf"),current())








    

# <<< berry phase plots >>>
begin#############
gr()
δs =  readdlm(datadir("Armando", "new", "delta.txt"), Float64)[:,1]
Δs =  readdlm(datadir("Armando", "new", "Del.txt"), Float64)[:,1]
Cs =  readdlm(datadir("Armando", "new", "Bp.txt"), Float64)[:,1]
Cs = reshape(Cs, length(Δs), length(δs))

heatmap(δs,Δs,Cs, size=(300, 200), colorbar=true, clims=(-1, 1), xlabel=L"\delta", ylabel=L"\Delta",colorbar_title=L"\gamma")
wsave(plotsdir("ionichubbard_berryphase", "L=6.pdf"),current())


δs =  readdlm(datadir("Armando", "L10", "delta.txt"), Float64)[:,1]
Δs =  readdlm(datadir("Armando", "L10", "Del.txt"), Float64)[:,1]
Cs =  readdlm(datadir("Armando", "L10", "Bp.txt"), Float64)[:,1]
Cs = reshape(Cs, length(Δs), length(δs))

heatmap(δs,Δs,Cs, size=(300, 200), colorbar=true, clims=(-1, 1), xlabel=L"\delta", ylabel=L"\Delta",colorbar_title=L"\gamma")
wsave(plotsdir("ionichubbard_berryphase", "L=10.pdf"),current())

δs =  readdlm(datadir("Armando", "Delta0", "delta.txt"), Float64)[:,1]
Δs =  readdlm(datadir("Armando", "Delta0", "Del.txt"), Float64)[:,1]
Cs =  readdlm(datadir("Armando", "Delta0", "Bp.txt"), Float64)[:,1]
Cs = reshape(Cs, length(Δs), length(δs))

heatmap(δs,Δs,(Cs .+ 2) .% 2 , size=(300, 200), clims = (0,2), colorbar=true, xlabel=L"\delta", ylabel=L"\Delta",colorbar_title=L"\gamma")
wsave(plotsdir("ionichubbard_berryphase", "Delta0.pdf"),current())

    
end###############