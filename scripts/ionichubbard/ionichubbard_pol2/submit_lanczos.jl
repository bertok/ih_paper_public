using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init_submit.jl"))

name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end] #return root folder of runjob script.

mkpath("_research/io/$name")

# create dic list of parameters:
allparams = Dict(
    :m => [IonicHubbard2],
    :L => [6,8,10],
    :Δ => collect(0.5:0.01:2),
    :δs => [collect(-1:0.01:1)],     
    :U => [4],
    :maxdim => ["increasing"],
)

dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:Δ])

runs = tmpsave(dicts)

for (i,r) in enumerate(partition(runs,4))
    pbs = "
    #!/bin/bash
    #PBS -N $(name)_$(i)
    #PBS -l nodes=1
    #PBS -l walltime=05:59:00
    #PBS -l mem=8gb
    #PBS -l vmem=8gb
    #PBS -o _research/io/$(name)/$(i).out
    #PBS -e _research/io/$(name)/$(i).err
    cd \$PBS_O_WORKDIR
    julia --project scripts/$name/runjob_lanczos.jl '$(r)'
    "
    
    submit = pipeline(`echo $pbs`, `qsub`)
    run(submit)
    sleep(0.5)
end

