using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init_submit.jl"))

name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end] #return root folder of runjob script.

mkpath("_research/io/$name")

# create dic list of parameters:
allparams = Dict(
    :m => [IonicHubbard2],
    :L => [40,80],
    :Δ => collect(-2:0.01:2),
    :δs => [collect(-1:0.01:1)],     
    :U => [4],
    :maxdim => ["increasing"],
)

dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:Δ])

runs = tmpsave(dicts)

outfile  = projectdir("_research","tmp","$(name)_runs.txt")
open(outfile, "w") do f
    for i in runs
      println(f, i)
    end
end 

jdf = "
executable = /usr/bin/julia
arguments = \"--project scripts/$name/runjob.jl \$(runfile) \"
error = _research/io/$(name)/\$(Cluster).\$(Process).err
log = _research/io/$(name)/\$(Cluster).log
output = _research/io/$(name)/\$(Cluster).\$(Process).out
request_memory = 8 GB
max_idle = 50

queue runfile from $outfile
"

submit = pipeline(`echo $jdf`, `condor_submit`)

run(submit)

