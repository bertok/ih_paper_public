using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))


r = ARGS[1]
name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end] #return root folder of runjob script.

d = load(projectdir("_research","tmp",r), "params")
if !isfile(datadir("sims",name,savename(d)*".bson"))
    f, t = @timed mps_ionic_hubbard2(d)
    @tagsave(datadir("sims",name,savename(d)*".bson"),f)
    println("Time Taken: $t seconds.")
    rm(projectdir("_research","tmp",r), force = true)
else
    println("Parameter set already calculated.")
end

