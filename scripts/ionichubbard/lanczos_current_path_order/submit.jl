using DrWatson
@quickactivate
include(srcdir("init_submit.jl"))

# create dic list of parameters:
allparams = Dict(
    :L => [6,8,10],   
    :T => [1000],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [@onlyif(:L == 6,1.40),@onlyif(:L == 8,1.33),@onlyif(:L == 10,1.28)],
    :RΔ => [0.2,1],
    :Rδ => 0.2 ,
    :ϕ₀ => 0 , 
    :dt => 0.1
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:U])

submit_jobs("ionichubbard/lanczos_current_path_order", dicts, 3600*48)