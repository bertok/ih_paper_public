using DrWatson

@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))

##
allparams = Dict(   #free fermion calculation.
    :T => [100],
    :U => [4],
    :m => [IonicHubbard],
    :Δc => [1.2],
    :RΔ => [2],
    :Rδ => 0.2,
    :ϕ₀ => 0,
    :maxdim => [100],
    :dt => 0.1,
)
dicts = resdict.(dict_list(allparams))

##
sort!(dicts, by = x -> x[:U])

r = run_vumps_evo(dicts[1])
# r1 = produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_energy_test"),dicts[1],run_vumps_evo)[1]
# r2 = produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_current_test.jld2"),dicts[2],d->run_vumps_evo(d,alg=d[:alg]))[1]
