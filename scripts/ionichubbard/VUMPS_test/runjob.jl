using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))

r = ARGS[1]

d = load(projectdir("_research", "tmp", r),"params")


produce_or_load(datadir("sims",foldername,name),d,run_vumps_evo)



