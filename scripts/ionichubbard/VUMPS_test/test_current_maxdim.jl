using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))

allparams = Dict(   #free fermion calculation.
    :T => [50],
    :U => [0],
    :m => [IonicHubbard],
    :Δ0 => [0],
    :RΔ => [3],
    :Rδ => 0.9,
    :ϕ₀ => 0,
    :maxdim => [40,60,80,100,200],
    :dt => 0.1
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:U])

d = dicts[1]

r = produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_current_test.jld2"),d,run_vumps_evo)[1]