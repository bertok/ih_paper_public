using DrWatson
@quickactivate
include(srcdir("init_submit.jl"))

# thd lim:
Δc = 1.2
Δs = 0.95


# create dic list of parameters:
allparams = Dict(
    :T => [1000],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [0,Δc],
    :RΔ => [@onlyif(:Δc!=0,0.2),
    @onlyif(:Δc!=0,1),
    @onlyif(:Δc==0,2),
    @onlyif(:Δc==0,5),
    @onlyif(:Δc==0,0.5),
    @onlyif(:Δc==0,(Δc+Δs)/2)],
    :Rδ => [0.2,0.9] ,
    :ϕ₀ => 0 , 
    :dt => 0.1,
    :maxdim => [40,60,80,100,120,140],
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:U])

##

submit_jobs("ionichubbard/VUMPS_test", dicts, 3600*24*28)

