using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))

allparams = Dict(
    :L => [6],
    :U => [4],
    :m => [IonicHubbard2PbcToy],
    :Δs => [-2.999:0.05:3.001],
    :δs => [-0.999:0.05:1.001],
    :γ => 0,
    :f => 1,
    :J => 1
)

dicts = dict_list(allparams)

d = dicts[1]

f, t = @timed lanczos_pol_pbc(d, d[:m]())

##


heatmap(f[:δs],f[:Δs],f[:pol], xlabel = L"\delta",ylabel = L"\Delta/t_0", title =savename(d;ignores=[:γ,:ϕ₀,:Δc,:m]), colorbar_title = L"P")
wsave(plotsdir("ionichubbard","toy","pol_"*savename(d)*"_.pdf"),current())

heatmap(f[:δs],f[:Δs],f[:spingap],clims = (0,6), xlabel = L"\delta",ylabel = L"\Delta/t_0", title =savename(d;ignores=[:γ,:ϕ₀,:Δc,:m]), colorbar_title = L"\Delta E_S")
wsave(plotsdir("ionichubbard","toy","spingap_"*savename(d)*"_.pdf"),current())

heatmap(f[:δs],f[:Δs],f[:intgap],clims = (0,6), xlabel = L"\delta",ylabel = L"\Delta/t_0", title =savename(d;ignores=[:γ,:ϕ₀,:Δc,:m]), colorbar_title = L"\Delta E_i")
wsave(plotsdir("ionichubbard","toy","intgap_"*savename(d)*"_.pdf"),current())