using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

# create dic list of parameters:
allparams = Dict(
    :L => [4,6,8],   
    :T => [100],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [0,@onlyif(:L == 6,1.40),@onlyif(:L == 8,1.33),@onlyif(:L == 10,1.28)],
    :RΔ => [@onlyif(:Δc != 0, 0.2),@onlyif(:Δc != 0, 0.2),@onlyif(:Δc == 0, 2),@onlyif(:Δc == 0 && :L == 8, 1.2)],
    :Rδ => 0.2 ,
    :ϕ₀ => 0
)
#
dicts = dict_list(allparams)

fs = []

for d in dicts
    @show d
    f, t = @timed lanczos_pol_kryl(d, conserve_sz=true, dt=0.1)
    push!(fs, f)
    println("Time Taken: $t seconds.")
end


begin
    
    plot(f4[:pol].-f4[:pol][1])
    plot!(2*f4[:qs]./f4[:qs][end])
    plot!(f6[:pol].-f6[:pol][1])
    plot!(2*f6[:qs]./f6[:qs][end])




end