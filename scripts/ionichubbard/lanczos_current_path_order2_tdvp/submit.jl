using DrWatson
@quickactivate
include(srcdir("init_submit.jl"))

Δcs = Dict(6=>1.4,:8=>1.33,:10=>1.28)
Δss = Dict(:6=>1.12,:8=>1.015,:10=>0.96)

# create dic list of parameters:
allparams = Dict(
    :L => [20],   
    :T => [5000],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [0,@onlyif(:L == 6,Δcs[:6]),@onlyif(:L == 8,Δcs[:8]),@onlyif(:L == 10,Δcs[:10])],
    :RΔ => [@onlyif(:Δc!=0,0.2),
    @onlyif(:Δc!=0,1),
    @onlyif(:Δc==0,2),
    @onlyif(:Δc==0 && :L == 6 ,(Δcs[:6]+Δss[:6])/2),
    @onlyif(:Δc==0 && :L == 8 ,(Δcs[:8]+Δss[:8])/2),
    @onlyif(:Δc==0 && :L == 10 ,(Δcs[:10]+Δss[:10])/2)],
    :Rδ => 0.2 ,
    :ϕ₀ => 0 , 
    :dt => 0.1,
    :maxdim => 50
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])

submit_jobs("ionichubbard/lanczos_current_path_order2_tdvp", dicts, 3600*24*20)