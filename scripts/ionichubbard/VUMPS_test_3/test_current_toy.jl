using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))

allparams = Dict(
    :L => [6],   
    :T => [100],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [0,@onlyif(:L == 6,1.2)],
    :RΔ => [@onlyif(:Δc != 0, 1),@onlyif(:Δc == 0 && :L == 6, 6)],
    :Rδ => 0.2 ,
    :ϕ₀ => 0.05,
    :dt => 0.01,
    :maxdim => 40
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:U])

d = dicts[2]

# r = produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_current_test.jld2"),d,run_vumps_evo)[1]
f = run_vumps_evo(d)