using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

# create dic list of parameters:
L=4
allparams = Dict(
    :L => [L],   
    :T => [100],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [0,@onlyif(:L == L,1.4)],
    :RΔ => [@onlyif(:Δc != 0, 1.5),@onlyif(:Δc == 0 && :L == L, 3)],
    :Rδ => 0.1,
    :ϕ₀ => 0,
    :dt => 0.1,
    :γ => 0,
    :f => [0],
    :J => [0]
)
#
dicts = dict_list(allparams)

fs = []

for d in dicts
    @show d
    f, t = @timed lanczos_current_kryl(d, conserve_sz=true)
    push!(fs, f)
    println("Time Taken: $t seconds.")
end

##

plot(fs[1]["pol"].-fs[1]["pol"][1], legend = false)
plot!(fs[1]["qs1"].-fs[1]["qs1"][1], legend = false)
plot!(fs[1]["qs2"].-fs[1]["qs2"][1], legend = false)


##

plot(fs[2]["pol"].-fs[2]["pol"][1], legend = false)
plot!(fs[2]["qs1"].-fs[2]["qs1"][1], legend = false)
plot!(fs[2]["qs2"].-fs[2]["qs2"][1], legend = false)


##

plot(fs[1]["intgap"])
plot!(fs[1]["spingap"])
plot!(fs[1]["chargegap"])

##

plot(fs[2]["intgap"])
plot!(fs[2]["spingap"])
plot!(fs[2]["chargegap"])