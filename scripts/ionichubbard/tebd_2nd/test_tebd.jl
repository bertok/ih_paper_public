using DrWatson

@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))
include(srcdir("vumps.jl"))
include(srcdir("vumpstest_path.jl"))
include(srcdir("itebd.jl"))

##
allparams = Dict(   #free fermion calculation.
    :T => [1000],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [0],
    :RΔ => [6],
    :Rδ => 0.9,
    :ϕ₀ => 0,
    :maxdim => [40],
    :dt => 0.01,
    :L => 8
)
dicts = resdict.(dict_list(allparams))

##
sort!(dicts, by = x -> x[:U])
d = dicts[1]
r = run_itebd(d;loading = true)
# r = lanczos_current_kryl(d)
# r1 = produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_energy_test"),dicts[1],run_vumps_evo)[1]
# r2 = produce_or_load(datadir("interactive","ionichubbard","vumps_nonint_current_test.jld2"),dicts[2],d->run_vumps_evo(d,alg=d[:alg]))[1]
