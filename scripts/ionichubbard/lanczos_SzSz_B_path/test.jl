using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.

##
# create dic list of parameters:
allparams = Dict(
    :L => [8],   
    :T => [100],
    :U => [4],
    :m => [IonicHubbardPbcToy],
    :Δc => [0],
    :RΔ => [4],
    :Rδ => [0.9],
    :ϕ₀ => 0, 
    :dt => 0.1,
    :J => 0,
    :f => 0.5,
)
#
dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:U])

dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])


d = dicts[1]

f, t = @timed lanczos_current_kryl(d, conserve_sz = true, calculate_inst = true)

##

plot!(f["ts"]./f["T"],f["qs"][2:end],label = "RM")
xlabel!(L"t/T")
ylabel!(L"\Delta Q")
# plot!(f[:Δs],f[:intgap],label = "intgap")
# plot!(f[:Δs],f[:intgap2],label = "intgap2")

