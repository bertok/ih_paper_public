using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init_submit.jl"))

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.


mkpath("_research/io/$name")
# :Δs => [collect(-2:0.1:2)],

## create dic list of parameters:
allparams = Dict(
:m => [IonicHubbard2Pbc],
:L => [6,8,10,20,40,60],
:Δs => [-2:0.1:2],
:δs => [0],     
:U => [4],
:maxdim => [1000]
)
dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:U])

runs = tmpsave(dicts)

outfile  = projectdir("_research","tmp","runs.txt")
open(outfile, "w") do f
    for i in runs
      println(f, i)
    end
end 

jdf = "
executable = /usr/bin/julia
arguments = \"--project $(dirname(PROGRAM_FILE))/runjob.jl \$(runfile) \"
error = _research/io/$(name)/\$(Cluster).\$(Process).err
log = _research/io/$(name)/\$(Cluster).log
output = _research/io/$(name)/\$(Cluster).\$(Process).out
request_memory = 10 GB
max_idle = 100

queue runfile from $outfile
"

submit = pipeline(`echo $jdf`, `condor_submit`)

run(submit)
