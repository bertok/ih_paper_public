using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))

r = ARGS[1]

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.

d = load(projectdir("_research", "tmp", r),"params")

if !isfile(datadir("sims",foldername,name,"U=$(d[:U])",savename(d)*".bson"))

    @show d
    f, time = @timed mps_ionic_hubbard2(d)
    @tagsave(datadir("sims",foldername,name,savename(d)*".bson"),f)
    
    println("Time Taken: $t seconds.")
    rm(projectdir("_research", "tmp", r))
else
    println("Parameter set already calculated.")
end


