using DrWatson
@quickactivate "mps-charge-pump"
include(srcdir("init.jl"))

foldername, name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end-1:end] # return root folder of runjob script.

##

# create dic list of parameters:
allparams = Dict(
    :L => [4,6,8,10,12],   
    :U => [4],
    :m => [IonicHubbard2PbcToy],
    :Δs => [-3:0.1:3],
    :δs => [-1:0.1:1],
    :J => [0],
    :f => 0.5,
)
#
dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:U])

dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:L])


d = dicts[1]

f, t = @timed lanczos_pol_pbc(d, d[:m]())

heatmap(f["δs"],f["Δs"],f["intgap"],label = "spingap")
heatmap(f["δs"],f["Δs"],f["pol"],label = "spingap")

# plot(f["Δs"],f["spingap"],label = "spingap")
# plot!(f["Δs"],f["intgap"],label = "intgap")
# plot!(f["Δs"],f["intgap2"],label = "intgap2")
# xlabel!(L"\Delta/J")

