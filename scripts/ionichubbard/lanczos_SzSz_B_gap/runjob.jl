using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))

r = ARGS[1]

d = load(projectdir("_research", "tmp", r),"params")


produce_or_load(datadir("sims",foldername,name),d,d->lanczos_pol_pbc(d, d[:m]()))



