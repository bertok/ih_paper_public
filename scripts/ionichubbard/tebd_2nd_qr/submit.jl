using DrWatson
@quickactivate
include(srcdir("init_submit.jl"))

# thd lim:
Δc = 1.2
Δs = 0.95

# create dic list of parameters:
allparams = Dict(
    :T => [1000],
    :U => [4],
    :m => [IonicHubbardPbc],
    :Δc => [0,Δc],
    :RΔ => [@onlyif(:Δc!=0,0.2),
    @onlyif(:Δc!=0,1),
    @onlyif(:Δc==0,2),
    @onlyif(:Δc==0,(Δc+Δs)/2)],
    :Rδ => [0.2] ,
    :ϕ₀ => [-0.003,1.497], 
    :dt => [0.02],
    :maxdim => [60,100,200,400],
    :cutoff => [1e-4,1e-5,1e-6]
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:maxdim])

##

submit_jobs("ionichubbard/tebd_2nd_qr", dicts, 3600*24*7)

