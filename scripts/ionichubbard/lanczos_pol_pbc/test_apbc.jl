using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))

#gaps_test
begin
    fs = []
    allparams = Dict(
        :L => [6,8,10],   
        :U => [4],
        :m => [IonicHubbard2Pbc],
        :Δs => [-2:0.01:2],
        :δs => [0]
    )

    dicts = dict_list(allparams)

    pgfplotsx(grid=false, framestyle = :box)

    plot(framestyle = :box, grid = false, size = (300,200))
    Δc = [1.4,1.33,1.28]
    Δs = [1.12,1.015,0.96]
    fs = []
    for (i,d) in enumerate(dicts)

        f, t = @timed lanczos_pol_pbc(d, d[:m]())
        push!(fs,f)
    end
    wsave(datadir("interactive","ionichubbard","gapstest.jld2"),"fs",fs)
end

begin
    fs = load(datadir("interactive","ionichubbard","gapstest.jld2"),"fs")
    for (i,f) in enumerate(fs)
        # plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:L])"), legend_title = L"L", color = i)
        plot(f[:Δs],abs.(f[:intgap])[:,1],size = (300,200), xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_\mathrm{exc}", legend_title = L"\delta = 0", color = i, linestyle = :auto)
        plot!(f[:Δs],abs.(f[:chargegap])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C", color = i, linestyle = :auto)
        # plot!(f[:Δs],abs.(f[:chargegapnew])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_C new", color = i, linestyle = :auto)
        plot!(f[:Δs],abs.(f[:spingap])[:,1], xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_S", color = i, linestyle = :auto)
        plot!(f[:Δs],abs.(f[:en2] - f[:en0])[:,1],size = (300,200), xlabel = L"\Delta/J", ylabel = L"\Delta E", label = L"\Delta E_2", color = i, linestyle = :auto)


        vline!(Δc[i:i], c = :black, label = false)
        vline!(Δs[i:i], linestyle = :dash, c = :black, label = false)
        vline!(-Δc[i:i], c = :black, label = false)
        vline!(-Δs[i:i], c = :black, linestyle = :dash, label = false)
        ylims!(0,3)

        wsave(plotsdir("ionichubbard", "delta0", "gaps_" * savename(f) * ".tikz"), current())
        wsave(plotsdir("ionichubbard", "delta0", "gaps_" * savename(f) * ".pdf"), current())
    
    end
    
    # current()
end





# Test:
# begin
# m = IonicHubbard2Pbc(L=2,Δ=2,δ=0,U=4)
# basis = generatefixedSzstates(m, Sz = 0)
# Htemp = sparsehamiltonian_template(m, basis)
# H = Matrix(Hfromtemp(m,Htemp...))
# end

begin
    m = BosonicSchwinger(L=2,B = 1, K = 1)
    basis = generatefixedQstates(m)
    Htemp = sparsehamiltonian_template(m, basis)
    # H = Hfromtemp(m,Htemp...)
    # Ψ = complex.(eigsolve(H, 3, :SR)[2])[3]
    H = Matrix(Hfromtemp(m,Htemp...))
    scatter()
    Ψ = eigen(H).vectors
    dns = zeros(basissize(m),2*m.L)
    for i in eachindex(basis)
        dns[i,:] = [localdensity_a(Ψ[:,i],basis,m);localdensity_b(Ψ[:,i],basis,m)]
    end
    heatmap(1:2*m.L,1:basissize(m),dns, xlabel = L"j", ylabel = L"\alpha", xticks = 1:2*m.L, colorbar_title = L"n")

    current()
end

begin
    m = IonicHubbard2Pbc(L=4,Δ=2,δ=0,U=0)
    basis = generatefixedSzstates(m,Sz=0)
    Htemp = sparsehamiltonian_template(m, basis)
    # H = Hfromtemp(m,Htemp...)
    # Ψ = complex.(eigsolve(H, 3, :SR)[2])[3]
    H = Matrix(Hfromtemp(m,Htemp...))
    # scatter()
    # Ψ = eigen(H).vectors[:,1:1]
    # for i in 1:1
    #     scatter!(localdensity_up(Ψ[:,i],basis,m), xlabel = L"j", ylabel = L"\langle n \rangle", label = L"a")
    #     scatter!(localdensity_dn(Ψ[:,i],basis,m), xlabel = L"j", ylabel = L"\langle n \rangle", label = L"b", ylims = (0,1))
    # end
    # current()
end