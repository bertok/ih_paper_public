using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))

begin
    allparams = Dict(
    :L => [2,4,6,8,10],   
    :U => [4],
    :m => [IonicHubbard2Pbc],
    :Δs => [-2:0.05:2],
    :δs => [0]
)
#
    dicts = dict_list(allparams)

    plot(framestyle = :box, grid = false)
    for d in dicts

        f, t = @timed lanczos_pol_pbc(d, d[:m]())
        plot!(f[:Δs],f[:en1] - f[:en0], xlabel = L"\Delta", ylabel = L"\Delta E", label = latexstring("$(f[:L])"), legend_title = L"L")
    end
    current()
end





# Test:
# begin
# m = IonicHubbard2Pbc(L=2,Δ=2,δ=0,U=4)
# basis = generatefixedSzstates(m, Sz = 0)
# Htemp = sparsehamiltonian_template(m, basis)
# H = Matrix(Hfromtemp(m,Htemp...))
# end


begin
    m = IonicHubbard2Pbc(L=4,Δ=2,δ=0,U=0)
    basis = generatefixedSzstates(m,Sz=0)
    Htemp = sparsehamiltonian_template(m, basis)
    # H = Hfromtemp(m,Htemp...)
    # Ψ = complex.(eigsolve(H, 3, :SR)[2])[3]
    H = Matrix(Hfromtemp(m,Htemp...))
    # scatter()
    # Ψ = eigen(H).vectors[:,1:1]
    # for i in 1:1
    #     scatter!(localdensity_up(Ψ[:,i],basis,m), xlabel = L"j", ylabel = L"\langle n \rangle", label = L"a")
    #     scatter!(localdensity_dn(Ψ[:,i],basis,m), xlabel = L"j", ylabel = L"\langle n \rangle", label = L"b", ylims = (0,1))
    # end
    # current()
end