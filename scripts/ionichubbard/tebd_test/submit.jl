using DrWatson
@quickactivate
include(srcdir("init_submit.jl"))

# thd lim:
Δc = 1.2
Δs = 0.95


allparams = Dict(
    :T => [15,100,1000],
    :U => [2],
    :m => [IonicHubbardPbc],
    :Δc => [0],
    :RΔ => [3],
    :Rδ => [0.9],
    :ϕ₀ => [0],
    :dt => [0.01,0.001],
    :maxdim => [60,100,200,400,800],
)
#
dicts = resdict.(dict_list(allparams))

sort!(dicts, by = x -> x[:U])

##

submit_jobs("ionichubbard/tebd_test", dicts, 3600*24*6)

