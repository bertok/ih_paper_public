using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init_submit.jl"))

name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end] #return root folder of runjob script.

mkpath("_research/io/$name")

# create dic list of parameters:
allparams = Dict(
    :L => [10],
    :RΔ => 3,
    :R => 0.5,     
    :T => 1,
    :Vso => [0],
    :U => collect(1:1:6),
    :m => [IntRmSpinObc],
    :maxdim => ["increasing"],
    :reverse => false,
    :method => ["lz","mps"]
)

dicts = dict_list(allparams)

sort!(dicts, by = x -> x[:U])

runs = tmpsave(dicts)

for (i,r) in enumerate(partition(runs,1))
    isf =[isfile(datadir("sims",name,"noSz",savename(load(projectdir("_research","tmp",sr),"params"))*".jld2")) for sr in r] #check whether at least one run in partition wasnt finished.
    if !all(isf)
        pbs = "
        #!/bin/bash
        #PBS -N $(name)_$(i)
        #PBS -l nodes=1
        #PBS -l walltime=23:59:00
        #PBS -l mem=8gb
        #PBS -l vmem=8gb
        #PBS -o _research/io/$(name)/$(i).out
        #PBS -e _research/io/$(name)/$(i).err
        cd \$PBS_O_WORKDIR
        julia --project scripts/$name/runjob_noSz.jl '$(r)'
        "
        submit = pipeline(`echo $pbs`, `qsub`)
        run(submit)
        sleep(0.5)
    else
        println("Partition already computed.")
    end
end

