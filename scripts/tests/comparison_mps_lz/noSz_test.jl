using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))


begin 
    allparams = Dict(
    :L => [6],
    :RΔ => 3,
    :R => 0.5,     
    :T => 1,
    :Vso => [0],
    :U => collect(1:1:6),
    :m => [IntRmSpinObc],
    :maxdim => ["increasing"],
    :reverse => false,
    :method => ["lz","mps"]
)

    dicts = dict_list(allparams)

    sort!(dicts, by=x -> x[:U])

    totaltime = @elapsed for d in dicts
        if d[:method] == "lz"
            f, time = @timed makesim_spinpump_lanczos_gap(d, ts=0.2:0.01:0.3)
            @tagsave(datadir("interactive", "comparison_mps_lz", "noSz", savename(d) * ".jld2"),strdict(f))
        elseif d[:method] == "mps"
        f, time = @timed mps_sweeps_spinpump_gap_noSz(d, ts=0.2:0.01:0.3)
        @tagsave(datadir("interactive", "comparison_mps_lz", "noSz", savename(d) * ".jld2"),strdict(f))
        end
        println("Time Taken: $time seconds.")
    end
    println("Total Time Taken: $(totaltime / (60 * 60)) hours.")
end