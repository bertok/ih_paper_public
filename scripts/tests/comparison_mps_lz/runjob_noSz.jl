using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))


rs = parsearr(ARGS[1])
name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end] #return root folder of runjob script.

totaltime = @elapsed for r in rs
    d = load(projectdir("_research","tmp",r),"params")
    if !isfile(datadir("sims",name,"noSz",savename(d)*".jld2"))
        if d[:method] == "lz"
            f, time = @timed makesim_spinpump_lanczos_gap(d, ts = 0:0.01:1)
            @tagsave(datadir("sims",name,"noSz",savename(d)*".jld2"),strdict(f))
        elseif d[:method] == "mps"
            f, time = @timed mps_sweeps_spinpump_gap_noSz(d, ts = 0:0.01:1)
            @tagsave(datadir("sims",name,"noSz",savename(d)*".jld2"),strdict(f))
        end
        println("Time Taken: $time seconds.")
        rm(projectdir("_research","tmp",r))
    else
        println("Parameter set already calculated.")
    end
end
println("Total Time Taken: $(totaltime/(60*60)) hours.")
