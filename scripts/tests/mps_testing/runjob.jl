using DrWatson
@quickactivate "mps-charge-pump"

include(srcdir("init.jl"))


rs = parsearr(ARGS[1])
name = splitpath(first(walkdir(dirname(PROGRAM_FILE)))[1])[end] #return root folder of runjob script.

totaltime = @elapsed for r in rs
    d = load(projectdir("_research","tmp",r))
    if !isfile(datadir("sims",name,savename(d)*".bson"))
        f, time = @timed mps_sweeps_ionic_hubbard(d)
        @tagsave(datadir("sims",name,savename(d)*".bson"),f)
    end
    println("Time Taken: $time seconds.")
    rm(projectdir("_research","tmp",r))
end
println("Total Time Taken: $(totaltime/(60*60)) hours.")
