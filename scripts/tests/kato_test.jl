using DrWatson
@quickactivate "mps-charge-pump"

using DataFrames
using ProgressMeter
using LaTeXStrings
using Plots
include(srcdir("Model.jl"))
include(srcdir("Prop.jl"))
include(srcdir("Measurement.jl"))
include(srcdir("init.jl"))
include(srcdir("simulations.jl"))



# create dic list of parameters:
allparams = Dict(
    :L => [400], # it is inside vector. It is expanded.
    :RΔ => 2.3,
    :Rδ => 0.5,     # single element inside vector; no expansion
    :dt => [0.01,0.05,0.001,0.005],
    :w => 4,
    :s => 1,
    :model => [RmPbcDiagDis],
    :prop => [Kato]
)

dicts = dict_list(allparams)

for (i,d) in enumerate(dicts)
    f = makesim(d)
    @tagsave(datadir("sims","m1_disorder2",f[:filename]*".bson"),f)
end

collect_results(datadir("sims","m1_disorder2"))
